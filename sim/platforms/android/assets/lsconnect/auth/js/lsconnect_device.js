var lsconnect = (function() {
    
    return {
    
        assertForAuth:function(callback) {
            var jsonstr = "";            
            if(callback==undefined || callback==null) {
                jsonStr = lsconnect_internal.assertForAuth('');
            } else {
                jsonStr = lsconnect_internal.assertForAuth(callback);
            }
            
            var jsonObj =  JSON.parse(jsonStr);
            if(jsonObj==null || jsonObj==undefined || !jsonObj.hasOwnProperty('endpoint') ) {
                return null;
            }
            
            return jsonObj;
        },
        
        getProfiles:function() {
            var str = lsconnect_internal.getProfiles();
            return JSON.parse(str);
        },
        
        getDisplaySpec:function() {
            var str =  lsconnect_internal.getDisplaySpec();
            return JSON.parse(str);
        },
        
        logout:function(endpoint) {
            var str = lsconnect_internal.logout(endpoint);
            return JSON.parse(str);
        },    
        
        openProfileAuth:function() {
            lsconnect_internal.openProfileAuth();
        },
        
        openPasswordLogin:function() {
            lsconnect_internal.openPasswordLogin();
        },
        
        agreementAccepted:function() {
            lsconnect_internal.agreementAccepted();
        },        
        
        getSelectedProfile:function() {
            var str = lsconnect_internal.getSelectedProfile();
            return JSON.parse(str);
        },
        
        getDeviceInfo:function() {
            var str = lsconnect_internal.getDeviceInfo();
            return JSON.parse(str);
        },        
        
        log:function(message) {
            lsconnect_internal.log(message);
        }        
        
    };
        
})();