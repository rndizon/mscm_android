var sso_lingeringReq = [];
var sso_lingeringId = 0;

var trxconnect = (function() {

    var authStatusEventRegistered = false;
    
    return {
    
        request:function(settings) {   

    	    var xhr = new XMLHttpRequest();
	        if (typeof(settings.contentType) == "undefined" || settings.contentType == "" || settings.contentType == null) {
	            settings.contentType = "text/xml";	
	        }
	        if (typeof(settings.methodType) == "undefined" || settings.methodType == "" || settings.methodType == null) {
	            settings.methodType = "GET";
	        }
	        if (typeof(settings.content) == "undefined" || settings.content== "" ) {
	            settings.content = null;
	        }

	        xhr.onreadystatechange = function() {
        
                if(xhr.readyState == 4) {

                    if(xhr.status >= 400) {
                        lsconnect.log("xhr.status:"+xhr.status + " url:" + settings.url);     
                        lsconnect.log("xhr.responseHeaders:"+xhr.getAllResponseHeaders());               
                        settings.fail(xhr);
                        return;
                    }
                    
                    if( xhr.status == 200 ) { //Should include more status codes here?
                        if (!processRedirect(xhr,settings)) {
                            settings.success(xhr.responseText); 
                        } 
	                    return;                  
                    }
                    
                    lsconnect.log("xhr.status:"+xhr.status + " url:" + settings.url);     
                    lsconnect.log("xhr.responseHeaders:"+xhr.getAllResponseHeaders());                       
                    settings.fail(xhr);
	            } 
	        };
	        xhr.open(settings.methodType, settings.url, true);
	        xhr.setRequestHeader("content-type", settings.contentType);
            xhr.setRequestHeader("_ssoClientType", "MSXML");
            var spec = lsconnect.getDeviceInfo();
            xhr.setRequestHeader("_clientDeviceSpec","width="+spec.width+",height="+spec.height+",appName="+spec.appName+",deviceType="+spec.deviceType+",appUUID="+spec.appUUID);            
            try {      	    
	            xhr.send(settings.content);		
	        } catch(e) {
	            lsconnect.log("Exception with sending request:" + e.toString() + " url:" + settings.url );
	        }	            
            return xhr;
        }
        
    };

    function processRedirect(xhr, settings) {
        var sso_status = xhr.getResponseHeader('SSO_STATUS');
        if(sso_status==undefined || sso_status==null || sso_status.length==0) {
            return false;
        }
        
        var xmlText = xhr.responseText;
        if(xmlText.indexOf('<?xml')<0) {
            return false;
        }
        
        lsconnect.log("sso content");
        lsconnect.log(xmlText);        
        var xmldoc = $.parseXML(xmlText);
        if(xmldoc==undefined) {
            return false;
        }
        var sso = $(xmldoc).find('SSO_STATUS');
        if(sso==undefined || sso.text() == undefined) {
            return false;
        }
                
        sso_lingeringReq[sso_lingeringId] = settings;        
        var flag = lsconnect_internal.updateAuthenStatus(settings.url, sso_lingeringId, xhr.responseText);
        var processed = "false";
        if("true"!=flag) {//no further processing needed
            processed = "true";
            lsconnect.openPasswordLogin();
        }
        
        sso_lingeringId++;        
        
        return processed=="true" || sso.text()=="Redirecting";   	
    };
    
    function getUrlParameterValue(urlStr, name) {
        var match = RegExp('[?&]' + name + '=([^&]*)').exec(urlStr);
        return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
    }
        
})();

function triggerAuthStatusEvent(jsonStr) {
    var obj = jQuery.parseJSON( jsonStr );
    jQuery(document).trigger('authStatusEvent', obj );
}

jQuery(document).ready(function(){

    jQuery(document).unbind('authStatusEvent').bind('authStatusEvent', 
        function(event, authStatus) {
            if(authStatus.isAuthenticated=='true'&&authStatus.isTimedout!='true') {
                if(sso_lingeringReq[authStatus.lingeringId]!=undefined && sso_lingeringReq[authStatus.lingeringId]!=null) {
                    var req = sso_lingeringReq[authStatus.lingeringId];
                    sso_lingeringReq[authStatus.lingeringId] = null;
                    trxconnect.request(req);
                }
            }                            
        }
    ); 
                
 });