package com.infor.mscm.scanner;

import android.app.Activity;
import android.content.Intent;
import android.content.Context;
import android.content.IntentFilter;
import android.content.BroadcastReceiver;
import android.util.Log;
import org.apache.cordova.CallbackContext;

public class MotorolaScanner extends Scanner 
{
	
    private static final String MOTOROLA_SOURCE_TAG = "com.motorolasolutions.emdk.datawedge.source";
    // private static final String MOTOROLA_LABEL_TYPE_TAG = "com.motorolasolutions.emdk.datawedge.label_type";
    // private static final String MOTOROLA_DECODE_DATA_TAG = "com.motorolasolutions.emdk.datawedge.decode_data";
    private static final String MOTOROLA_DATA_STRING_TAG = "com.motorolasolutions.emdk.datawedge.data_string";
	
	private static final String MOTOROLA_ACTION_SCANNERINPUTPLUGIN = "com.motorolasolutions.emdk.datawedge.api.ACTION_SCANNERINPUTPLUGIN";
	private static final String MOTOROLA_EXTRA_PARAM = "com.motorolasolutions.emdk.datawedge.api.EXTRA_PARAMETER";
	
	private static final String MOTOROLA_INTENT_RECEIVER = "com.infor.pou.mscm.scanner.RECEIVER";

	private Activity m_activityMain;
	private IntentFilter m_intentFilter;
	
	public MotorolaScanner() 
	{
		m_intentFilter = new IntentFilter();
		m_intentFilter.addAction(MOTOROLA_INTENT_RECEIVER);
	}
	
	@Override
	public void initialize(Activity activity) 
	{
		super.initialize(activity);
		
		m_activityMain = activity;
		m_activityMain.registerReceiver(intentBroadcastReceiver, m_intentFilter);		
		
		Intent i = new Intent();
		i.setAction(MOTOROLA_ACTION_SCANNERINPUTPLUGIN);
		
		if (m_IsEnabled)
		{
			i.putExtra(MOTOROLA_EXTRA_PARAM, "ENABLE_PLUGIN");
		}
		else
		{
			i.putExtra(MOTOROLA_EXTRA_PARAM, "DISABLE_PLUGIN");
		}
				
		m_activityMain.sendBroadcast(i);

	}
	
	@Override
	public void deinitialize() 
	{
		super.deinitialize();
		
		m_activityMain.unregisterReceiver(intentBroadcastReceiver);		
		
		Intent i = new Intent();
		i.setAction(MOTOROLA_ACTION_SCANNERINPUTPLUGIN);
		i.putExtra(MOTOROLA_EXTRA_PARAM, "ENABLE_PLUGIN");
		m_activityMain.sendBroadcast(i);
	}

	@Override
	public void enableScanner(CallbackContext context)
	{
		super.enableScanner(context);
		
		Intent i = new Intent();
		i.setAction(MOTOROLA_ACTION_SCANNERINPUTPLUGIN);        
		i.putExtra(MOTOROLA_EXTRA_PARAM, "ENABLE_PLUGIN");
		m_activityMain.sendBroadcast(i);
	}
	
	@Override
	public void disableScanner() 
	{
		super.disableScanner();
		
		Intent i = new Intent();
		i.setAction(MOTOROLA_ACTION_SCANNERINPUTPLUGIN);        
		i.putExtra(MOTOROLA_EXTRA_PARAM, "DISABLE_PLUGIN");
		m_activityMain.sendBroadcast(i);
	}
	

	public void processIntent(Intent receivedIntent) 
	{
		String dataSource = receivedIntent.getStringExtra(MOTOROLA_SOURCE_TAG);
		String dataReceived = receivedIntent.getStringExtra(MOTOROLA_DATA_STRING_TAG);
		
		if (dataSource.equalsIgnoreCase("scanner")) 
		{
			if (dataReceived != null && dataReceived.length() > 0) 
			{
				
				/*AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						m_activityMain);

					// set title
					alertDialogBuilder.setTitle(dataSource);

					// set dialog message
					alertDialogBuilder
						.setMessage(dataReceived)
						.setCancelable(true);

						// create alert dialog
						AlertDialog alertDialog = alertDialogBuilder.create();

						// show it
						alertDialog.show();*/
				
				scanSuccess(dataReceived);
			}
			else
			{
				scanFailed();
				ScannerSound.playFailedSound();
			}
		}
	}
	
	private BroadcastReceiver intentBroadcastReceiver = new BroadcastReceiver() 
	{
		@Override
		public void onReceive(Context context, Intent intent) 
		{
			if (intent.getAction().contentEquals(MOTOROLA_INTENT_RECEIVER))
			{
				processIntent(intent);
			}
		}
	};

}
