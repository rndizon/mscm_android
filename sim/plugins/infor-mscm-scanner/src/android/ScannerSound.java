package com.infor.mscm.scanner;

import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.app.Activity;
import org.apache.cordova.CordovaActivity;

import java.io.IOException;

import org.apache.cordova.LOG;

public class ScannerSound 
{
	private static AssetFileDescriptor m_afdSuccessSoundFile = null;
	private static AssetFileDescriptor m_afdFailedSoundFile = null;
	
	public static void initialize (Activity activity) 
	{
		CordovaActivity ca = (CordovaActivity)activity;
		try 
		{
			m_afdSuccessSoundFile = ca.getAssets().openFd("www/common/sounds/beep.wav");
			m_afdFailedSoundFile = ca.getAssets().openFd("www/common/sounds/buzz.wav");
		}
		catch (IOException e) {
			LOG.i("CordovaLog", "ScannerSound::initialize() IOException");
		}
	}
	
	public static void clear () 
	{
		try 
		{
			m_afdSuccessSoundFile.close();
			m_afdFailedSoundFile.close();
			
			m_afdSuccessSoundFile = null;
			m_afdFailedSoundFile = null;
		}
		catch (IOException e) 
		{
			LOG.i("CordovaLog", "ScannerSound::clear() IOException");
		}
	}
	
	public static void playSuccessSound () 
	{
		if (m_afdSuccessSoundFile != null)
		{
			try {
				MediaPlayer mediaPlayer = new MediaPlayer();
				mediaPlayer.setDataSource(m_afdSuccessSoundFile.getFileDescriptor(),
										  m_afdSuccessSoundFile.getStartOffset(),
										  m_afdSuccessSoundFile.getLength());
				mediaPlayer.prepare();
				mediaPlayer.start();
			}
			catch (IOException e) {
				LOG.i("CordovaLog", "ScannerSound::playSuccessSound IOException");
			}
		}
	}
	
	public static void playFailedSound () 
	{
		if (m_afdFailedSoundFile != null)
		{
			try {
				MediaPlayer mediaPlayer = new MediaPlayer();
				mediaPlayer.setDataSource(m_afdFailedSoundFile.getFileDescriptor(),
										  m_afdFailedSoundFile.getStartOffset(),
										  m_afdFailedSoundFile.getLength());
				mediaPlayer.prepare();
				mediaPlayer.start();
			}
			catch (IOException e) {
				LOG.i("CordovaLog", "ScannerSound::playFailedSound IOException");
			}
		}
	}
}
