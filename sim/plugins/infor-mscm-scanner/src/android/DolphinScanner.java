package com.infor.mscm.scanner;

/**
 * Created by rndizon on 1/25/2016.
 */
import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

import com.honeywell.aidc.*;

public class DolphinScanner extends Scanner implements BarcodeReader.BarcodeListener,BarcodeReader.TriggerListener
{
    private static BarcodeReader mBarcodeReader;
    private AidcManager mManager;
    protected Activity mActivity;
    protected DolphinScanner mSelf;

    @Override
    public void initialize(Activity _activity) {

        mActivity = _activity;
        mSelf = this;

        AsyncTaskRunner runner = new AsyncTaskRunner();
        runner.execute();

    }

    @Override
    public void deinitialize() {

        if (mBarcodeReader != null) {
            // close BarcodeReader to clean up resources.
            mBarcodeReader.close();
            mBarcodeReader = null;

            // unregister barcode event listener
            mBarcodeReader.removeBarcodeListener(mSelf);

            // unregister trigger state change listener
            mBarcodeReader.removeTriggerListener(mSelf);
        }

        if (mManager != null) {
            // close AidcManager to disconnect from the scanner service.
            // once closed, the object can no longer be used.
            mManager.close();
        }
    }

    @Override
    public void disableScanner()
    {
        super.disableScanner();

        if (mBarcodeReader != null) {
            // release the scanner claim so we don't get any scanner
            // notifications while paused.
            mBarcodeReader.release();
        }

    }


    @Override
    public void onBarcodeEvent(final BarcodeReadEvent event) {

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                //Toast.makeText(mActivity, "Scanned Data "+event.getBarcodeData(), Toast.LENGTH_SHORT).show();
                scanSuccess(event.getBarcodeData());
            }
        });

    }

    // these events can be used to implement custom trigger modes if the
    // automatic behavior provided by the scanner service is insufficient for
    // your application. The following code demonstrates a "toggle" mode
    // implementation, where the state of the scanner changes each time the scan
    // trigger is pressed.
    @Override
    public void onTriggerEvent(TriggerStateChangeEvent event) {
        try {
            // only handle trigger presses
            // turn on/off aimer, illumination and decoding
            mBarcodeReader.aim(event.getState());
            mBarcodeReader.light(event.getState());
            mBarcodeReader.decode(event.getState());

        } catch (ScannerNotClaimedException e) {
            e.printStackTrace();
//            Toast.makeText(mActivity, "Scanner is not claimed", Toast.LENGTH_SHORT).show();
        } catch (ScannerUnavailableException e) {
            e.printStackTrace();
      //      Toast.makeText(mActivity, "Scanner unavailable", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onFailureEvent(BarcodeFailureEvent arg0) {
        // TODO Auto-generated method stub

    }

    /**
     *    Task Runner Class
     * */
    private class AsyncTaskRunner extends AsyncTask <String, String, BarcodeReader>{


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected BarcodeReader doInBackground(String... params) {

            // create the AidcManager providing a Context and a
            // CreatedCallback implementation.
            AidcManager.create(mActivity, new AidcManager.CreatedCallback() {

                @Override
                public void onCreated(AidcManager aidcManager) {
                 //   Toast.makeText(mActivity, "AidcManager created", Toast.LENGTH_SHORT).show();
                    mManager = aidcManager;
                    mBarcodeReader = mManager.createBarcodeReader();

                }
            });

            return mBarcodeReader;
        }

        @Override
        protected void onPostExecute(BarcodeReader eventReader) {
            super.onPostExecute(eventReader);

            if (mBarcodeReader != null) {

                try {
                    mBarcodeReader.claim();

                } catch (ScannerUnavailableException e) {
                    e.printStackTrace();
                    Toast.makeText(mActivity, "Scanner unavailable", Toast.LENGTH_SHORT).show();
                }

                // register bar code event listener
                mBarcodeReader.addBarcodeListener(mSelf);

                // set the trigger mode to client control
                try {
                    mBarcodeReader.setProperty(BarcodeReader.PROPERTY_TRIGGER_CONTROL_MODE,
                            BarcodeReader.TRIGGER_CONTROL_MODE_CLIENT_CONTROL);
                } catch (UnsupportedPropertyException e) {
                    Toast.makeText(mActivity, "Failed to apply properties", Toast.LENGTH_SHORT).show();
                }
                // register trigger state change listener
                mBarcodeReader.addTriggerListener(mSelf);
            }

            //Toast.makeText(mActivity, "Done BarcodeReader initialization!", Toast.LENGTH_SHORT).show();
        }


    }

}
