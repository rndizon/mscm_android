(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('CompanyChangeController', Body);

		function Body($state,
					  $scope,
					  $ionicLoading,
					  $ionicViewSwitcher,
					  $ionicModal, 
					  filterFilter,
					  $ionicHistory,
					  CRUD, 
					  DmeManager,
					  LocalData,
					  CONSTANT,
					  $ionicPopup
					  ) 
		{
					
			var vm = this;
			vm.companyList = []; 
			vm.searchText = ""; //initialize
			vm.doRefresh = doRefresh;
			vm.currentUser = LocalData.loadSession('currentUser');
			vm.users = JSON.parse(LocalData.loadData('users'));
			     	
			$scope.$on('$ionicView.afterEnter', function() {
			    	
				vm.defaultCompanyID = LocalData.loadData("ParCountCompanyID");
				vm.defaultCompanyName = LocalData.loadData("ParCountCompanyName");
				vm.showCompanyList();
				
			});

			function doRefresh () {
	    	  	//show loading icon
				$ionicLoading.show({
					templateUrl: CONSTANT.SpinnerTemplate
				});

       			vm.showCompanyList();
			}

			vm.goBack = function(){

					$ionicHistory.goBack();
			}

			vm.showCompanyList = function() {
				var companyParams = {};

				if (vm.searchText.trim()) {
					companyParams.CompanyID = vm.searchText.trim();
				}

				CRUD.select("Par_Company_Table","*",companyParams)
				 .then(function(result){
		 		 	 	console.log('Callback: select result:' + result[0].CompanyDesc);
		 		 	 	vm.companyList = result;
   						$scope.$broadcast('scroll.refreshComplete');
						$ionicLoading.hide();
		 		});
				

			};

			vm.saveCompanySelected = function () {
				
				LocalData.saveData("ParCountCompanyID", vm.defaultCompanyID);

				Object.keys(vm.companyList).forEach(function(idx){
					if (vm.companyList[idx].CompanyID == vm.defaultCompanyID) {
						LocalData.saveData("ParCountCompanyName", vm.companyList[idx].CompanyDesc);
						if(!vm.users[vm.currentUser].defaultParCompanyID) {
							saveParCountDefCompany(vm.companyList[idx].CompanyID,vm.companyList[idx].CompanyDesc);
						} else {
							goToPrevState();
						}
					}

				});
				
			};

			function goToPrevState() {
				if(LocalData.loadSession("PrevStateName")){
					$state.go(LocalData.loadSession("PrevStateName"));
				}else{
					$state.go('app.my-par-forms');
				}
			}

			function saveParCountDefCompany(compID,compName) {
					vm.popUpMsg = "set-def-company";
					var confirmPopup  = $ionicPopup.confirm({
	                    cssClass: 'custom-popup',
						templateUrl: "html/templates/pop-up.html",
						scope: $scope,
	       				cancelText: "No",
	        			okText: "Yes"
					});

					confirmPopup.then( function(res) {
						if(res == true) {
							vm.users[vm.currentUser].defaultParCompanyID = compID;
							vm.users[vm.currentUser].defaultParCompanyDescription = compName;
							LocalData.saveData('users',JSON.stringify(vm.users));
							goToPrevState();
						} else {
							goToPrevState();
						}
					});
			}
			
	    	vm.clearSearch = function(){
				vm.searchText = '';
			}; 
			
			function doRefresh() {

				$ionicLoading.show({
					templateUrl: CONSTANT.SpinnerTemplate
				});

				vm.showCompanyList();

				$scope.$broadcast('scroll.refreshComplete');
				$ionicLoading.hide();

			}

		}
				
		
})();
