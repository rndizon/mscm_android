(function(){
    'use strict';
	 
	angular
	    .module('app')
	    .factory('CommonInfoService', Body);


	    function Body(CRUD) {
	    	
	    	var selectedCompany = "";
	    	var selectedParforms = [];
	    	var quantityValues = [];
	    	
	    	return {
	    		
	    		setSelectedCompany : setSelectedCompany,
	    		getSelectedCompany : getSelectedCompany,
	    		setSelectedParforms : setSelectedParforms,
	    		getSelectedParforms : getSelectedParforms,
	    		fillQuantityField : fillQuantityField,
	    		setQuantityValues : setQuantityValues,
	    		getQuantityValues : getQuantityValues,
	    		setLastLogin:setLastLogin,
	    		clearTable:clearTable,
	    		getDateTime:getDateTime,
	    		getTimeDifference:getTimeDifference
	    		
	    	};

	    	function setSelectedCompany(companyID){
	    		selectedCompany = companyID;
	    	}
	    	
	    	function getSelectedCompany () {
	    		return selectedCompany;
	    	}
	    	
	    	function setSelectedParforms (arrySelectedParforms) {
	    		selectedParforms = angular.copy(arrySelectedParforms);
	    	}
	    	
	    	function getSelectedParforms () {
	    		return selectedParforms;
	    	}
	    	function fillQuantityField(index,val,id){ 
					document.getElementById(id+"-quantity-"+index).value = val;
					document.getElementById(id+"-quantity-"+index).focus();

			}
			function setQuantityValues(id,totalItems){
				quantityValues = [];
				for(var i = 0; i < totalItems; i++){
					var value = "";
					if(document.getElementById(id+"-quantity-"+i).value.length > 0){
						value = document.getElementById(id+"-quantity-"+i).value;
					}
					console.log(i+" : "+value);
					quantityValues.push(value);
				}

			}
			function getQuantityValues(){
				return quantityValues;
			}
	    	
	    	function setLastLogin(){
	    		navigator
	    		.globalization
	    		.dateToString( new Date(),
			        function (date) { LocalData.saveSession('LastLogin',date.value);},
			        function () { time = "" },
			        { formatLength: 'short', selector: 'date and time' }
			    );
			   
	    	}
	    	function clearTable(table){
				    return CRUD.remove(table);
			}
			function getDateTime() {
			    var now     = new Date(); 
			    var year    = now.getFullYear();
			    var month   = now.getMonth()+1; 
			    var day     = now.getDate();
			    var hour    = now.getHours();
			    var minute  = now.getMinutes();
			    var second  = now.getSeconds(); 
			    if(month.toString().length == 1) {
			        var month = '0'+month;
			    }
			    if(day.toString().length == 1) {
			        var day = '0'+day;
			    }   
			    if(hour.toString().length == 1) {
			        var hour = '0'+hour;
			    }
			    if(minute.toString().length == 1) {
			        var minute = '0'+minute;
			    }
			    if(second.toString().length == 1) {
			        var second = '0'+second;
			    }   
			    var dateTime = year+'-'+month+'-'+day+' '+hour+':'+minute+':'+second;   
			     return dateTime;
			}
			/**
				Sample usage:

				var laterdate = new Date("2016-03-15 12:02:13");     
				var earlierdate = new Date("2016-03-15 10:02:13");  
				timeDifference(laterdate,earlierdate);
			*/
			function getTimeDifference(laterdate,earlierdate) {
				var diffArray = [];
			    var difference = laterdate.getTime() - earlierdate.getTime();

			    var rawMinutes = Math.floor(difference/1000/60);
			 
			    var daysDifference = Math.floor(difference/1000/60/60/24);
			    difference -= daysDifference*1000*60*60*24
			 
			    var hoursDifference = Math.floor(difference/1000/60/60);
			    difference -= hoursDifference*1000*60*60
			 
			    var minutesDifference = Math.floor(difference/1000/60);
			    difference -= minutesDifference*1000*60
			 
			    var secondsDifference = Math.floor(difference/1000);
			 	
			 	diffArray.push(rawMinutes);
			 	diffArray.push(daysDifference);
			 	diffArray.push(hoursDifference);
			 	diffArray.push(minutesDifference);
			 	diffArray.push(secondsDifference);
			 	
			 	return diffArray;

			 	//alert('difference = ' + daysDifference + ' day/s ' + hoursDifference + ' hour/s ' + minutesDifference + ' minute/s ' + secondsDifference + ' second/s ');
			}
	    	
		}
})();
