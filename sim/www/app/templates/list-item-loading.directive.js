(function(){
    'use strict';
   
  angular
      .module('app')
      .directive('itemLoading', function(ParCountService,CycleCountService,PFPService,LocalData,CRUD) {
        
        function generateHTML(item){
            var htmlText = '',
                color = '';

                var percentage = (parseInt(item.TotalItemCounted)/parseInt(item.ItemCount));
                    percentage = percentage * 100;

                if(parseInt(item.TotalItemCounted) == 0 || parseInt(item.TotalItemCounted) < 1){
                    color = "";
                }else if(parseInt(item.TotalItemCounted) < parseInt(item.ItemCount) && parseInt(item.TotalItemCounted) > 0){
                  color = "orange";
                }else{
                  color = "green"; 
                }
                htmlText = '<div class="empty">';
                htmlText +='<div class="innerscale '+color+'" style="height:'+parseInt(percentage)+'%;"></div>';
                htmlText +='</div>';
                htmlText +='<div class="count">'+ item.TotalItemCounted +' / '+item.ItemCount +'</div>';

                return htmlText;
        }

        return {
              restrict: 'EA',
              template:'<ion-spinner icon="crescent" class="md"></ion-spinner>',
              scope: {
                myDirective: '@',
                itemParam: '@itemParam'
              },
              link: function(scope, element, attrs) {
                
                    //console.log("Module: "+attrs.itemModule);
                    if(attrs.itemModule == "par-count"){

                      scope.$watch('itemParam', function(jsonString){
                          if(jsonString){
                            var item = JSON.parse(jsonString);
                              if(parseInt(item.IsItemDownloaded) > 0){
                                element.empty();
                                element.append(generateHTML(item));
                                  //element.replaceWith(generateHTML(item));
                              }else{

                                var requestString = "<getparformdetails>";
                                requestString += '<par locid="'+ item.LocationID +'"/>';
                                requestString += "</getparformdetails>";

                                ParCountService
                                .getParformDetails(requestString)
                                .then(function(response) {

                                      var queryParamsUpdate = {};
                                          queryParamsUpdate.LocationID = item.LocationID;
                                          CRUD.update("Par_Location_Table","IsItemDownloaded",1,queryParamsUpdate); 

                                      ParCountService
                                      .saveParformItems(response)
                                      .then(function(){

                                            var queryParams = {};
                                            queryParams.CompanyID = LocalData.loadData("ParCountCompanyID");
                                            queryParams.IsSelected = 1;
                                            queryParams.LocationID = item.LocationID;

                                            var columns = "parLoc.IsItemDownloaded,";
                                              columns +="(select COUNT(*) from Par_Location_Table left join Par_Item_Table ";
                                              columns +="on Par_Location_Table.LocationID = Par_Item_Table.LocationID ";
                                              columns +="where Par_Item_Table.LocationID = parLoc.LocationID) as ItemCount,";

                                              columns +="(select COUNT(*) from Par_Location_Table left join Par_Transaction_Table ";
                                              columns +="on Par_Location_Table.LocationID = Par_Transaction_Table.LocationID ";
                                              columns +="where Par_Transaction_Table.LocationID = parLoc.LocationID) as TotalItemCounted ";

                                            CRUD.select("Par_Location_Table as parLoc",columns, queryParams)
                                              .then(function(result) {
                                               // element.replaceWith(generateHTML(result[0]));
                                                element.empty();
                                                element.append(generateHTML(result[0]));
                                            });

                                      });
                                });
                              }
                          }
                      });
                    }else if(attrs.itemModule == "cycle-count"){
                      scope.$watch('itemParam', function(jsonString){
                          if(jsonString){
                            var item = JSON.parse(jsonString);
                              if(parseInt(item.IsItemDownloaded) > 0){
                                element.empty();
                                element.append(generateHTML(item));
                              }else{

                                var requestString = "<get_cycle_details>";
                                requestString += '<countform countformid="'+ item.CountFormID +'"/>';
                                requestString += "</get_cycle_details>";

                                CycleCountService
                                .getCountFormDetails(requestString)
                                .then(function(response) {

                                     var queryParamsUpdate = {};
                                      queryParamsUpdate.CountFormID = item.CountFormID;
                                    CRUD.update("Cycle_Count_Form_Table","IsItemDownloaded",1,queryParamsUpdate);

                                    CycleCountService
                                    .saveCountFormDetails(response)
                                    .then(function(){
                                        
                                        var query = "SELECT *,";
                                        query +="(select count(*) from Cycle_Count_Form_Table left join Cycle_Item_Table USING(CountFormID) ";
                                        query +="where Cycle_Item_Table.CountFormID = cycleCount.CountFormID) as ItemCount,";
                                        query +="(select COUNT(*) from Cycle_Count_Form_Table left join Cycle_Transaction_Table USING(CountFormID) ";
                                        query +="where Cycle_Transaction_Table.CountFormID = cycleCount.CountFormID) as TotalItemCounted ";  
                                        query +="FROM Cycle_Count_Form_Table as cycleCount ";
                                        query +="WHERE CountFormID = "+item.CountFormID+" AND cycleCount.IsSelected = 1 AND cycleCount.CompanyID="+LocalData.loadData("CycleCompanyID");

                                      CRUD.inlineQuery(query)
                                          .then(function(result) {
                                              element.empty();
                                              element.append(generateHTML(result[0]));
                                          });
                                    });

                                });
                              }
                          }
                      });
                    }else{
                      scope.$watch('itemParam', function(jsonString){
                          if(jsonString){
                            var item = JSON.parse(jsonString);
                              if(parseInt(item.IsItemDownloaded) > 0){
                                element.empty();
                                element.append(generateHTML(item));
                              }else{

                                var requestString = "<getPFPdetails>";
                                requestString += '<invloc locid="'+ item.InventoryLocationID +'"/>';
                                requestString += '<par locid="'+ item.ParLocationID +'"/>';
                                requestString += "</getPFPdetails>";

                                PFPService
                                  .getPFPDetails(requestString)
                                  .then(function(response){

                                        var queryParamsUpdate = {};
                                        queryParamsUpdate.ParLocationID = item.ParLocationID;
                                        CRUD.update("PFP_Par_Table","IsItemDownloaded",1,queryParamsUpdate);

                                       PFPService
                                        .savePFPDetails(response)
                                        .then(function(){

                                            var query = "select *,";
                                              query+= "(select count(ItemID) FROM PFP_Task_Table where ParLocationID = p_table.ParLocationID) as ItemCount, ";
                                              query+= "(select count(*) FROM PFP_Transaction_Table where ParLocationID = p_table.ParLocationID) as TotalItemCounted ";
                                              query+= "from PFP_Par_Table p_table ";
                                              query+= "where IsSelected = 1 AND ";
                                              query+= "InventoryLocationID = "+item.InventoryLocationID+" and ParLocationID = "+item.ParLocationID;

                                            CRUD.inlineQuery(query)
                                            .then(function(result){
                                              element.empty();
                                              element.append(generateHTML(result[0]));
                                            });

                                        });

                                  });

                                
                                
                              }
                          }
                      });
                    }
                    
              }
             
          };
      });
      
})();