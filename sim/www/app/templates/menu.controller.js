(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('MenuController', Body);

		function Body($state,
					  $scope,
					  $ionicPopup,
					  LocalData)
		{
			
			var vm = this;

			vm.firstEnter = firstEnter;
			vm.signOutConfirmation = signOutConfirmation;


			function firstEnter(page) {
				LocalData.saveSession('firstEnter' + page,"1");
			}

			function signOutConfirmation() {

				var confirmPopup  = $ionicPopup.confirm({
                    cssClass: 'custom-popup',
					template: "Are you sure you want to sign out ?",
					scope: $scope,
       				cancelText: "No",
        			okText: "Signout"
				});

				confirmPopup.then( function(res) {
					if(res == true) {
						console.log('return true');
						InforSecurityService.signOut();
						$ionicSideMenuDelegate.toggleLeft();
						$state.go('app.welcome');
						//InforSecurityService.signIn();
					}
				});
			}


		}
				
		
})();
