(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('IndexController', Body);

		function Body($state,$scope,$rootScope,$ionicLoading,$ionicSideMenuDelegate,
				$ionicViewSwitcher,$window,$ionicPopup,
				AuthService,LocalData,CONSTANT,
				InforSecurityService,CommonInfoService,CRUD,d3Init,
				$ionicPlatform,
				$ionicTabsDelegate,
				$timeout,$ionicSlideBoxDelegate){
				
			 var vm = this;
			 vm.toggleGroup = toggleGroup;
			 vm.isGroupShown = isGroupShown;
			 vm.goTo = goTo;
			 vm.locationDetails = '';
			 vm.getLocation = getLocation;
			 vm.ParCountSelectChange = ParCountSelectChange;
			vm.CycleCountSelectChange = CycleCountSelectChange;
			vm.PickForParSelectChange = PickForParSelectChange;
			vm.currentSlideNumber = 1;
			vm.slideHasChanged = slideHasChanged;
			vm.goToSlide = goToSlide;			
			vm.IndexView = 0;
			 
			 /*Par Count Data*/
			 vm.ParformsCounted = 0;
		 	 vm.ItemsCounted 	= 0;
		 	 vm.TimeSpent 		= '0:00';
		 	 vm.LastParCount 	= '--';

			 $scope.$on('$ionicView.beforeEnter', function() {
				
			 	vm.cycleCountData = {
				 	selectIdsCounted:0,
				 	ItemsCounted:0,
				 	TimeSpent:'0:00'

				};
				vm.pickForParData = {
				 	ParformsPicked:0,
				 	ItemsPulled:0,
				 	TimeSpent:'0:00'

				};	
				if(LocalData.loadSession('LastLogin')){
				 	vm.getLastLogin = LocalData.loadSession('LastLogin');
				 }else{
				 	if(typeof navigator.globalization !== "undefined") {
				 		navigator
			    		.globalization
			    		.dateToString( new Date(),
					        function (date) { 
					        	vm.getLastLogin = date.value;
					        },
					        function () { time = "" },
					        { formatLength: 'short', selector: 'date and time' }
					    );
				 	}
				 	
				 }

				var columns = "*,COUNT(CountedParforms) as ParformsCounted";
				CRUD.select("Par_TransHistory_Table",columns)
				    .then(function(result) {
				    
				 	vm.ParformsCounted = result[0].ParformsCounted;
				 	vm.ItemsCounted = result[0].CountedItems;
				 	vm.TimeSpent = result[0].TimeSpent;
				 	vm.LastParCount = result[0].Timestamp;
					 	   		 
				});

				 vm.doLogin(); 

				
								
				chart1 = d3Init.getd3init("#ParCountChart", [temp_data_ParCount_day1,temp_data_ParCount_day2]);
				chart2 = d3Init.getd3init("#CycleCountChart", [temp_data_ParCount_day1,temp_data_ParCount_day2]);
				chart3 = d3Init.getd3init("#PickForParChart", [temp_data_ParCount_day1,temp_data_ParCount_day2]);
				

			});

			 
			vm.doLogin = function(){
				//show loading icon
				/*$ionicLoading.show({
					templateUrl: CONSTANT.SpinnerTemplate
				});*/ 
				AuthService.doLogin()
				.then(function(value){
					//alert("2");
					$rootScope.Modules = LocalData.loadSession("Modules");
					//alert("sad "+$rootScope.Modules.length);
					//$ionicViewSwitcher.nextDirection('forward');
					//$ionicLoading.hide();
					
					
				});



			};
			 /* --
			 *  Trigger Lawson security plugin 
			 *  Temporary, this will be moved to correct Controller after completing all controllers
			 *  --*/
			InforSecurityService.signIn();


			
			vm.sigout =  function(){InforSecurityService.signOut();}
			 

			  /*
			   * if given group is the selected group, deselect it
			   * else, select the given group
			   */
			  function toggleGroup(group) {

			    if (vm.isGroupShown(group)) {
			      vm.shownGroup = null;
			    } else {
			      vm.shownGroup = group;
			    }
			  };

			 function isGroupShown(group) {
			    return vm.shownGroup === group;
			  };

			 function goTo(url) {

				//$ionicViewSwitcher.nextDirection('right');
			    $state.go(url);
			 }

			 function getLocation() {

			    if ($window.navigator.geolocation) {
			    	vm.locationDetails = "lbl_location_unknown";
			        $window.navigator.geolocation.getCurrentPosition(showPosition,onErr,{timeout:10000});
			    } else {
			        vm.locationDetails = "Geolocation is not supported";
			    }
			}
			function showPosition(position) {
			    vm.locationDetails = "Latitude: " + position.coords.latitude +
			    "<br>Longitude: " + position.coords.longitude;
			}

			function onErr(error) {
				console.log(error);
			}

			vm.getLocation();

			/*====================================
				DROPDOWN
			====================================*/
			function ParCountSelectChange(selval){
				console.log('****** ParCountSelectChange ******');
				if (selval=="day"){
					d3Init.d3update(chart1, "#ParCountChart", [temp_data_ParCount_day1,temp_data_ParCount_day2], 'day');					
				}else if (selval=="week"){					
					d3Init.d3update(chart1, "#ParCountChart", [temp_data_ParCount_week1,temp_data_ParCount_week2], 'week');					
				}else if (selval=="month"){					
					d3Init.d3update(chart1, "#ParCountChart", [temp_data_ParCount_month1,temp_data_ParCount_month2], 'month');					
				}
			}
			function CycleCountSelectChange(selval){				
				console.log('****** CycleCountSelectChange ******');
				if (selval=="day"){
					d3Init.d3update(chart2, "#CycleCountChart", [temp_data_ParCount_day1,temp_data_ParCount_day2], 'day');
				}else if (selval=="week"){
					d3Init.d3update(chart2, "#CycleCountChart", [temp_data_ParCount_week1,temp_data_ParCount_week2], 'week');
				}else if (selval=="month"){
					d3Init.d3update(chart2, "#CycleCountChart", [temp_data_ParCount_month1,temp_data_ParCount_month2], 'month');
				}
			}
			function PickForParSelectChange(selval){				
				console.log('****** PickForParSelectChange ******');
				if (selval=="day"){
					d3Init.d3update(chart3, "#PickForParChart", [temp_data_ParCount_day1,temp_data_ParCount_day2], 'day');
				}else if (selval=="week"){
					d3Init.d3update(chart3, "#PickForParChart", [temp_data_ParCount_week1,temp_data_ParCount_week2], 'week');
				}else if (selval=="month"){
					d3Init.d3update(chart3, "#PickForParChart", [temp_data_ParCount_month1,temp_data_ParCount_month2], 'month');
				}
			}
			
			/*====================================
				TAB SLIDER
			====================================*/
			var isgotoslide = false;
			function slideHasChanged(index){				
				//	SELECT TAB
				$timeout(function(){
					$ionicTabsDelegate.select(index, false);
				},200);
			}
			
			function goToSlide(gotoslide){
				$ionicSlideBoxDelegate.slide(gotoslide);
			}
			
			 /* --
			 *  Trigger Lawson security plugin 
			 *  Temporary, this will be moved to correct Controller after completing all controllers
			 *  --*/
			//InforSecurityService.signIn();


			function signOutConfirmation() {
				var confirmPopup  = $ionicPopup.confirm({
                    cssClass: 'custom-popup',
					template: "Are you sure you want to sign out ?",
					scope: $scope,
       				cancelText: "No",
        			okText: "Signout"
				});

				confirmPopup.then( function(res) {
					if(res == true) {
						console.log('return true');
						InforSecurityService.signOut();
						$ionicSideMenuDelegate.toggleLeft();
						$state.go('app.welcome');
						//InforSecurityService.signIn();
					}
				});

			}

			var chart1 = null;
			var chart2 = null;
			var chart3 = null;
			
			/*
				FORMAT
				-	color	:	color of line and points
				-	x		:	datetime
				-	y		:	amount
				-	t		:	time in minutes
				-	p		:	parcount
			*/
			 
			//	DAY 1/2
			var temp_data_ParCount_day1 = [
						{color:"#61982F"}						
						,{x:'2016-05-04 01:00:00', y:0, t:153, p:1}
						,{x:'2016-05-04 02:00:00', y:50, t:153, p:1}
						,{x:'2016-05-04 03:00:00', y:250, t:153, p:1}
						,{x:'2016-05-04 04:00:00', y:600, t:115, p:1}
						,{x:'2016-05-04 05:00:00', y:1000, t:116, p:1}
						,{x:'2016-05-04 06:00:00', y:200, t:117, p:1}
						,{x:'2016-05-04 07:00:00', y:396, t:128, p:1}
						,{x:'2016-05-04 08:00:00', y:396, t:128, p:1}
						,{x:'2016-05-04 09:00:00', y:396, t:128, p:1}
						,{x:'2016-05-04 10:00:00', y:396, t:128, p:1}
						,{x:'2016-05-04 11:00:00', y:396, t:128, p:1}
						,{x:'2016-05-04 12:00:00', y:396, t:128, p:1}
						,{x:'2016-05-04 13:00:00', y:396, t:128, p:1}
						,{x:'2016-05-04 14:00:00', y:396, t:128, p:1}
						,{x:'2016-05-04 15:00:00', y:396, t:128, p:1}
						,{x:'2016-05-04 16:00:00', y:396, t:128, p:1}
						,{x:'2016-05-04 17:00:00', y:396, t:128, p:1}
						,{x:'2016-05-04 18:00:00', y:396, t:128, p:1}
						,{x:'2016-05-04 19:00:00', y:396, t:128, p:1}
						,{x:'2016-05-04 20:00:00', y:396, t:128, p:1}
						,{x:'2016-05-04 21:00:00', y:396, t:128, p:1}
						,{x:'2016-05-04 22:00:00', y:396, t:128, p:1}
						,{x:'2016-05-04 23:00:00', y:396, t:128, p:1}
						,{x:'2016-05-04 24:00:00', y:396, t:128, p:1}
			];
			
			//	DAY 2/2	
			var temp_data_ParCount_day2 = [
						{color:"#000000"}
						,{x:'2016-05-04 01:30:00', y:0, t:153, p:1}
						,{x:'2016-05-04 02:30:00', y:50, t:153, p:1}
						,{x:'2016-05-04 03:30:00', y:250, t:153, p:1}
						,{x:'2016-05-04 04:30:00', y:600, t:115, p:1}
						,{x:'2016-05-04 05:30:00', y:1000, t:116, p:1}
						,{x:'2016-05-04 06:30:00', y:200, t:117, p:1}
						,{x:'2016-05-04 07:30:00', y:396, t:128, p:1}
						,{x:'2016-05-04 08:30:00', y:396, t:128, p:1}
						,{x:'2016-05-04 09:30:00', y:396, t:128, p:1}
						,{x:'2016-05-04 10:30:00', y:396, t:128, p:1}
						,{x:'2016-05-04 11:30:00', y:396, t:128, p:1}
						,{x:'2016-05-04 12:30:00', y:396, t:128, p:1}
						,{x:'2016-05-04 13:30:00', y:396, t:128, p:1}
						,{x:'2016-05-04 14:30:00', y:396, t:128, p:1}
						,{x:'2016-05-04 15:30:00', y:396, t:128, p:1}
						,{x:'2016-05-04 16:30:00', y:396, t:128, p:1}
						,{x:'2016-05-04 17:30:00', y:396, t:128, p:1}
						,{x:'2016-05-04 18:30:00', y:396, t:128, p:1}
						,{x:'2016-05-04 19:30:00', y:396, t:128, p:1}
						,{x:'2016-05-04 20:30:00', y:396, t:128, p:1}
						,{x:'2016-05-04 21:30:00', y:396, t:128, p:1}
						,{x:'2016-05-04 22:30:00', y:396, t:128, p:1}
						,{x:'2016-05-04 23:30:00', y:396, t:128, p:1}
						,{x:'2016-05-04 24:30:00', y:396, t:128, p:1}
					];
			
			//	WEEK 1/2
			var temp_data_ParCount_week1 = [
						{color:"#61982F"}
						,{x:'2016-03-04 02:00:00', y:0, t:153, p:3}
						,{x:'2016-03-05 20:00:00', y:50, t:153, p:3}
						,{x:'2016-03-06 02:00:00', y:250, t:153, p:3}
						,{x:'2016-03-07 02:00:00', y:100, t:121215, p:3}
						,{x:'2016-03-08 02:00:00', y:100, t:121216, p:3}
						,{x:'2016-03-09 02:00:00', y:200, t:121217, p:3}
						,{x:'2016-03-10 02:00:00', y:196, t:121218, p:3}
					];
			
			//	WEEK 2/2
			var temp_data_ParCount_week2 = [
						{color:"#000000"}
						,{x:'2016-03-04 02:00:00', y:0, t:153, p:3}
						,{x:'2016-03-05 04:00:00', y:50, t:153, p:3}
						,{x:'2016-03-06 04:30:00', y:250, t:153, p:3}
						,{x:'2016-03-07 05:10:00', y:100, t:121215, p:3}
						,{x:'2016-03-08 05:55:00', y:100, t:121216, p:3}
						,{x:'2016-03-09 06:05:00', y:200, t:121217, p:3}
						,{x:'2016-03-10 06:12:00', y:196, t:121218, p:3}
			];
			
			//	MONTH 1/2
			var temp_data_ParCount_month1 = [
						{color:"#61982F"}
						,{x:'2016-03-01 01:00:00', y:0, t:153, p:10}
						,{x:'2016-03-08 12:00:00', y:50, t:153, p:11}
						,{x:'2016-03-15 03:00:00', y:250, t:153, p:12}
						,{x:'2016-03-30 04:00:00', y:20, t:121, p:13}						
			];
			
			//	MONTH 2/2
			var temp_data_ParCount_month2 = [
						{color:"#000000"}
						,{x:'2016-03-01 01:30:00', y:0, t:153, p:10}
						,{x:'2016-03-08 12:30:00', y:50, t:153, p:11}
						,{x:'2016-03-15 03:30:00', y:250, t:153, p:12}
						,{x:'2016-03-30 04:30:00', y:20, t:121, p:13}	
			];
				


		}
		
		
		
})();
