(function() {
    'use strict';
    
    angular
        .module('motionIconsServices', ['ngResource'])
        
        .factory('motionIconsFactory', function($resource) {
            return $resource('../data/svg-icons.json', {}, {
                query: { 
                    method: 'GET',
                    cache: true
                }
            });
        });
})();