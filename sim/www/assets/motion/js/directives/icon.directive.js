(function () {
    'use strict';

    angular
        .module('motionIconsDirectives', [])
    
        .directive('mxiD', function () {
            return function (scope, element, attrs) {
                attrs.$observe('mxiD', function (value) {
                    element.attr('d', value);
                });
            };
        })
        .directive('mxiSvg', function() {
            return {
                restrict: 'EA',
                replace: true,
                templateUrl: 'data/svg.html'
            };
        })
        .directive('mxiIcon', function() {
            return {
                restrict: 'EA',
                replace: true,
                scope: {
                    icon: '@'
                },
                template: '<svg class="icon"><use xlink:href=""></use></svg>',
                compile: function compile(iElement, tAttrs) {
                    return {
                      pre: function (scope, iElement) {
                        iElement.find('use').attr('xlink:href', '#' + scope.icon);
                      }
                    };
                }
            };
        });
})();