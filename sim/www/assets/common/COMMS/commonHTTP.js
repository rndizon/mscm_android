/*
 ***************************************************************
 *                                                             *
 *                           NOTICE                            *
 *                                                             *
 *   THIS SOFTWARE IS THE PROPERTY OF AND CONTAINS             *
 *   CONFIDENTIAL INFORMATION OF INFOR AND/OR ITS		      *
 *   AFFILIATES OR SUBSIDIARIES AND SHALL NOT BE DISCLOSED     *
 *   WITHOUT PRIOR WRITTEN PERMISSION. LICENSED CUSTOMERS MAY  *
 *   COPY AND ADAPT THIS SOFTWARE FOR THEIR OWN USE IN         *
 *   ACCORDANCE WITH THE TERMS OF THEIR SOFTWARE LICENSE       *
 *   AGREEMENT. ALL OTHER RIGHTS RESERVED.                     *
 *                                                             *
 *   (c) COPYRIGHT 2012 INFOR.  ALL RIGHTS RESERVED.			  *
 *   THE WORD AND DESIGN MARKS SET FORTH HEREIN ARE            *
 *   TRADEMARKS AND/OR REGISTERED TRADEMARKS OF INFOR          *
 *   AND/OR ITS AFFILIATES AND SUBSIDIARIES. ALL			      *
 *   RIGHTS RESERVED.  ALL OTHER TRADEMARKS LISTED HEREIN ARE  *
 *   THE PROPERTY OF THEIR RESPECTIVE OWNERS.                  *
 *                                                             *
 ***************************************************************
 */
 
function AppVersionObject(productLine, systemCode, alertErrors, serverUrl, funcAfterCall, bUseSecurityPlugin) {
	productLine = (!productLine) ? "" : productLine.toUpperCase();
	systemCode = (!systemCode) ? "" : systemCode.toUpperCase();

	AppVersionObject._singleton = this;

	// public
	this.productLine = productLine;
	this.systemCode = systemCode;
	this.alertErrors = (typeof(alertErrors) == "boolean") ? alertErrors : true;
	this.serverUrl = serverUrl;
	this.funcAfterCall = funcAfterCall;
	this.useSecurityPlugin = (typeof (bUseSecurityPlugin) == "boolean") ? bUseSecurityPlugin : false;
	this.longAppVersion = null;
	this.appVersion = null;
	this.errorMessage = null;
	this.projDataObj = null;
	this.systemDataObj = null;
	this.lookUpProdLine = "";
	this.DATA_URL_900 = "/servlet/Router/Data/Erp";

	this.getAppVersionData();
}

AppVersionObject.prototype._singleton = null;
AppVersionObject.username = "";
AppVersionObject.password = "";

AppVersionObject.prototype.getAppVersionData = function () {
	var xmlhttp = new XMLHttpRequest();
		
	DataObject.username = AppVersionObject.username;
	DataObject.password = AppVersionObject.password;
	DataObject.DATA_URL_900 = this.serverUrl + this.DATA_URL_900;

	this.projDataObj = new DataObject(window, DataObject.TECHNOLOGY_900, xmlhttp, AppVersionObject.processProjectQueryResponse, this.useSecurityPlugin);
	this.projDataObj.setParameter("PROD", "GEN");
	this.projDataObj.setParameter("FILE", "PROJECT");
	this.projDataObj.setParameter("FIELD", "productline");
	this.projDataObj.setParameter("INDEX", "PRJSET2");
	this.projDataObj.setParameter("MAX", 1);
	this.projDataObj.setParameter("KEY", this.productLine);
	this.projDataObj.callData();
};

AppVersionObject.processProjectQueryResponse = function () {
	console.log("AppVersionObject.processProjectQueryResponse nbrRecs = " + AppVersionObject._singleton.projDataObj.getNbrRecs());

	AppVersionObject._singleton.lookUpProdLine = AppVersionObject._singleton.productLine;
	if (AppVersionObject._singleton.projDataObj.getNbrRecs() > 0) {
		AppVersionObject._singleton.lookUpProdLine = AppVersionObject._singleton.projDataObj.getRecord(0).getFieldValue('productline');
	}
	
	AppVersionObject._singleton.getSystemData();
};

AppVersionObject.prototype.getSystemData = function () {
	var xmlhttp = new XMLHttpRequest();

	DataObject.username = AppVersionObject.username;
	DataObject.password = AppVersionObject.password;
	DataObject.DATA_URL_900 = this.serverUrl + this.DATA_URL_900;

	this.systemDataObj = new DataObject(window, DataObject.TECHNOLOGY_900, xmlhttp, AppVersionObject.processSystemQueryResponse, this.useSecurityPlugin);
	this.systemDataObj.setParameter("PROD", "GEN");
	this.systemDataObj.setParameter("FILE", "SYSTEM");
	this.systemDataObj.setParameter("FIELD", "releaseinfo");
	this.systemDataObj.setParameter("MAX", 1);
	this.systemDataObj.setParameter("KEY", this.lookUpProdLine + "=" + this.systemCode);
	this.systemDataObj.callData();
};

AppVersionObject.processSystemQueryResponse = function () {
	var releaseNbr = "";
	var releaseTkns;
	
	if (AppVersionObject._singleton.systemDataObj.getNbrRecs() > 0) {
		// releaseNbr should be of the form: "Release x.x.x" or "Release x.x.x MSP#xx"
		releaseNbr = AppVersionObject._singleton.systemDataObj.getRecord(0).getFieldValue("releaseinfo");
		releaseTkns = releaseNbr.split(" ");
		if (releaseTkns.length >= 2) {
			var releaseStr = releaseTkns[1];
			// make sure we have 3 release digits
			var appParams = releaseStr.split(".");
			var nbrParams = appParams.length;
			if (nbrParams < 3) {
				for (var j = nbrParams; j < 3; j++) {
					appParams[j] = "0";
				}
			}
			releaseStr = appParams.join(".");
			AppVersionObject._singleton.appVersion = releaseStr;
			AppVersionObject._singleton.longAppVersion = releaseStr;
			// add the MSP level, if it exists
			if (releaseTkns.length >= 3 && releaseTkns[2].indexOf("MSP#") != -1) {
				AppVersionObject._singleton.longAppVersion += releaseTkns[2].replace("MSP#",".");
			} else {
				AppVersionObject._singleton.longAppVersion += ".00";
			}
			var appDigits = AppVersionObject._singleton.longAppVersion.split(".");
			var len;
			for (var i = 0; i < appDigits.length; i++) {
				len = appDigits[i].length; 
				if (len < 2) {
					for (var j = len; j < 2; j++) {
						appDigits[i] = "0" + appDigits[i];
					}	
				}	
			}
			len = appDigits.length - 1;
			AppVersionObject._singleton.longAppVersion = appDigits.join(".");
			AppVersionObject._singleton.appVersion = appDigits.slice(0, len).join(".");
		}
	} else {
		// some error occurred
		if (AppVersionObject._singleton.alertErrors) {
			util.alert('Error getting application version.');
		}
	}

	AppVersionObject._singleton.funcAfterCall();
};

