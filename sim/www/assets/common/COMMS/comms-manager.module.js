/***************************************************************
 *                                                             *
 *                           NOTICE                            * 
 *                                                             *
 *   THIS SOFTWARE IS THE PROPERTY OF AND CONTAINS             *
 *   CONFIDENTIAL INFORMATION OF LAWSON SOFTWARE AND/OR ITS    *
 *   AFFILIATES OR SUBSIDIARIES AND SHALL NOT BE DISCLOSED     *
 *   WITHOUT PRIOR WRITTEN PERMISSION. LICENSED CUSTOMERS MAY  *
 *   COPY AND ADAPT THIS SOFTWARE FOR THEIR OWN USE IN         *
 *   ACCORDANCE WITH THE TERMS OF THEIR SOFTWARE LICENSE       *
 *   AGREEMENT. ALL OTHER RIGHTS RESERVED.                     *
 *                                                             *
 *   (c) COPYRIGHT 2015 LAWSON SOFTWARE.  ALL RIGHTS RESERVED. *
 *   THE WORD AND DESIGN MARKS SET FORTH HEREIN ARE            *
 *   TRADEMARKS AND/OR REGISTERED TRADEMARKS OF LAWSON         *
 *   SOFTWARE AND/OR ITS AFFILIATES AND SUBSIDIARIES. ALL      *
 *   RIGHTS RESERVED.  ALL OTHER TRADEMARKS LISTED HEREIN ARE  *
 *   THE PROPERTY OF THEIR RESPECTIVE OWNERS.                  *
 *                                                             *
 ***************************************************************
 *                                                             *
 *  v1.0 - 01/28/2016 - Initial release                        *
 *  														   *
 *  InforCommsManager 										   *
 *                                                             *   
 ***************************************************************/

(function(){
	'use strict';
	
	angular
		.module('InforCommsManager', [])
		.factory("CM",Body);

		function  Body(SoapCall){

			var inforCommsManager = {
				doCommsCall: doCommsCall,
			}; 

			var /*mSoapUserName = 'u12665',
				mSoapPassword = 'lawson',
				mSoapBaseURL = "http://phman2k12:9080/axis/services/urn:",*/
				mSoapUserName = 'u12665',
				mSoapPassword = 'lawson',
				mSoapBaseURL = "http://172.30.83.34:9080/axis/services/urn:",
				mSoarpRequestToken = "1107:u19670";
			var mCallBack = null;	

			return inforCommsManager;

			/**
			*	Public Functions
			*/
			function doCommsCall(_commsCallType,_commsArgument){

				if(_commsCallType == "soap"){
					mCallBack = SoapCall.post(mSoapBaseURL+_commsArgument.URL,_commsArgument.Action,_commsArgument.Params);
				}

				return mCallBack;
			}
			

			/**
			*	Private Functions
			*/

		}


})();