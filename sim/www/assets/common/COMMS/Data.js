/*
***************************************************************
*                                                             *
*                           NOTICE                            * 
*                                                             *
*   THIS SOFTWARE IS THE PROPERTY OF AND CONTAINS             *
*   CONFIDENTIAL INFORMATION OF INFOR AND/OR ITS		      *
*   AFFILIATES OR SUBSIDIARIES AND SHALL NOT BE DISCLOSED     *
*   WITHOUT PRIOR WRITTEN PERMISSION. LICENSED CUSTOMERS MAY  *
*   COPY AND ADAPT THIS SOFTWARE FOR THEIR OWN USE IN         *
*   ACCORDANCE WITH THE TERMS OF THEIR SOFTWARE LICENSE       *
*   AGREEMENT. ALL OTHER RIGHTS RESERVED.                     *
*                                                             *
*   (c) COPYRIGHT 2012 INFOR.  ALL RIGHTS RESERVED.			  *
*   THE WORD AND DESIGN MARKS SET FORTH HEREIN ARE            *
*   TRADEMARKS AND/OR REGISTERED TRADEMARKS OF INFOR          *
*   AND/OR ITS AFFILIATES AND SUBSIDIARIES. ALL			      *
*   RIGHTS RESERVED.  ALL OTHER TRADEMARKS LISTED HEREIN ARE  *
*   THE PROPERTY OF THEIR RESPECTIVE OWNERS.                  *
*                                                             *
***************************************************************
*/

// Define global variables to JSLint.
// True indicates that the variable may be assigned to by this file.
// False indicates that the variable may not be assigned to by this file.
/*global window: false,
		 Connection: false,
		 DOMParser: false,
		 util: false,
		 RecordObject: false,
		 RelatedRecordsObject: false,
		 trxconnect: false
*/

//
//	DEPENDENCIES:
//		common.js
//-----------------------------------------------------------------------------
//
//	PARAMETERS:
//		mainWnd			(optional) location of the common.js file
//		techVersion		(optional) defaults to 9.0.0
//		httpRequest		(optional) reference to a request method (i.e. SSORequest, httpRequest, etc..)
//							if null, the call() method will always return null
//		funcAfterCall	(optional) function to run after the data call is executed
//-----------------------------------------------------------------------------
function DataObject(mainWnd, techVersion, httpRequest, funcAfterCall, bUseSecurityPlugin) {
	this.mainWnd = mainWnd || window;
	this.httpRequest = httpRequest || null;
	this.techVersion = techVersion || DataObject.TECHNOLOGY_900;
	this.funcAfterCall = funcAfterCall;
	this.isPost = true;
	this.showErrors = true;
	this.dom = null;	
	this.useSecurityPlugin = (typeof (bUseSecurityPlugin) == "boolean") ? bUseSecurityPlugin : false;
	this.hasError = false;

	// arrays for fields
	this.paramsAry = [];
	this.oneTimeParamsAry = [];

	// private fields (for DataObject only!)
	this.nbrRecs = null;
	this.prevCall = null;
	this.nextCall = null;
	this.reloadCall = null;
	this.columns = [];
	this.records = [];
	this.drillCall = null;
	this.isPrev = false;
	this.isNext = false;	
}
//-- static variables ---------------------------------------------------------
DataObject.TECHNOLOGY_803 = "8.0.3";
DataObject.TECHNOLOGY_900 = "9.0.0";
DataObject.DATA_URL_803 = "/servlet/dme";
DataObject.DATA_URL_900 = "/servlet/Router/Data/Erp";
//-----------------------------------------------------------------------------
DataObject.prototype.setParameter = function (name, value, oneTimeOnly) {
	if (oneTimeOnly) {
		this.oneTimeParamsAry[name] = value;
	} else {
		this.paramsAry[name] = value;
	}
};
//-----------------------------------------------------------------------------
DataObject.prototype.removeParameter = function (name, oneTimeOnly) {
	if (oneTimeOnly) {
		this.setParameter(name, null, oneTimeOnly);
	} else {
		this.setParameter(name, null);
	}
};
//-----------------------------------------------------------------------------
DataObject.prototype.resetPrivateVars = function () {
	this.nbrRecs = null;
	this.prevCall = null;
	this.nextCall = null;
	this.reloadCall = null;
	this.columns = [];
	this.records = [];
	this.hasError = false;
};
//-----------------------------------------------------------------------------
DataObject.prototype.getParameter = function (name, oneTimeOnly) {
	if (oneTimeOnly) {
		return (this.oneTimeParamsAry[name] || null);
	} else {
		return (this.paramsAry[name] || null);
	}
};
//-----------------------------------------------------------------------------
DataObject.prototype.getUrl = function () {
	var url = (this.techVersion == DataObject.TECHNOLOGY_900)
			? DataObject.DATA_URL_900
			: DataObject.DATA_URL_803;
	return (url + "?" + this.getQuery());
};
//-----------------------------------------------------------------------------
DataObject.prototype.getQuery = function () {
	var query = "";
	var val;
	if (this.techVersion == DataObject.TECHNOLOGY_803) {
		query += "OUT=XML&";
	}

	for (var i in this.paramsAry) {
		val = this.paramsAry[i];
		if (val == null) {
			continue;
		}

		// if it is a GET or contains a & or %, it needs to be escaped
		val = String(val);
		if (!this.isPost || val.indexOf("&") != -1 || val.indexOf("%") != -1 || val.indexOf("+") != -1
				|| val.indexOf("|") || val.indexOf("^")) {
			//val = this.mainWnd.cmnEscapeIt(val);
			
			
			
			val = escape("" + val);

			// this step must be AFTER the javascript escape
			// manually escape the + signs... javascript escape won't do this
			if (val.indexOf("+") != -1)
				val = val.split("+").join("%2B");
			
		}
		query += i + "=" + val + "&";
	}
	
	// one time manual parameters, which are assumed to be escaped
	for (var j in this.oneTimeParamsAry) {
		val = this.oneTimeParamsAry[j];
		if (val == null) {
			continue;
		}

		query += j + "=" + val + "&";
	}
	this.oneTimeParamsAry = [];

	// remove the ending "&" and return it
	return query.substring(0, query.length - 1);
};

//-----------------------------------------------------------------------------
DataObject.prototype.hasError = function () {	
	return this.hasError;
};
//-----------------------------------------------------------------------------
DataObject.username;
DataObject.password;
DataObject.prototype.callData = function (addToUrl) {
	var query;
	
    if (arguments.length > 0 && !addToUrl) {
        return null;
    }

	if (!this.httpRequest) {
		return null;
	}

	this.resetPrivateVars();

	var callingFunction = this;
	if (typeof navigator.network === 'undefined' || navigator.network.connection.type != Connection.NONE) {
		var parseResponse = function (responseText) {
			var parser = new DOMParser();
			var responseXML = parser.parseFromString(responseText, "text/xml");
			callingFunction.dom = responseXML;
			//callingFunction.dom = this.responseXML;
			if (responseXML && responseXML.documentElement.nodeName == "ERROR") {
				var details = responseXML.documentElement.firstChild.nextSibling.firstChild.nodeValue;
				if (details.length > 1024) {
					details = details.substring(0, 1024) + "...";
				}
				var msg = responseXML.documentElement.firstChild.firstChild.nodeValue
					+ "\n\n" + callingFunction.getUrl() + "  -  callData() - Data.js"
					+ "\n\n" + details;
				console.log(msg);
				util.alert(msg, 
					function () {
						if (callingFunction.funcAfterCall) {
							callingFunction.funcAfterCall();
						}
					});
				return;
			}
			if (callingFunction.funcAfterCall) {
				callingFunction.funcAfterCall();
			}
		};
		var handleError = function (e) {
		  this.hasError = true;
			var msg = 'There was a problem connecting to the server. Please verify your server status and host and port settings.\n';
			console.log(msg + e.error);
			// JI-MA-198- Handled undefined string on no wifi error
			util.alert(msg + (e.error == undefined ? "": e.error), 
				function () {
					if (callingFunction.funcAfterCall) {
						callingFunction.funcAfterCall();
					}
				});
			return;
		};
		var timestamp = new Date().getTime();
		// Append a query string parameter, "_=[TIMESTAMP]", to the URL to fix client caching problem.
		query = this.getUrl() + (addToUrl || "") + "&_=" + timestamp;
		console.log("DataObject query=" + query);

		if (callingFunction.useSecurityPlugin == true) {
			
			console.log("callingFunction.useSecurityPlugin == true1 ");
			
			var xhr = trxconnect.request({
				url: query,
				methodType: 'GET',
				contentType: 'text/plain',
				cache: false,
				success: function (responseText) {
					
					//alert(responseText);
					//console.log("Ito and response "+ responseText);
					
					parseResponse(responseText);
				},
				fail: function (e) {
					console.log("error");
					return handleError(e);
				}
			});
		} else {
			
			console.log("callingFunction.useSecurityPlugin == false2 ");
			
			this.httpRequest.onload = function () {
				console.log(this.responseText);
				parseResponse(this.responseText);
			};
			this.httpRequest.onerror = function (e) {
				return handleError(e);
			};		
			this.httpRequest.open('GET', query);
			this.httpRequest.setRequestHeader('Authorization', 'Basic ' + window.btoa(DataObject.username + ':' + DataObject.password));
			this.httpRequest.setRequestHeader('Cache-Control', 'no-cache');
			this.httpRequest.setRequestHeader('Pragma', 'no-cache');
			this.httpRequest.send();
		}
	} else {
	  this.hasError = true;
		console.log('This application requires an internet connection.  Please check your connection and try again.');
		util.alert('This application requires an internet connection.  Please check your connection and try again.',
			function () {
				if (callingFunction.funcAfterCall) {
					callingFunction.funcAfterCall();
				}
			});
		return null;
	}
	return null;

	if (this.isPost) {
		var url = (this.techVersion == DataObject.TECHNOLOGY_900)
				? DataObject.DATA_URL_900
				: DataObject.DATA_URL_803;
		query = this.getQuery() + (addToUrl || "");
		this.dom = this.httpRequest(url, query, "application/x-www-form-urlencoded", null, this.showErrors);
	} else {
		this.dom = this.httpRequest(this.getUrl() + (addToUrl || ""), null, null, null, this.showErrors);
	}

	if (this.funcAfterCall) {
		this.funcAfterCall();
	}

	return this.dom;
};
//-----------------------------------------------------------------------------
DataObject.prototype.isNextCall = function () {	
	return this.isNext;
};
//-----------------------------------------------------------------------------
DataObject.prototype.isPrevCall = function () {	
	return this.isPrev;
};
//-----------------------------------------------------------------------------
DataObject.prototype.callNext = function () {	
	this.isPrev = false;
	this.isNext = true;
	return (this.getNextCall()) ? this.callData(this.getNextCall()) : null;
};
//-----------------------------------------------------------------------------
DataObject.prototype.callPrev = function () {	
	this.isPrev = true;
	this.isNext = false;
	return (this.getPrevCall()) ? this.callData(this.getPrevCall()) : null;
};
//-----------------------------------------------------------------------------
DataObject.prototype.reload = function () {
	this.isPrev = false;
	this.isNext = false;
	return (this.getReloadCall()) ? this.callData(this.getReloadCall()) : null;
};
//-----------------------------------------------------------------------------
DataObject.prototype.getNbrRecs = function () {
	if (this.nbrRecs == null && this.dom) {
		this.nbrRecs = this.dom.getElementsByTagName("RECORD").length;
	}
	return this.nbrRecs;
};
//-----------------------------------------------------------------------------
DataObject.prototype.getColumnIndex = function (name) {
	if (!name) {
		return null;
	}

	// set up the columns array
	if (this.columns.length == 0 && this.dom) {
		this.getColumnName(0);
	}

	var idx = this.columns[name.toUpperCase()];
	if (typeof (idx) == "undefined") {
		return null;
	} else {
		return idx;
	}
};
//-----------------------------------------------------------------------------
DataObject.prototype.getColumnName = function (idx) {
	if (typeof (idx) != "number") {
		return null;
	}

	if (this.columns.length == 0 && this.dom) {
		var columnNodes = this.dom.getElementsByTagName("COLUMN");
		for (var i = 0; i < columnNodes.length; i++) {
			// index the columns array by number and string
			var fldName = columnNodes[i].getAttribute("header");
			if (fldName != null) {
				fldName = fldName.split("_").join("-").toUpperCase();
				this.columns[i] = fldName;
				this.columns[fldName] = i;
			}
		}
	}

	if (idx < 0 || idx > this.columns.length - 1) {
		return null;
	} else {
		return this.columns[idx];
	}
};
//-----------------------------------------------------------------------------
DataObject.prototype.getRecord = function (idx) {
	if (typeof (idx) != "number") {
		return null;
	}

	if (this.records.length == 0 && this.dom) {
		var recordNodes = this.dom.getElementsByTagName("RECORD");
		for (var i = 0; i < recordNodes.length; i++) {
			this.records[i] = new RecordObject(this, recordNodes[i]);
		}
	}

	if (idx < 0 || idx > this.records.length - 1) {
		return null;
	} else {
		return this.records[idx];
	}
};
//-----------------------------------------------------------------------------
DataObject.prototype.getNextCall = function () {
	if (this.nextCall == null && this.dom) {
		// loop backwards through just the children of the top node
		// NEXTCALL is always towards the end
		for (var i = this.dom.documentElement.childNodes.length - 1; i >= 0; i--) {
			var node = this.dom.documentElement.childNodes[i];
			if (node.nodeName.toUpperCase() == "NEXTCALL") {
				this.nextCall = this.mainWnd.cmnGetNodeCDataValue(node);

				// add a prepending "&"
				if (this.nextCall.substring(0, 1) != "&") {
					this.nextCall = "&" + this.nextCall;
				}
	
				// remove the ending "&"
				if (this.nextCall.substring(this.nextCall.length - 1) == "&") {
					this.nextCall = this.nextCall.substring(0, this.nextCall.length - 1);
				}

				break;
			}
		}
	}
	return this.nextCall;
};
//-----------------------------------------------------------------------------
DataObject.prototype.getPrevCall = function () {
	if (this.prevCall == null && this.dom) {
		// loop backwards through just the children of the top node
		// PREVCALL is always towards the end
		for (var i = this.dom.documentElement.childNodes.length - 1; i >= 0; i--) {
			var node = this.dom.documentElement.childNodes[i];
			if (node.nodeName.toUpperCase() == "PREVCALL") {
				this.prevCall = this.mainWnd.cmnGetNodeCDataValue(node);

				// add a prepending "&"
				if (this.prevCall.substring(0, 1) != "&") {
					this.prevCall = "&" + this.prevCall;
				}

				// remove the ending & for 8.0.3 technology
				if (this.prevCall.substring(this.prevCall.length - 1) == "&") {
					this.prevCall = this.prevCall.substring(0, this.prevCall.length - 1);
				}

				break;
			}
		}
	}
	return this.prevCall;
};
//-----------------------------------------------------------------------------
DataObject.prototype.getReloadCall = function () {
	if (this.reloadCall == null && this.dom) {
		var reloadNodes = this.dom.getElementsByTagName("RELOAD");
		if (reloadNodes.length > 0) {
			this.reloadCall = this.mainWnd.cmnGetNodeCDataValue(reloadNodes[0]);

			// add a prepending "&"
			if (this.reloadCall.substring(0, 1) != "&") {
				this.reloadCall = "&" + this.reloadCall;
			}

			// remove the ending & for 8.0.3 technology
			if (this.reloadCall.substring(this.reloadCall.length - 1) == "&") {
				this.reloadCall = this.reloadCall.substring(0, this.reloadCall.length - 1);
			}
		}
	}
	return this.reloadCall;
};
//-----------------------------------------------------------------------------
DataObject.prototype.getDrillCall = function () {
	if (this.drillCall == null && this.dom) {
		var drillNodes = this.dom.getElementsByTagName("IDABASE");
		if (drillNodes.length > 0) {
			var drillNode = drillNodes[0];
			this.drillCall = drillNode.getAttribute("executable") + "?" + this.mainWnd.cmnGetNodeCDataValue(drillNodes[0]);
		}
	}
	return this.drillCall;
};
//-- end data object code
//-----------------------------------------------------------------------------



//-----------------------------------------------------------------------------
//-- start record object code
function RecordObject(dataObj, recordNode) {
	this.dataObj = dataObj;
	this.recordNode = recordNode;
	this.colNodes = null;
	this.nbrRelRecs = null;
	this.relRecsAry = null;
	this.drillUrl = null;
}
//-----------------------------------------------------------------------------
RecordObject.prototype.getNbrRelRecs = function () {
	if (this.nbrRelRecs == null) {
		this.nbrRelRecs = this.recordNode.getElementsByTagName("RELRECS").length;
	}
	return this.nbrRelRecs;
};
//-----------------------------------------------------------------------------
RecordObject.prototype.getRelatedRecordsName = function (idx) {
	if (typeof (idx) != "number" || this.getNbrRelRecs() == 0) {
		return null;
	}

	if (this.relRecsAry == null) {
		this.relRecsAry = [];
		var relRecsNodes = this.recordNode.getElementsByTagName("RELRECS");
		for (var i = 0; i < relRecsNodes.length; i++) {
			this.relRecsAry[i] = new RelatedRecordsObject(this, relRecsNodes[i]);
		}
	}

	if (idx < 0 || idx > this.relRecsAry.length - 1) {
		return null;
	}

	return this.relRecsAry[idx].getName();
};
//-----------------------------------------------------------------------------
RecordObject.prototype.getRelatedRecords = function (strOrNbr) {
	if ((typeof (strOrNbr) != "string" && typeof (strOrNbr) != "number") || this.getNbrRelRecs() == 0) {
		return null;
	}

	// make sure the relRecsAry is populated!
	this.getRelatedRecordsName(-1);

	var nbr = -1;
	if (typeof (strOrNbr) == "string") {
		// find out if a name matches
		for (var i = 0; i < this.getNbrRelRecs(); i++) {
			if (strOrNbr.toUpperCase() == this.relRecsAry[i].getName()) {
				nbr = i;
				break;
			}
		}
	} else {
		nbr = strOrNbr;
	}

	if (nbr < 0 || nbr > this.relRecsAry.length - 1) {
		return null;
	}

	return this.relRecsAry[nbr];
};
//-----------------------------------------------------------------------------
RecordObject.prototype.getIndex = function () {
	for (var i = 0; i < this.dataObj.getNbrRecs(); i++) {
		if (this.dataObj.records[i] == this) {
			return i;
		}
	}
	return null;
};
//-----------------------------------------------------------------------------
RecordObject.prototype.getColValue = function (idx) {
	if (typeof (idx) != "number") {
		return null;
	}

	var cols = this.getColNodes();
	if (idx < 0 || idx > cols.length - 1) {
		return null;
	}

	var occNodes = cols[idx].getElementsByTagName("OCC");
	if (occNodes.length > 0) {
		var occAry = [];
		for (var i = 0; i < occNodes.length; i++) {
			occAry[i] = this.dataObj.mainWnd.cmnGetNodeCDataValue(occNodes[i]);
		}
		return occAry;
	} else {
		return this.dataObj.mainWnd.cmnGetNodeCDataValue(cols[idx]);
	}
};
//-----------------------------------------------------------------------------
RecordObject.prototype.getColNode = function (idx) {
	if (typeof (idx) != "number") {
		return null;
	}

	var cols = this.getColNodes();
	if (idx < 0 || idx > cols.length - 1) {
		return null;
	}

	return cols[idx];
};
//-----------------------------------------------------------------------------
RecordObject.prototype.getFieldValue = function (fieldName) {
	return this.getColValue(this.dataObj.getColumnIndex(fieldName));
};
//-----------------------------------------------------------------------------
RecordObject.prototype.getColNodes = function () {
	if (this.colNodes == null) {
		this.colNodes = this.recordNode.getElementsByTagName("COL");
	}
	return this.colNodes;
};
//-----------------------------------------------------------------------------
RecordObject.prototype.getDrillUrl = function () {
	if (this.drillUrl == null && this.dataObj) {
		if (this.dataObj.drillCall == null) {
			this.dataObj.getDrillCall();
		}
		var drillNodes = this.recordNode.getElementsByTagName("IDACALL");
		if (drillNodes.length > 0) {
			this.drillUrl = this.dataObj.drillCall + this.dataObj.mainWnd.cmnGetNodeCDataValue(drillNodes[0]);
		}
	}
	return this.drillUrl;
};
//-- end record object code
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//-- start related records object code
function RelatedRecordsObject(recordObj, relRecsNode) {
	this.recordObj = recordObj;
	this.dataObj = recordObj.dataObj;
	this.relRecsNode = relRecsNode;
	this.name = null;
	this.nbrRecs = null;
	this.columns = [];
	this.nextCall = null;
}
//-----------------------------------------------------------------------------
RelatedRecordsObject.prototype.getName = function () {
	if (this.name == null && this.relRecsNode) {
		this.name = this.relRecsNode.getAttribute("name");
		if (this.name != null) {
			this.name = this.name.split("_").join("-").toUpperCase();
		}
	}
	return this.name;
};
//-----------------------------------------------------------------------------
RelatedRecordsObject.prototype.getNbrRecs = function () {
	if (this.nbrRecs == null && this.relRecsNode) {
		this.nbrRecs = this.relRecsNode.getElementsByTagName("RELREC").length;
	}
	return this.nbrRecs;
};
//-----------------------------------------------------------------------------
RelatedRecordsObject.prototype.getNbrColumns = function () {
	// set up the columns array first!
	if (this.columns.length == 0) {
		this.getColumnName(0);
	}
	return this.columns.length;
};
//-----------------------------------------------------------------------------
RelatedRecordsObject.prototype.getColumnIndex = function (name) {
	if (!name) {
		return null;
	}

	// set up the columns array first!
	if (this.columns.length == 0) {
		this.getColumnName(0);
	}

	var idx = this.columns[name.toUpperCase()];
	if (typeof (idx) == "undefined") {
		return null;
	} else {
		return idx;
	}
};
//-----------------------------------------------------------------------------
RelatedRecordsObject.prototype.getColumnName = function (idx) {
	if (typeof (idx) != "number") {
		return null;
	}

	if (this.columns.length == 0) {
		// make sure the columns array on the dataObj is ready!
		if (this.dataObj.columns.length == 0) {
			this.dataObj.getColumnName(0);
		}

		var len = this.dataObj.columns.length;
		for (var i = 0; i < len; i++) {
			// index the relColumns array by number and string
			var colName = this.dataObj.getColumnName(i);
			if (colName != null && colName.indexOf(this.getName() + ".") == 0) {
				var colIdx = this.columns.length;
				this.columns[colIdx] = colName;
				this.columns[colName] = colIdx;
			}
		}
	}

	if (idx < 0 || idx > this.columns.length - 1) {
		return null;
	} else {
		return this.columns[idx];
	}
};
//-----------------------------------------------------------------------------
RelatedRecordsObject.prototype.getValue = function (recIdx, colIdxOrName) {
	if (typeof (recIdx) != "number" || recIdx < 0 || recIdx > this.getNbrRecs() - 1) {
		return null;
	}

	var colIdx = -1;
	if (typeof (colIdxOrName) == "string") {
		colIdx = this.getColumnIndex(colIdxOrName);
	} else if (typeof (colIdxOrName) == "number") {
		colIdx = colIdxOrName;
	}

	if (colIdx < 0 || colIdx > this.getNbrColumns() - 1) {
		return null;
	}

	try {
		var relRecNodes = this.relRecsNode.getElementsByTagName("RELREC");
		var colNodes = relRecNodes[recIdx].getElementsByTagName("COL");
		return this.dataObj.mainWnd.cmnGetNodeCDataValue(colNodes[colIdx]);
	} catch (e) {
		return null;
	}
};
//-----------------------------------------------------------------------------
RelatedRecordsObject.prototype.getNextCall = function () {
	if (this.nextCall == null && this.relRecsNode) {
		var nextCallNodes = this.relRecsNode.getElementsByTagName("NEXTCALL");
		if (nextCallNodes.length > 0) {
			this.nextCall = this.dataObj.mainWnd.cmnGetNodeCDataValue(nextCallNodes[0]);
		}
	}
	return this.nextCall;
};
//-----------------------------------------------------------------------------
RelatedRecordsObject.prototype.getDataObjForNextCall = function () {
	if (!this.getNextCall()) {
		return null;
	}

	var relDataObj = new DataObject(this.dataObj.mainWnd, this.dataObj.techVersion, this.dataObj.httpRequest, null);
	relDataObj.showErrors = this.dataObj.showErrors;	
	var nextAry = this.getNextCall().split("&");
	for (var i = 0; i < nextAry.length; i++) {
		var nAry = nextAry[i].split("=");
		var oneTimeParam = (nAry.length > 0 && (nAry[0] == "PREV" || nAry[0] == "BEGIN")) ? true : false;	 
		switch (nAry.length) {
		case 0:
			break;
		case 1:
			relDataObj.setParameter(nAry[0], "", oneTimeParam);
			break;
		case 2:
			relDataObj.setParameter(nAry[0], nAry[1], oneTimeParam);
			break;				
		default:
			relDataObj.setParameter(nAry[0], nextAry[i].substring(nextAry[i].indexOf("=") + 1), oneTimeParam);
			break;
		}
	}
	return relDataObj;
};
//-- end related records object code
//-----------------------------------------------------------------------------
