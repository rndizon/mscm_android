/*
	TODO
	- set hours, weeks, days date range (In init and update)
	- round up/down so 13:15:00 will show at 13:00:00

*/

//https://github.com/mbostock/d3/wiki/Time-Formatting
//============================
//	CONFIG
//============================
//	format of date INPUT
var chartDateInput = "%Y-%m-%d %H:%M:%S";
//	format of date OUTPUT
var chartDateOutput = "%I %p";
var chartDateOutputweek = "%a";
var chartDateOutputmonth = "Week %W";
//	set chart margins
var chartmargin = {top:10,left:50,right:30,bottom:60};
//	set chart legend margin
var chartlegendmargin = {top: 0,left: 0, right: 0, bottom: 0};
//	show interactive guidelines (hover and press)
var chartUseGuidelines = false;
//	show Y Axis labels
var chartshowYAxis = true;
//	show X Axis labels
var chartshowXAxis = true;
//	show chart legend labels
var chartshowLegend = false;
//	set chart line inner area background
var chartIsArea = false;

var chartUseVoronoi = true;
				
//	Y AXIS	:	AMOUNT
var chartYaxislabeldistance = 5;
var chartYaxistickpadding = 10;
var chartYaxisticks = 5;
var chartYaxisLabel = '';//'Amount';
					
//	X AXIS	:	DATE
var chartXaxislabeldistance = 5;
var chartXaxistickpadding = 10;
var chartXaxisticks = 5;
var chartXaxisLabel = '';//'Date';				


(function(){
 	'use strict';

	angular
		.module("app")
		.factory('d3Init', Body);
		
		function Body($ionicPlatform, $timeout){			
			var service = {
		        getd3init:getd3init,
				d3update:d3update,
		    }			
			return service;
			
			//=====================================
			//	CONSOLE LOGGING
			//=====================================
			function d3log(messa){
				console.log(messa);
			}
			
			function d3dirlog(messa){				
				console.dir(messa);
			}
			
			//=====================================
			//	GET MAX Y VALUE
			//=====================================
			function GetDataValuesYMax(datavalues){
				var yarr = [];
				if (datavalues instanceof Array) {
					//d3log('datavalues is array');
				}else{
					//d3log('datavalues is NOT array');
				}
				for (var i=0;i<datavalues.length;i++){
					var gety = datavalues[i].y;
					//d3log('gety :' + gety);
					yarr.push(gety);
				}
				var largest = Math.max.apply(Math, yarr);
				/*var max = Math.max.apply(Math, yarr.map(function(i) {
					var largest = i[0];
				}));​*/
				//d3log(largest);
				return largest;
			}
			
			//=====================================
			//	GET ALL DATES
			//=====================================
			function GetDataDates(datavalues){
				var yarr = [];
				if (datavalues instanceof Array) {
					//d3log('datavalues is array');
				}else{
					//d3log('datavalues is NOT array');
				}
				for (var i=0;i<datavalues.length;i++){
					var gety = datavalues[i].x;
					//d3log('gety :' + gety);
					if (gety!='' && gety!=null && gety!=undefined){
						yarr.push(new Date(gety))
						//yarr.push(gety);
					}					
				}				
				return yarr;
			}
			
			//=====================================
			//	GET TOTAL T SUM
			//=====================================
			function GetDataValuesTTotal(datavalues){
				var yarr = [];
				if (datavalues instanceof Array) {
					//d3log('datavalues is array');
				}else{
					//d3log('datavalues is NOT array');
				}
				var totalsum = 0;				
				for (var i=0;i<datavalues.length;i++){
					var gety = datavalues[i].t;
					//d3log('get y : ' + gety);
					totalsum+=gety;
				}				
				return totalsum;
			}
			
			//=====================================
			//	CONVERT Minutes to hh:mm
			//=====================================
			function ConvertMinToHours(minin){
				var hours = Math.floor(minin / 60);          
				var minutes = minin % 60;
				return hours+':'+minutes+' hrs';
				//return Math.round(minin/60);
			}
			
			function getMonday(d) {
				d = new Date(d);
				var day = d.getDay(),
				diff = d.getDate() - day + (day == 0 ? -7:1); // adjust when day is sunday
				return new Date(d.setDate(diff));
			}
			
			//	NOT WORKING
			function getMidWeekday(d) {
				d = new Date(d);
				var day = d.getDay(),
				diff = d.getDate() - day + (day == 0 ? -4:1); // adjust when day is sunday
				return new Date(d.setDate(diff));
			}
			
			//=====================================
			//	CHART INIT
			//=====================================
			function getd3init(setchartid, datavalues){
					//	set chart svg id
					var chartIdsvg = setchartid + ' svg';					
					var totalarrays = datavalues.length;
					var totaltimetotal = 0;
					var getdata = [];
					var getmaxyarr = [];
					//var alldates = [];
					
					for (var i=0;i<datavalues.length;i++){
						//	GET COLOR IF SET
						//var thisarr = datavalues[i];
						var setcolor = datavalues[i][0].color;
						var thisdataarr = datavalues[i].slice(0);
						//	REMOVE COLOR
						thisdataarr.splice(0,1);						
						if (setcolor){
							// REMOVE COLOR
							//var getarr = datavalues[i];
							//thisdataarr = thisdataarr.slice(0);
							//getarr.splice(0,1);
							// SET DATA ARRAY
							getdata.push({values: thisdataarr,key:' Title here '+(i+1),color: setcolor});							
						}else{
							//d3log('setcolor : '+setcolor);
							//setcolor = '#61982F';
							setcolor = 'red';
							// SET DATA ARRAY
							getdata.push({values: thisdataarr,key:' Title here '+(i+1),color: setcolor});
						}
						//	GET MAX Y VALUE AND ADD TO ARRAY
						var getcurmax = GetDataValuesYMax(thisdataarr);
						getmaxyarr.push({y:getcurmax});
						
						//	CALC TOTAL TIME
						var gettt = GetDataValuesTTotal(thisdataarr);
						//d3log('gettt : ' + gettt);
						totaltimetotal+=parseInt(gettt);
						
						//	GET ALL DATES FROM EACH ARRAY AND ADD TO TOTAL DATES ARRAY
						//var thisalldates = GetDataDates(datavalues[i]);						
						//alldates = alldates.concat(thisalldates);
					}					
					//d3dirlog(alldates);
					
					//d3dirlog(getmaxyarr);
					//var asdsa = GetDataValuesYMax(getmaxyarr);
					//d3log('WTF : ' + asdsa);
					//	CALC MAX Y Value and add 10% to it for the top
					var getmaxy = GetDataValuesYMax(getmaxyarr);
					var getytoadd = (getmaxy/10);
					//var getytoadd = 0;
					//d3log('getytoadd : ' + getytoadd);


					// GET TOTAL TIME AND SET
					//d3log('totaltimetotal : ' + totaltimetotal);
					d3.select(setchartid+'TotalTime').text(ConvertMinToHours(totaltimetotal));
					
					//======================================
					//	CHART SETUP
					//======================================
					var chart = nv.models.lineChart()
						.x(function(d){
							//return d;
							return new Date(d.x);
						})
						.useInteractiveGuideline(chartUseGuidelines)
						.useVoronoi(chartUseVoronoi)						
						.isArea(chartIsArea)						
						.showYAxis(chartshowYAxis)
						.showXAxis(chartshowXAxis)
						.showLegend(chartshowLegend)
						//	GET MAX Y VALUE AND ADD 100
						.yDomain([0,getmaxy+getytoadd])
						.margin(chartmargin)
						.clipEdge(false)
						
						;
						
					//https://nvd3-community.github.io/nvd3/examples/documentation.html#lineChart
					//chart.showVoronoi(true);
					
					//======================================
					//	LINE
					//======================================
					chart.lines.interactive(true);
		
					//======================================
					//	LEGEND
					//======================================
					chart.legend.margin(chartlegendmargin);

					//======================================
					// BOTTOM DATE
					//======================================					
					//	GET ALL DATES FROM EACH ARRAY TO GET MIN AND MAX
					//var alldates = GetDataDates(datavalues[0]);
					//var maxDate = new Date(Math.max.apply(null,alldates));
					//d3log('maxDate : ' + maxDate);
					//var minDate = new Date(Math.min.apply(null,alldates));
					//d3log('minDate : ' + minDate);
					//d3dirlog(alldates);
					//var nowdate = new Date("YYYY-MM-DD HH:MM:SS");
					//https://github.com/mbostock/d3/wiki/Time-Intervals#hour
					
					/*var nowdate = new Date();
					d3log('nowdate : ' + nowdate);
					var starthourtemp = nowdate.getYear() + '/' + (nowdate.getMonth()+1) + '/' + nowdate.getDay() + ' 00:00:00';
					d3log('starthourtemp : ' + starthourtemp);
					var starthour = new Date(starthourtemp);
					var endhourhourtemp = nowdate.getYear() + '/' + (nowdate.getMonth()+1) + '/' + nowdate.getDay() + ' 23:59:59';
					d3log('endhourhourtemp : ' + endhourhourtemp);
					var endhour = new Date(endhourhourtemp);
					d3log('starthour : ' + starthour);
					d3log('endhour : ' + endhour);*/
					//chart.reduceXTicks(true);
					
					//d3dirlog(DayHourListArr);
					
					chart.xAxis
					.showMaxMin(true)
					.axisLabel(chartXaxisLabel)					
					//.tickFormat(function(d) { return (d) + ' months';})
					//.tickFormat(function(d, i) { return (i++) + ' months';})					
					.tickFormat(function(d) {
						if (d == null) {
							return 'N/A';
						}
						//return d;
						return d3.time.format(chartDateOutput)(new Date(d));
						//return d3.time.format('%H')(new Date(d));
						//return d3.time.format("%H");
					})
					.ticks(12)
					//.tickValues(DayHourListArr)
					//.tickValues(d3.time.hour.range(0,23,1))
					//.tickValues(alldates)
					//.tickValues(d3.time.hours(starthour,endhour,3))										
					//.tickValues(new Date(),new Date(), 1)					
					//.tickValues(DayHourListArr)
					
					/*
					http://stackoverflow.com/questions/21316617/d3-js-nvd3-date-on-x-axis-only-some-dates-are-show
					*/					
					
					.axisLabelDistance(chartXaxislabeldistance)
					.tickPadding(chartXaxistickpadding)					
					.staggerLabels(true)
					.rotateLabels(-60)
					;
					
					//chart.forceX(DayHourListArr);
					
					//======================================
					//	X SCALING
					//======================================
					//	fixes misalignment of timescale with line graph					
					//chart.xScale(d3.time.scale.utc());
					chart.xScale(d3.time.scale());
					/*chart.xScale(d3.time.scale()
					.domain([new Date, new Date])
					.nice(d3.time.hour)
					.range([0, width - 50]));*/
		
					//======================================
					//	LEFT AMOUNT
					//======================================
					chart.yAxis
					//.highlightZero(true)
					.axisLabel(chartYaxisLabel)
					.tickFormat(d3.format('.00f'))
					.axisLabelDistance(chartYaxislabeldistance)
					.tickPadding(chartYaxistickpadding)
					.showMaxMin(true)
					.staggerLabels(true)
					.ticks(chartYaxisticks)
					;
		
					//======================================
					//	Y SCALING
					//======================================					
					//chart.yScale(d3.scale.linear());

					//	CALC WIDTH/HEIGHT
					var width = parseInt(d3.select(setchartid).style('width'), 10);
					var height = parseInt(d3.select(setchartid).style('height'), 10);					
		
					//	CALL CHART (BUILD)
					d3.select(chartIdsvg)
					.datum(getdata)
					//.transition()
					//.duration(500)
					.call(chart);					
					
					//	LINE ANIMATION
					d3.selectAll('path.nv-line').each(function(){
						d3.select(d3.select(this).node().parentNode)
							.append('rect')
							.attr('width','100%')
							.attr('height',(height-(chartmargin.top+chartmargin.bottom+chartXaxislabeldistance+chartXaxistickpadding)))
							//.attr('height','100%')
							.attr('x','0')
							.attr('y','0')
							//.attr('width',width)
							//.attr('height',(height-(margin.top+margin.bottom+40)))
							.attr('class','linemasker')
							;
					});
		
					//	ADD POINT LABELS ONLY IF 1 LINE
					if (totalarrays==1){
						$timeout(function(){
							//SetChartsFloatingLabels(chart, false);
						},200);	
					}					
		
					//	EVENT : UPDATE (RESIZE)
					nv.utils.windowResize(function(){
						chart.update();
						//SetChartsFloatingLabels(chart, true);
					});
					return chart;
			}
			
			//=====================================
			//	UPDATE
			//=====================================
			function d3update(chart, setchartid, newdatavalues, xtype){
				d3log('D3 Update run');
				d3log('setchartid : ' + setchartid);
				//d3dirlog(newdatavalues);
				//	set chart svg id
				var chartIdsvg = setchartid + ' svg';					
				var totalarrays = newdatavalues.length;
				var totaltimetotal = 0;
				var getnewdata = [];
				var getmaxyarr = [];
				var alldates = [];
				for (var i=0;i<newdatavalues.length;i++){												
					//	GET COLOR IF SET
					//var thisarr = datavalues[i];
					var setcolor = newdatavalues[i][0].color;
					var thisdataarr = newdatavalues[i].slice(0);
					//	REMOVE COLOR
					thisdataarr.splice(0,1);
					d3dirlog(thisdataarr);
					
					if (setcolor){
						// REMOVE COLOR
						//var getarr = datavalues[i];
						//thisdataarr = thisdataarr.slice(0);
						//getarr.splice(0,1);
						// SET DATA ARRAY
						getnewdata.push({values: thisdataarr,key:' Title here '+(i+1),color: setcolor});							
					}else{
						//d3log('setcolor : '+setcolor);
						//setcolor = '#61982F';
						setcolor = 'red';
						// SET DATA ARRAY
						getnewdata.push({values: thisdataarr,key:' Title here '+(i+1),color: setcolor});
					}
					//	GET MAX Y VALUE AND ADD TO ARRAY
					var getcurmax = GetDataValuesYMax(thisdataarr);
					getmaxyarr.push({y:getcurmax});
					
					//	CALC TOTAL TIME
					var gettt = GetDataValuesTTotal(thisdataarr);
					//d3log('gettt : ' + gettt);
					totaltimetotal+=parseInt(gettt);
					
					//	GET ALL DATES FROM EACH ARRAY AND ADD TO TOTAL DATES ARRAY
					var thisalldates = GetDataDates(newdatavalues[i]);						
					alldates = alldates.concat(thisalldates);
				}
				
				//d3dirlog(getmaxyarr);
				//var asdsa = GetDataValuesYMax(getmaxyarr);
				//d3log('WTF : ' + asdsa);
				//	CALC MAX Y Value and add 10% to it for the top
				var getmaxy = GetDataValuesYMax(getmaxyarr);
				var getytoadd = (getmaxy/10);
				//var getytoadd = 0;
				
				
				// GET TOTAL TIME AND SET
				//d3log('totaltimetotal : ' + totaltimetotal);
				d3.select(setchartid+'TotalTime').text(ConvertMinToHours(totaltimetotal));				
				d3log('********');
				//	UPDATE CHART
				chart = nv.models.lineChart()
					.x(function(d){						
						var tempd = new Date(d.x);
						if (xtype=="week"){
							//	reset hours
							tempd.setHours(0,0,0,0);
							return tempd;
						}
						else if (xtype=="month"){
							//	reset hours
							//tempd.setHours(0,0,0,0);
							//	reset days
							//d3log('tempd org : ' + tempd);
							//tempd = new Date(getMonday(tempd));
							//tempd = new Date(getMidWeekday(tempd));
							//d3log('new tempd : ' + tempd);
							//tempd.setDays(0,0,0,0);
							//https://github.com/mbostock/d3/wiki/Time-Intervals#week
							//d3log('tempd : ' + tempd);
							return tempd;
						}
						else{
							return tempd;
						}
							//return new Date(d.x);
						})
					.useInteractiveGuideline(chartUseGuidelines)
					.useVoronoi(chartUseVoronoi)						
					.isArea(chartIsArea)						
					.showYAxis(chartshowYAxis)
					.showXAxis(chartshowXAxis)
					.showLegend(chartshowLegend)
					//	GET MAX Y VALUE AND ADD 100
					.yDomain([0,getmaxy+getytoadd])
					.margin(chartmargin)
					.clipEdge(false)
					;
						
				chart.lines.interactive(true);			
				chart.legend.margin(chartlegendmargin);
				
				
				//	GET ALL DATES FROM EACH ARRAY TO GET MIN AND MAX
				//var alldates = GetDataDates(datavalues[0]);
				//var maxDate = new Date(Math.max.apply(null,alldates));
				//d3log('maxDate : ' + maxDate);
				//var minDate = new Date(Math.min.apply(null,alldates));
				//d3log('minDate : ' + minDate);
				//d3dirlog(alldates);
				
				
				//	SET SCALE MODE (day, week, month)
				//var scalemode = d3.time.hour.range(minDate,maxDate,3);
				var useDateFormatting = chartDateOutput;
				var setticks = 12;
				var showminmaxbottom = true;
				var settickval = null;
				
				//	WEEK : SHOW DAYS
				//	ADJUST SO THAT TIME IS ALWAYS 00:00:00
				if (xtype=="week"){
					//scalemode = d3.time.day.range(minDate,maxDate,1);
					useDateFormatting = chartDateOutputweek;
					setticks = 7;
				}
				//	MONTH : SHOW WEEKS
				//	ADJUST SO THAT DATE IS ALWAYS MIDDLE OF WEEK?!?!
				else if (xtype=="month"){					
					//scalemode = d3.time.week.range(minDate,maxDate,1);
					useDateFormatting = chartDateOutputmonth;
					setticks = 4;
					showminmaxbottom = false;
					//	REMOVE OLD MAXMIN
					d3.selectAll('.nv-axisMaxMin').remove();					
					settickval = alldates;
				}
				//chart.showVoronoi(true);
				
				//	BOTTOM AXIS
				chart.xAxis
				.showMaxMin(showminmaxbottom)
				.axisLabel(chartXaxisLabel)
				.tickFormat(function(d){
					if (d == null) {
						return 'N/A';
					}
					return d3.time.format(useDateFormatting)(new Date(d));					
				})
				.ticks(setticks)
				.tickValues(settickval)
				//.tickValues(scalemode)
				.axisLabelDistance(chartXaxislabeldistance)
				.tickPadding(chartXaxistickpadding)
				//.ticks(chartXaxisticks)
				.staggerLabels(true)
				.rotateLabels(-60)
				;
				
				//chart.forceX(alldates);
				
				//chart.xScale(d3.scale.linear());
				chart.xScale(d3.time.scale());
				//chart.xScale(d3.time.scale.utc());
				
				//	LEFT AXIS
				chart.yAxis
					.axisLabel(chartYaxisLabel)
					.tickFormat(d3.format('.00f'))
					.axisLabelDistance(chartYaxislabeldistance)
					.tickPadding(chartYaxistickpadding)					
					.staggerLabels(true)
					.ticks(chartYaxisticks)
					.showMaxMin(true)
					;
				
				// Update the SVG with the new data and call chart
				d3.select(chartIdsvg)
				.datum(getnewdata)				
				.transition()
				.duration(500)
				.call(chart);
								
				//	UPDATE POINT LABELS				
				if (totalarrays==1){
					$timeout(function(){
						//SetChartsFloatingLabels(chart, true);
					},200);	
				}
				
				nv.utils.windowResize(function(){
					chart.update();
					//SetChartsFloatingLabels(chart, true);
				});
			}
			
			//=====================================
			//	SET/UPDATE FLOATING LABESL
			//=====================================
			function SetChartsFloatingLabels(charttoupdate, isupdate){
				//d3log('SetChartsFloatingLabels isupdate : ' + isupdate);
				var getd = 0,gettransf = 0;
				var setlineheight = -15;
				var setlinecenteradjust = -15;				
	
				//	IF UPDATING THEN REMOVE OLD TEXT LABELS
				if (isupdate){
					d3.selectAll('.nv-scatterWrap .nv-group > text').remove();
				}
				
				//	LOOP AND ADD TEXT LABELS				
				d3.selectAll('.nv-scatterWrap .nv-groups > g > path').each(function(){
					var getd = d3.select(this).attr('d');
					var gettransf = d3.select(this).attr('transform');
					//d3log('gettransf : ' + gettransf);
			
					//	CHECK TRANSFORM AND ADJUST LABELS (TOP AND BOTTOM)
					var chktrans = gettransf;
					var chktransok = 0;			
					if (chktrans.indexOf('translate(')>-1){
						chktrans = chktrans.replace('translate(','');
						chktransok++;
					}			
					if (chktrans.indexOf(')')>-1){
						chktrans = chktrans.replace(')','');
						chktransok++;
					}
					if (chktransok==2){
						var chktransarr = chktrans.split(',');
						//	IS AT BOTTOM : MOVE LABEL UP
						if (chktransarr[0]<75){							
							setlineheight = -15;							
						}
						//	IS AT TOP : MOVE LABEL DOWN
						if (chktransarr[1]<75){							
							setlineheight = 25;
						}
					}
					var settxtclass = '';
					if (isupdate){
						settxtclass = 'noanim';
					}
					//	FIRST LINE
					//======================
					d3.select(d3.select(this).node().parentNode).append('text')
					.attr('class',settxtclass)
					.datum( d3.select(this).data() )
					.text( function(d) {						
						var tpar = d[0][0].p;						
						var ret='P: ' + tpar + '';						
						return ret;				
					})
					.attr('p',function(d){
						var tpar = d[0][0].p;
						return tpar;
					})
					.attr('d',function(d){
						return getd;
					})
					.attr('transform',function(d){				
						return gettransf;
					})
					.attr('x',function(d){
						return setlinecenteradjust;
					})
					.attr('y',function(d){				
						return setlineheight;
					})
					;
				});
			}
		
		}
		/*	END BODY FUNCTION */
})();	
			
			
			
			
			
			
			
			