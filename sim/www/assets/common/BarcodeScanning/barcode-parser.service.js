/***************************************************************
 *                                                             *
 *                           NOTICE                            * 
 *                                                             *
 *   THIS SOFTWARE IS THE PROPERTY OF AND CONTAINS             *
 *   CONFIDENTIAL INFORMATION OF LAWSON SOFTWARE AND/OR ITS    *
 *   AFFILIATES OR SUBSIDIARIES AND SHALL NOT BE DISCLOSED     *
 *   WITHOUT PRIOR WRITTEN PERMISSION. LICENSED CUSTOMERS MAY  *
 *   COPY AND ADAPT THIS SOFTWARE FOR THEIR OWN USE IN         *
 *   ACCORDANCE WITH THE TERMS OF THEIR SOFTWARE LICENSE       *
 *   AGREEMENT. ALL OTHER RIGHTS RESERVED.                     *
 *                                                             *
 *   (c) COPYRIGHT 2015 LAWSON SOFTWARE.  ALL RIGHTS RESERVED. *
 *   THE WORD AND DESIGN MARKS SET FORTH HEREIN ARE            *
 *   TRADEMARKS AND/OR REGISTERED TRADEMARKS OF LAWSON         *
 *   SOFTWARE AND/OR ITS AFFILIATES AND SUBSIDIARIES. ALL      *
 *   RIGHTS RESERVED.  ALL OTHER TRADEMARKS LISTED HEREIN ARE  *
 *   THE PROPERTY OF THEIR RESPECTIVE OWNERS.                  *
 *                                                             *
 ***************************************************************
 *                                                             *
 *  v1.0 - 8/7/2015 - Initial release                          *
 *                                                             *   
 ***************************************************************/
  (function() {
	'use strict';
	
	angular
		.module('InforScannerManager')
		.service("BarcodeParser",BarcodeParser);

		function BarcodeParser(){

			var that = this;

			var barcodeParser = {
				parseBarcode: parseBarcode
			};

			return barcodeParser;

			/*--------------------Implementation-------------------*/
			function parseBarcode(rawBarcode)
			{
				var barcodeData = {};
				barcodeData["type"] 	= BAR_CODE_UNKNOWN;
				barcodeData["codeId"] 	= rawBarcode.substr(0,BAR_CODE_ID_LENGTH);
				barcodeData["input"] 	= rawBarcode.substr(BAR_CODE_ID_LENGTH);
				barcodeData["data"]		= null;
				
				if (rawBarcode)
				{
					var lawsonData = parseLawsonBarcode(rawBarcode);
					
					if (lawsonData["type"] != BAR_CODE_UNKNOWN)
					{
						barcodeData["type"] = lawsonData["type"];
						barcodeData["data"] = lawsonData["data"];
						
						return barcodeData;
					}
					
					var gtinData = parseGTINBarcode(rawBarcode);
							
					if (gtinData["type"] != BAR_CODE_UNKNOWN)
					{
						barcodeData["type"] = gtinData["type"];
						barcodeData["data"] = gtinData["data"];
						
						return barcodeData;
					}
					
					var gs1Data = parseGS1Barcode(rawBarcode);
							
					if (gs1Data["type"] != BAR_CODE_UNKNOWN)
					{
						barcodeData["type"] = gs1Data["type"];
						barcodeData["data"] = gs1Data["data"];
						
						return barcodeData;
					}
					
					var hibcData = parseHIBCBarcode(rawBarcode);
					
					if (hibcData["type"] != BAR_CODE_UNKNOWN)
					{
						barcodeData["type"] = hibcData["type"];
						barcodeData["data"] = hibcData["data"];
						
						return barcodeData;
					}
				}
				
				return barcodeData;
			}
			/*--------------------Private methods and properties------------------*/
			// BarcodeParser Constants

			// BarCode Types
			var BAR_CODE_UNKNOWN				=  0,
			BAR_CODE_BIN					=  1,
			BAR_CODE_ITEMNO				=  2,
			BAR_CODE_LOCATION				=  3,
			BAR_CODE_DEL_TICKET			=  4,
			BAR_CODE_TRACK_NUM				=  5,
			BAR_CODE_REVENUECTR			=  6,		
			BAR_CODE_GTIN					=  7,
			BAR_CODE_GS1					=  8,
			BAR_CODE_HIBC_PRI				=  9,
			BAR_CODE_HIBC_SEC				= 10,
			BAR_CODE_ADD_ON				= 11,
			
			BAR_CODE_ID_LENGTH				= 3,
			
			// LAWSON BarCode Prefix
			LAWSON_PREFIX_LOCATION			= "LL:",
			LAWSON_PREFIX_BIN 				= "BL:",
			LAWSON_PREFIX_ITEMNO			= "IL:",
			LAWSON_PREFIX_PARITEM			= "PI:",
			LAWSON_PREFIX_DELIVERYTICKET	= "LT:",
			LAWSON_PREFIX_TRACKINGNUMBER	= "CT:",
			LAWSON_PREFIX_REVENUECENTER	= "RC:",
			LAWSON_PREFIX_LAWSON_TAG		= "]C0",
			
			// LAWSON BarCode Data
			LAWSON_BIN_LENGTH				= 7,
			LAWSON_ITEMNO_LENGTH			= 32,
			LAWSON_LOCATION_LENGTH			= 10,
			LAWSON_DELTICKET_LENGTH		= 32,
			LAWSON_TRACKNO_LENGTH			= 32,
			LAWSON_REVCTR_LENGTH			= 32,
			LAWSON_PREFIX_LENGTH			= 3,
			
			// GTIN BarCode Prefix
			GTIN_PREFIX_EAN_UPC     		= "]E0",
			GTIN_PREFIX_ADD_ON_2    		= "]E1",
			GTIN_PREFIX_ADD_ON_5    		= "]E2",
			GTIN_PREFIX_WITH_ADD_ON 		= "]E3",
			GTIN_PREFIX_EAN_8       		= "]E4",
			GTIN_PREFIX_ITF_14      		= "]I1",
			GTIN_PREFIX_ITF_14_2    		= "]I0",
			GTIN_PREFIX_DATA_MATRIX 		= "]d1",
			GTIN_PREFIX_PDF_417     		= "]L2",
			GTIN_PREFIX_QR_CODE     		= "]Q1",
			GTIN_PREFIX_CODABLOCK_F		= "]O4",
			
			// GTIN BarCode Data
			GTIN_PREFIX_LENGTH 			= 3,
			GTIN8_LENGTH       			= 8,
			GTIN12_LENGTH      			= 12,
			GTIN12_COMP_LENGTH 			= 8, // Including leading 0 and check digit
			GTIN13_LENGTH      			= 13,
			GTIN14_LENGTH      			= 14,
			GTIN_ADD_ON_2_LENGTH  			= 2,
			GTIN_ADD_ON_5_LENGTH  			= 5,

			// GS1 BarCode Prefix
			GS1_PREFIX						= "]C1",
			GS1_PREFIX_DATA_BAR      		= "]e0",
			GS1_PREFIX_DATA_MATRIX   		= "]d2",
			GS1_PREFIX_DATA_MATRIX_2 		= "]d1",
			
			// GTIN BarCode Data
			GS1_FNC1_ASCII_CODE			= 29,
			GS1_PREFIX_LENGTH				= 3,
			
			// GTIN Supported Elements
			GS1_ELEMENT_GTIN				= "01",
			GS1_ELEMENT_LOT				= "10",
			GS1_ELEMENT_SERIAL				= "21",
			GS1_ELEMENT_EXPIRYDATE			= "17",
			GS1_ELEMENT_EXPIRYDATETIME		= "7003",
			
			// HIBC BarCode Prefix
			HIBC_PREFIX_1 					= "]C0",
			HIBC_PREFIX_2					= "]d1",
			
			HIBC_SECONDARY_DATA_PREFIX_1	= "$$+",
			HIBC_SECONDARY_DATA_PREFIX_2	= "$$",
			HIBC_SECONDARY_DATA_PREFIX_3	= "$+",
			HIBC_SECONDARY_DATA_PREFIX_4	= "$",
			
			HIBC_QTY_2_PREFIX				= "8",
			HIBC_QTY_5_PREFIX				= "9",
			
			HIBC_DATE_MMYY_PREFIX_1		= "0",
			HIBC_DATE_MMYY_PREFIX_2		= "1",
			HIBC_DATE_MMDDYY_PREFIX		= "2",
			HIBC_DATE_YYMMDD_PREFIX		= "3",
			HIBC_DATE_YYMMDDHH_PREFIX		= "4",
			HIBC_DATE_YYJJJ_PREFIX			= "5",
			HIBC_DATE_YYJJJHH_PREFIX		= "6",
			HIBC_DATE_NULL_PREFIX			= "7",
			
			// HIBC BarCode Data
			HIBC_PREFIX_LENGTH				= 3,
			HIBC_ID_CODE_LENGTH			= 4,
			HIBC_UNIT_MEASURE_LENGTH		= 1,
			HIBC_CHECK_CHAR_LENGTH			= 1,

			HIBC_QTY_2_LENGTH				= 2,
			HIBC_QTY_5_LENGTH				= 5,
			
			HIBC_DATE_MMYY_LENGTH			= 4,
			HIBC_DATE_MMDDYY_LENGTH		= 6,
			HIBC_DATE_YYMMDD_LENGTH		= 6,
			HIBC_DATE_YYMMDDHH_LENGTH		= 8,
			HIBC_DATE_YYJJJ_LENGTH			= 5,
			HIBC_DATE_YYJJJHH_LENGTH		= 7;
			
			// End of BarcodeParser Constants

			// BarcodeParser Functions
			// Lawson Barcode Parsing Functions
			function parseLawsonBarcode(rawBarcode)
			{
				console.log("parseLawsonBarcode start");
				
				var output = {};
				var barcodePrefix = "";
				var barcodeData = "";
				var startIndex = 0;
				
				if (rawBarcode <= LAWSON_PREFIX_LENGTH)
				{
					output["type"] = BAR_CODE_UNKNOWN;
					output["data"] = null;
					
					return output;
				}
				
				if (rawBarcode.indexOf(LAWSON_PREFIX_LAWSON_TAG) == 0)
				{
					startIndex = LAWSON_PREFIX_LAWSON_TAG.length;
				}
				
				if (rawBarcode.indexOf(":") != -1)
				{
					startIndex = rawBarcode.indexOf(":") - 2;
				}
				
				barcodePrefix = rawBarcode.substr(startIndex, LAWSON_PREFIX_LENGTH);
				barcodeData = rawBarcode.substr(startIndex + LAWSON_PREFIX_LENGTH);
				
				output["type"] = parseLawsonBarcodeType(barcodePrefix);
				output["data"] = parseLawsonBarcodeData(output["type"], barcodeData);
				
				if (output["data"] == null)
				{
					output["type"] = BAR_CODE_UNKNOWN;
				}
				
				return output;
			}

			function parseLawsonBarcodeType(barcodePrefix)
			{
				console.log("parseLawsonBarcodeType start");
				
				var type = BAR_CODE_UNKNOWN;
				
				if (barcodePrefix == LAWSON_PREFIX_BIN)
				{
					type = BAR_CODE_BIN;
				}
				else if (barcodePrefix == LAWSON_PREFIX_LOCATION)
				{
					type = BAR_CODE_LOCATION;
				}
				else if (barcodePrefix == LAWSON_PREFIX_ITEMNO || barcodePrefix == LAWSON_PREFIX_PARITEM)
				{
					type = BAR_CODE_ITEMNO;
				}
				else if (barcodePrefix == LAWSON_PREFIX_DELIVERYTICKET)
				{
					type = BAR_CODE_DEL_TICKET;
				}
				else if (barcodePrefix == LAWSON_PREFIX_TRACKINGNUMBER)
				{
					type = BAR_CODE_TRACK_NUM;
				}
				else if (barcodePrefix == LAWSON_PREFIX_REVENUECENTER)
				{
					type = BAR_CODE_REVENUECTR;
				}
				
				return type;
			}

			function parseLawsonBarcodeData(barcodeType, barcodeData)
			{
				console.log("parseLawsonBarcodeData start");
				
				var data = {};
				
				switch (barcodeType)
				{
				case BAR_CODE_BIN:
					
					if (barcodeData.length > LAWSON_BIN_LENGTH)
					{
						return null;
					}
					
					data["bin"] = barcodeData;
					
					break;
					
				case BAR_CODE_LOCATION:
					
					if (barcodeData.length > LAWSON_LOCATION_LENGTH)
					{
						return null;
					}
					
					var delimiterIndex = barcodeData.indexOf(":");
					
					if (delimiterIndex == -1) // Barcode contains Facility ID data only
					{
						data["facilityID"] = barcodeData;
						data["locationID"] = "";
					}
					else if (delimiterIndex) // Barcode contains Facility ID and Location ID
					{
						data["facilityID"] = barcodeData.substr(0, delimiterIndex);
						
						if (delimiterIndex < barcodeData.length - 1)
						{
							data["locationID"] = barcodeData.substr(delimiterIndex + 1);
						}
						else
						{
							data["locationID"] = "";
						}
					}
					else // Barcode contains Location ID data only
					{
						data["facilityID"] = "";
						data["locationID"] = barcodeData.substr(1);
					}
					
					break;
					
				case BAR_CODE_ITEMNO:

					if (barcodeData.length > LAWSON_ITEMNO_LENGTH)
					{
						return null;
					}
					
					data["itemNo"] = barcodeData;
					
					break;
					
				case BAR_CODE_DEL_TICKET:

					if (barcodeData.length > LAWSON_DELTICKET_LENGTH)
					{
						return null;
					}
					
					data["deliveryTicket"] = barcodeData;
					
					break;
					
				case BAR_CODE_TRACK_NUM:

					if (barcodeData.length > LAWSON_TRACKNO_LENGTH)
					{
						return null;
					}
					
					data["trackingNumber"] = barcodeData;
					
					break;
					
				case BAR_CODE_REVENUECTR:

					if (barcodeData.length > LAWSON_REVCTR_LENGTH)
					{
						return null;
					}
					
					data["revenueCenter"] = barcodeData;
					
					break;

				default:
					data = null;
					break;
				}
				
				return data;
			}
			//End of Lawson Barcode Parsing Functions

			// GTIN Barcode Parsing Functions
			function parseGTINBarcode(rawBarcode)
			{
				console.log("parseGTINBarcode start");
				
				var output = {};
				var barcodePrefix = "";
				var barcodeData = "";
				
				output["type"] = BAR_CODE_UNKNOWN;
				output["data"] = null;
				
				if (rawBarcode <= GTIN_PREFIX_LENGTH)
				{
					return output;
				}
				
				barcodePrefix = rawBarcode.substr(0, GTIN_PREFIX_LENGTH);
				barcodeData = rawBarcode.substr(GTIN_PREFIX_LENGTH);

				// Try to parse GTIN-14
				output["data"] = parseGTIN14(barcodePrefix, barcodeData);
				
				if (output["data"] == null)
				{
					// Try to parse GTIN-13
					output["data"] = parseGTIN13(barcodePrefix, barcodeData);
				}
				
				if (output["data"] == null)
				{
					// Try to parse GTIN-12
					output["data"] = parseGTIN12(barcodePrefix, barcodeData);
				}
				
				if (output["data"] == null)
				{
					// Try to parse GTIN-8
					output["data"] = parseGTIN8(barcodePrefix, barcodeData);
				}
				
				if (output["data"] != null)
				{
					output["type"] = BAR_CODE_GTIN;
				}
				else
				{
					// Try to parse GTIN add-on
					output["type"] = BAR_CODE_ADD_ON;
					output["data"] = parseGTINAddOn(barcodePrefix, barcodeData);
					
					if (output["data"] == null)
					{
						output["type"] = BAR_CODE_UNKNOWN;
					}
				}					
				
				return output;
			}

			function parseGTIN14(barcodePrefix, barcodeData)
			{
				if (barcodeData.length != GTIN14_LENGTH)
				{
					return null;
				}
				
				if (barcodePrefix != GTIN_PREFIX_ITF_14
				&&  barcodePrefix != GTIN_PREFIX_ITF_14_2
				&&  barcodePrefix != GTIN_PREFIX_DATA_MATRIX
				&&  barcodePrefix != GTIN_PREFIX_PDF_417
				&&  barcodePrefix != GTIN_PREFIX_QR_CODE
				&&  barcodePrefix != GTIN_PREFIX_CODABLOCK_F)
				{
					return null;
				}
				
				return parseGTINData(barcodeData);
			}
			function parseGTIN13(barcodePrefix, barcodeData)
			{
				if (barcodeData.length < GTIN13_LENGTH)
				{
					return null;
				}
				
				// GTIN-12 has leading zero in Symbol devices
				if (barcodeData.charAt(0) == "0")
				{
					// Remove leading zero and then process as GTIN-12
					return parseGTIN12(barcodePrefix, barcodeData.substr(1));
				}
				
				// Process GTIN-13
				if (barcodeData.length == GTIN13_LENGTH
				&&  barcodePrefix  == GTIN_PREFIX_EAN_UPC
				||  barcodePrefix == GTIN_PREFIX_DATA_MATRIX)
				{
					return parseGTINData(barcodeData);
				}
				
				// Process GTIN-13 with Add On	
				if (barcodeData.length > GTIN13_LENGTH
				&&  barcodePrefix == GTIN_PREFIX_WITH_ADD_ON)
				{
					if (barcodeData.length == (GTIN13_LENGTH + GTIN_ADD_ON_2_LENGTH)
					||  barcodeData.length == (GTIN13_LENGTH + GTIN_ADD_ON_5_LENGTH))
					{
						var data = parseGTINData(barcodeData.substr(0, GTIN13_LENGTH));
						
						if (data)
						{
							data["addOnSymbol"] = barcodeData.substr(GTIN13_LENGTH);
						}
						
						return data;
					}
				}
				
				return null;
				
			}

			function parseGTIN12(barcodePrefix, barcodeData)
			{
				if (barcodePrefix == GTIN_PREFIX_EAN_UPC)
				{
					if (barcodeData.length == GTIN12_LENGTH)
					{
						return parseGTINData(barcodeData);
					}
					else if (barcodeData.length == GTIN12_COMP_LENGTH)
					{
						return parseCompressedGTIN12(barcodeData);
					}
					else if (barcodeData.length == GTIN12_COMP_LENGTH - 1)
					{
						return parseCompressedGTIN12("0" + barcodeData);
					}
				}
				else if (barcodePrefix == GTIN_PREFIX_WITH_ADD_ON)
				{
					if (barcodeData.length == (GTIN12_LENGTH + GTIN_ADD_ON_2_LENGTH)
					||  barcodeData.length == (GTIN12_LENGTH + GTIN_ADD_ON_5_LENGTH))
					{
						return parseGTIN12WithAddOn(barcodeData);
					}
					
					if (barcodeData.length == (GTIN12_COMP_LENGTH + GTIN_ADD_ON_2_LENGTH)
					||  barcodeData.length == (GTIN12_COMP_LENGTH + GTIN_ADD_ON_5_LENGTH))
					{
						return parseCompressedGTIN12WithAddOn(barcodeData);
					}
				}
				
				return null;
				
			}

			function parseCompressedGTIN12(barcodeData)
			{
				// Removes leading 0 and check digit before decoding GTIN data
				var compressedData = barcodeData.substr(1, barcodeData.length - 2);
				var decodedData = decodeUPCE(compressedData);
				
				if (decodedData)
				{
					var checkDigit = caclulateCheckDigit(decodedData);
					
					if (checkDigit == barcodeData.charAt(barcodeData.length - 1))
					{
						return parseGTINData(decodedData + checkDigit);
					}
				}
				
				return null;
			}

			function parseGTIN12WithAddOn(barcodeData)
			{
				var data = parseGTINData(barcodeData.substr(0, GTIN12_LENGTH));
					
				if (data)
				{
					data["addOnSymbol"] = barcodeData.substr(GTIN12_LENGTH);
				}
					
				return data;
			}

			function parseCompressedGTIN12WithAddOn(barcodeData)
			{
				var data = parseCompressedGTIN12(barcodeData.substr(0, GTIN12_COMP_LENGTH));
				
				if (data)
				{
					data["addOnSymbol"] = barcodeData.substr(GTIN12_COMP_LENGTH);
				}
				
				return data;
			}

			function parseGTIN8(barcodePrefix, barcodeData)
			{
				if (barcodeData.length == GTIN8_LENGTH
				&&  barcodePrefix == GTIN_PREFIX_EAN_8)
				{
					return parseGTINData(barcodeData);	
				}
				
				return null;
			}

			function parseGTINAddOn(barcodePrefix, barcodeData)
			{
				var data = {};
					
				if (barcodePrefix == GTIN_PREFIX_ADD_ON_2 
				&&  barcodeData.length == GTIN_ADD_ON_2_LENGTH)
				{
					data["gtin"] = "";
					data["addOnSymbol"] = barcodeData;
					
					return data;
				}
				
				if (barcodePrefix == GTIN_PREFIX_ADD_ON_5 
				&&  barcodeData.length == GTIN_ADD_ON_5_LENGTH)
				{
					data["gtin"] = "";
					data["addOnSymbol"] = barcodeData;
					
					return data;
				}
				
				return null;	
			}

			function parseGTINData(barcodeData)
			{
				var dataLength = barcodeData.length;
				var data = {};
				
				if (dataLength != GTIN8_LENGTH
				&&  dataLength != GTIN12_LENGTH
				&&  dataLength != GTIN13_LENGTH
				&&  dataLength != GTIN14_LENGTH)
				{
					return null;
				}
				
				data["id"] = barcodeData.substr(0, dataLength - 1);
				data["checkDigit"] = barcodeData.charAt(dataLength - 1);
				data["addOnSymbol"] = "";
				
				var checkDigit = caclulateCheckDigit(data["id"]);
				
				if (data["checkDigit"] != checkDigit)
				{
					return null;
				}
				
				data["gtin"] = data["id"] + data["checkDigit"];
				
				return data;
			}
				
			function caclulateCheckDigit(barcodeData)
			{
				if (!barcodeData.length)
				{
					return null;
				}

				var multipliers = [3, 1];
				var sum = 0;
				var remainder = 0;
				var checkDigit = 0;

				// Reverses order of barcode data to start check digit calculation at the last digit to first digit
				var input = barcodeData.split("").reverse().join("");

				for (var dataIndex = 0; dataIndex < input.length; dataIndex++)
				{
					sum = sum + (parseInt(input.charAt(dataIndex)) * multipliers[dataIndex % 2]);		
				}

				remainder = sum % 10;

				if (remainder)
				{
					checkDigit = 10 - remainder;
				}
				
				return checkDigit;	
			}

			function decodeUPCE(compressedData)
			{
				if (compressedData.length != GTIN12_COMP_LENGTH - 2)
				{
					return null;
				}
				
				var lastDigit = compressedData.charAt(compressedData.length - 1);
				var output = "";
				var index = 0;
				
				output = "0";
				output = output + compressedData.charAt(index++);
				output = output + compressedData.charAt(index++);
				
				if (lastDigit == "0" ||  lastDigit == "1" ||  lastDigit == "2")
				{
					output = output + lastDigit;
					output = output + "0";
					output = output + "0";
					output = output + "0";
					output = output + "0";
					output = output + compressedData.charAt(index++);
					output = output + compressedData.charAt(index++);
					output = output + compressedData.charAt(index++);
					
					return output;
				}
				
				output = output + compressedData.charAt(index++);
				
				if (lastDigit == "3")
				{
					output = output + "0";
					output = output + "0";
					output = output + "0";
					output = output + "0";
					output = output + "0";
					output = output + compressedData.charAt(index++);
					output = output + compressedData.charAt(index++);
					
					return output;
				}
				
				output = output + compressedData.charAt(index++);
				
				if (lastDigit == "4")
				{
					output = output + "0";
					output = output + "0";
					output = output + "0";
					output = output + "0";
					output = output + "0";
					output = output + compressedData.charAt(index++);
					
					return output;
				}
				
				output = output + compressedData.charAt(index++);
				output = output + "0";
				output = output + "0";
				output = output + "0";
				output = output + "0";
				output = output + lastDigit;	

				return output;	
			}
			//End of GTIN Barcode Parsing Functions

			//GS1 Barcode Parsing Functions
			function parseGS1Barcode(rawBarcode)
			{
				console.log("parseGS1Barcode start");
				
				var output = {};
				var barcodePrefix = "";
				var barcodeData = "";
				
				output["type"] = BAR_CODE_UNKNOWN;
				output["data"] = null;
				
				if (rawBarcode <= GS1_PREFIX_LENGTH)
				{
					return output;
				}
				
				barcodePrefix = rawBarcode.substr(0, GS1_PREFIX_LENGTH);
				barcodeData = rawBarcode.substr(GS1_PREFIX_LENGTH);
				
				if (barcodePrefix == GS1_PREFIX
				||  barcodePrefix == GS1_PREFIX_DATA_BAR
				||  barcodePrefix == GS1_PREFIX_DATA_MATRIX
				||  barcodePrefix == GS1_PREFIX_DATA_MATRIX_2)
				{
					output["type"] = BAR_CODE_GS1;
					output["data"] = parseGS1BarcodeData(barcodeData);
				}
				
				if (output["data"] == null)
				{
					output["type"] = BAR_CODE_UNKNOWN;
				}
				
				return output;
			}

			function parseGS1BarcodeData(barcodeData)
			{
				var data = {};
				
				var applicationID = "";
				var elementValue = "";
				var elemementLength = 0;
				
				var delimiterFNC = String.fromCharCode(GS1_FNC1_ASCII_CODE);
				var delimiterIndex = 0;
				
				var parsedData = barcodeData;	

				data["gtin"] = "";
				data["lot"] = "";
				data["serial"] = "";
				data["expirydate"] = 0;
				
				for (var parsedDataIndex = 0; parsedDataIndex < barcodeData.length; parsedDataIndex += elemementLength)
				{
					parsedData = parsedData.substr(elemementLength);
					
					// Removes all leading FNC1 characters of the parsing data  
					delimiterIndex = parsedData.indexOf(delimiterFNC);
					while (delimiterIndex == 0)
					{
						parsedData = parsedData.substr(1); 
						parsedDataIndex++;
						delimiterIndex = parsedData.indexOf(delimiterFNC);
					}
					
					// Get Application ID
					applicationID = parseGS1ApplicationID(parsedData);
					if (applicationID == null)
					{
						return null;
					}
					
					// Get Element Value
					elementValue = parseGS1ElementValue(applicationID, parsedData);
					if (elementValue == "")
					{
						return null;
					}
					
					// Set Element Length for parsing iteration
					elemementLength = applicationID.length + elementValue.length;
					
					// Set parsed data
					if (applicationID == GS1_ELEMENT_GTIN)
					{
						var gtinData = parseGTINData(elementValue);
						data["gtin"] = gtinData["gtin"];
					}
					else if (applicationID == GS1_ELEMENT_LOT)
					{
						data["lot"] = elementValue;
					}
					else if (applicationID == GS1_ELEMENT_SERIAL)
					{
						data["serial"] = elementValue;
					}
					else if (applicationID == GS1_ELEMENT_EXPIRYDATE)
					{
						data["expirydate"] = parseGS1ExpiryDate(elementValue);
					}
					else if (applicationID == GS1_ELEMENT_EXPIRYDATETIME)
					{
						data["expirydate"] = parseGS1ExpiryDateTime(elementValue);
					}
					else
					{
						// Skip unsupported application id values
					}
				}
				
				return data;
			}

			function parseGS1ApplicationID(barcodeData)
			{
				var idLength = 0;
				var firstIDChar = barcodeData.charAt(0);
				var secondIDChar = barcodeData.charAt(1);
				
				switch (firstIDChar)
				{
				case "0":
				case "1":
				case "9":
					idLength = 2;
					break;
				case "2":
					if (secondIDChar == "0" || secondIDChar == "1" || secondIDChar == "2")
					{
						idLength = 2;
					}
					else
					{
						idLength = 3;
					}
					break;
				case "3":
					if (secondIDChar == "0" || secondIDChar == "7")
					{
						idLength = 2;
					}
					else
					{
						idLength = 4; // Include decimal point position indicator
					}
					break;
				case "4":
					idLength = 3;
					break;
				case "7":
				case "8":
					idLength = 4;
					break;
				default:
					idLength = 0;
					break;
				}
				
				if (idLength == 0)
				{
					return null;
				}
				
				return barcodeData.substr(0, idLength);
			}

			function parseGS1ElementValue(applicationID, barcodeData)
			{
				var elementValue = "";
				var elementValueLength = 0;
				var delimiterFNC = String.fromCharCode(GS1_FNC1_ASCII_CODE);
				
				// Fixed Length Table
				var elementLengthTable = {};
				
				elementLengthTable[GS1_ELEMENT_GTIN] 				= 14;
				elementLengthTable[GS1_ELEMENT_EXPIRYDATE] 		= 6;
				elementLengthTable[GS1_ELEMENT_EXPIRYDATETIME] 	= 10;
				
				elementLengthTable["00"]	= 18;
				elementLengthTable["02"] 	= 14;
				
				elementLengthTable["11"] 	= 6;
				elementLengthTable["12"] 	= 6;
				elementLengthTable["13"] 	= 6;
				elementLengthTable["15"] 	= 6;
				
				elementLengthTable["20"] 	= 2;
				
				elementLengthTable["410"] 	= 13;
				elementLengthTable["411"] 	= 13;
				elementLengthTable["412"] 	= 13;
				elementLengthTable["413"] 	= 13;
				elementLengthTable["414"] 	= 13;
				elementLengthTable["415"] 	= 13;
				
				if (elementLengthTable.hasOwnProperty(applicationID)) // Fixed Length Value
				{
					elementValueLength = elementLengthTable[applicationID];
				}
				else // Variable Length Value
				{
					var delimiterIndex = barcodeData.indexOf(delimiterFNC);
					
					if (delimiterIndex != -1) 
					{
						// Delimiter Found
						elementValueLength = delimiterIndex - applicationID.length;
					}
					else 
					{
						// Delimiter Not Found or End of String
						elementValueLength = barcodeData.length - applicationID.length;
					}
				}
				
				return barcodeData.substr(applicationID.length, elementValueLength);
			}

			function parseGS1ExpiryDate(barcodeData)
			{
				// Based on GS1 format of YYMMDD
				var yearData	= barcodeData.substr(0, 2);
				var monthData 	= barcodeData.substr(2, 2);
				var day 		= barcodeData.substr(4, 2);
					
				var year 	= expand2DigitYear(yearData);
				var month 	= parseInt(monthData) - 1; // 0-11 notation
				
				var expiryDate = new Date(year, month, day);
				
				return expiryDate.getTime(); // Returns integer equivalent of current date
			}

			function parseGS1ExpiryDateTime(barcodeData)
			{
				// Based on GS1 format of YYMMDDHHMM
				var yearData 	= barcodeData.substr(0, 2);
				var monthData 	= barcodeData.substr(2, 2);
				var day 		= barcodeData.substr(4, 2);
				var hour 		= barcodeData.substr(6, 2);
				var minute 		= barcodeData.substr(8, 2);
				
				
				var year 	= expand2DigitYear(yearData);
				var month 	= parseInt(monthData) - 1; // 0-11 notation
				
				var expiryDate = new Date(year, month, day, hour, minute);
				
				return expiryDate.getTime(); // Returns integer equivalent of current date/time
			}
			//End of GS1 Barcode Parsing Functions

			// HIBC Barcode Parsing Functions
			function parseHIBCBarcode(rawBarcode)
			{
				console.log("parseHIBCBarcode start");
				
				var output = {};
				var barcodePrefix = "";
				var barcodeData = "";
				
				output["type"] = BAR_CODE_UNKNOWN;
				output["data"] = null;
				
				if (rawBarcode <= HIBC_PREFIX_LENGTH)
				{
					return output;
				}
				
				barcodePrefix = rawBarcode.substr(0, HIBC_PREFIX_LENGTH);
				barcodeData = rawBarcode;
				
				if (barcodePrefix == HIBC_PREFIX_1
				||  barcodePrefix == HIBC_PREFIX_2)
				{
					 barcodeData = rawBarcode.substr(HIBC_PREFIX_LENGTH);
				}

				output["type"] = BAR_CODE_HIBC_PRI;
				output["data"] = parseHIBCPrimaryDataStructure(barcodeData);
				
				if (output["data"] != null)
				{
					return output;
				}
				
				output["type"] = BAR_CODE_HIBC_SEC;
				output["data"] = parseHIBCSecondaryDataStructure(barcodeData, false);
				
				if (output["data"] != null)
				{
					return output;
				}
				
				// Unknown Barcode 
				output["type"] = BAR_CODE_UNKNOWN;
				output["data"] = null;
				
				return output;
			}

			function parseHIBCPrimaryDataStructure(barcodeData)
			{
				var data = {};
				var parsedData = barcodeData;
				var isConcatenated = false;
				
				if (parsedData.charAt(0) != "+" || checkCharNotAplhabet(parsedData.charAt(1)))
				{
					return null;
				}
				 
				parsedData = parsedData.substr(1); // Skip '+'
				
				// HIBC Primary Structure
				data["primary"] = {}; 
				data["primary"]["hibc"] = "";
				data["primary"]["id"] = "";
				data["primary"]["pcn"] = "";
				data["primary"]["uom"] = "";
				data["primary"]["checkChar"] = "";
				
				if (parsedData.length < HIBC_ID_CODE_LENGTH)
				{
					return null;
				}
				
				data["primary"]["id"] = parsedData.substr(0, HIBC_ID_CODE_LENGTH); // ID Code
				
				parsedData = parsedData.substr(HIBC_ID_CODE_LENGTH);

				var slashIndex = parsedData.indexOf("/");
				var pcnLength = 0;
				
				// if last character is slash, still primary only
				if (slashIndex != -1 && slashIndex < (parsedData.length - 1))
				{
					isConcatenated = true;
					pcnLength = slashIndex - 1; // excluding unit of measure
				}
				else
				{
					pcnLength = parsedData.length - 2; // excluding check character and unit of measure
				}
				
				data["primary"]["pcn"] = parsedData.substr(0, pcnLength); // Product Catalog Number
				
				parsedData = parsedData.substr(pcnLength);
				
				if (parsedData.length < HIBC_UNIT_MEASURE_LENGTH)
				{
					return null;
				}
				
				data["primary"]["uom"] = parsedData.substr(0, HIBC_UNIT_MEASURE_LENGTH); // Unit of Measure
				
				if (isNaN(data["primary"]["uom"]))
				{
					return null;
				}
				
				parsedData = parsedData.substr(HIBC_UNIT_MEASURE_LENGTH);
				
				data["secondary"] = "";
				
				if (isConcatenated)
				{
					parsedData = parsedData.substr(1); // Skip "/"
					
					var dataStructure = parseHIBCSecondaryDataStructure(parsedData, isConcatenated);
					
					if (dataStructure == null)
					{
						return null;
					}
					
					data["secondary"] = dataStructure["secondary"];
					
					parsedData = parsedData.substr(dataStructure["length"]);
				}
				
				if (parsedData.length + HIBC_CHECK_CHAR_LENGTH > barcodeData.length)
				{
					return null;
				}
				
				data["primary"]["checkChar"] = parsedData.substr(0, HIBC_CHECK_CHAR_LENGTH);
				
				var calcCheckChar = caclulateCheckCharacter(barcodeData.substr(0, barcodeData.length - HIBC_CHECK_CHAR_LENGTH));
				
				if (calcCheckChar == null)
				{
					return null;
				}
				
				if (data["primary"]["checkChar"] != calcCheckChar)
				{
					return null;
				}
				
				data["primary"]["hibc"] = data["primary"]["id"] + data["primary"]["pcn"] + data["primary"]["uom"] // HIBC
				
				return data;
			}

			function parseHIBCSecondaryDataStructure(barcodeData, isConcatenated)
			{
				var data = {};
				var parsedData = barcodeData;
				var parsedLength = 0;
					
				if (isConcatenated == false)
				{
					if (parsedData.charAt(0) != "+")
					{
						return null;
					}

					parsedLength++;
					parsedData = parsedData.substr(parsedLength); // Skips "+"
				}
				
				//  Character should either be a number or "$" symbol
				if (isNaN(parsedData.charAt(0)) && parsedData.charAt(0) != "$") 
				{
					return null;
				}
				
				var resultData = parseQuantityDateSerial(parsedData, isConcatenated);
				
				if (resultData == null)
				{
					resultData = parseQuantityDateLot(parsedData, isConcatenated);
				}
				
				if (resultData == null)
				{
					resultData = parseSerialNumber(parsedData, isConcatenated);
				}
				
				if (resultData == null)
				{
					resultData = parseLotBatchNumber(parsedData, isConcatenated);
				}
				
				if (resultData == null)
				{
					resultData = parseJulianDate(parsedData);
					
					// Special case for some barcodes
					if (resultData)
					{
						if (parsedData.length > resultData["length"])
						{
							var remainingResult = parseLotBatchOrSerial(parsedData.substr(resultData["length"]), isConcatenated);
							
							if (remainingResult)
							{
								resultData["data"]["lot"] = remainingResult["data"];
								resultData["length"] = resultData["length"] + remainingResult["length"];
							}
						}
					}
				}
				
				if (resultData == null)
				{
					return null
				}
				
				data["secondary"] = resultData["data"];
						
				if (isConcatenated == false)
				{
					parsedData = parsedData.substr(resultData["length"]);
					
					data["secondary"]["linkChar"] = parsedData.charAt(0);
					data["secondary"]["checkChar"] = parsedData.charAt(1);
					
					var calcCheckChar = caclulateCheckCharacter(barcodeData.substr(0, barcodeData.length - HIBC_CHECK_CHAR_LENGTH));
					
					if (calcCheckChar == null)
					{
						return null;
					}
					
					if (data["secondary"]["checkChar"] != calcCheckChar)
					{
						return null;
					}
				}
				else
				{
					data["secondary"]["linkChar"] = "";
					data["secondary"]["checkChar"] = "";
					data["length"] = parsedLength + resultData["length"];
				}
				
				return data;
			}

			function parseQuantityDateSerial(barcodeData, isConcatenated)
			{
				var returnData = {};
				
				returnData["data"] = initHIBCSecondaryStructure();
				returnData["length"] = 0;
				
				var parsedLength = 0;
				var parsedData 	= barcodeData;
				
				if (parsedData.substr(0, HIBC_SECONDARY_DATA_PREFIX_1.length) != HIBC_SECONDARY_DATA_PREFIX_1)
				{
					return null;
				}
				
				if (isNaN(parsedData.substr(HIBC_SECONDARY_DATA_PREFIX_1.length, 1)))
				{
					return null;
				}
				
				parsedLength = HIBC_SECONDARY_DATA_PREFIX_1.length; // Skips "$$+"
				parsedData = parsedData.substr(parsedLength);
				
				var result = parseQuantity(parsedData);
				
				if (result)
				{
					returnData["data"]["qty"] = result["data"];
					parsedData = parsedData.substr(result["length"]);
					parsedLength = parsedLength + result["length"];
				}
				
				result = parseExpiryDate(parsedData);
				
				if (result)
				{
					returnData["data"]["expiryDate"] = result["data"];
					parsedData = parsedData.substr(result["length"]);
					parsedLength = parsedLength + result["length"];			
				}
				
				result = parseLotBatchOrSerial(parsedData, isConcatenated);
				
				if (result)
				{
					returnData["data"]["serial"] = result["data"];
					parsedLength = parsedLength + result["length"];
				}
				
				// Checks if no data was parsed
				if (parsedLength == HIBC_SECONDARY_DATA_PREFIX_1.length) 
				{
					return null;
				}

				returnData["length"] = parsedLength;
				
				return returnData;
			}

			function parseQuantityDateLot(barcodeData, isConcatenated)
			{
				var returnData = {};
				
				returnData["data"] = initHIBCSecondaryStructure();
				returnData["length"] = 0;
				
				var parsedLength = 0;
				var parsedData 	= barcodeData;
				
				if (parsedData.substr(0, HIBC_SECONDARY_DATA_PREFIX_2.length) != HIBC_SECONDARY_DATA_PREFIX_2)
				{
					return null;
				}
				
				if (isNaN(parsedData.substr(HIBC_SECONDARY_DATA_PREFIX_2.length, 1)))
				{
					return null;
				}
				
				parsedLength = HIBC_SECONDARY_DATA_PREFIX_2.length; // Skips "$$"
				parsedData = parsedData.substr(parsedLength);
				
				var result = parseQuantity(parsedData);
				
				if (result)
				{
					returnData["data"]["qty"] = result["data"];
					parsedData = parsedData.substr(result["length"]);
					parsedLength = parsedLength + result["length"];
				}
				
				result = parseExpiryDate(parsedData);
				
				if (result)
				{
					returnData["data"]["expiryDate"] = result["data"];
					parsedData = parsedData.substr(result["length"]);
					parsedLength = parsedLength + result["length"];			
				}
				
				result = parseLotBatchOrSerial(parsedData, isConcatenated);
				
				if (result)
				{
					returnData["data"]["lot"] = result["data"];
					parsedLength = parsedLength + result["length"];
				}
				
				// Checks if no data was parsed
				if (parsedLength == HIBC_SECONDARY_DATA_PREFIX_2.length) 
				{
					return null;
				}

				returnData["length"] = parsedLength;
				
				return returnData;
			}

			function parseSerialNumber(barcodeData, isConcatenated)
			{
				var returnData = {};
				
				returnData["data"] = initHIBCSecondaryStructure();
				returnData["length"] = 0;
				
				var parsedLength = 0;
				var parsedData 	= barcodeData;
				
				if (parsedData.substr(0, HIBC_SECONDARY_DATA_PREFIX_3.length) != HIBC_SECONDARY_DATA_PREFIX_3)
				{
					return null;
				}
				
				parsedLength = HIBC_SECONDARY_DATA_PREFIX_3.length; // Skips "$+"
				parsedData = parsedData.substr(parsedLength);
				
				var result = parseLotBatchOrSerial(parsedData, isConcatenated);
				
				if (result)
				{
					returnData["data"]["serial"] = result["data"];
					parsedLength = parsedLength + result["length"];
				}
				
				// Checks if no data was parsed
				if (parsedLength == HIBC_SECONDARY_DATA_PREFIX_3.length) 
				{
					return null;
				}
				
				returnData["length"] = parsedLength;
				
				return returnData;
			}

			function parseLotBatchNumber(barcodeData, isConcatenated)
			{
				var returnData = {};
				
				returnData["data"] = initHIBCSecondaryStructure();
				returnData["length"] = 0;
				
				var parsedLength = 0;
				var parsedData 	= barcodeData;
				
				if (parsedData.substr(0, HIBC_SECONDARY_DATA_PREFIX_4.length) != HIBC_SECONDARY_DATA_PREFIX_4)
				{
					return null;
				}
				
				parsedLength = HIBC_SECONDARY_DATA_PREFIX_4.length; // Skips "$"
				parsedData = parsedData.substr(parsedLength);
				
				var result = parseLotBatchOrSerial(parsedData, isConcatenated);
				
				if (result)
				{
					returnData["data"]["lot"] = result["data"];
					parsedLength = parsedLength + result["length"];
				}
				
				// Checks if no data was parsed
				if (parsedLength == HIBC_SECONDARY_DATA_PREFIX_4.length) 
				{
					return null;
				}

				returnData["length"] = parsedLength;

				return returnData;
			}

			function parseJulianDate(barcodeData, isConcatenated)
			{
				var returnData = {};
				
				returnData["data"] = initHIBCSecondaryStructure();
				returnData["length"] = 0;
				
				var parsedData = barcodeData.substr(0, HIBC_DATE_YYJJJ_LENGTH);
					
				if (isNaN(parsedData))
				{
					return null;
				}
				
				var expiryDate = convertJulianDate(parsedData.substr(0, 2), parsedData.substr(2, 3));
				
				if (expiryDate == null)
				{
					return null;
				}
				
				returnData["data"]["expiryDate"] = expiryDate.getTime();
				returnData["length"] = HIBC_DATE_YYJJJ_LENGTH;
				
				return returnData;
			}

			function parseQuantity(barcodeData)
			{
				var returnData = {};
				
				var quantityPrefix = barcodeData.charAt(0);
				var quantityLength = 0;
				
				returnData["data"] = "";
				returnData["length"] = 0;
				
				switch (quantityPrefix)
				{
				case HIBC_QTY_2_PREFIX: // '8'
					quantityLength = HIBC_QTY_2_LENGTH;
					break;
				
				case HIBC_QTY_5_PREFIX: // '9'
					quantityLength = HIBC_QTY_5_LENGTH;
					break;
				
				default:
					return null;
				}
				
				if (barcodeData.length < quantityPrefix.length + quantityLength)
				{
					return null;
				}
				
				returnData["data"] = barcodeData.substr(quantityPrefix.length, quantityLength);
				
				if (isNaN(returnData["data"]))
				{
					return null;
				}
				
				returnData["length"] = quantityPrefix.length + quantityLength;
				
				return returnData;
				
			}

			function parseExpiryDate(barcodeData)
			{
				var returnData = {};
				
				var expiryDatePrefix = barcodeData.charAt(0);
				var expiryDateString = "";
				var expiryDate = null;
				
				var year = 0;
				var month = 0;
				var day = 0;
				var hour = 0;
				
				var julianDate = null;
				
				returnData["data"] = 0;
				returnData["length"] = 0;
				
				switch (expiryDatePrefix)
				{
				case HIBC_DATE_NULL_PREFIX: 	// '7'
					returnData["length"] = HIBC_DATE_NULL_PREFIX.length;
					return returnData;
					
				case HIBC_DATE_MMYY_PREFIX_1:	// '0'
				case HIBC_DATE_MMYY_PREFIX_2: 	// '1'

					if (barcodeData.length < HIBC_DATE_MMYY_LENGTH)
					{
						return null;
					}
					
					expiryDatePrefix = "";
					expiryDateString = barcodeData.substr(0, HIBC_DATE_MMYY_LENGTH);
					
					if (isNaN(expiryDateString))
					{
						return null;
					}

					year = expand2DigitYear(expiryDateString.substr(2, 2));
					month = parseInt(expiryDateString.substr(0, 2)) - 1; // 0-11 notation
					
					expiryDate = new Date(year, month);
					
					break;
				
				case HIBC_DATE_MMDDYY_PREFIX: 	// '2'

					if (barcodeData.length < expiryDatePrefix.length + HIBC_DATE_MMDDYY_LENGTH)
					{
						return null;
					}
					
					expiryDateString = barcodeData.substr(expiryDatePrefix.length, HIBC_DATE_MMDDYY_LENGTH);
					
					if (isNaN(expiryDateString))
					{
						return null;
					}

					month 	= parseInt(expiryDateString.substr(0, 2)) - 1; // 0-11 notation
					year 	= expand2DigitYear(expiryDateString.substr(2, 2));
					day		= parseInt(expiryDateString.substr(4, 2));
					
					expiryDate = new Date(year, month, day);
					
					break;
				
				case HIBC_DATE_YYMMDD_PREFIX: 	// '3'

					if (barcodeData.length < expiryDatePrefix.length + HIBC_DATE_YYMMDD_LENGTH)
					{
						return null;
					}
					
					expiryDateString = barcodeData.substr(expiryDatePrefix.length, HIBC_DATE_YYMMDD_LENGTH);
					
					if (isNaN(expiryDateString))
					{
						return null;
					}

					year 	= expand2DigitYear(expiryDateString.substr(0, 2));
					month 	= parseInt(expiryDateString.substr(2, 2)) - 1; // 0-11 notation
					day		= parseInt(expiryDateString.substr(4, 2));
					
					expiryDate = new Date(year, month, day);
					
					break;
				
				case HIBC_DATE_YYMMDDHH_PREFIX: // '4'

					if (barcodeData.length < expiryDatePrefix.length + HIBC_DATE_YYMMDDHH_LENGTH)
					{
						return null;
					}
					
					expiryDateString = barcodeData.substr(expiryDatePrefix.length, HIBC_DATE_YYMMDDHH_LENGTH);
					
					if (isNaN(expiryDateString))
					{
						return null;
					}

					year 	= expand2DigitYear(expiryDateString.substr(0, 2));
					month 	= parseInt(expiryDateString.substr(2, 2)) - 1; // 0-11 notation
					day		= parseInt(expiryDateString.substr(4, 2));
					hour	= parseInt(expiryDateString.substr(6, 2));
					
					expiryDate = new Date(year, month, day, hour);
					
					break;
					
				case HIBC_DATE_YYJJJ_PREFIX: // '5'

					if (barcodeData.length < expiryDatePrefix.length + HIBC_DATE_YYJJJ_LENGTH)
					{
						return null;
					}
					
					expiryDateString = barcodeData.substr(expiryDatePrefix.length, HIBC_DATE_YYJJJ_LENGTH);
					
					if (isNaN(expiryDateString))
					{
						return null;
					}

					julianDate = convertJulianDate(expiryDateString.substr(0, 2), expiryDateString.substr(2, 3));
					
					if (julianDate == null)
					{
						return null;
					}
					
					year 	= julianDate.getFullYear();
					month 	= julianDate.getMonth();
					day		= julianDate.getDate();
					
					expiryDate = new Date(year, month, day);
					
					break;
					
				case HIBC_DATE_YYJJJHH_PREFIX: // '6'

					if (barcodeData.length < expiryDatePrefix.length + HIBC_DATE_YYJJJHH_LENGTH)
					{
						return null;
					}
					
					expiryDateString = barcodeData.substr(expiryDatePrefix.length, HIBC_DATE_YYJJJHH_LENGTH);
					
					if (isNaN(expiryDateString))
					{
						return null;
					}

					julianDate = convertJulianDate(expiryDateString.substr(0, 2), expiryDateString.substr(2, 3));
					
					if (julianDate == null)
					{
						return null;
					}
					
					year 	= julianDate.getFullYear();
					month 	= julianDate.getMonth();
					day		= julianDate.getDate();
					hour	= parseInt(expiryDateString.substr(5, 2));
					
					expiryDate = new Date(year, month, day, hour);
					
					break;
					
				default:
					return null;
				}	
				
				returnData["data"] = expiryDate.getTime();
				returnData["length"] = expiryDatePrefix.length + expiryDateString.length;
				
				return returnData;
			}

			function parseLotBatchOrSerial(barcodeData, isConcatenated)
			{
				var returnData = {};
				
				returnData["data"] = "";
				returnData["length"] = 0;
			
				if (isConcatenated)
				{
					returnData["length"] = barcodeData.length - 1; // Excludes check character
				}
				else
				{
					returnData["length"] = barcodeData.length - 2; // Excludes check character and link character
				}
				
				if (returnData["length"] > 0)
				{
					returnData["data"] = barcodeData.substr(0, returnData["length"]);
				}
				else
				{
					return null;
				}

				return returnData;
			}

			function convertJulianDate(julianYear, julianDay)
			{
				var julianDate = null;
				var year = 0;
				var month = 0;
				var day = 0;
				
				var isLeapYear = false;
				var totalDays = julianDay;
				var monthDays = 0;
				
				if (julianYear.length != 2 ||  julianDay.length != 3 )
				{
					return null;
				}
				
				year = expand2DigitYear(julianYear);
				
				if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0)
				{
					isLeapYear = true;
				}
				
				for (month = 0; month < 12; month++) // 0-11 month notation
				{
					switch (month)
					{
					case 0:		// January
						monthDays = 31;
						break;
					case 1:		// February
						if (isLeapYear)
						{
							monthDays = 29;
						}
						else
						{
							monthDays = 28;
						}
						break;
					case 2:		// March
						monthDays = 31;
						break;
					case 3:		// April
						monthDays = 30;
						break;
					case 4:		// May
						monthDays = 31;
						break;
					case 5:		// June
						monthDays = 30;
						break;
					case 6:		// July
						monthDays = 31;
						break;
					case 7:		// August
						monthDays = 31;
						break;
					case 8:		// September
						monthDays = 30;
						break;
					case 9:		// October
						monthDays = 31;
						break;
					case 10:	// November
						monthDays = 30;
						break;
					case 11:	// December
						monthDays = 31;
						break;
					}
					
					if (totalDays < monthDays)
					{
						day = totalDays;
						break;
					}
					
					totalDays = totalDays - monthDays;
				}
				
				if (month >= 12)
				{
					return null;
				}
				
				julianDate = new Date(year, month, day);
				
				return julianDate;
			}

			function initHIBCSecondaryStructure()
			{
				var secodaryData = {};
				secodaryData["qty"] = "";
				secodaryData["serial"] = "";
				secodaryData["lot"] = "";
				secodaryData["expiryDate"] = "";
				
				return secodaryData;
			}

			function caclulateCheckCharacter(barcodeData)
			{
				var charValue = 0;
				var charSum = 0;
				
				for (var dataIndex = 0; dataIndex < barcodeData.length; dataIndex++)
				{
					switch (barcodeData.charAt(dataIndex))
					{
					case '0': charValue =  0; break;
					case '1': charValue =  1; break;
					case '2': charValue =  2; break;
					case '3': charValue =  3; break;
					case '4': charValue =  4; break;
					case '5': charValue =  5; break;
					case '6': charValue =  6; break;
					case '7': charValue =  7; break;
					case '8': charValue =  8; break;
					case '9': charValue =  9; break;
					case 'A': charValue = 10; break;
					case 'B': charValue = 11; break;
					case 'C': charValue = 12; break;
					case 'D': charValue = 13; break;
					case 'E': charValue = 14; break;
					case 'F': charValue = 15; break;
					case 'G': charValue = 16; break;
					case 'H': charValue = 17; break;
					case 'I': charValue = 18; break;
					case 'J': charValue = 19; break;
					case 'K': charValue = 20; break;
					case 'L': charValue = 21; break;
					case 'M': charValue = 22; break;
					case 'N': charValue = 23; break;
					case 'O': charValue = 24; break;
					case 'P': charValue = 25; break;
					case 'Q': charValue = 26; break;
					case 'R': charValue = 27; break;
					case 'S': charValue = 28; break;
					case 'T': charValue = 29; break;
					case 'U': charValue = 30; break;
					case 'V': charValue = 31; break;
					case 'W': charValue = 32; break;
					case 'X': charValue = 33; break;
					case 'Y': charValue = 34; break;
					case 'Z': charValue = 35; break;
					case '-': charValue = 36; break;
					case '.': charValue = 37; break;
					case ' ': charValue = 38; break;
					case '$': charValue = 39; break;
					case '/': charValue = 40; break;
					case '+': charValue = 41; break;
					case '%': charValue = 42; break;
					
					default : return null; 
					}
					
					charSum = charSum + charValue;
				}
				
				var checkCharValue = charSum % 43;
				var checkChar = null;
				
				switch(checkCharValue)
				{
				case  0: checkChar = '0'; break;
				case  1: checkChar = '1'; break;
				case  2: checkChar = '2'; break;
				case  3: checkChar = '3'; break;
				case  4: checkChar = '4'; break;
				case  5: checkChar = '5'; break;
				case  6: checkChar = '6'; break;
				case  7: checkChar = '7'; break;
				case  8: checkChar = '8'; break;
				case  9: checkChar = '9'; break;
				case 10: checkChar = 'A'; break;
				case 11: checkChar = 'B'; break;
				case 12: checkChar = 'C'; break;
				case 13: checkChar = 'D'; break;
				case 14: checkChar = 'E'; break;
				case 15: checkChar = 'F'; break;
				case 16: checkChar = 'G'; break;
				case 17: checkChar = 'H'; break;
				case 18: checkChar = 'I'; break;
				case 19: checkChar = 'J'; break;
				case 20: checkChar = 'K'; break;
				case 21: checkChar = 'L'; break;
				case 22: checkChar = 'M'; break;
				case 23: checkChar = 'N'; break;
				case 24: checkChar = 'O'; break;
				case 25: checkChar = 'P'; break;
				case 26: checkChar = 'Q'; break;
				case 27: checkChar = 'R'; break;
				case 28: checkChar = 'S'; break;
				case 29: checkChar = 'T'; break;
				case 30: checkChar = 'U'; break;
				case 31: checkChar = 'V'; break;
				case 32: checkChar = 'W'; break;
				case 33: checkChar = 'X'; break;
				case 34: checkChar = 'Y'; break;
				case 35: checkChar = 'Z'; break;
				case 36: checkChar = '-'; break;
				case 37: checkChar = '.'; break;
				case 38: checkChar = ' '; break;
				case 39: checkChar = '$'; break;
				case 40: checkChar = '/'; break;
				case 41: checkChar = '+'; break;
				case 42: checkChar = '%'; break;
				}
				
				return checkChar;
			}
			// End of HIBC Barcode Functions

			// Common Functions
			function expand2DigitYear(parsedYear)
			{
				// Based on GS1 General Specifications Section 7.13
				var inputYear = parseInt(parsedYear);
				var today = new Date();
				var currentYear = today.getFullYear();
				
				var currentCentury = currentYear - currentYear % 100;
				var difference = inputYear - currentYear % 100;

				if (difference >= 51 && difference <= 99)
				{
					// previous century
					return currentCentury - 100 + inputYear;
				}

				if (difference >= -99 && difference <= -50)
				{
					// next century
					return currentCentury + 100 + inputYear;
				}

				//current century
				return parseInt(currentCentury + inputYear);
			}

			function checkCharNotAplhabet(barcodeData)
			{
				if (barcodeData.match(/[a-zA-Z]/) == null)
				{
					return true;
				}
				
				return false
			}
			// End of Common Functions
			
			// End of BarcodeParser Functions
		}
	
})();	