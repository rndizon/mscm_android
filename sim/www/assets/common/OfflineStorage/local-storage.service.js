(function(){

	  'use strict';

	  	angular
	    .module("StorageManagerModule",['ngStorage'])
	    .factory('LocalData', Body);

	    Body.$inject = ["$localStorage","$sessionStorage"];

	    function Body($localStorage,$sessionStorage) {

	    	var localStorageData = {
                saveData: saveData,
                loadData: loadData,
                removeData: removeData,
                saveSession: saveSession,
                loadSession: loadSession 
            };

            return localStorageData;

            function saveData(key,value){
                                                                
               $localStorage[key] = value;

            }

            function loadData(key) {
                return $localStorage[key];
            }

            function removeData(key){
                //$localStorage.removeItem(key);
            }

            function saveSession(key,value) {
                $sessionStorage[key] = value;
            }

            function loadSession(key) {
                return $sessionStorage[key];
            }




	    }


})();