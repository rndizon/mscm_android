/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
 */

package com.infor.rad.mscm;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import org.apache.cordova.*;
import org.apache.cordova.engine.SystemWebViewEngine;

import com.infor.mscm.scanner.ScannerManager;

public class MainActivity extends CordovaActivity
{
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set by <content src="index.html" /> in config.xml
        loadUrl(launchUrl);

        //Creates Scanner Instance
        ScannerManager.createScannerInstance();

        /*CordovaWebView cwv = (CordovaWebView) appView.getEngine().getCordovaWebView();

        cwv.getView().setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.v("CordovaLog", "GET VIEW Oncreate Onkey ");
                if (ScannerManager.getScannerInstance().processScanKey(keyCode, event)) {
                    Log.v("CordovaLog", "GET VIEW Naprcess scan key ");
                    return true;
                }

                return false;
            }
        });

        //Sets Key Listener for processing of Scan Key press in AppView
        appView.getView().setOnKeyListener(new View.OnKeyListener()
        {
            public boolean onKey(View v, int keyCode, KeyEvent event)
            {
            	Log.v("CordovaLog", "GET VIEW Oncreate Onkey ");
              if (ScannerManager.getScannerInstance().processScanKey(keyCode, event))
              {
            	  Log.v("CordovaLog", "GET VIEW Naprcess scan key ");
                return true;
              }

              return false;
            }
        });
        appView.getEngine().getCordovaWebView().getView().setOnKeyListener(new View.OnKeyListener()
        {
            public boolean onKey(View v, int keyCode, KeyEvent event)
            {
            	Log.v("CordovaLog", "Oncreate Onkey getCordovaWebView");
              if (ScannerManager.getScannerInstance().processScanKey(keyCode, event))
              {
            	  Log.v("CordovaLog", "Naprcess scan key getCordovaWebView");
                return true;
              }

              return false;
            }
        });*/

    }
   /* @Override
    public boolean dispatchKeyEvent(KeyEvent event)
    {
        if (ScannerManager.getScannerInstance().processScanKey(event.getKeyCode(), event))
        {
            Log.v("CordovaLog", "Naprcess scan key getCordovaWebView");
            return true;
        }

        return true;
    }*/
    @Override
    public void onResume() {
        super.onResume();
        //Security resume
       // ClientAuthenticator.getInstance().resume();
      //Initialized Scanner Instance
        ScannerManager.getScannerInstance().initialize(this);
    }

    @Override
    public void onDestroy() {
        //Security stop
        //ClientAuthenticator.getInstance().stop();
      //Clears Scanner Instance
        ScannerManager.getScannerInstance().deinitialize();
        super.onDestroy();
     }
    @Override
    public void onPause() {
        super.onPause();
    }
    @Override
    public boolean onKeyDown(int keyCode,KeyEvent event)
    {
    	Log.v("CordovaLog", "Onkeydown Onkey ");

      /*if (ScannerManager.getScannerInstance().processScanKey(keyCode, event))
      {
    	  Log.v("CordovaLog", "Onkeydown Naprcess scan key ");
        return true;
      }*/

      return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode,KeyEvent event)
    {
      /*  String a = ""+keyCode+"";
    	Log.v("CordovaLog", "Onkeyup Onkey ");
      if (ScannerManager.getScannerInstance().processScanKey(keyCode, event))
      {
    	  Log.v("CordovaLog", "Onkeyup napracess Onkey ");
        return true;
      }*/

      return super.onKeyUp(keyCode, event);
    }
}
