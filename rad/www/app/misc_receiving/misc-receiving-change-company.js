(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('MiscReceivingChangeCompanyController', Body);

			function Body($state,
					  $scope,
					  $ionicLoading,
					  $ionicViewSwitcher,
					  $ionicModal, 
					  $ionicHistory,
					  CRUD, 
					  LocalData,
					  CommonInfoService,
					  CONSTANT,
					  DockLogService) {

			var vm = this;
			vm.saveCompanySelected = saveCompanySelected;
			vm.clearSearch = clearSearch;
			vm.showCompanyList = showCompanyList;
		//	vm.doRefresh = doRefresh;

			vm.companyList = [];
			vm.selectedCompanyID = LocalData.loadData('miscRecCompanyID');
			vm.selectedCompanyDesc = LocalData.loadData('miscRecCompanyName');
			vm.currentUser = LocalData.loadData('userID');
			vm.userDef = LocalData.loadData('miscRecDef');
			vm.miscRec = [];




			$scope.$on('$ionicView.beforeEnter', function() {
				showCompanyList();

//				getMiscRec();

			})

/*			function getMiscRec() {
				CRUD.select("MiscRecv_Transaction","*").then(function(result){
					vm.miscRec = result;
				})
			}  */

		/*	function doRefresh(){

				$ionicLoading.show({
					templateUrl: CONSTANT.SpinnerTemplate
				});

				DockLogService.getDocklogConfig().then(function(result){
					CRUD.remove("DockLog_Company").then(function(res){
						DockLogService.saveCompAndLocInSql(result).then(function(){
							
							vm.showCompanyList();

							vm.selectedCompanyID = LocalData.loadData('miscRecCompanyID');
							vm.selectedCompanyDesc = LocalData.loadData('miscRecCompanyName');
							
							$ionicLoading.hide();
							$scope.$broadcast('scroll.refreshComplete');
						})
					})

				});

			} */

			function saveCompanySelected() {		

				var params = {};

				if(LocalData.loadData('miscRecCompanyID') != vm.selectedCompanyID) {

				/*	if(vm.miscRec.length > 0) {
						CRUD.remove("MiscRecv_Transaction");
					} */

					params.CompanyID = vm.selectedCompanyID;
					params.LocationType = "receiving";

					if(vm.selectedCompanyID == vm.userDef.recCompanyID) {
						params.LocationID = vm.userDef.recLocationID;
					} 

					CRUD.select("MiscRecv_Location","*",params).then(function(result){

						LocalData.saveData('miscRecLocationID',result[0].LocationID);
						LocalData.saveData('miscRecLocationName',result[0].LocationDescription);
						LocalData.saveData('miscRecLocationCode',result[0].LocationCode); 

						
					})


				} 

				LocalData.saveData("miscRecCompanyID", vm.selectedCompanyID);

				Object.keys(vm.companyList).forEach(function(x){
					if(vm.companyList[x].CompanyID == vm.selectedCompanyID) {
						LocalData.saveData('miscRecCompanyName',vm.companyList[x].CompanyDesc);
						vm.selectedCompanyDesc = vm.companyList[x].CompanyDesc;
					}
				})
				
				$state.go('app.misc-receiving');

			}; 

			function showCompanyList() {
				var companyParams = {};
				
				companyParams.UserID = vm.currentUser;

				CRUD.select("MiscRecv_Company","*",companyParams)
				 .then(function(result){
		 		 	 	vm.companyList = result;
		 		});

			};



		    function clearSearch(){
				vm.searchText = '';
			}; 
		};
				
		
})();
