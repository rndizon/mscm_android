(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('MiscReceivingChangeLocationController', Body);

			function Body($state,
					  $scope,
					  $ionicLoading,
					  $ionicViewSwitcher,
					  $ionicModal, 
					  $ionicHistory,
					  CRUD, 
					  LocalData,
					  CommonInfoService,
					  CONSTANT,
					  DockLogService) {

			var vm = this;
		//	vm.saveLocationSelected = saveLocationSelected;
			vm.clearSearch = clearSearch;
			vm.showLocationList = showLocationList;
	//		vm.doRefresh = doRefresh;
	//		vm.getDockLog = getDockLog;

			vm.selectedLocationID = LocalData.loadData('miscRecLocationID');
			vm.selectedLocationName = LocalData.loadData('miscRecLocationName');
			vm.selectedLocationCode = LocalData.loadData('miscRecLocationCode');
			vm.selectedCompanyID = LocalData.loadData('miscRecCompanyID');
			vm.locationList = [];



			$scope.$on('$ionicView.beforeEnter', function() {
				showLocationList();


			//	vm.getDockLog();
			})

	/*		function doRefresh(){

				$ionicLoading.show({
					templateUrl: CONSTANT.SpinnerTemplate
				});

				DockLogService.getDocklogConfig().then(function(result){

					CRUD.inlineQuery("DELETE FROM DockLog_Location WHERE CompanyID = '" + vm.selectedCompanyID  + "'").then(function(){

						DockLogService.saveLocationInSql(result).then(function(){

							vm.showLocationList();
							$ionicLoading.hide();
							$scope.$broadcast('scroll.refreshComplete');
						});
					})

				})


			}

			function getDockLog() {
				CRUD.select("DockLog_Transaction","*").then(function(result){
					vm.dockLog = result;
				})
			} */

	/*		function saveLocationSelected() {			

				if(LocalData.loadData('dockLogLocationID') != vm.selectedLocationID && vm.dockLog.length > 0) {
					CRUD.remove("DockLog_Transaction");
				}

				LocalData.saveData("dockLogLocationID", vm.selectedLocationID);

				Object.keys(vm.locationList).forEach(function(x){
					if(vm.locationList[x].LocationID == vm.selectedLocationID) {
						LocalData.saveData('dockLogLocationName',vm.locationList[x].LocationName);
						LocalData.saveData('dockLogLocationCode',vm.locationList[x].LocationCode);

						vm.selectedLocationCode = vm.locationList[x].LocationCode;
						vm.selectedLocationName = vm.locationList[x].LocationName;
					}
				})

				$state.go('app.dock-log');
			}; */

			function showLocationList() {

				var locationParam = {};

				locationParam.CompanyID = vm.selectedCompanyID;
				locationParam.LocationType = "receiving";

				CRUD.select("MiscRecv_Location","*",locationParam)
				 .then(function(result){
		 		 	 	//vm.locationList = result;

	 		 	 	Object.keys(result).forEach(function(ind){

	 		 	 		console.log(result[ind])

	 		 	 		vm.locationList.push({
	 		 	 			LocationID: result[ind].LocationID,
	 		 	 			LocationName: result[ind].LocationDescription,
	 		 	 			LocationCode: result[ind].LocationCode
	 		 	 		})
	 		 	 	})


	 		 	 	console.log(vm.locationList);

		 		});

			};



		    function clearSearch(){
				vm.searchText = '';
			}; 
		};
				
		
})();
