(function(){
    'use strict';
	 
	angular
	    .module('app')
	    .factory('MiscReceivingService',Body);

	    function Body($q,
	    			  CRUD,
	    			  LocalData,
	    			  CM
	    	) 
	    {
		  
		    var service = {
		        getReceivingConfig : getReceivingConfig,
		        saveMiscRecvConfig: saveMiscRecvConfig,

		        getAllCarriers: getAllCarriers
		    };
		    
		    var requestToken = LocalData.loadData('token');
		    var userID = LocalData.loadData('userID');

		    return service;  	
		 	
		 	function getReceivingConfig () {
				var args = {
		 			URL:    "ReceivingService",
		 			Action: "getReceivingConfig",
		 			Params: {"requestString":"<getreceivingconfig></getreceivingconfig>","token":requestToken}
		 		};

		 		return CM.doCommsCall("soap",args);
		 	}

		 	function getCompanyCarrier(_companyID) {

				var args = {
		 			URL:    "ReceivingService",
		 			Action: "getCompanyCarriers",
		 			Params: {"requestString":"<getcompanycarriers><facility id='" + _companyID + "' /></getcompanycarriers>","token":requestToken}
		 		};

		 		return CM.doCommsCall("soap",args);
		 	}

		 	function getAllCarriers() {

				var deffered = $q.defer();
	    		var promises = [];


		 		CRUD.select("MiscRecv_Company","*").then(function(companies){	
		 			Object.keys(companies).forEach(function(compInd){

	    				getCompanyCarrier(companies[compInd].CompanyID).then(function(result){

	    				//	console.log(companies[compInd].CompanyID);
		 					var carriersBatch = "VALUES";
			 				var value = xmlToJson(result);
							var xmlResponse = value.root.body.getCompanyCarriersResponse.getCompanyCarriersReturn;
							var object = ReturnDataObject(xmlResponse);	
							var carriers = object.getcompanycarriersresponse.carrier;	
							var carrierArr = [];

							if(carriers) {
								if(carriers instanceof Array){
									carrierArr = carriers;
								} else {
									carrierArr.push(carriers);
								}

								Object.keys(carrierArr).forEach(function(carrierArrInd){


									insertTrackFormat(carrierArr[carrierArrInd]).then(function(carrierArrRes){
										deffered.resolve(carrierArrRes);
									});

									carriersBatch += "('" + companies[compInd].CompanyID + "','" + carrierArr[carrierArrInd].attributes.id + "','" + carrierArr[carrierArrInd].attributes.code + "','" + carrierArr[carrierArrInd].attributes.name + "'),";
							
								})

	    						CRUD.inlineQuery("INSERT INTO MiscRecv_Carrier (CompanyID,CarrierID,CarrierCode,CarrierName) " + carriersBatch.slice(0,-1)).then(function(res){
	    							deffered.resolve(res);
	    						})
							}

	    				});


		 			})
		 		})

			 	promises.push(deffered.promise);			 	
				return $q.all(promises);
		 	}

		 	function insertTrackFormat(_carrier){

				var deffered = $q.defer();
	    		var promises = [];
		 		var trackformatArr = [];



		 		if(_carrier.trackformat instanceof Array) {
		 			trackformatArr = _carrier.trackformat;
		 		} else {
		 			trackformatArr.push(_carrier.trackformat);
		 		}



		 		Object.keys(trackformatArr).forEach(function(ind){

		 		//	console.log(trackformatArr[ind].attributes)
		 			var params = {
		 				CarrierID: _carrier.attributes.id,
		 				TrackFormatLength: trackformatArr[ind].attributes.length,
		 				TrackFormatStartsWith: trackformatArr[ind].attributes.startswith,
		 				TrackFormatStartsPos: trackformatArr[ind].attributes.startpos,
		 				TrackFormatEndPos: trackformatArr[ind].attributes.endpos,
		 				TrackFormatMapType: trackformatArr[ind].attributes.maptype,
		 				SupplierStartPos: trackformatArr[ind].attributes.supplierstartpos,
		 				SupplierEndPos: trackformatArr[ind].attributes.supplierendpos
		 			};

		 			CRUD.select("MiscRecv_TrackFormat","*",params).then(function(trackFormatRes){


		 				console.log(trackFormatRes.length);

		 				if(trackFormatRes.length == 0) {

		 					console.log("Zero");

				 			CRUD.insert("MiscRecv_TrackFormat",params).then(function(res){
				 				deffered.resolve(res);
				 			});  
		 				} else {
		 					console.log("More thant zero");
		 				}

		 			}) 


		 		})

			 	promises.push(deffered.promise);			 	
				return $q.all(promises);
		 	}

		 	function saveMiscRecvConfig(result) {
				var deffered = $q.defer();
	    		var promises = [];
		 		var value = xmlToJson(result);
				var xmlResponse = value.root.body.getReceivingConfigResponse.getReceivingConfigReturn;
				var object = ReturnDataObject(xmlResponse);	

				var receivingConfig = object.getreceivingconfigresponse.receivingconfig;

				LocalData.saveData('miscRecvConfig',receivingConfig.attributes);

				saveCompAndLocInSql(receivingConfig.fac).then(function(compAndLocRes){
					deffered.resolve(compAndLocRes);
				});

				saveVendor(receivingConfig.vngrp).then(function(vendorRes){
					deffered.resolve(vendorRes);
				})

				saveNote(receivingConfig.note).then(function(noteRes){
					deffered.resolve(noteRes);
				});


			 	promises.push(deffered.promise);			 	
				return $q.all(promises);

		 	}

		 	function saveNote(note) {
				var deffered = $q.defer();
	    		var promises = [];
	    		var noteBatch = "VALUES";

		 		Object.keys(note).forEach(function(noteInd){
					var noteDetails = note[noteInd].attributes.att.split("¶");
		 			noteBatch += "('" + noteDetails[0] + "','" + noteDetails[1] + "'),";
		 		});

		 		CRUD.inlineQuery("INSERT INTO MiscRecv_Note (NoteCode,NoteDesc) " + noteBatch.slice(0,-1)).then(function(res){
		 			deffered.resolve(res);
		 		});

			 	promises.push(deffered.promise);			 	
				return $q.all(promises);


		 	}

		 	function saveVendor(vngrp) {
				var deffered = $q.defer();
	    		var promises = [];

	    		Object.keys(vngrp).forEach(function(vngrpInd){

	    			if(vngrp[vngrpInd].ven) {

	    				var vendorArr = [];
	    				var vendorBatchArr = [];
	    				var ctr = 0;

	    				if(vngrp[vngrpInd].ven instanceof Array) {
	    					vendorArr = vngrp[vngrpInd].ven;
	    				} else {
	    					vendorArr.push(vngrp[vngrpInd].ven);
	    				}


	    				Object.keys(vendorArr).forEach(function(vendorArrInd){
	    					var vendorDetails = vendorArr[vendorArrInd].attributes.att.split("¶");


	    					vendorBatchArr[vendorArrInd] = {
	    						VendorGroupID: vngrp[vngrpInd].attributes.id,
	    						VendorID: vendorDetails[0],
	    						VendorCode: vendorDetails[1],
	    						VendorName: vendorDetails[2]
	    					}

	    					ctr++;
	    				});

	    				insertVendor(vendorBatchArr,ctr);

	    			}
	    		})


			 	promises.push(deffered.promise);			 	
				return $q.all(promises);

		 	}

		 	function insertVendor(vendorBatch,ctr) {

		 		if(ctr < 300) {
		 			var vBatch = "VALUES";
		 			
		 			Object.keys(vendorBatch).forEach(function(vInd){

		 				vBatch += "('"+ vendorBatch[vInd].VendorGroupID + "','"+ vendorBatch[vInd].VendorID + "','"+ vendorBatch[vInd].VendorCode + "','"+ vendorBatch[vInd].VendorName + "'),";
		 			});

		 			CRUD.inlineQuery("INSERT INTO MiscRecv_Vendor (VendorGroupID,VendorID,VendorCode,VendorName)" + vBatch.slice(0,-1)).then(function(res){

		 			})	


		 		} else {

		 			console.warn("Currently not saving more than 300 vendors");

		 		}

		 	}

		 	function saveCompAndLocInSql(fac) {

				var deffered = $q.defer();
	    		var promises = [];
		 		var CompanyBatch = "VALUES";
	    		var miscRecvConfig = LocalData.loadData('miscRecvConfig');
				var miscRecDef = {}


		 		Object.keys(fac).forEach(function(facIndex){

		 			var compDetails = fac[facIndex].attributes.att.split("¶");
		 			CompanyBatch += "('" + userID + "','" + compDetails[2] + "','" + compDetails[0] + "','" + compDetails[1] + "'),";

		 			/*
						Delievery Location
		 			*/


		 			if(fac[facIndex].dloc) {
		 				var dLocArray = [];
		 				
		 				if(fac[facIndex].dloc instanceof Array) {
		 					dLocArray = fac[facIndex].dloc;
		 				} else {
		 					dLocArray.push(fac[facIndex].dloc);
		 				}

		 				var dLocationBatchArr = [];
		 				var ctr = 0;

		 				Object.keys(dLocArray).forEach(function(dLocIndex){

		 					var dLocationDetails = dLocArray[dLocIndex].attributes.att.split("¶");


		 					if(miscRecvConfig.userdefdelloc  == dLocationDetails[0]) {
		 						miscRecDef.delLocationID =  dLocationDetails[0];
		 						miscRecDef.delCompanyID = compDetails[0];
		 					}

	 						dLocationBatchArr[dLocIndex] = {
	 							companyID:  compDetails[0].replace(/\'/g, ""),
	 							locationID: dLocationDetails[0].replace(/\'/g, ""),
	 							locationType: 'delivery',
	 							locationCode: dLocationDetails[1].replace(/\'/g, ""),
	 							locationDesc: dLocationDetails[2].replace(/\'/g, ""),
	 							isDefLoc: "0"
	 						};

	 						ctr++;

		 				})

		 				deffered.resolve(insertLocation(dLocationBatchArr,ctr));
		 				 
		 			}

		 			/*
						Receiving Location
		 			*/

		 			if(fac[facIndex].rloc)	 {

		 				var rLocArray = [];

		 				if(fac[facIndex].rloc instanceof Array) {
		 					rLocArray = fac[facIndex].rloc;
		 				} else {
		 					rLocArray.push(fac[facIndex].rloc);
		 				}

		 				var rLocationBatchArr = [];
		 				var ctr = 0;

		 				Object.keys(rLocArray).forEach(function(rLocIndex){

		 					var rLocationDetails = rLocArray[rLocIndex].attributes.att.split("¶");

		 					if(miscRecvConfig.userdefloc == rLocationDetails[0]) {
		 						miscRecDef.recLocationID =  rLocationDetails[0];
		 						miscRecDef.recCompanyID = compDetails[0];

		 						LocalData.saveData('miscRecCompanyID',compDetails[0]);
		 						LocalData.saveData('miscRecCompanyName',compDetails[1]);

		 						LocalData.saveData('miscRecLocationID',rLocationDetails[0]);
		 						LocalData.saveData('miscRecLocationCode',rLocationDetails[1].replace(/\'/g, ""))
		 						LocalData.saveData('miscRecLocationName',rLocationDetails[2].replace(/\'/g, ""));

		 					} else {
		 						/*
									Set First on the list
		 						*/

		 					//	console.warn("Set first on the list as company default");
		 					}

	 						rLocationBatchArr[rLocIndex] = {
	 							companyID:  compDetails[0],
	 							locationID: rLocationDetails[0],
	 							locationType: 'receiving',
	 							locationCode: rLocationDetails[1].replace(/\'/g, ""),
	 							locationDesc: rLocationDetails[2].replace(/\'/g, ""),
	 							isDefLoc: "0"
	 						}; 

	 						ctr++;
		 				})


	 					deffered.resolve(insertLocation(rLocationBatchArr,ctr));
		 			}



		 		})

		 		CRUD.inlineQuery("INSERT INTO MiscRecv_Company (UserID,VendorGroupID,CompanyID,CompanyDesc) " + CompanyBatch.slice(0,-1)).then(function(compResult){
		 			deffered.resolve(compResult);
		 		})

		 		LocalData.saveData('miscRecDef',miscRecDef);

			 	promises.push(deffered.promise);			 	
				return $q.all(promises);
		 	}

			function insertLocation(locBatch,ctr) {
				
				var deffered = $q.defer();
	    		var promises = [];
	    		var dLocationBatch = "VALUES";

				if(ctr < 300) {


					Object.keys(locBatch).forEach(function(ind){

 						dLocationBatch += "('" + locBatch[ind].companyID + "','" + locBatch[ind].locationID +  "','" +  locBatch[ind].locationType + "','" + locBatch[ind].locationCode +  "','" + locBatch[ind].locationDesc +  "','" + locBatch[ind].isDefLoc + "'),";

					})

					CRUD.inlineQuery("INSERT INTO MiscRecv_Location (CompanyID,LocationID,LocationType,LocationCode,LocationDescription,isDefaultLocation) " + dLocationBatch.slice(0,-1)).then(function(locResult){
						deffered.resolve(locResult)
					}) 
						
					


				} else {

					var chunkResult = chunkArray(locBatch,300);

					Object.keys(chunkResult).forEach(function(chunkInd){
						var dLocationBatch = "VALUES";

						Object.keys(chunkResult[chunkInd]).forEach(function(chunkedInd){
							var newLocBatch = chunkResult[chunkInd][chunkedInd];
 							dLocationBatch += "('" + newLocBatch.companyID + "','" + newLocBatch.locationID +  "','" +  newLocBatch.locationType + "','" + newLocBatch.locationCode +  "','" + newLocBatch.locationDesc +  "','" + newLocBatch.isDefLoc + "'),";
						})

						CRUD.inlineQuery("INSERT INTO MiscRecv_Location (CompanyID,LocationID,LocationType,LocationCode,LocationDescription,isDefaultLocation) " + dLocationBatch.slice(0,-1)).then(function(locResult){
							deffered.resolve(locResult)
						}) 

					})

				}
			
			 	promises.push(deffered.promise);			 	
				return $q.all(promises);
			} 

			function chunkArray(arrayVal,chunkSize) {


				var batchArr = [];
				var ctr = 0;

				var groups = arrayVal.map( function(e,i){ 

					if(i%chunkSize===0) {
						batchArr[ctr] = arrayVal.slice(i,i+chunkSize);
						ctr++;
					} 
					
				})

				return batchArr;
			}

		}

})();

