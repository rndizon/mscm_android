(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('MiscReceivingController', Body);

			function Body($state,
					$scope,
					filterFilter,
					$ionicPopover,
					$ionicViewSwitcher,
					MiscReceivingService,
					CRUD,
					LocalData,
					$ionicLoading,
					CONSTANT,
					CarrierParser,
					CommonInfoService ) {

			var vm = this;
			vm.goTo = goTo;
			vm.miscReceivingOption = miscReceivingOption;	
			vm.addTrackingNumber = addTrackingNumber;
			vm.getMiscRec = getMiscRec;
			vm.selectAll = selectAll;
			vm.clearAll = clearAll;
			vm.checkboxClicked = checkboxClicked;
			vm.showSelectAll = showSelectAll;
			vm.deleteTrackingNumber = deleteTrackingNumber;


			vm.miscRec = [];
			vm.date = new Date();


		    $scope.$on('$ionicView.beforeEnter', function() {

		    	if(!LocalData.loadData("MiscRecvSoapCall")) {
		    		
					$ionicLoading.show({
						templateUrl: CONSTANT.SpinnerTemplate
					});

			    	MiscReceivingService.getReceivingConfig().then(function(getConfigResult){
			    		MiscReceivingService.saveMiscRecvConfig(getConfigResult).then(function(saveConfigRes){
			    			MiscReceivingService.getAllCarriers().then(function(){



						    	vm.selectedCompanyID = LocalData.loadData('miscRecCompanyID');
						    	vm.selectedCompanyName = LocalData.loadData('miscRecCompanyName');

						    	vm.selectedLocationID = LocalData.loadData('miscRecLocationID');
						    	vm.selectedLocationCode = LocalData.loadData('miscRecLocationCode');
						    	vm.selectedLocationName = LocalData.loadData('miscRecLocationName');

				    			LocalData.saveData('MiscRecvSoapCall',"1");
								$ionicLoading.hide();

			    			});


			    		});
			    	})
			    } 
			    
			    CRUD.select("MiscRecv_TrackFormat","*").then(function(res){
			    	console.log(res);
			    })

			    vm.getMiscRec();

		    })

		    $scope.$on('$ionicView.afterEnter',function(){

		    	vm.selectedCompanyID = LocalData.loadData('miscRecCompanyID');
		    	vm.selectedCompanyName = LocalData.loadData('miscRecCompanyName');

		    	vm.selectedLocationID = LocalData.loadData('miscRecLocationID');
		    	vm.selectedLocationCode = LocalData.loadData('miscRecLocationCode');
		    	vm.selectedLocationName = LocalData.loadData('miscRecLocationName');
		    })

			
			function getMiscRec() {
				CRUD.select("MiscRecv_Transaction","*").then(function(result){
					vm.miscRec = result;
				})
			}	

			function addTrackingNumber() {

				var carrierDetails = CarrierParser.getData(vm.modTrackingNum);

				var transactionParams = {};

				var trackingNumVal = "",carrierNameVal = "";

				if(carrierDetails.name) {
					trackingNumVal = carrierDetails.tracking;
					carrierNameVal = carrierDetails.name;
				} else {
					trackingNumVal = vm.modTrackingNum;
					carrierNameVal = '-';
				}

				// Check if tracking is already exist
				var foundInArr = CommonInfoService.findInArrayObject(vm.miscRec,"TrackingNumber",trackingNumVal);
				if(foundInArr > -1) {
					
					vm.popUpMsg = 'tracking-exist';
					var conf = CommonInfoService.confirmPopUp($scope);

					conf.then(function(res){
						if(res) {

							/*
								Update Array
							*/
							vm.miscRec.splice(foundInArr,1);

							CRUD.inlineQuery("DELETE FROM MiscRecv_Transaction WHERE TrackingNumber = '" + trackingNumVal + "'").then(function(result){
								console.log(result);
							}) 
							
							vm.modTrackingNum = "";
						}

					})	

				} else {

					vm.miscRec.unshift({
						TrackingNumber: trackingNumVal,
						CarrierName: carrierNameVal,
						Timestamp: (vm.date.getTime() / 1000)
					});

					transactionParams.TrackingNumber = trackingNumVal;
					transactionParams.Timestamp = (vm.date.getTime() / 1000);
					transactionParams.CarrierName = carrierNameVal;

					CRUD.insert('MiscRecv_Transaction',transactionParams).then(function(){
						console.log('New transaction added');
					})

					vm.modTrackingNum = "";
				}
		
			}


			function deleteTrackingNumber() {
				vm.selectedMiscRec  = filterFilter( vm.miscRec, {val:true});
				var foundInArr;

				Object.keys(vm.selectedMiscRec).forEach(function(x){

					foundInArr = CommonInfoService.findInArrayObject(vm.miscRec,"TrackingNumber",vm.selectedMiscRec[x].TrackingNumber);
					
					// Delete in array
					vm.miscRec.splice(foundInArr,1);
					// Delete in Database

					CRUD.inlineQuery('DELETE FROM MiscRecv_Transaction WHERE TrackingNumber = "' + vm.selectedMiscRec[x].TrackingNumber + '"').then(function(){
						console.log("Tracking Deleted");
					})

				});

				vm.clearAll();
				vm.showOptionToDelete = false;
			}

			function checkboxClicked() {
				vm.selectedMiscRec  = filterFilter( vm.miscRec, {val:true});

				if(vm.selectedMiscRec.length > 0) {
					vm.showOptionToDelete = true;
				} else {
					vm.showOptionToDelete = false;
				}

				vm.totalSelected = vm.selectedMiscRec.length;
				showSelectAll();
			}

			function showSelectAll() {
				if(vm.selectedMiscRec.length != vm.miscRec.length) {
					vm.showSelectAll = true;
				} else {
					vm.showSelectAll = false;
				}
			}


			function clearAll() {

			    for(var x = 0; x < vm.miscRec.length; x++) {
			    	vm.miscRec[x].val = false;
			    }

			    vm.checkboxClicked()
			}

			function selectAll() {

			    for(var x = 0; x < vm.miscRec.length; x++) {
			    	vm.miscRec[x].val = true;
			    }

			    vm.checkboxClicked();
			}


			function goTo($url,$navDirection) {

				if(vm.popover) {
					vm.popover.hide();
				}

				if($navDirection) {
		   			$ionicViewSwitcher.nextDirection($navDirection);
				}

				$state.go($url);
			}

	    	function miscReceivingOption($event,$popupType) {
		    	$ionicPopover.fromTemplateUrl('html/templates/option-more.html', {
				        scope: $scope
				}).then(function(popover) {
				        vm.popover = popover;
				        if($popupType) {
				       		vm.popUpType = $popupType;
				        } else {
				        	vm.popUpType = 'misc-receiving';
				        }
				        popover.show($event)
				}); 
	    	};
		};
				
		
})();
