(function(){
 	'use strict';
 	
		angular
		    .module('app')
			.config(function($stateProvider, $urlRouterProvider) {
		     
		     $stateProvider
		      .state("app.misc-receiving", {
		      	cache: false,
                changeColor: 'bar-amethyst-7',
			    url: "/misc-receiving", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/misc_receiving/misc-receiving.html",
					      controller: "MiscReceivingController as vm"
					 }
				  }
			      
		      })

		      .state("app.misc-receiving-change-company", {
		      	cache: false,
                changeColor: 'bar-amethyst-7',
			    url: "/misc-receiving-change-company", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/templates/company-change-view.html",
					      controller: "MiscReceivingChangeCompanyController as vm"
					 }
				  }
			      
		      })

		      .state("app.misc-receiving-change-location", {
		      	cache: false,
                changeColor: 'bar-amethyst-7',
			    url: "/misc-receiving-change-location", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/templates/location-change-view.html",
					      controller: "MiscReceivingChangeLocationController as vm"
					 }
				  }
			      
		      })

		      .state("app.misc-receiving-delivery", {
		      	cache: false,
                changeColor: 'bar-amethyst-7',
			    url: "/misc-receiving-delivery", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/misc_receiving/misc-receiving-delivery.html",
					      controller: "MiscReceivingDeliveryController as vm"					      
					 }
				  }
			      
		      })

		      .state("app.misc-receiving-vendor", {
		      	cache: false,
                changeColor: 'bar-amethyst-7',
			    url: "/misc-receiving-vendor", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/misc_receiving/misc-receiving-vendor.html",    
					 }
				  }
			      
		      })

		      .state("misc-receiving-receipt", {
		      	cache: false,
                changeColor: 'bar-amethyst-7',
			    url: "/misc-receiving-receipt", 
//			      views :{
//			      	'menu-content':{
					      templateUrl: "html/misc_receiving/misc-receiving-receipt.html",
					      controller: "MiscReceivingReceiptController as vm"					      
//					 }
//				  }
			      
		      })

		      .state("app.misc-receiving-printer-list", {
		      	cache: false,
                changeColor: 'bar-amethyst-7',
			    url: "/misc-receiving-printer-list", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/templates/printer-change-view.html"				      
					 }
				  }
		      })

		     });
		     
})();