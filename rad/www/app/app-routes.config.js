(function(){
 	'use strict';
 	
		angular
		    .module('app')
			.config(function($stateProvider, $urlRouterProvider) {
		     
		     $stateProvider
		     .state('app', {
		        url: '/app',
		        abstract: true,
		        templateUrl: 'html/templates/menu-template.html'
		        //controller: 'ParCountController as vm'
		      
		    })
			.state("log-in", {
				url: "/log-in", 
				templateUrl: "html/templates/log-in.html",
				controller: "IndexController as vm"
			})

			.state("app.welcome", {
				url: "/welcome", 
				changeColor: 'bar-emerald-7',
				views :{
					'menu-content':{
						templateUrl: "html/templates/welcome.html",
						controller: "IndexController as vm"
					}
				}
			});



		      $urlRouterProvider.otherwise('/log-in');
		  });
		
		
		
})();