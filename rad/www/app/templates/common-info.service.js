(function(){
    'use strict';
	 
	angular
	    .module('app')
	    .factory('CommonInfoService', Body);


	    function Body($ionicPopup) {
	    	var vm = this;

			function findInArrayObject(array, attr, value) {
			    for(var i = 0; i < array.length; i += 1) {
			        if(array[i][attr] === value) {
			            return i;
			        }
			    }
			}

			function confirmPopUp(scp,oText,cText) {

				var okText = oText || "Yes";
				var canText = cText || "No";

				return $ionicPopup.confirm({
					cssClass: 'custom-popup',
					templateUrl: "html/templates/pop-up.html",
					scope: scp,
					cancelText: canText,
					okText: okText
				});
			}
	    	
	    	return {
	    		
	    		findInArrayObject 	: findInArrayObject,
	    		confirmPopUp		: confirmPopUp 
	    		
	    	};
	    	
		}
})();
