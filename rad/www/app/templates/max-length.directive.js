(function(){
    'use strict';
   
  angular
      .module('app')
      .directive('maxLength', function() {
        return {
          require: '?ngModel',
          link: function(scope, element, attrs, ngModelCtrl) {


			element.bind('keyup', function () {
					var maxVal = attrs.maxLength;
					var oldValue = element.val().slice(0,-1);

					if(element.val().length > maxVal) {
						element.val(oldValue);
					}

			});

          }
        };
      });
      
})();