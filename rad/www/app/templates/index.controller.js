(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('IndexController', Body);

		function Body(InforSecurityService,
			LocalData,
			$state){
				
			 var vm = this;
			 
			 /* --
			 *  Trigger Lawson security plugin 
			 *  Temporary, this will be moved to correct Controller after completing all controllers
			 *  --*/
			//InforSecurityService.signIn();
			
			vm.sigout =  function(){InforSecurityService.signOut();}
			

			vm.doLogin = function(){
				//show loading icon
				/*$ionicLoading.show({
					templateUrl: CONSTANT.SpinnerTemplate
				});
				AuthService.doLogin()
				.then(function(value){
					$rootScope.Modules = LocalData.loadData("Modules");
					//alert("sad "+$rootScope.Modules.length);
					$ionicViewSwitcher.nextDirection('forward');
					$ionicLoading.hide();
					$state.go('app.welcome');
				});*/
			//	LocalData.saveData("SoapToken", "1001:MSCMADMIN");
				$state.go('app.welcome');
			};

		}
		
		
		
})();
