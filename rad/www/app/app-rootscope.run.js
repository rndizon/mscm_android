(function(){
 	'use strict';

	angular
		.module("app")
		.run(function(LocalData,CRUD,$cordovaToast,$ionicViewService,$ionicPlatform,$rootScope,ScannerManager,BarcodeParser,$ionicLoading,$ionicViewSwitcher, $ionicPopup) {
			
			$ionicPlatform.ready(function() {
			    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
			    // for form inputs)
			    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
			      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
			      cordova.plugins.Keyboard.disableScroll(true);
			    }
			    if (window.StatusBar) {
			      // org.apache.cordova.statusbar required
			      StatusBar.styleLightContent();
			    }
			 });

			 // Disable BACK button on home
			/*  $ionicPlatform.registerBackButtonAction(function(event) {
			    if (true) { // your check here
			      $ionicPopup.confirm({
			        title: 'System warning',
			        template: 'are you sure you want to exit the Application?'
			      }).then(function(res) {
			        if (res) {
			          ionic.Platform.exitApp();
			        }
			      })
			    }
			  }, 100); */


		  /* ------------------------
		   *  State Change Events 
		   * ------------------------ */
		  	$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){ 
		  		
				 LocalData.saveData("PrevStateName", fromState.name);
				    //  event.preventDefault(); 
				      // transitionTo() promise will be rejected with 
				      // a 'transition prevented' error
		  		//console.log(toState.name + " "+ function() {}; romState.name);
		  		var direction = null;
			    if (fromState.stateUpward === toState.name) direction = 'back';
			    if (toState.stateUpward === fromState.name) direction = 'forward';
			    if (direction) $ionicViewSwitcher.nextDirection(direction);


			
	            if (toState.changeColor) {
	                $rootScope.setNavColor = toState.changeColor;
	            } else {
	                $rootScope.setNavColor = false;
	            }

	            /*
					Temporary
	            */

	            LocalData.saveData('userID','u12665');
	            LocalData.saveData('token',"1039:u12665");

			    
					
			});
			$rootScope.$on('$stateNotFound',function(event, unfoundState, fromState, fromParams){ 
			    console.log(unfoundState.to); // "lazy.state"
			    console.log(unfoundState.toParams); // {a:1, b:2}
			    console.log(unfoundState.options); // {inherit:false} + default options
			    alert("Error state");
			});
			$rootScope.$on('$stateChangeSuccess',function(event, toState, toParams, fromState, fromParams){ 
			/*	ScannerManager.enableScanner(function(data){
					alert("ENABLED");
		       			alert(JSON.stringify(BarcodeParser.parseBarcode(data)));
					},function(data){ alert("ERROR A"+ data); }); */

				//console.log("SELECTED "+LocalStorageManagerService.loadData("DownloadedParformDetails").length)
	
			});
			
		});		
			
})();