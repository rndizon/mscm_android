(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('DockLoggingChangeCompanyController', Body);

			function Body($state,
					  $scope,
					  $ionicLoading,
					  $ionicViewSwitcher,
					  $ionicModal, 
					  $ionicHistory,
					  CRUD, 
					  LocalData,
					  CommonInfoService,
					  CONSTANT,
					  DockLogService) {

			var vm = this;
			vm.saveCompanySelected = saveCompanySelected;
			vm.clearSearch = clearSearch;
			vm.showCompanyList = showCompanyList;
			vm.doRefresh = doRefresh;

			vm.companyList = [];
			vm.selectedCompanyID = LocalData.loadData('dockLogCompanyID');
			vm.selectedCompanyDesc = LocalData.loadData('dockLogCompanyDesc');
			vm.currentUser = LocalData.loadData('userID');
			vm.userDef = LocalData.loadData('dockLogDef');
			vm.dockLog = [];




			$scope.$on('$ionicView.beforeEnter', function() {
				showCompanyList();

				getDockLog();


				console.log(vm.userDef);
			})

			function getDockLog() {
				CRUD.select("DockLog_Transaction","*").then(function(result){
					vm.dockLog = result;
				})
			}

			function doRefresh(){

				$ionicLoading.show({
					templateUrl: CONSTANT.SpinnerTemplate
				});

				DockLogService.getDocklogConfig().then(function(result){
					CRUD.remove("DockLog_Company").then(function(res){
						DockLogService.saveCompAndLocInSql(result).then(function(){
							
							vm.showCompanyList();

							vm.selectedCompanyID = LocalData.loadData('dockLogCompanyID');
							vm.selectedCompanyDesc = LocalData.loadData('dockLogCompanyDesc');
							
							$ionicLoading.hide();
							$scope.$broadcast('scroll.refreshComplete');
						})
					})

				});

			}

			function saveCompanySelected() {		

				var params = {};

				if(LocalData.loadData('dockLogCompanyID') != vm.selectedCompanyID) {

					if(vm.dockLog.length > 0) {
						CRUD.remove("DockLog_Transaction");
					}

					params.CompanyID = vm.selectedCompanyID;
					params.UserID = vm.currentUser;

					if(vm.selectedCompanyID == vm.userDef.companyID) {
						params.LocationID = vm.userDef.locationID;
					}

					CRUD.select("DockLog_Location","*",params).then(function(result){

						LocalData.saveData('dockLogLocationID',result[0].LocationID);
						LocalData.saveData('dockLogLocationName',result[0].LocationName);
						LocalData.saveData('dockLogLocationCode',result[0].LocationCode);
						
					})


				} 

				LocalData.saveData("dockLogCompanyID", vm.selectedCompanyID);

				Object.keys(vm.companyList).forEach(function(x){
					if(vm.companyList[x].CompanyID == vm.selectedCompanyID) {
						LocalData.saveData('dockLogCompanyDesc',vm.companyList[x].CompanyDesc);
						vm.selectedCompanyDesc = vm.companyList[x].CompanyDesc;
					}
				})
				
				$state.go('app.dock-log');

			};

			function showCompanyList() {
				var companyParams = {};
				
				companyParams.UserID = vm.currentUser;

				CRUD.select("DockLog_Company","*",companyParams)
				 .then(function(result){
		 		 	 	vm.companyList = result;
		 		});

			};



		    function clearSearch(){
				vm.searchText = '';
			}; 
		};
				
		
})();
