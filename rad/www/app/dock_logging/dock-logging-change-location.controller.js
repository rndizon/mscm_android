(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('DockLoggingChangeLocationController', Body);

			function Body($state,
					  $scope,
					  $ionicLoading,
					  $ionicViewSwitcher,
					  $ionicModal, 
					  $ionicHistory,
					  CRUD, 
					  LocalData,
					  CommonInfoService,
					  CONSTANT,
					  DockLogService) {

			var vm = this;
			vm.saveLocationSelected = saveLocationSelected;
			vm.clearSearch = clearSearch;
			vm.showLocationList = showLocationList;
			vm.doRefresh = doRefresh;
			vm.getDockLog = getDockLog;

			vm.selectedLocationID = LocalData.loadData('dockLogLocationID');
			vm.selectedLocationName = LocalData.loadData('dockLogLocationName');
			vm.selectedLocationCode = LocalData.loadData('dockLogLocationCode');
			vm.selectedCompanyID = LocalData.loadData('dockLogCompanyID');
			vm.locationList = [];



			$scope.$on('$ionicView.beforeEnter', function() {
				showLocationList();

				vm.getDockLog();
			})

			function doRefresh(){

				$ionicLoading.show({
					templateUrl: CONSTANT.SpinnerTemplate
				});

				DockLogService.getDocklogConfig().then(function(result){

					CRUD.inlineQuery("DELETE FROM DockLog_Location WHERE CompanyID = '" + vm.selectedCompanyID  + "'").then(function(){

						DockLogService.saveLocationInSql(result).then(function(){

							vm.showLocationList();
							$ionicLoading.hide();
							$scope.$broadcast('scroll.refreshComplete');
						});
					})

				})


			}

			function getDockLog() {
				CRUD.select("DockLog_Transaction","*").then(function(result){
					vm.dockLog = result;
				})
			}

			function saveLocationSelected() {			

				if(LocalData.loadData('dockLogLocationID') != vm.selectedLocationID && vm.dockLog.length > 0) {
					CRUD.remove("DockLog_Transaction");
				}

				LocalData.saveData("dockLogLocationID", vm.selectedLocationID);

				Object.keys(vm.locationList).forEach(function(x){
					if(vm.locationList[x].LocationID == vm.selectedLocationID) {
						LocalData.saveData('dockLogLocationName',vm.locationList[x].LocationName);
						LocalData.saveData('dockLogLocationCode',vm.locationList[x].LocationCode);

						vm.selectedLocationCode = vm.locationList[x].LocationCode;
						vm.selectedLocationName = vm.locationList[x].LocationName;
					}
				})

				$state.go('app.dock-log');
			};

			function showLocationList() {

				var locationParam = {};

				locationParam.CompanyID = vm.selectedCompanyID;

				CRUD.select("DockLog_Location","*",locationParam)
				 .then(function(result){
		 		 	 	vm.locationList = result;
		 		});

			};



		    function clearSearch(){
				vm.searchText = '';
			}; 
		};
				
		
})();
