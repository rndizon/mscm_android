(function(){
    'use strict';
	 
	angular
	    .module('app')
	    .factory('DockLogService',Body);

	    function Body($q,
	    			  CRUD,
	    			  LocalData,
	    			  CM
	    	) 
	    {
		  
		    var service = {
		        getDocklogConfig: getDocklogConfig,

		        saveLocationInSql: saveLocationInSql,
		        saveCompAndLocInSql: saveCompAndLocInSql,

		        getDockLogCompanyCarriers:getDockLogCompanyCarriers,
		        saveAllCarriersInSql: saveAllCarriersInSql,
		        saveCarriersInSql: saveCarriersInSql, 

		        getPrinter: getPrinter,
		        saveAllPrintersInSql: saveAllPrintersInSql,
		        savePrinterInSql: savePrinterInSql,

		        sendDockLog: sendDockLog
		    };
		    
		    var requestToken = LocalData.loadData('token');
		    var userID = LocalData.loadData('userID');

		    return service;  	
		 	
		 	function getDocklogConfig() {
				var args = {
		 			URL:    "ReceivingService",
		 			Action: "getDocklogConfig",
		 			Params: {"requestString":"<getdocklogconfigresponse></getdocklogconfigresponse>","token":requestToken}
		 		};

		 		return CM.doCommsCall("soap",args);
		 	}

		 	function getDockLogCompanyCarriers(companyID) {
				var args = {
		 			URL:    "ReceivingService",
		 			Action: "getDockLogCompanyCarriers",
		 			Params: {"requestString":"<getdocklogcompanycarriers><facility id='" + companyID + "' /></getdocklogcompanycarriers>","token":requestToken}
		 		};


		 		return CM.doCommsCall("soap",args);
		 	}

		 	function getPrinter(_companyID,_locationID) {

		 		/*
					get unique user id from token of Printer
		 		*/

		 		var splitedToken = requestToken.split(":");

				var args = {
		 			URL:    "ReceivingService",
		 			Action: "getPrinters",
		 			Params: {"userID":splitedToken[0],"company":_companyID,"location":_locationID}
		 		};


		 		return CM.doCommsCall("soap",args);
		 	}

		 	function sendDockLog(requestString) {


				var args = {
		 			URL:    "ReceivingService",
		 			Action: "sendDocklog",
		 			Params: {"requestString":requestString,"token":requestToken}
		 		};


		 		return CM.doCommsCall("soap",args);
		 	}

		 	function savePrinterInSql(result) {
				var deffered = $q.defer();
	    		var promises = [];

				var printerBatch = "VALUES";	
				var value = xmlToJson(result);
				var xmlResponse = value.root.body.getPrintersResponse.getPrintersReturn;
				var object = ReturnDataObject(xmlResponse);
				var printersObj = object.getprinters.printer;
				var printers = [];

				var companyID = LocalData.loadData('dockLogCompanyID');



				if(printersObj instanceof Array) {
					printers = printersObj;
				} else {
					printers.push(printersObj);
				}


				Object.keys(printers).forEach(function(index){

					if(printers[index].attributes.type == "R") {
						printerBatch += "('" + companyID + "','" + printers[index].attributes.id + "','" + userID + "','" + printers[index].attributes.name + "','',''),";
					}
				});

				CRUD.inlineQuery("INSERT INTO DockLog_Printer (CompanyID,PrinterID,UserID,PrinterName,isUserDefaults,isCompanyDefaults) " + printerBatch.slice(0,-1)).then(function(res){
					deffered.resolve(res);
				}); 


			 	promises.push(deffered.promise);			 	
				return $q.all(promises);

		 	}


		 	function saveAllPrintersInSql() {
				var printerArr = [];
				var printerObj = {};
				var deffered = $q.defer();
	    		var promises = [];

				CRUD.select("DockLog_Location","*").then(function(locations){

					
					Object.keys(locations).forEach(function(ind){

						
						getPrinter(locations[ind].CompanyID,locations[ind].LocationID).then(function(result){

							var printerBatch = "VALUES";	
				 			var value = xmlToJson(result);
							var xmlResponse = value.root.body.getPrintersResponse.getPrintersReturn;
							var object = ReturnDataObject(xmlResponse);
							var printersObj = object.getprinters.printer;
							var printers = [];

							if(printersObj) {
								if(printersObj instanceof Array) {
									printers = printersObj;
								} else {
									printers.push(printersObj);
								}



								Object.keys(printers).forEach(function(ind2){
									if(printers[ind2].attributes.type == "R") {
										var params = {};
										params.CompanyID = locations[ind].CompanyID;
										params.PrinterID = printers[ind2].attributes.id;

										/*
											Check if printer Exist
										*/
										CRUD.select("DockLog_Printer","*",params).then(function(dockLogRes){

											if(dockLogRes.length == 0) {
												printerBatch += "('" + locations[ind].CompanyID + "','" + printers[ind2].attributes.id + "','" + userID  + "','" + printers[ind2].attributes.name + "','',''),";
											}

											if(printerBatch != "VALUES") {
												CRUD.inlineQuery("INSERT INTO DockLog_Printer (CompanyID,PrinterID,UserID,PrinterName,isUserDefaults,isCompanyDefaults) " + printerBatch.slice(0,-1)).then(function(res){
													deffered.resolve(res);
												});
											}

										})

									}


								});

							}


						});
							
					}); 





				});

			  	
			  	promises.push(deffered.promise);
				
				return $q.all(promises);
		 	}

		 	function saveCarriersInSql(result){
				var deffered = $q.defer();
	    		var promises = [];

	 			var value = xmlToJson(result);
				var xmlResponse = value.root.body.getDockLogCompanyCarriersResponse.getDockLogCompanyCarriersReturn;
				var object = ReturnDataObject(xmlResponse);
				var carrierObj = object.getdocklogcompanycarriersresponse.carrier;
				var carrier = [];
				var trackFormat = [];
				var carrierBatch = "VALUES";
				var companyID = LocalData.loadData('dockLogCompanyID');


				if(carrierObj instanceof Array) {
					carrier = carrierObj;
				} else {
					carrier.push(carrierObj)
				}	


 				Object.keys(carrier).forEach(function(ind2){
 					carrierBatch += "('" + companyID + "','" + carrier[ind2].attributes.id  + "','" + carrier[ind2].attributes.code  + "','" + carrier[ind2].attributes.name  + "'),";


					if(carrier[ind2].trackformat instanceof Array) {
						trackFormat = carrier[ind2].trackformat;
					} else {
						trackFormat.push(carrier[ind2].trackformat);
					}		

					var trackFormatBatch  = "VALUES";
					Object.keys(trackFormat).forEach(function(ind3){

						var startsWith =  trackFormat[ind3].attributes.startswith || "";
						var startsPos = trackFormat[ind3].attributes.startpos || "";
						var endPos = trackFormat[ind3].attributes.endpos || "";
						var trackLen =  trackFormat[ind3].attributes.length || "";

						trackFormatBatch += "('" + carrier[ind2].attributes.code + "','" + trackLen + "','" + startsWith + "','" + startsPos + "','" + endPos + "'),";
					})  

					CRUD.inlineQuery("INSERT INTO DockLog_TrackFormat (VendorID,TrackFormatLength,TrackFormatStartsWith,TrackFormatStartPos,TrackFormatEndPos)" + trackFormatBatch.slice(0,-1)).then(function(res){
						deffered.resolve(res);
					}) 

 				});


				CRUD.inlineQuery("INSERT INTO DockLog_Vendor (CompanyID,VendorID,VendorCode,VendorName) " + carrierBatch.slice(0,-1)).then(function(res){
					deffered.resolve(res);
				}); 



			  	promises.push(deffered.promise);
				return $q.all(promises);
		 	}

		 	function saveAllCarriersInSql() {

				var deffered = $q.defer();
	    		var promises = [];
		 		/*
					Save carrier and track format in sql
		 		*/

		 		CRUD.select("DockLog_Company","*").then(function(companies){

			 		Object.keys(companies).forEach(function(ind){		 		

						getDockLogCompanyCarriers(companies[ind].CompanyID).then(function(result){
				 			var value = xmlToJson(result);
							var xmlResponse = value.root.body.getDockLogCompanyCarriersResponse.getDockLogCompanyCarriersReturn;
							var object = ReturnDataObject(xmlResponse);
							var carrier = object.getdocklogcompanycarriersresponse.carrier;

					 		if(carrier) {

		 						var carrierBatch = "VALUES";
		 						var trackFormatBatch = "VALUES";
					 			if(carrier instanceof Array) {

					 				Object.keys(carrier).forEach(function(ind2){
					 					carrierBatch += "('" + companies[ind].CompanyID  + "','" + carrier[ind2].attributes.id  + "','" + carrier[ind2].attributes.code  + "','" + carrier[ind2].attributes.name  + "'),";

					 					if(carrier[ind2].trackformat instanceof Array) {
					 						Object.keys(carrier[ind2].trackformat).forEach(function(ind3){
					 							trackFormatBatch += "('" + carrier[ind2].attributes.code + "','" + carrier[ind2].trackformat[ind3].attributes.length + "','" + carrier[ind2].trackformat[ind3].attributes.startswith + "','" + carrier[ind2].trackformat[ind3].attributes.startpos + "','" + carrier[ind2].trackformat[ind3].attributes.endpos + "'),";

					 						})
					 					} else {

				 							trackFormatBatch += "('" + carrier[ind2].attributes.code + "','" + carrier[ind2].trackformat.attributes.length + "','" + carrier[ind2].trackformat.attributes.startswith + "','" + carrier[ind2].trackformat.attributes.startpos + "','" + carrier[ind2].trackformat.attributes.endpos + "'),";

					 					}

					 				});

					 			} else {

					 				carrierBatch += "('" + companies[ind].CompanyID  + "','" + carrier.attributes.id  + "','" + carrier.attributes.code  + "','" + carrier.attributes.name  + "'),";


					 					if(carrier.trackformat instanceof Array) {
					 						Object.keys(carrier.trackformat).forEach(function(ind4){
					 							trackFormatBatch += "('" + carrier.attributes.code + "','" + carrier.trackformat[ind4].attributes.length + "','" + carrier.trackformat[ind4].attributes.startswith + "','" + carrier.trackformat[ind4].attributes.startpos + "','" + carrier.trackformat[ind4].attributes.endpos + "'),";

					 						})
					 					} else {
					 						
				 							trackFormatBatch += "('" + carrier[ind2].attributes.code + "','" + carrier[ind2].trackformat.attributes.length + "','" + carrier[ind2].trackformat.attributes.startswith + "','" + carrier[ind2].trackformat.attributes.startpos + "','" + carrier[ind2].trackformat.attributes.endpos + "'),";

					 					}
					 				
					 			}

					 			CRUD.inlineQuery("INSERT INTO DockLog_TrackFormat (VendorID,TrackFormatLength,TrackFormatStartsWith,TrackFormatStartPos,TrackFormatEndPos)" + trackFormatBatch.slice(0,-1)).then(function(res){
					 				deffered.resolve(res);
					 			})
					 			trackFormatBatch = "";

								CRUD.inlineQuery("INSERT INTO DockLog_Vendor (CompanyID,VendorID,VendorCode,VendorName) " + carrierBatch.slice(0,-1)).then(function(res){
									deffered.resolve(res);
								});
								carrierBatch = "";

					 		} 
						})		 

			 		});

		 		})
					

			  	promises.push(deffered.promise);
				
				return $q.all(promises);
		 	}

		 	function saveLocationInSql(result){

				var deffered = $q.defer();
	    		var promises = [];	
		 		var value = xmlToJson(result);
				var xmlResponse = value.root.body.getDocklogConfigResponse.getDocklogConfigReturn;
				var object = ReturnDataObject(xmlResponse);	
				var companyID = LocalData.loadData('dockLogCompanyID');

				var facility = object.getdocklogconfigresponse.docklogconfig.facility;		
				var locationBatch = "VALUES";

				Object.keys(facility).forEach(function(facIndex){
					if(facility[facIndex].attributes.id == companyID) {
						var location = facility[facIndex].rloc;

						Object.keys(location).forEach(function(locIndex){
							var locCode = location[locIndex].attributes.code;
							var locName = location[locIndex].attributes.descr;
							var locID = location[locIndex].attributes.id;

							locationBatch += "('" + companyID + "','" + userID + "','" + locID + "','" + locName + "','" + locCode + "'),";
						})


						CRUD.inlineQuery("INSERT INTO DockLog_Location ('CompanyID','UserID','LocationID','LocationName','LocationCode')" + locationBatch.slice(0,-1)).then(function(res){
							deffered.resolve(res);
						});
					}
				})



			  	promises.push(deffered.promise);
				
				return $q.all(promises);
		 	}


		    function saveCompAndLocInSql (result) {

				var deffered = $q.defer();
	    		var promises = [];

		 		var value = xmlToJson(result);
				var xmlResponse = value.root.body.getDocklogConfigResponse.getDocklogConfigReturn;
				var object = ReturnDataObject(xmlResponse);	

				var userDefLoc = object.getdocklogconfigresponse.docklogconfig.attributes.userdefloc;
				var autoPrint = object.getdocklogconfigresponse.docklogconfig.attributes.autoprint;
				var facility = object.getdocklogconfigresponse.docklogconfig.facility;		

				var companyBatch = "VALUES";
				var locationBatch = "VALUES";
				var defLocId,defLocDesc,defLocCode,defComID,defComDesc;


				Object.keys(facility).forEach(function(ind){

					companyBatch += "('" + userID + "','" + facility[ind].attributes.id + "','" + facility[ind].attributes.descr + "'),";
					
				  	if(facility[ind].rloc instanceof Array){

						Object.keys(facility[ind].rloc).forEach(function(ind2){
							locationBatch += "('" + facility[ind].attributes.id + "','" + userID + "','" + facility[ind].rloc[ind2].attributes.id + "','" + facility[ind].rloc[ind2].attributes.descr + "','" + facility[ind].rloc[ind2].attributes.code + "'),";

							if(facility[ind].rloc[ind2].attributes.id == userDefLoc) {
								defLocDesc = facility[ind].rloc[ind2].attributes.descr;
								defLocCode = facility[ind].rloc[ind2].attributes.code;
								defComID = facility[ind].attributes.id;
								defComDesc = facility[ind].attributes.descr;

							}
						});

					}else{

						locationBatch += "('" + facility[ind].attributes.id + "','" + userID + "','" + facility[ind].rloc.attributes.id + "','" + facility[ind].rloc.attributes.descr + "','" + facility[ind].rloc.attributes.code + "'),";

						if(facility[ind].rloc.attributes.id == userDefLoc) {
							defLocDesc = facility[ind].rloc.attributes.descr;
							defLocCode = facility[ind].rloc.attributes.code;
							defComID = facility[ind].attributes.id;
							defComDesc = facility[ind].attributes.descr;
						}
					}

				});

				/*
					If user has a default location
				*/
				if(userDefLoc) {
					defLocId = userDefLoc;

			 		LocalData.saveData('dockLogCompanyID',defComID);
			 		LocalData.saveData('dockLogCompanyDesc',defComDesc);

			 		LocalData.saveData('dockLogLocationID',defLocId);
			 		LocalData.saveData('dockLogLocationName',defLocDesc);
			 		LocalData.saveData('dockLogLocationCode',defLocCode);

					LocalData.saveData('dockLogDef',{companyID: defComID,locationID: userDefLoc});

				} else {

					if(facility[0].rloc instanceof Array) {

						defLocId = facility[0].rloc[0].attributes.id;
						defLocDesc = facility[0].rloc[0].attributes.descr;
						defLocCode = facility[0].rloc[0].attributes.code;
					} else {

						defLocId = facility[0].rloc.attributes.id;
						defLocDesc = facility[0].rloc.attributes.descr;
						defLocCode = facility[0].rloc.attributes.code;
					}


			 		LocalData.saveData('dockLogCompanyID',facility[0].attributes.id);
			 		LocalData.saveData('dockLogCompanyDesc',facility[0].attributes.descr);

			 		LocalData.saveData('dockLogLocationID',defLocId);
			 		LocalData.saveData('dockLogLocationName',defLocDesc);
			 		LocalData.saveData('dockLogLocationCode',defLocCode);

				}

				LocalData.saveData('dockLogAutoPrint',autoPrint);

				/*
					Save Default Company and Location
				*/

				CRUD.remove("DockLog_Company");
				CRUD.inlineQuery("INSERT INTO DockLog_Company (UserID,CompanyID,CompanyDesc) " + companyBatch.slice(0,-1)).then(function(res){
					deffered.resolve(res);
				});

				CRUD.remove("DockLog_Location");
				CRUD.inlineQuery("INSERT INTO DockLog_Location (CompanyID,UserID,LocationID,LocationName,LocationCode) " + locationBatch.slice(0,-1)).then(function(res){
					deffered.resolve(res);
				});
				
		  		

			  	promises.push(deffered.promise);
				
				return $q.all(promises);

		    }


		}
})();

