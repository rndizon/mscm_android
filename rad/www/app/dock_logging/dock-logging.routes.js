(function(){
 	'use strict';
 	
		angular
		    .module('app')
			.config(function($stateProvider, $urlRouterProvider) {
		     
		     $stateProvider
		      .state("app.dock-log", {
		      	cache: false,
		        changeColor: 'bar-ruby-5',
			    url: "/dock-log", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/dock_logging/dock-log.html",
					      controller: "DockLoggingController as vm"
					 }
				  }
			      
		      })

		      .state("app.print-dock-log", {
		      	cache: false,
		        changeColor: 'bar-ruby-5',
			    url: "/print-dock-log", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/dock_logging/print-dock-log.html",
					      controller: "PrintDockLogController as vm"
					 }
				  }
		      })

		     .state("app.dock-log-change-vendor", {
		      	cache: false,
		        changeColor: 'bar-ruby-5',
			    url: "/dock-log-change-vendor", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/dock_logging/vendor-list.html",
					      controller: "DockLoggingChangeVendorController as vm"
					 }
				  }
		      }) 

		      .state("app.dock-log-change-company", {
		      	cache: false,
		        changeColor: 'bar-ruby-5',
			    url: "/dock-log-change-company", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/templates/company-change-view.html",
					      controller: "DockLoggingChangeCompanyController as vm"
					 }
				  }
		      })

		      .state("app.dock-log-change-location", {
		      	cache: false,
		        changeColor: 'bar-ruby-5',
			    url: "/dock-log-change-location", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/templates/location-change-view.html",
					      controller: "DockLoggingChangeLocationController as vm"
					 }
				  }
		      })

		      .state("app.dock-log-change-printer", {
		      	cache: false,
		        changeColor: 'bar-ruby-5',
			    url: "/dock-log-change-printer", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/templates/printer-change-view.html",
					      controller: "DockLoggingChangePrinterController as vm"
					 }
				  }
		      })

		     });
		     
})();