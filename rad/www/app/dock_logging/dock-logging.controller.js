(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('DockLoggingController', Body);

			function Body(
				$state,$scope,
				filterFilter,$ionicPopover,
				$ionicViewSwitcher,LocalData,
				CRUD,CommonInfoService,
				$ionicPopup,CarrierParser,
				DockLogService,
				ScannerManager,BarcodeDataManager,
				$ionicLoading,CONSTANT) {

			var vm = this;
			vm.goTo = goTo;
			vm.clearAll	= clearAll; 
			vm.selectAll = selectAll; 
			vm.checkboxClicked = checkboxClicked;
			vm.dockLoggingOption = dockLoggingOption;	
			vm.addDockLog = addDockLog;
			vm.deleteDockLog = deleteDockLog;
			vm.print = print;
			vm.send = send;
			vm.getDockLog = getDockLog;
			vm.setFirstVendorAsDef = setFirstVendorAsDef;
			vm.changeCompanyOrLoc = changeCompanyOrLoc;
			vm.searchCompany = searchCompany;

			vm.totalSelected = 0;
			vm.dockLog	= [];
			vm.vendorList = [];
			vm.currentUser = LocalData.loadData('userID');
			vm.date = new Date();
			



			ScannerManager.enableScanner(function(data){
			

       			var	locationObj = BarcodeDataManager.ScanBarCode(data,'location');

       			
				vm.searchCompany(locationObj.companyID);

			},function(data){ alert("ERROR A"+ data); });


		    $scope.$on('$ionicView.beforeEnter', function() {


		    	if(!LocalData.loadData("dockLogSoapCall")) {

					$ionicLoading.show({
						templateUrl: CONSTANT.SpinnerTemplate
					});

					
					DockLogService.getDocklogConfig().then(function(result){
						DockLogService.saveCompAndLocInSql(result).then(function(){

							vm.selectedCompanyID = LocalData.loadData('dockLogCompanyID');
							vm.selectedCompanyDesc = LocalData.loadData('dockLogCompanyDesc');
							vm.selectedLocationID = LocalData.loadData('dockLogLocationID');
							vm.selectedLocationName = LocalData.loadData('dockLogLocationName');
							vm.selectedLocationCode = LocalData.loadData('dockLogLocationCode');
							vm.dockLogAutoPrint = LocalData.loadData('dockLogAutoPrint');

							DockLogService.saveAllCarriersInSql().then(function(){

								DockLogService.saveAllPrintersInSql().then(function(){
								});
								
									$ionicLoading.hide();
									LocalData.saveData("dockLogSoapCall","1");
							});

						});
					});
				}  else {
					
					vm.vendorID = LocalData.loadData('docklogVendorID');
					vm.vendorName = LocalData.loadData('dockLogVendorName');
					vm.printerName = LocalData.loadData('dockLogPrinterName');
					vm.selectedCompanyID = LocalData.loadData('dockLogCompanyID');
					vm.selectedCompanyDesc = LocalData.loadData('dockLogCompanyDesc');
					vm.selectedLocationID = LocalData.loadData('dockLogLocationID');
					vm.selectedLocationName = LocalData.loadData('dockLogLocationName');
					vm.selectedLocationCode = LocalData.loadData('dockLogLocationCode');
					vm.dockLogAutoPrint = LocalData.loadData('dockLogAutoPrint');
				}



				vm.getDockLog();
		    })

			 $scope.$on('$ionicView.afterEnter', function() {
			 	

			 })

			function searchCompany(_companyID) {

				var params = {}

				params.CompanyID = _companyID

				CRUD.select("DockLog_Company","*",params).then(function(result){
					console.log(result);
				})
			}


			function print() {

				console.log('Print');

				vm.goTo('app.print-dock-log','slideUp');

			}

			function send() {
				console.log('Send');

				vm.goTo('app.print-dock-log','slideUp');
			}

			function setFirstVendorAsDef() {
				var vendorParams = {};
				
				vendorParams.LocationID = vm.selectedLocationID;

				CRUD.select("DockLog_Vendor","*",vendorParams)
				 .then(function(result){

			 		LocalData.saveData('docklogVendorID',result[0].VendorID);
			 		LocalData.saveData('dockLogVendorName',result[0].VendorName);
			 		
		 		 	 vm.vendorID = result[0].VendorID
		 		 	 vm.vendorName = result[0].VendorName;
		 		});
			}

			function getDockLog() {

				CRUD.inlineQuery("SELECT * FROM DockLog_Transaction ORDER BY UID Desc")
					.then(function(result){
						vm.dockLog = result;
					})

			}


			function addDockLog() {

				var carrierDetails = CarrierParser.getData(vm.modTrackingNum);

				var transactionParams = {};

				var trackingNumVal = "",carrierNameVal = "";

				if(carrierDetails.name) {
					trackingNumVal = carrierDetails.tracking;
					carrierNameVal = carrierDetails.name;
				} else {
					trackingNumVal = vm.modTrackingNum;
					carrierNameVal = '-';
				}

				// Check if tracking is already exist
				var foundInArr = CommonInfoService.findInArrayObject(vm.dockLog,"TrackingNumber",trackingNumVal);
				if(foundInArr > -1) {
					
					vm.popUpMsg = 'tracking-exist';
					var conf = CommonInfoService.confirmPopUp($scope);

					conf.then(function(res){
						if(res) {

							/*
								Update Array
							*/
							vm.dockLog.splice(foundInArr,1);
							/*vm.dockLog.unshift({
								TrackingNumber: trackingNumVal,
								CarrierName: carrierNameVal,
								Timestamp: (vm.date.getTime() / 1000)
							});  */


						/*	CRUD.inlineQuery("UPDATE DockLog_Transaction SET CarrierName = '" + carrierNameVal + "',Timestamp = '"
										 + (vm.date.getTime() / 1000) + "' WHERE TrackingNumber = '" + trackingNumVal + "'").then(function(result){
								console.log(result);
							}) */

							CRUD.inlineQuery("DELETE FROM DockLog_Transaction WHERE TrackingNumber = '" + trackingNumVal + "'").then(function(result){
								console.log(result);
							}) 
							
							vm.modTrackingNum = "";
						}

					})	

				} else {

					vm.dockLog.unshift({
						TrackingNumber: trackingNumVal,
						CarrierName: carrierNameVal,
						Timestamp: (vm.date.getTime() / 1000)
					});

					transactionParams.TrackingNumber = trackingNumVal;
					transactionParams.Timestamp = (vm.date.getTime() / 1000);
					transactionParams.CarrierName = carrierNameVal;

					CRUD.insert('DockLog_Transaction',transactionParams).then(function(){
						console.log('New transaction added');
					})

					vm.modTrackingNum = "";
				}
		
			}


			function deleteDockLog() {
				vm.selectedDockLog  = filterFilter( vm.dockLog, {val:true});
				var foundInArr;

				Object.keys(vm.selectedDockLog).forEach(function(x){

					foundInArr = CommonInfoService.findInArrayObject(vm.dockLog,"TrackingNumber",vm.selectedDockLog[x].TrackingNumber);
					
					// Delete in array
					vm.dockLog.splice(foundInArr,1);
					// Delete in Database

					CRUD.inlineQuery('DELETE FROM DockLog_Transaction WHERE TrackingNumber = "' + vm.selectedDockLog[x].TrackingNumber + '"').then(function(){
						console.log("Tracking Deleted");
					})

				});

				vm.clearAll();
				vm.showOptionToDelete = false;
			}

			function checkboxClicked() {
				vm.selectedDockLog  = filterFilter( vm.dockLog, {val:true});

				if(vm.selectedDockLog.length > 0) {
					vm.showOptionToDelete = true;
				} else {
					vm.showOptionToDelete = false;
				}

				vm.totalSelected = vm.selectedDockLog.length;
				showSelectAll();
			}

			function showSelectAll() {
				if(vm.selectedDockLog.length != vm.dockLog.length) {
					vm.showSelectAll = true;
				} else {
					vm.showSelectAll = false;
				}
			}


			function clearAll() {

			    for(var x = 0; x < vm.dockLog.length; x++) {
			    	vm.dockLog[x].val = false;
			    }

			    vm.checkboxClicked()
			}

			function selectAll() {

			    for(var x = 0; x < vm.dockLog.length; x++) {
			    	vm.dockLog[x].val = true;
			    }

			    vm.checkboxClicked();
			}

			function goTo($url,$navDirection) {

				if(vm.popover) {
					vm.popover.hide();
				}

				if($navDirection) {
		   			$ionicViewSwitcher.nextDirection($navDirection);
				}

				$state.go($url);
			}


	    	function dockLoggingOption($event,$popupType) {
		    	$ionicPopover.fromTemplateUrl('html/templates/option-more.html', {
				        scope: $scope
				}).then(function(popover) {
				        vm.popover = popover;

			       		vm.popUpType = ($popupType ? $popupType : 'dock-logging');

				        popover.show($event)
				}); 
	    	};


	    	function changeCompanyOrLoc(_url) {

	    		if(vm.dockLog.length > 0) {

	    			if(_url == "app.dock-log-change-company") {
						vm.popUpMsg = 'com-clear-tracking';
	    			} else {
						vm.popUpMsg = 'loc-clear-tracking';
	    			}


					var conf = CommonInfoService.confirmPopUp($scope);

					conf.then(function(res){
						if(res) {
							$state.go(_url);
						}
					})
	    		}  else  {
					$state.go(_url);
	    		} 
	    	}
		};
				
		
})();
