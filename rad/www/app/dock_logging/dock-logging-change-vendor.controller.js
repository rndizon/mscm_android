(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('DockLoggingChangeVendorController', Body);

			function Body($state,
					  $scope,
					  $ionicLoading,
					  $ionicViewSwitcher,
					  $ionicModal, 
					  $ionicHistory,
					  CRUD, 
					  LocalData,
					  CONSTANT,
					  DockLogService) {

			var vm = this;
			vm.clearSearch = clearSearch;
			vm.getVendorList = getVendorList;
			vm.saveVendor = saveVendor;
			vm.doRefresh = doRefresh;
			vm.getVendorList = getVendorList;

			vm.vendorList = [];
			vm.selectedCompanyID = LocalData.loadData('dockLogCompanyID')
			vm.selectedLocationID = LocalData.loadData('dockLogLocationID');
			vm.selectedVendorID = LocalData.loadData('dockLogVendorID');
			vm.selectedVendorName = LocalData.loadData('dockLogVendorName');
			vm.selectedVendorCode = LocalData.loadData('dockLogVendorCode');
			vm.searchText = "";



		    $scope.$on('$ionicView.beforeEnter', function() {

				getVendorList();
		    })

			function doRefresh(){

				$ionicLoading.show({
					templateUrl: CONSTANT.SpinnerTemplate
				});

				DockLogService.getDockLogCompanyCarriers(vm.selectedCompanyID).then(function(result){
					var params = {};
					params.CompanyID = vm.selectedCompanyID;

					CRUD.select("DockLog_Vendor","*",params).then(function(vendorRes){
						Object.keys(vendorRes).forEach(function(ind){
							CRUD.inlineQuery("DELETE FROM DockLog_TrackFormat WHERE VendorID = '" + vendorRes[ind].VendorID + "'");
						});  
					})

					CRUD.inlineQuery('DELETE FROM DockLog_Vendor WHERE CompanyID = "' + vm.selectedCompanyID  + '"').then(function(){
						DockLogService.saveCarriersInSql(result).then(function(res){
							console.log(res);
							console.log('res');

							vm.getVendorList();
							
							$ionicLoading.hide();
							$scope.$broadcast('scroll.refreshComplete');
						}) 

					})



				})



			}


			function saveVendor() {				

				LocalData.saveData("dockLogVendorID", vm.selectedVendorID);

				Object.keys(vm.vendorList).forEach(function(x){
					if(vm.vendorList[x].VendorID == vm.selectedVendorID) {
						LocalData.saveData('dockLogVendorName',vm.vendorList[x].VendorName);
						LocalData.saveData('dockLogVendorCode',vm.vendorList[x].VendorCode);

						vm.selectedVendorCode = vm.vendorList[x].VendorCode;
						vm.selectedVendorName = vm.vendorList[x].VendorName;
					}
				})

				$state.go('app.dock-log');
			}; 

			function getVendorList() {
				var vendorParams = {};
				
				vendorParams.CompanyID = vm.selectedCompanyID;

				CRUD.select("DockLog_Vendor","*",vendorParams)
				 .then(function(result){
		 		 	 	vm.vendorList = result;
		 		});

			};



		    function clearSearch(){
				vm.searchText = '';
			}; 
		};
				
		
})();
