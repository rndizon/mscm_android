(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('PrintDockLogController', Body);

			function Body($scope,
						$state,$ionicViewSwitcher,
						CRUD,DockLogService,
						LocalData,$q,$filter) {

			var vm = this;
			vm.goTo = goTo;	
			vm.getDockLog = getDockLog;
			vm.generateRequestString = generateRequestString;

			vm.date = new Date();
			vm.dockLog	= [];
			vm.dockLogLocationID = LocalData.loadData('dockLogLocationID');
			vm.dockLogVendor = LocalData.loadData('dockLogVendorName');
			vm.userID = LocalData.loadData('userID');
			vm.companyID = LocalData.loadData('dockLogCompanyID');
			vm.locationID = LocalData.loadData('dockLogLocationID');
			vm.vendorID = LocalData.loadData('dockLogVendorID');

			


		    $scope.$on('$ionicView.beforeEnter', function() {
		    	vm.getDockLog().then(function(){

		    		DockLogService.sendDockLog(vm.generateRequestString()).then(function(result){

			 			var value = xmlToJson(result);
						var xmlResponse = value.root.body.sendDocklogResponse.sendDocklogReturn;
						var object = ReturnDataObject(xmlResponse);


		    			var responseDate = object.senddocklogresponse.return_success.attributes.date;
		    			var responseTime = object.senddocklogresponse.return_success.attributes.time;

		    			console.log(responseTime + responseDate);
		    			var params = {};

		    			params.UID = "";
		    			params.UserID = vm.userID;	
		    			params.Timestamp = (vm.date.getTime() / 1000);
		    			params.CompanyID = vm.companyID;
		    			params.LocationID = vm.locationID;
		    			params.CarrierID = vm.vendorID;
		    			params.CountedPackages = vm.dockLog.length;

		    			CRUD.insert("DockLog_TransHistory",params).then(function(result){
		    				if(result.rowsAffected > 0) {

		    					CRUD.remove("DockLog_Transaction").then(function(res){
		    						
		    					});


		    				}

		    			})

		    		});
		    	});

		    })	

		    function generateRequestString() {

				var date = new Date(vm.dockLog[0].Timestamp * 1000);
		    	var returnString;

				returnString =  '<senddocklog>'
				returnString += 	'<docklog locid="' + vm.dockLogLocationID + '" carrierid="1147" timestart="' + $filter('date')(date, 'MM/dd/yy hh:mm:ss a') + '">'; 

				Object.keys(vm.dockLog).forEach(function(ind){

					returnString +=	'<trackingno trackingno="' + vm.dockLog[ind].TrackingNumber + '"/>';

				})	

				returnString +=		'</docklog>';
				returnString +=		'<printer>';
				returnString +=			'<document id="6"/>';
				returnString +=		'</printer>';
				returnString +=	'</senddocklog> ';



				return returnString;
		    }

			function getDockLog() {
				var deffered = $q.defer();
				var promises = []

				CRUD.inlineQuery("SELECT * FROM DockLog_Transaction ORDER BY UID Desc")
				 .then(function(result){
				 	deffered.resolve(result);
				 	vm.dockLog = result;
		 		});

			  	promises.push(deffered.promise);
				
				return $q.all(promises);
			}

			function goTo(_url,_navDirection) {

				if(vm.popover) {
					vm.popover.hide();
				}

				if(_navDirection) {
		   			$ionicViewSwitcher.nextDirection(_navDirection);
				}

				$state.go(_url);
			}
		};
				
		
})();
