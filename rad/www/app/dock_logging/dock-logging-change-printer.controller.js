(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('DockLoggingChangePrinterController', Body);

			function Body($state,
					  $scope,
					  CRUD, 
					  LocalData,
					  CONSTANT,
					  DockLogService,
					  $ionicLoading) {

			var vm = this;
			vm.savePrinter = savePrinter;
			vm.clearSearch = clearSearch;
			vm.getPrinter = getPrinter;
			vm.doRefresh = doRefresh;

			vm.companyID = LocalData.loadData('dockLogCompanyID');
			vm.locationID = LocalData.loadData('dockLogLocationID');
			vm.userID = LocalData.loadData('userID');
			vm.selectedPrinterID = LocalData.loadData('dockLogPrinterID');
			vm.selectedPrinterName = LocalData.loadData('dockLogPrinterName');
			vm.printerList = [];


		    $scope.$on('$ionicView.beforeEnter', function() {

				vm.getPrinter();

				var params = {};
				params.CompanyID = vm.companyID;

				console.log(vm.companyID);
				CRUD.select("DockLog_Printer","*",params).then(function(result){
					console.log(result)
				})
		    })

			function doRefresh(){

				$ionicLoading.show({
					templateUrl: CONSTANT.SpinnerTemplate
				});


				DockLogService.getPrinter(vm.companyID,vm.locationID).then(function(result){


					CRUD.inlineQuery("DELETE FROM DockLog_Printer WHERE CompanyID = '" + vm.companyID + "'").then(function(){

						DockLogService.savePrinterInSql(result).then(function(res){

							vm.getPrinter();
							$ionicLoading.hide();
							$scope.$broadcast('scroll.refreshComplete');

						}); 

					});
				})

			}

			function getPrinter(){
				var params = {};

				params.CompanyID = vm.companyID;
				params.UserID = vm.userID;

				CRUD.select("DockLog_Printer","*",params).then(function(result){
					vm.printerList = result;
				});
			}

			function savePrinter() {			
				LocalData.saveData("dockLogPrinterID", vm.selectedPrinterID );

				Object.keys(vm.printerList).forEach(function(x){

					if(vm.printerList[x].PrinterID == vm.selectedPrinterID) {
						LocalData.saveData('dockLogPrinterName',vm.printerList[x].PrinterName);
						vm.selectedPrinterName = vm.printerList[x].PrinterName;
					}

				})

				$state.go('app.dock-log');
			};



		    function clearSearch(){
				vm.searchText = '';
			}; 
		};
				
		
})();
