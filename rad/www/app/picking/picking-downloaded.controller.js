(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('PickingDownloadedController', Body);

			function Body($state,
					$scope,
					filterFilter,
					$ionicPopover,
					$ionicViewSwitcher) {

			var vm = this;
			vm.goTo = goTo;
			vm.pickingOption = pickingOption;		
			vm.editMode = editMode;

			vm.pickingItems = [
				{name: "Shipment 238", det: "CHRIS 1 - FILL STOCK", qty: 3},
				{name: "Shipment 238", det: "CHRIS 1 - FILL STOCK", qty: 2}
			];

			function editMode() {
				vm.showOptionToDelete = true;
			}

			function goTo($url,$navDirection) {

				if(vm.popover) {
					vm.popover.hide();
				}

				if($navDirection) {
		   			$ionicViewSwitcher.nextDirection($navDirection);
				}

				$state.go($url);
			}

	    	function pickingOption($event,$popupType) {
		    	$ionicPopover.fromTemplateUrl('html/templates/option-more.html', {
				        scope: $scope
				}).then(function(popover) {
				        vm.popover = popover;
				        if($popupType) {
				       		vm.popUpType = $popupType;
				        } else {
				        	vm.popUpType = 'picking';
				        }
				        popover.show($event)
				}); 
	    	};
		};
				
		
})();
