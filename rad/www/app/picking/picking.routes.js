(function(){
 	'use strict';
 	
		angular
		    .module('app')
			.config(function($stateProvider, $urlRouterProvider) {
		     
		     $stateProvider
		      .state("app.picking", {
		      	cache: false,
                changeColor: 'bar-azure-8', 
			    url: "/picking", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/picking/picking.html",
					      controller: "PickingController as vm"
					 }
				  }
			      
		      })

		      .state("app.picking-downloaded", {
		      	cache: false,
                changeColor: 'bar-azure-8', 
			    url: "/picking-downloaded", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/picking/picking-downloaded.html",
					      controller: "PickingDownloadedController as vm"
					 }
				  }
			      
		      })

		      .state("app.picking-downloaded-details", {
		      	cache: false,
                changeColor: 'bar-azure-8', 
			    url: "/picking-downloaded-details", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/picking/picking-downloaded-details.html",
					      controller: "PickingDownloadedDetailsController as vm"
					 }
				  }
		      })

		      .state("app.picking-downloaded-details-list", {
		      	cache: false,
                changeColor: 'bar-azure-8', 
			    url: "/picking-downloaded-details-list", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/picking/picking-downloaded-details-list.html",
					      controller: "PickingDownloadedDetailsListController as vm"
					 }
				  }
		      })

		      .state("app.picking-send", {
		      	cache: false,
                changeColor: 'bar-azure-8', 
			    url: "/picking-send", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/picking/picking-send.html",
					      controller: "PickingSendController as vm"
					 }
				  }
		      })

		      .state("app.picking-inventory-location", {
		      	cache: false,
                changeColor: 'bar-azure-8', 
			    url: "/picking-inventory-location", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/picking/picking-inventory-location.html",
					      controller: "PickingInventoryLocationController as vm"
					 }
				  }
			      
		      })

	     });
		     
})();