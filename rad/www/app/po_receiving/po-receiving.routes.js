(function(){
 	'use strict';
 	
		angular
		    .module('app')
			.config(function($stateProvider, $urlRouterProvider) {
		     
		     $stateProvider
		      .state("app.po-receiving", {
		      	cache: false,
                changeColor: 'bar-amber-7', 
			    url: "/po-receiving", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/po_receiving/po-receiving.html",
					      controller: "POReceivingController as vm"
					 }
				  }
			      
		      })

		      .state("app.po-receiving-details", {
		      	cache: false,
                changeColor: 'bar-amber-7', 
			    url: "/po-receiving-details", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/po_receiving/po-receiving-details.html",
					      controller: "POReceivingDetailsController as vm"
					 }
				  }
			      
		      })

		      .state("app.po-receiving-show-lines", {
		      	cache: false,
                changeColor: 'bar-amber-7', 
			    url: "/po-receiving-show-lines", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/po_receiving/po-receiving-show-lines.html",
					      controller: "POReceivingShowLinesController as vm"
					 }
				  }
			      
		      })

		      .state("app.po-receiving-show-lines-list", {
		      	cache: false,
                changeColor: 'bar-amber-7', 
			    url: "/po-receiving-show-lines-list", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/po_receiving/po-receiving-show-lines-list.html",
					      controller: "POReceivingShowLinesListController as vm"
					 }
				  }
			      
		      })

		      .state("app.po-receiving-done", {
		      	cache: false,
                changeColor: 'bar-amber-7', 
			    url: "/po-receiving-done", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/po_receiving/po-receiving-done.html",
					      controller: "POReceivingDoneController as vm"
					 }
				  }
			      
		      })

		      .state("app.po-receiving-send-details", {
		      	cache: false,
                changeColor: 'bar-amber-7', 
			    url: "/po-receiving-send-details", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/po_receiving/po-receiving-send-details.html",
					      controller: "POReceivingDoneController as vm"
					 }
				  }
			      
		      })

		      .state("app.po-receiving-location", {
		      	cache: false,
                changeColor: 'bar-amber-7', 
			    url: "/po-receiving-location", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/po_receiving/po-receiving-location.html",
					      controller: "POReceivingLocationController as vm"
					 }
				  }
			      
		      })
		      
		      .state("app.po-number-lookup", {
		      	cache: false,
                changeColor: 'bar-amber-7', 
			    url: "/po-number-lookup", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/po_receiving/po-number-lookup.html",
					      controller: "PONumberLookupController as vm"
					 }
				  }
			      
		      })
		      
		      .state("app.po-number-lookup-filter", {
		      	cache: false,
                changeColor: 'bar-amber-7', 
			    url: "/po-number-lookup-filter", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/po_receiving/po-number-lookup-filter.html",
					      controller: "PONumberLookupFilterController as vm"
					 }
				  }
			      
		      })
		      
		      .state("app.po-receiving-change-company", {
		      	cache: false,
                changeColor: 'bar-amber-7', 
			    url: "/po-receiving-change-company", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/templates/company-change-view.html"
					 }
				  }
			      
		      })

		     });
		     
})();