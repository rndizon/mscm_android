(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('POReceivingShowLinesController', Body);

			function Body($state,
					$scope,
					filterFilter,
					$ionicPopover,
					$ionicViewSwitcher) {

			var vm = this;
			vm.goTo = goTo;
			vm.toggleGroup = toggleGroup;
			vm.isGroupShown = isGroupShown;

			function goTo($url,$navDirection) {

				if(vm.popover) {
					vm.popover.hide();
				}

				if($navDirection) {
		   			$ionicViewSwitcher.nextDirection($navDirection);
				}

				$state.go($url);
			}

		  	/*
				* if given group is the selected group, deselect it
				* else, select the given group
			*/
			function toggleGroup(group) {

				if (vm.isGroupShown(group)) {
					vm.shownGroup = null;
				} else {
					vm.shownGroup = group;
				}
			};

			function isGroupShown(group) {
				return vm.shownGroup === group;
			};
		};
				
		
})();
