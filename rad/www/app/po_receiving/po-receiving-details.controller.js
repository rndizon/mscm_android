(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('POReceivingDetailsController', Body);

			function Body($state,
					$ionicViewSwitcher) {

			var vm = this;
			vm.goTo = goTo;
			vm.toggleGroup = toggleGroup;
			vm.isGroupShown = isGroupShown;

			function goTo($url,$navDirection) {

				if(vm.popover) {
					vm.popover.hide();
				}

				if($navDirection) {
		   			$ionicViewSwitcher.nextDirection($navDirection);
				}

				$state.go($url);
			}

		  	/*
				* if given group is the selected group, deselect it
				* else, select the given group
			*/
			function toggleGroup(group) {

				if (vm.isGroupShown(group)) {
                    vm.open = "true";
					vm.shownGroup = null;
                    console.log(vm.shownGroup);
				} else {
					vm.shownGroup = group;
                    console.log(vm.shownGroup);
				}
			};

			function isGroupShown(group) {
				return vm.shownGroup === group;
			};
		};
				
		
})();
