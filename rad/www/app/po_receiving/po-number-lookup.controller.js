(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('PONumberLookupController', Body);

			function Body($state,
					$ionicViewSwitcher) {

			var vm = this;
			vm.goTo = goTo;	

			function goTo($url,$navDirection) {

				if(vm.popover) {
					vm.popover.hide();
				}

				if($navDirection) {
		   			$ionicViewSwitcher.nextDirection($navDirection);
				}

				$state.go($url);
			}
		};
				
		
})();
