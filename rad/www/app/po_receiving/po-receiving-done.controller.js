(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('POReceivingDoneController', Body);

			function Body($state,
					$scope,
					$ionicViewSwitcher,
					$ionicPopover) {

			var vm = this;
			vm.goTo = goTo;
			vm.POReceivingOption = POReceivingOption;

			function goTo($url,$navDirection) {

				if(vm.popover) {
					vm.popover.hide();
				}

				if($navDirection) {
		   			$ionicViewSwitcher.nextDirection($navDirection);
				}

				$state.go($url);
			}

	    	function POReceivingOption($event,$popupType) {
		    	$ionicPopover.fromTemplateUrl('html/templates/option-more.html', {
				        scope: $scope
				}).then(function(popover) {
				        vm.popover = popover;
				        if($popupType) {
				       		vm.popUpType = $popupType;
				        } else {
				        	vm.popUpType = 'po-receiving';
				        }
				        popover.show($event)
				}); 
	    	};

		};
				
		
})();
