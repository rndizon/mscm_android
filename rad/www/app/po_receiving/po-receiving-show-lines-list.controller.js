(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('POReceivingShowLinesListController', Body);

			function Body($state,
					$scope,
					filterFilter,
					$ionicPopover,
					$ionicViewSwitcher) {

			var vm = this;
			vm.goTo = goTo;
			vm.cancelSearch = cancelSearch;

			function goTo($url,$navDirection) {

				if(vm.popover) {
					vm.popover.hide();
				}

				if($navDirection) {
		   			$ionicViewSwitcher.nextDirection($navDirection);
				}

				$state.go($url);
			}

			function cancelSearch() {
				vm.searchText = "";
			}

		};
				
		
})();
