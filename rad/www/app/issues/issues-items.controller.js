(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('IssuesItemsController', Body);

			function Body($state,
					$scope,
					$ionicViewSwitcher) {

			var vm = this;
			vm.goTo = goTo;

			function goTo($url,$navDirection) {

				if(vm.popover) {
					vm.popover.hide();
				}

				if($navDirection) {
		   			$ionicViewSwitcher.nextDirection($navDirection);
				}

				$state.go($url);
			}

		};
				
		
})();
