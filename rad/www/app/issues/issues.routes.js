(function(){
 	'use strict';
 	
		angular
		    .module('app')
			.config(function($stateProvider, $urlRouterProvider) {
		     
		     $stateProvider
		      .state("app.issues", {
		      	cache: false,
                changeColor: 'bar-turquoise-8',
			    url: "/issues", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/issues/issues.html",
					      controller: "IssuesController as vm"
					 }
				  }
		      })

		      .state("app.issues-change-comp-and-loc", {
		      	cache: false,
                changeColor: 'bar-turquoise-8',
			    url: "/issues-change-comp-and-loc", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/templates/comp-and-loc-change-view.html",
					 }
				  }
		      })

		      .state("app.issues-details", {
		      	cache: false,
                changeColor: 'bar-turquoise-8',
			    url: "/issues-details", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/issues/issues-details.html",
					      controller: "IssuesDetailsController as vm"
					 }
				  }
		      })

		      .state("app.issues-details-list", {
		      	cache: false,
                changeColor: 'bar-turquoise-8',
			    url: "/issues-details-list", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/issues/issues-details-list.html",
					      controller: "IssuesDetailsListController as vm"
					 }
				  }
		      })

		      .state("app.issues-done", {
		      	cache: false,
                changeColor: 'bar-turquoise-8',
			    url: "/issues-done", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/issues/issues-done.html",
					      controller: "IssuesDoneController as vm"
					 }
				  }
		      })

		      .state("app.issues-change-company", {
		      	cache: false,
                changeColor: 'bar-turquoise-8',
			    url: "/issues-change-company", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/templates/company-change-view.html",
					 }
				  }
		      })

		      .state("app.issues-change-location", {
		      	cache: false,
                changeColor: 'bar-turquoise-8',
			    url: "/issues-change-location", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/templates/location-change-view.html",
					 }
				  }
		      })

		      .state("app.issues-items", {
		      	cache: false,
                changeColor: 'bar-turquoise-8',
			    url: "/issues-items", 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/templates/item-change-view.html",
					      controller: "IssuesItemsController as vm"
					 }
				  }
		      })


		     });
		     
})();