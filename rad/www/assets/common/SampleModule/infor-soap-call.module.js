/**
	Custom SOAP call

	need to include js-soapclient thir-paty /thirdparty/js-soapclient/soapclient.js
*/
(function(){
	'use strict';
	
	angular
		.module('InforSoapCall', [])

		.factory("$soap",['$q',function($q){

			var service = {
				post 		   : post,
				setCredentials : setCredentials
			};

			return service;

			////////////////////////////////
			function post(url, action, params){
					var deferred = $q.defer();
					
					//Create SOAPClientParameters
					var soapParams = new SOAPClientParameters();
					for(var param in params){
						soapParams.add(param, params[param]);
					}
					
					//Create Callback
					var soapCallback = function(e){
						if(e.constructor.toString().indexOf("function Error()") != -1){
							deferred.reject("An error has occurred.");
						} else {
							deferred.resolve(e);
						}
					};
					
					SOAPClient.invoke(url, action, soapParams, true, soapCallback);

					return deferred.promise;
			}

			function setCredentials(username, password){
					SOAPClient.username = username;
					SOAPClient.password = password;
			}

			
		}]);
})();