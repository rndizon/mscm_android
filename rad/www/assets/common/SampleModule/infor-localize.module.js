(function(){
	'use strict';
	
	angular
		.module('InforLocalization', [])
		.factory("InforLocale",InforLocale);

	InforLocale.$inject = ["$http"];
	
	function InforLocale($http){

			var service = {
				Locale : getLocalization 
			};

			return service;

			function getLocalization(locale,callback){
					$http
						.get("assets/localization/json."+locale+".js")
						.success(callback);
			}

	}	



})();	