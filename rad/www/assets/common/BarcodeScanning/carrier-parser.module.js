/***************************************************************
 *                                                             *
 *                           NOTICE                            * 
 *                                                             *
 *   THIS SOFTWARE IS THE PROPERTY OF AND CONTAINS             *
 *   CONFIDENTIAL INFORMATION OF LAWSON SOFTWARE AND/OR ITS    *
 *   AFFILIATES OR SUBSIDIARIES AND SHALL NOT BE DISCLOSED     *
 *   WITHOUT PRIOR WRITTEN PERMISSION. LICENSED CUSTOMERS MAY  *
 *   COPY AND ADAPT THIS SOFTWARE FOR THEIR OWN USE IN         *
 *   ACCORDANCE WITH THE TERMS OF THEIR SOFTWARE LICENSE       *
 *   AGREEMENT. ALL OTHER RIGHTS RESERVED.                     *
 *                                                             *
 *   (c) COPYRIGHT 2015 LAWSON SOFTWARE.  ALL RIGHTS RESERVED. *
 *   THE WORD AND DESIGN MARKS SET FORTH HEREIN ARE            *
 *   TRADEMARKS AND/OR REGISTERED TRADEMARKS OF LAWSON         *
 *   SOFTWARE AND/OR ITS AFFILIATES AND SUBSIDIARIES. ALL      *
 *   RIGHTS RESERVED.  ALL OTHER TRADEMARKS LISTED HEREIN ARE  *
 *   THE PROPERTY OF THEIR RESPECTIVE OWNERS.                  *
 *                                                             *
 ***************************************************************
 *                                                             *
 *  v1.0 - 01/28/2016 - Initial release                        *
 *  														   *
 *  InforCommsManager 										   *
 *                                                             *   
 ***************************************************************/

(function(){
	'use strict';
	
	angular
		.module('InforScannerManager')
		.factory("CarrierParser",Body);

		function  Body(CommonInfoService,filterFilter){

			var CarrierParser = {
				getData: getData,
			}; 


			// 11, 12, 18, 22
			var carrierDetails = [
				{ name: "DHL 11", trackingLen: 11, fetchLength: 11, startsWith: "Num", startPos: 1, endPos: 11},
				{ name: "UPS 11", trackingLen: 11,  fetchLength: 11, startsWith: "Alp", startPos: 1, endPos: 11},

				{ name: "DHL 12", trackingLen: 12, fetchLength: 11, startsWith: "Alp", startPos: 2, endPos: 12},
				{ name: "UPS 12", trackingLen: 12, fetchLength: 12, startsWith: "Num", startPos: 1, endPos: 12},

				{ name: "Fedex Airbill", trackingLen: 18, fetchLength: 18, startsWith: "", startPos: 2, endPos: 13},
				{ name: "UPS 18",trackingLen: 18, fetchLength: 18, startsWith: "1Z", startPos: 1, endPos: 18},

				{ name: "FedEx Ground", trackingLen: 22, fetchLength: 15, startsWith: "96[11-13]", startPos: 8, endPos: 22},
				{ name: "USPS 22", trackingLen: 22, fetchLength: 22, startsWith: "91,420", startPos: 1, endPos: 22},

				{ name: "USPS 13",trackingLen: 13, fetchLength: 18, startsWith: "EO", startPos: 1, endPos: 13},
				{ name: "USPS 20",trackingLen: 30, fetchLength: 18, startsWith: "AlpOrNum", startPos: 11, endPos: 30},
				{ name: "Fedex (32)",trackingLen: 32, fetchLength: 12, startsWith: "AlpOrNum", startPos: 17, endPos: 28},
				{ name: "Fedex (34)",trackingLen: 34, fetchLength: 14, startsWith: "AlpOrNum", startPos: 21, endPos: 34}]


			return CarrierParser;

			function getData(_trackingNumber){
				

				var returnVal = {};
				var carrier =  getCarrier(_trackingNumber);
				var barCode = fetchBarcode(carrier,_trackingNumber);

				returnVal.name = carrier.name;
				returnVal.tracking = barCode;


				return returnVal;
			}

			function fetchBarcode(_carrier,_trackingNumber){

				return _trackingNumber.slice((_carrier.startPos - 1),_carrier.endPos)
			}

			function getCarrier(_trackingNumber) {
				var trackingLength = _trackingNumber.length;
				var carrierObj = filterFilter(carrierDetails, {trackingLen: trackingLength});
				var returnVal = "";
				var tempVar = "";
                var numOnly = /[0-9]/;


				if(trackingLength == 34) {

					tempVar = filterFilter(carrierObj, {startsWith: "AlpOrNum",trackingLen: 34});
					returnVal = tempVar[0];

				} else  if(trackingLength == 32) {

					tempVar = filterFilter(carrierObj, {startsWith: "AlpOrNum",trackingLen: 32});
					returnVal = tempVar[0];

				} else  if(trackingLength == 30) {

					tempVar = filterFilter(carrierObj, {startsWith: "AlpOrNum",trackingLen: 30});
					returnVal = tempVar[0];

				} else  if(trackingLength == 13) {

					tempVar = filterFilter(carrierObj, {startsWith: "EO",trackingLen: 13});
					returnVal = tempVar[0];

				}

				else if( trackingLength == 22 && _trackingNumber.slice(0,4) == "9611" || 
					trackingLength == 22 && _trackingNumber.slice(0,4) == "9612" || 
					trackingLength == 22 && _trackingNumber.slice(0,4) == "9613") {

					tempVar = filterFilter(carrierObj, {startsWith: "96[11-13]"});
					
					returnVal = tempVar[0];

				} else if( trackingLength == 22 && _trackingNumber.slice(0,3) == "420") {
					tempVar = filterFilter(carrierObj, {startsWith: "91,420"});

					returnVal = tempVar[0];

				} else if( trackingLength == 22 && _trackingNumber.slice(0,2) == "91") { 
					tempVar = filterFilter(carrierObj, {startsWith: "91,420"});

					returnVal = tempVar[0];

				} else if( trackingLength == 18 && _trackingNumber.slice(0,2) == "1Z") { 
					tempVar = filterFilter(carrierObj, {startsWith: "1Z"});

					returnVal = tempVar[0];

				} else if( trackingLength == 18 && _trackingNumber.slice(0,2) != "1Z") { 
					tempVar = filterFilter(carrierObj, {startsWith: ""});

					returnVal = tempVar[0];

				} else if( trackingLength == 12 && numOnly.test(_trackingNumber.slice(0,1)) == true) { 
					tempVar = filterFilter(carrierObj, {startsWith: "Num",trackingLen: 12});
					
					returnVal = tempVar[0];

				} else if( trackingLength == 12 && numOnly.test(_trackingNumber.slice(0,1)) == false) { 
					tempVar = filterFilter(carrierObj, {startsWith: "Alp",trackingLen: 12});
					
					returnVal = tempVar[0];

				} else if( trackingLength == 11 && numOnly.test(_trackingNumber.slice(0,1)) == true) { 
					tempVar = filterFilter(carrierObj, {startsWith: "Num",trackingLen: 11});
					
					returnVal = tempVar[0];
				} else if( trackingLength == 11 && numOnly.test(_trackingNumber.slice(0,1)) == false) { 
					tempVar = filterFilter(carrierObj, {startsWith: "Alp",trackingLen: 11});
					
					returnVal = tempVar[0];
				} 


				return returnVal;
			}

		}


})();