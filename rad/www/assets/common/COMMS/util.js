/*
***************************************************************
*                                                             *
*                           NOTICE                            * 
*                                                             *
*   THIS SOFTWARE IS THE PROPERTY OF AND CONTAINS             *
*   CONFIDENTIAL INFORMATION OF INFOR AND/OR ITS		      *
*   AFFILIATES OR SUBSIDIARIES AND SHALL NOT BE DISCLOSED     *
*   WITHOUT PRIOR WRITTEN PERMISSION. LICENSED CUSTOMERS MAY  *
*   COPY AND ADAPT THIS SOFTWARE FOR THEIR OWN USE IN         *
*   ACCORDANCE WITH THE TERMS OF THEIR SOFTWARE LICENSE       *
*   AGREEMENT. ALL OTHER RIGHTS RESERVED.                     *
*                                                             *
*   (c) COPYRIGHT 2012 INFOR.  ALL RIGHTS RESERVED.			  *
*   THE WORD AND DESIGN MARKS SET FORTH HEREIN ARE            *
*   TRADEMARKS AND/OR REGISTERED TRADEMARKS OF INFOR          *
*   AND/OR ITS AFFILIATES AND SUBSIDIARIES. ALL			      *
*   RIGHTS RESERVED.  ALL OTHER TRADEMARKS LISTED HEREIN ARE  *
*   THE PROPERTY OF THEIR RESPECTIVE OWNERS.                  *
*                                                             *
***************************************************************
*/

// Define global variables to JSLint.
// True indicates that the variable may be assigned to by this file.
// False indicates that the variable may not be assigned to by this file.
/*global $: false,
		 escape: false,
		 unescape: false
*/

var util = {};

// parseUrlParams - Takes a jQuery page div selector as input.  Extracts the URL parameters
//                  from the data-url attribute on the page.  Then, returns an object with 
//                  property/value pairs.
//
// Example:
//     Parent page passes URL get data parameters.
//         <a href="myReqsList.html?type=99&typeDesc=ALL">All</a>
//
//     Child page does the following to get the type and typeDesc parameters.
//         var parsedObj = util.parseUrlParams($('#myReqsListPage'));  // #myReqsListPage is the id of the page div on the child page.
//         var type = parsedObj.type;
//         var typeDesc = parsedObj.typeDesc;
util.parseUrlParams = function (jQueryPageDivSelector) {
	var urlParamsObj = {};
	
	var dataObj = $.mobile.path.parseUrl(jQueryPageDivSelector.attr('data-url'));
	var urlParamString = dataObj.search;
	
	urlParamString = urlParamString.replace("?", "");  // Replace ? with empty string.
	
	var e, 
		a = /\+/g,  // Regex for replacing addition symbol with a space 
		r = /([^&=]+)=?([^&]*)/g, 
		d = function (s) { return unescape(decodeURIComponent(s.replace(a, " "))); }, 
		q = urlParamString;
 
	while (e = r.exec(q)) {
       urlParamsObj[d(e[1])] = d(e[2]);
	}
    
    return urlParamsObj;
};

// encodeUrlParam - Use this function to encode URL parameter data.
util.encodeUrlParam = function (urlParam) {
	return encodeURIComponent(escape(urlParam));
};

//decodeUrlParam - Use this function to decode URL parameter data.
util.decodeUrlParam = function (urlParam) {
	return unescape(decodeURIComponent(urlParam));
};

util.showLoadingMsg = function (message) {
	//console.log("util.showLoadingMsg");
	if ($.mobile.loading) {
		//console.log("jQuery Mobile 1.2");
		$.mobile.loading('show', {theme: "a", text: message, textVisible: true});
	} else {
		//console.log("Not jQuery Mobile 1.2");
		$.mobile.loadingMessage = message;
		if ($.mobile.hasOwnProperty('loadingMessageTextVisible')) {
		    $.mobile.loadingMessageTextVisible = true;
		}
		$('body').addClass('ui-loading');
		$.mobile.showPageLoadingMsg();
	}
};

util.parseUrlParamString = function (urlParamString) {
    var urlParamsObj = {};
          
    var e, 
          a = /\+/g,  // Regex for replacing addition symbol with a space 
          r = /([^&=]+)=?([^&]*)/g, 
          d = function (s) { return unescape(decodeURIComponent(s.replace(a, " "))); }, 
          q = urlParamString;

    while (e = r.exec(q)) {
     urlParamsObj[d(e[1])] = d(e[2]);
    }
  
  return urlParamsObj;
};


util.hideLoadingMsg = function () {
	//console.log("util.hideLoadingMsg");
	if ($.mobile.loading) {
		//console.log("jQuery Mobile 1.2");
		$.mobile.loading('hide');
	} else {
		//console.log("Not jQuery Mobile 1.2");
		$('body').removeClass('ui-loading');
		$.mobile.hidePageLoadingMsg();
		// Default back to the jQuery Mobile default message.
		$.mobile.loadingMessage = "Loading...";
	}
};

// alert - To be used instead of the JavaScript alert.  If Phonegap is available,
//         then the Phonegap alert will be called.  If not, then the JavaScript
//         alert will be called.  The Phonegap alert calls the native Android
//         and iOS alert.
// Parameters:
//     message - The alert message that will be displayed.
//     okCallback - The callback function that will be executed when the OK button
//                  is clicked.
// Example: 
//     util.alert("It will be done!", function(){console.log("You clicked OK!");});
util.alert = function (message, okCallback) {
	var okFunction = function () {	
	};
	
	if (typeof okCallback !== "undefined") {
		okFunction = okCallback;
	}
	
	if (typeof navigator.network === 'undefined') { // Not Phonegap, it's a web browser.
		alert(message);
		okFunction();
	} else {
		// Phonegap
		navigator.notification.alert(message, okFunction);
	}
};

//  confirm - To be used instead of the JavaScript confirm.  If Phonegap is available,
//            then the Phonegap confirm will be called.  If not, then the JavaScript
//            confirm will be called.  The Phonegap confirm calls the native Android
//			  and iOS confirm.
// Parameters:
//     message - The confirm message that will be displayed.
//     okCallback - The callback function that will be executed when the OK button
//                  is clicked.
//     cancelCallback - The callback function that will be executed when the Cancel 
//                      button is clicked.
//Example: 
//    util.confirm("Do it?", function(){console.log("You clicked OK!");}, function(){console.log("You clicked Cancel!");});
util.confirm = function (message, okCallback, cancelCallback) {
	var okFunction = function () {
	};
	var cancelFunction = function () {
	};
	
	if (typeof okCallback !== "undefined") {
		okFunction = okCallback;
	}
	if (typeof cancelCallback !== "undefined") {
		cancelFunction = cancelCallback;
	}
	
	if (typeof navigator.network === 'undefined') { // Not Phonegap, it's a web browser.
		var response = confirm(message);
		if (response) {
			okFunction();
		} else {
			cancelFunction();
		}
	} else {
		//Phonegap
		var onConfirm = function (button) {
			if (button == 1) {
				okFunction();
			} else if (button == 2) {
				cancelFunction();
			}
		};
		
		navigator.notification.confirm(message, onConfirm);
	}
};

util.formatPhoneNbr = function (ctryCode, phoneNbr, phoneExt) {
	var formattedPhoneNbr = "";
	if (phoneNbr) {
		if (ctryCode) {
			formattedPhoneNbr = ctryCode;
			if (phoneNbr.indexOf("-") !== -1) {
				formattedPhoneNbr += "-";
			} else {
			    formattedPhoneNbr += " ";
			}
		}
		formattedPhoneNbr += phoneNbr;
	}
	if (phoneExt) {
		formattedPhoneNbr += " " + phoneExt;
	}
	return formattedPhoneNbr;
};