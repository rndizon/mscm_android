/*
***************************************************************
*                                                             *
*                           NOTICE                            * 
*                                                             *
*   THIS SOFTWARE IS THE PROPERTY OF AND CONTAINS             *
*   CONFIDENTIAL INFORMATION OF INFOR AND/OR ITS		      *
*   AFFILIATES OR SUBSIDIARIES AND SHALL NOT BE DISCLOSED     *
*   WITHOUT PRIOR WRITTEN PERMISSION. LICENSED CUSTOMERS MAY  *
*   COPY AND ADAPT THIS SOFTWARE FOR THEIR OWN USE IN         *
*   ACCORDANCE WITH THE TERMS OF THEIR SOFTWARE LICENSE       *
*   AGREEMENT. ALL OTHER RIGHTS RESERVED.                     *
*                                                             *
*   (c) COPYRIGHT 2012 INFOR.  ALL RIGHTS RESERVED.			  *
*   THE WORD AND DESIGN MARKS SET FORTH HEREIN ARE            *
*   TRADEMARKS AND/OR REGISTERED TRADEMARKS OF INFOR          *
*   AND/OR ITS AFFILIATES AND SUBSIDIARIES. ALL			      *
*   RIGHTS RESERVED.  ALL OTHER TRADEMARKS LISTED HEREIN ARE  *
*   THE PROPERTY OF THEIR RESPECTIVE OWNERS.                  *
*                                                             *
***************************************************************
*/

// Define global variables to JSLint.
// True indicates that the variable may be assigned to by this file.
// False indicates that the variable may not be assigned to by this file.
/*global window: false,
		 Connection: false,
		 DOMParser: false,
		 util: false,
		 trxconnect: false
*/

//-----------------------------------------------------------------------------
//
//	DEPENDENCIES:
//		common.js
//-----------------------------------------------------------------------------
//
//	PARAMETERS:
//		mainWnd			(optional) location of the common.js file
//		techVersion		(optional) defaults to 9.0.0
//		httpRequest		(optional) reference to a request method (i.e. SSORequest, httpRequest, etc..)
//							if null, the call() method will always return null
//		funcAfterCall	(optional) function to run after the data call is executed
//-----------------------------------------------------------------------------
function TransactionObject(mainWnd, techVersion, httpRequest, funcAfterCall, bUseSecurityPlugin) {
	this.mainWnd = mainWnd || window;
	this.httpRequest = httpRequest || null;
	this.techVersion = techVersion || TransactionObject.TECHNOLOGY_900;
	this.funcAfterCall = funcAfterCall;
	this.isPost = true;
	this.dom = null;
	this.useSecurityPlugin = (typeof (bUseSecurityPlugin) == "boolean") ? bUseSecurityPlugin : false;

	// array for fields
	this.paramsAry = [];

	// private fields (for TransactionObject only!)
	this.message = null;
	this.msgNbr = null;
}
//-- static variables ---------------------------------------------------------
TransactionObject.TECHNOLOGY_803 = "8.0.3";
TransactionObject.TECHNOLOGY_900 = "9.0.0";
TransactionObject.TRANSACTION_URL_803 = "/servlet/ags";
TransactionObject.TRANSACTION_URL_900 = "/servlet/Router/Transaction/Erp";
//-- static response messages -------------------------------------------------
TransactionObject.ADD_COMPLETE_CONTINUE = "Add Complete - Continue";
TransactionObject.CHANGE_COMPLETE_CONTINUE = "Change Complete - Continue";
TransactionObject.FIELD_IS_REQUIRED = "Field is required";
TransactionObject.INQUIRY_COMPLETE = "Inquiry Complete";
TransactionObject.RECORDS_DELETED = "Records Have Been Deleted";
TransactionObject.SUCCESS_MSGNBR = "000";
//-----------------------------------------------------------------------------
TransactionObject.prototype.setParameter = function (name, value) {
	this.paramsAry[name] = value;
};
//-----------------------------------------------------------------------------
TransactionObject.prototype.removeParameter = function (name) {
	this.setParameter(name, null);
};
//-----------------------------------------------------------------------------
TransactionObject.prototype.resetPrivateVars = function () {
	this.message = null;
	this.msgNbr = null;
};
//-----------------------------------------------------------------------------
TransactionObject.prototype.getParameter = function (name) {
	return (this.paramsAry[name] || null);
};
//-----------------------------------------------------------------------------
TransactionObject.prototype.getUrl = function () {
	var url = (this.techVersion == TransactionObject.TECHNOLOGY_900)
			? TransactionObject.TRANSACTION_URL_900
			: TransactionObject.TRANSACTION_URL_803;
	return (url + "?" + this.getQuery());
};
//-----------------------------------------------------------------------------
TransactionObject.prototype.getQuery = function () {
	var query = "";
	if (this.techVersion == TransactionObject.TECHNOLOGY_803) {
		query += "_OUT=XML&";
	}

	for (var i in this.paramsAry) {
		var val = this.paramsAry[i];
		if (val == null) {
			continue;
		}

		// escape if call is a GET or 803 tech or has a & or %
		val = String(val);
		if (!this.isPost || 
				this.techVersion == TransactionObject.TECHNOLOGY_803 || 
				val.indexOf("&") != -1 || 
				val.indexOf("+") != -1 || 
				val.indexOf("%") != -1 || 
				val.indexOf(" ") != -1 ||
				val.indexOf("#") != -1)
		{
			val = this.mainWnd.cmnEscapeIt(val);
		}

		query += i + "=" + val + "&";
	}

	// remove the ending "&" and return it
	return query.substring(0, query.length - 1);
};
//-----------------------------------------------------------------------------
TransactionObject.username;
TransactionObject.password;
TransactionObject.prototype.callTransaction = function () {
	if (!this.httpRequest) {
		return null;
	}

	this.resetPrivateVars();
	
	var callingFunction = this;
	if (typeof navigator.network === 'undefined' || navigator.network.connection.type != Connection.NONE) {
		var parseResponse = function (responseText) {
			var parser = new DOMParser();
			var responseXML = parser.parseFromString(responseText, "text/xml");
			callingFunction.dom = responseXML;
			//callingFunction.dom = this.responseXML;
			if (responseXML && responseXML.documentElement.nodeName == "ERROR") {
				var details = responseXML.documentElement.firstChild.nextSibling.firstChild.nodeValue;
				if (details.length > 1024) {
					details = details.substring(0, 1024) + "...";
				}
				var msg = responseXML.documentElement.firstChild.firstChild.nodeValue
					+ "\n\n" + callingFunction.getUrl() + "  -  callTransaction() - Transaction.js"
					+ "\n\n" + details;
				console.log(msg);
				util.alert(msg, 
					function () {
						if (callingFunction.funcAfterCall) {
							callingFunction.funcAfterCall();
						}
					});
				return;
			}			
			if (callingFunction.funcAfterCall) {
				callingFunction.funcAfterCall();
			}
		};
		var handleError = function (e) {
			var msg = 'There was a problem connecting to the server. Please verify your server status and host and port settings.\n';
			console.log(msg + e.error);
			// JI-MA-198- Handled undefined string on no wifi error
			util.alert(msg + (e.error == undefined ? "": e.error), 
				function () {
					if (callingFunction.funcAfterCall) {
						callingFunction.funcAfterCall();
					}
				});
			return;
		};
		var timestamp = new Date().getTime();
		// Append a query string parameter, "_=[TIMESTAMP]", to the URL to fix client caching problem.
		var query = this.getUrl() + "&_=" + timestamp;
		console.log("Transaction query=" + query);

		if (callingFunction.useSecurityPlugin == true) {
			var xhr = trxconnect.request({
				url: query,
				methodType: 'GET',
				contentType: 'text/plain',
				cache: false,
				success: function (responseText) {
					parseResponse(responseText);
				},
				fail: function (e) {
					return handleError(e);
				}
			});
		} else {
			this.httpRequest.onload = function () {
				parseResponse(this.responseText);
			};
			this.httpRequest.onerror = function (e) {
				return handleError(e);
			};		
			this.httpRequest.open('GET', query);
			this.httpRequest.setRequestHeader('Authorization', 'Basic ' + window.btoa(TransactionObject.username + ':' + TransactionObject.password));
			this.httpRequest.setRequestHeader('Cache-Control', 'no-cache');
			this.httpRequest.setRequestHeader('Pragma', 'no-cache');
			this.httpRequest.send();
		}	
	} else {	
		console.log('This application requires an internet connection.  Please check your connection and try again.');
		util.alert('This application requires an internet connection.  Please check your connection and try again.',
			function () {
				if (callingFunction.funcAfterCall) {
					callingFunction.funcAfterCall();
				}
			});
		return null;
	}		
	return null;
	
	if (this.isPost) {
		var url = (this.techVersion == TransactionObject.TECHNOLOGY_900)
				? TransactionObject.TRANSACTION_URL_900
				: TransactionObject.TRANSACTION_URL_803;
		this.dom = this.httpRequest(url, this.getQuery(), "application/x-www-form-urlencoded");
	} else {
		this.dom = this.httpRequest(this.getUrl());
	}

	if (this.funcAfterCall) {
		this.funcAfterCall();
	}

	return this.dom;
};
//-----------------------------------------------------------------------------
TransactionObject.prototype.getMessage = function () {
	if (!this.dom) {
		return null;
	}

	if (this.message == null) {
		var nodes = this.dom.getElementsByTagName("Message");
		if (nodes && nodes.length > 0) {
			this.message = this.mainWnd.cmnGetElementText(nodes.item(0));
		}
	}
	return this.message;
};
//-----------------------------------------------------------------------------
TransactionObject.prototype.getMsgNbr = function () {
	if (!this.dom) {
		return;
	}

	if (this.msgNbr == null) {
		var nodes = this.dom.getElementsByTagName("MsgNbr");
		if (nodes && nodes.length > 0) {
			this.msgNbr = this.mainWnd.cmnGetElementText(nodes.item(0));
		}
	}
	return this.msgNbr;
};
//-----------------------------------------------------------------------------
TransactionObject.prototype.getValue = function (name) {
	if (name && this.dom) {
		var nameNodes = this.dom.getElementsByTagName(name);
		if (nameNodes && nameNodes.length > 0) {
			return (this.mainWnd.cmnGetElementText(nameNodes.item(0)) || null);
		}
	}
	return null;
};