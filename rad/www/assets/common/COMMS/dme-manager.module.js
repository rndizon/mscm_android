 /***************************************************************
 *                                                             *
 *                           NOTICE                            * 
 *                                                             *
 *   THIS SOFTWARE IS THE PROPERTY OF AND CONTAINS             *
 *   CONFIDENTIAL INFORMATION OF LAWSON SOFTWARE AND/OR ITS    *
 *   AFFILIATES OR SUBSIDIARIES AND SHALL NOT BE DISCLOSED     *
 *   WITHOUT PRIOR WRITTEN PERMISSION. LICENSED CUSTOMERS MAY  *
 *   COPY AND ADAPT THIS SOFTWARE FOR THEIR OWN USE IN         *
 *   ACCORDANCE WITH THE TERMS OF THEIR SOFTWARE LICENSE       *
 *   AGREEMENT. ALL OTHER RIGHTS RESERVED.                     *
 *                                                             *
 *   (c) COPYRIGHT 2015 LAWSON SOFTWARE.  ALL RIGHTS RESERVED. *
 *   THE WORD AND DESIGN MARKS SET FORTH HEREIN ARE            *
 *   TRADEMARKS AND/OR REGISTERED TRADEMARKS OF LAWSON         *
 *   SOFTWARE AND/OR ITS AFFILIATES AND SUBSIDIARIES. ALL      *
 *   RIGHTS RESERVED.  ALL OTHER TRADEMARKS LISTED HEREIN ARE  *
 *   THE PROPERTY OF THEIR RESPECTIVE OWNERS.                  *
 *                                                             *
 ***************************************************************
 *                                                             *
 *  v1.0 - 9/3/2015 - Initial release                          *
 *                                                             *   
 ***************************************************************/
 (function() {
	'use strict';
	
	angular
		.module('InforCommsManager', [])
		.service("DmeManager",DMEManager);

	function DMEManager($ionicLoading) {
		
		var dmeManager = {
			init: initialize,
			setParam: setParameter,
			send: send
		}
		
		return dmeManager;
		
		/*--------------------Implementation-------------------*/
		function initialize(callbackFunction){
			// Constructor
		    var _self = this;
		    var userObj = appDataObject.userObj;
		  
		    DataObject.DATA_URL_900 = appDataObject.lawServerURL + "/servlet/Router/Data/Erp";    
		    DataObject.username = appDataObject.username;
		    DataObject.password = appDataObject.password;

		    var xmlhttp = new XMLHttpRequest();
		    var dataObject = new DataObject(window, DataObject.TECHNOLOGY_900, xmlhttp, 
		                                    function() {
											    // If there's no record, display alert and clear the list 
												if (_self.dataObj.getNbrRecs() == 0)
												{
												    _self.callbackFunction(null);
												    return;
												}
												
												var jsonArr = [];   
												
												var colNameArr = [];
												var idx = 0;
												
												// Get the column names
												var cName;
												while (cName = _self.dataObj.getColumnName(idx++))
												    colNameArr.push(cName);
											
											    // Save key/value to JSON    		 
												for (var i = 0; i < _self.dataObj.getNbrRecs(); i++)
											   	{
											   	    // Load the column names
												    var record = _self.dataObj.getRecord(i);
												    var dataRow = {};
												    for (var colCtr = 0; colCtr < colNameArr.length; colCtr++)
												    {   
												        var columnName = colNameArr[colCtr];
												        dataRow[columnName] = record.getFieldValue(columnName).trim();
												    }
												    
												    jsonArr.push(dataRow);	
											    }
											    
											     _self.callbackFunction(jsonArr);
		                                    }, 
		                                    true);
		    if (dataObject)
		    {
		        dataObject.setParameter("PROD", userObj.prodline);
		        dataObject.setParameter("OTMMAX", "1");
		        dataObject.setParameter("XIDA", "FALSE");
		        dataObject.setParameter("XKEYS", "FALSE");
		        dataObject.setParameter("XRELS", "FALSE");
		    }	

		    this.dataObj = dataObject;
		  
		    this.callbackFunction = callbackFunction;
		}
		
		function setParameter(fKey, fValue){
			this.dataObj.setParameter(fKey, fValue);
		}
		
		function send()
		{	  
		    this.dataObj.callData();
		   
		}
	    
	}
	
})();	
