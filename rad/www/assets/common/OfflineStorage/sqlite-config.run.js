/***************************************************************
 *                                                             *
 *                           NOTICE                            * 
 *                                                             *
 *   THIS SOFTWARE IS THE PROPERTY OF AND CONTAINS             *
 *   CONFIDENTIAL INFORMATION OF LAWSON SOFTWARE AND/OR ITS    *
 *   AFFILIATES OR SUBSIDIARIES AND SHALL NOT BE DISCLOSED     *
 *   WITHOUT PRIOR WRITTEN PERMISSION. LICENSED CUSTOMERS MAY  *
 *   COPY AND ADAPT THIS SOFTWARE FOR THEIR OWN USE IN         *
 *   ACCORDANCE WITH THE TERMS OF THEIR SOFTWARE LICENSE       *
 *   AGREEMENT. ALL OTHER RIGHTS RESERVED.                     *
 *                                                             *
 *   (c) COPYRIGHT 2015 LAWSON SOFTWARE.  ALL RIGHTS RESERVED. *
 *   THE WORD AND DESIGN MARKS SET FORTH HEREIN ARE            *
 *   TRADEMARKS AND/OR REGISTERED TRADEMARKS OF LAWSON         *
 *   SOFTWARE AND/OR ITS AFFILIATES AND SUBSIDIARIES. ALL      *
 *   RIGHTS RESERVED.  ALL OTHER TRADEMARKS LISTED HEREIN ARE  *
 *   THE PROPERTY OF THEIR RESPECTIVE OWNERS.                  *
 *                                                             *
 ***************************************************************
 *                                                             *
 *  v1.0 - 9/7/2015 - Initial release                          *
 *                                                             *   
 ***************************************************************/
 var SQLiteDb = null;
 (function() {
	'use strict';
	
	angular
	.module('InforOfflineStorageManager')
	.run(function($ionicPlatform, $cordovaSQLite,DB_CONFIG,DBA) {

		$ionicPlatform.ready(function() {
	    
		    window.plugins.sqlDB.copy("mscm_data.db", 0, function() {
	            SQLiteDb = $cordovaSQLite.openDB("mscm_data.db");
	            console.error("Success");
	        }, function(error) {
	            console.error("There was an error copying the database: " + error.message);
	            SQLiteDb = $cordovaSQLite.openDB("mscm_data.db");
	        });


		 });


	});
	
})();	
