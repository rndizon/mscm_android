/***************************************************************
 *                                                             *
 *                           NOTICE                            * 
 *                                                             *
 *   THIS SOFTWARE IS THE PROPERTY OF AND CONTAINS             *
 *   CONFIDENTIAL INFORMATION OF LAWSON SOFTWARE AND/OR ITS    *
 *   AFFILIATES OR SUBSIDIARIES AND SHALL NOT BE DISCLOSED     *
 *   WITHOUT PRIOR WRITTEN PERMISSION. LICENSED CUSTOMERS MAY  *
 *   COPY AND ADAPT THIS SOFTWARE FOR THEIR OWN USE IN         *
 *   ACCORDANCE WITH THE TERMS OF THEIR SOFTWARE LICENSE       *
 *   AGREEMENT. ALL OTHER RIGHTS RESERVED.                     *
 *                                                             *
 *   (c) COPYRIGHT 2015 LAWSON SOFTWARE.  ALL RIGHTS RESERVED. *
 *   THE WORD AND DESIGN MARKS SET FORTH HEREIN ARE            *
 *   TRADEMARKS AND/OR REGISTERED TRADEMARKS OF LAWSON         *
 *   SOFTWARE AND/OR ITS AFFILIATES AND SUBSIDIARIES. ALL      *
 *   RIGHTS RESERVED.  ALL OTHER TRADEMARKS LISTED HEREIN ARE  *
 *   THE PROPERTY OF THEIR RESPECTIVE OWNERS.                  *
 *                                                             *
 ***************************************************************
 *                                                             *
 *  v1.0 - 9/8/2015 - Initial release                          *
 *                                                             *   
 ***************************************************************/
 (function() {
	'use strict';
	
	angular
		.module('InforGlobalization')
		.config(function($translateProvider) {
			
			$translateProvider.translations('es', {

				/*Side Menu */
	            menu_par_count: "Conde Par",
	            pick_for_par: "Elija Por Par",
	            cycle_count: "Contador de Cíclos",
	            logout: "Cerrar sesión",
	            settings: "Ajustes",

	            /* Content Text */
	            all_parforms: "Todas las Formas Par",
	            select_parforms_to_download: "Seleccione formas par descargar",
	            search_par_forms: "Buscar formas Par"
	        });


	    });
	
})();	
