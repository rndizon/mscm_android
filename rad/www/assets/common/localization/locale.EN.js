/***************************************************************
 *                                                             *
 *                           NOTICE                            * 
 *                                                             *
 *   THIS SOFTWARE IS THE PROPERTY OF AND CONTAINS             *
 *   CONFIDENTIAL INFORMATION OF LAWSON SOFTWARE AND/OR ITS    *
 *   AFFILIATES OR SUBSIDIARIES AND SHALL NOT BE DISCLOSED     *
 *   WITHOUT PRIOR WRITTEN PERMISSION. LICENSED CUSTOMERS MAY  *
 *   COPY AND ADAPT THIS SOFTWARE FOR THEIR OWN USE IN         *
 *   ACCORDANCE WITH THE TERMS OF THEIR SOFTWARE LICENSE       *
 *   AGREEMENT. ALL OTHER RIGHTS RESERVED.                     *
 *                                                             *
 *   (c) COPYRIGHT 2015 LAWSON SOFTWARE.  ALL RIGHTS RESERVED. *
 *   THE WORD AND DESIGN MARKS SET FORTH HEREIN ARE            *
 *   TRADEMARKS AND/OR REGISTERED TRADEMARKS OF LAWSON         *
 *   SOFTWARE AND/OR ITS AFFILIATES AND SUBSIDIARIES. ALL      *
 *   RIGHTS RESERVED.  ALL OTHER TRADEMARKS LISTED HEREIN ARE  *
 *   THE PROPERTY OF THEIR RESPECTIVE OWNERS.                  *
 *                                                             *
 ***************************************************************
 *                                                             *
 *  v1.0 - 9/8/2015 - Initial release                          *
 *                                                             *   
 ***************************************************************/
 (function() {
	'use strict';
	
	angular
		.module('InforGlobalization')
		.config(function($translateProvider) {
			
			$translateProvider.translations('en', {

				/*Side Menu */
                lbl_home: "Home",
	            lbl_dock_logging: "Dock Logging",
	            lbl_misc_receiving: "Misc Receiving",
	            lbl_po_receiving: "P.O. Receiving",
	            lbl_issues: "Issues",
	            lbl_issues_and_return: "Issues and Return",
	            lbl_picking: "Picking",
	            lbl_delivery: "Delivery",
	            lbl_settings: "Settings",
	            lbl_signout: "Signout",

	            all_parforms: "All Par Forms",
	            my_parforms: "My Par Forms",
	            search_par_forms: "Search Par forms",
	            par_count_title: "Par Count",
	            enter_purchase_order: "Enter Purchase Order",
	            search_a_location: "Search location",
	            search_a_vendor: "Search vendor",
	            search_a_printer: "Search printer",
	            search_a_po_number: "Search P.O. Number",
	            search_line_items: "Search Line Items",
	            search_items: "Search Item",
	            search_companies: "Search companies",
	            search_carrier: "Search carrier",

	            /* Buttons Text */
	            search_button : "SEARCH",
	            lbl_download: "DOWNLOAD",
	            btn_send: "SEND",
	            btn_edit: "EDIT",
	            btn_delete: "DELETE",
	            btn_done: "DONE",
	            btn_cancel: "CANCEL",
	            btn_print: "PRINT",
	            btn_deliver: "DELIVER",
	            btn_continue: "CONTINUE",
	            btn_filter: "FILTER",
	            btn_show_lines: "SHOW LINES",
	            btn_previous: "PREVIOUS",
	            btn_next: "NEXT",
	            btn_details: "DETAILS",
	            btn_return: "RETURN",
	            btn_add: "ADD",
	            btn_submit: "SUBMIT",
	            btn_update: "UPDATE",

	            /* Sub header instructions */
	            lbl_download_parcount: "Tap to Start par counting or Tap Send to Sync to back office.",
	            select_parforms_to_download: "Select par forms to download.",
	            tap_to_delete: "Tap check box to Delete.",
	            scan_or_enter_to_start_counting: "Scan or Enter par count to start counting. Tap Done to finish.",
	            select_company_and_delivery_location: "Select Company and delivery location",
	            scan_barcode_or_enter_tracking: "Scan or enter tracking number to list to dock log",
	            scan_barcode_or_enter_tracking_misc_rec: "Scan or enter carrier tracking number to receive miscellaneous packages",
	            scan_barcode_or_enter_details_to_start_po: "Scan barcode or enter details to start P.O. Receiving",
	            filter_purchase_order_number: "Filter Purchase Order Number",
	            check_mscm_server_for_status: "Check the MSCM Server for status of upload ",
	            check_report_for_delivery: "Check 'Report' for Delivery Document",
	            check_label_for_delivery_label: "Check 'Label 2' for Delivery Label",
	            add_item_and_requesting_location: "Add Item and Requesting Location",
	            select_inventory_location: "Select Inventory Location",
	            update_item_or_add_req_location: "Update Item or Add Requesting Location",
	            select_item_to_download: "Select Item to download o r tap refresh icon to update",
	            shipment_line_number: "Shipment line number",
	            to_toggle_grouping_option_tap: "To toggle grouping options tap",
	            select_location_and_press_send_to_submit: "Select location(s) and press 'SEND' to submit",

	            /* Label */
	            lbl_company: "Company",
	            lbl_location: "Location",
	            lbl_printer: "Printer",
	            lbl_change_company: "Change Company",
	            lbl_change_location: "Change Location",
	            lbl_change_printer: "Change Printer",
	            lbl_change_vendor: "Change Vendor",
	            lbl_change_po_company: "Change PO Company",
	            lbl_change_comp_and_loc: "Change Comp & Loc",
	            lbl_change_company_and_location: "Change Company and Location",
	            lbl_select_company: "Select Company",
	            lbl_select_location: "Select Location",
	            lbl_select_printer: "Select Printer",
	            lbl_select_receiving: "Select Receiving",
	            lbl_select_receiving_location: "Select Receiving Location",
	            lbl_select_item: "Select Item",
	            lbl_select_requesting: "Select Requesting",
	            lbl_count: "Count",
	            lbl_tracking: "Tracking",
	            lbl_tracking_number: "Tracking number",
	            lbl_enter_tracking_number: "Enter tracking number",
	            lbl_vendor: "Vendor",
	            lbl_select_vendor: "Select Vendor",
	            lbl_select_carrier: "Select Carrier",
	            lbl_confirm_receipt: "Confirm Receipt",
	            lbl_receives: "Receives",
	            lbl_received: "Received",
	            lbl_carrier: "Carrier",
	            lbl_carrier_tracking: "Carrier Tracking",
	            lbl_deliver_to: "Deliver to",
	            lbl_print_delivery_documents: "Print Delivery Documents",
	            lbl_reference: "Reference",
	            lbl_reference_number: "Reference Number",
	            lbl_reject_delivery: "Reject Delivery",
	            lbl_enter_delivery_reference: "Enter Delivery reference",
	            lbl_po_number: "P.O. Number",
	            lbl_pack_slip: "Pack Slip",
	            lbl_packing_slip: "Packing slip",
	            lbl_date: "Date",
	            lbl_po_date: "P.O. Date",
	            lbl_time: "Time",
	            lbl_dock_log_created: "Dock Log Created",
	            lbl_selected: "Selected",
	            lbl_po_no: "P.O. No.",
	            lbl_po_number: "P.O. Number",
	            lbl_release_number: "Release Number",
	            lbl_po_release_number: "P.O. Release No.",
	            lbl_po_code: "P.O. Code",
	            lbl_look_up: "Look Up",
	            lbl_select_po_number: "Select P.O. Number",
	            lbl_filter: "Filter",
	            lbl_po_status: "P.O. Status",
	            lbl_buyer: "Buyer",
	            lbl_ship_to: "Ship To",
	            lbl_requester: "Requester",
	            lbl_receiving_location: "Receiving Location",
	            lbl_bill_of_lading: "Bill of Lading",
	            lbl_enter_received_qty: "Enter Received Quantity",
	            lbl_gtin_upn: "GTIN/UPN",
	            lbl_manufacturing_item: "Manufacturing Item",
	            lbl_vendor_item: "Vendor Item",
	            lbl_note: "Note",
	            lbl_delivery_date: "Delivery Date",
	            lbl_priority: "Priority",
	            lbl_remaining: "Remaining",
	            lbl_line_details: "Line Details",
	            lbl_required: "Required",
	            lbl_additional_info: "Additional Info",
	            lbl_cancel_backorder: "Cancel Backorder",
	            lbl_split_qty: "Split Qty",
	            lbl_total_of: "Total of",
	            lbl_item: "Item",
	            lbl_items: "Items",
	            lbl_line_items: "Line Items",
	            lbl_finish_receiving: "Finish Receiving",
	            lbl_lines: "Lines",
	            lbl_total_lines: "Total Lines",
	            lbl_lines_received: "Lines Received",
	            lbl_total_tracking_number: "Total Tracking Number",
	            lbl_send_options: "Send Options",
	            lbl_save: "Save",
	            lbl_add_release_and_print: "Add, Release, and Print",
	            lbl_save_release_and_print: "Save, Release, and Print",
	            lbl_add_receipt: "Add receipt",
	            lbl_add_vendor: "Add Vendor",
	            lbl_add_carrier: "Add Carrier",
	            lbl_mfr_no: "Mfr No.",
	            lbl_bin: "Bin",
	            lbl_bin_no: "Bin No",
	            lbl_serial_lot: "Serial/Lot",
	            lbl_requesting_location: "Requesting Location",
	            lbl_ea: "EA",
	            lbl_line_number: "Line Number",
	            lbl_lot: "Lot",
	            lbl_loc_type: "Loc Type",
	            lbl_location_type: "Location Type",
	            lbl_assignment: "Assignment",
	            lbl_enter_inventory_location: "Enter Inventory Location",
	            lbl_edit: "Edit",
	            lbl_additional_options: "Additional Options",
	            lbl_void_kill: "Void / Kill",
	            lbl_pull: "Pull",
	            lbl_pulled: "Pulled",
	            lbl_shipment_lines: "Shipment Lines",
	            lbl_pick: "Pick",
	            lbl_pick_lines: "Pick Lines",
	            lbl_submit_picking: "Submit Picking",
	            lbl_select_all: "Select All",
	            lbl_clear_all: "Clear All",
	            lbl_tracking_already_exist: "Tracking number already on list. Remove ?",
	            lbl_changing_loc_clear_tracking: "Changing receiving location clears all entered tracking numbers. Proceed ?",
	            lbl_changing_com_clear_tracking: "Changing company clears all entered tracking numbers. Proceed ?",
	            lbl_dock_log_summary: "Dock Log Summary",
	            lbl_delivery_options: "Delivery Options",
	            lbl_deliver: "Deliver",
	            lbl_allow: "Allow",
	            lbl_reject: "Reject",
	            lbl_select_delivery_vendor_and_reference: "Select delivery vendor and reference",
	            lbl_add_code_and_delivery_note: "Add code and delivery note",
	            lbl_note_code: "Note code",
	            lbl_description: "Description",
	            lbl_info: "Info",
	            lbl_delivery_details: "Delivery details",
	            lbl_misc_receipt_summary: "Misc Receipt Summary",
	            lbl_label_printer: "Label Printer",
	            lbl_report_printer: "Report Printer",
	            lbl_pull_to_refresh: "Pull to refresh",



	        });


	    });
	
})();	
