(function(){
 	'use strict';

	angular
		.module("InforSecurityModule")
		.run(function($rootScope) {
			
			/*
			 *  Public sign out method of Lawson security 
			 */
			//$rootScope.signOut = InforSecurityService.signOut();
			$rootScope.signInCallBack = function(){
				

					var userContext = lsconnect.getSelectedProfile();
				  	if (userContext.isAuthenticated == 'true') 
				    {
				  		
				  		alert(userContext.profileName+ " : " + userContext.userName+ " "+userContext.isAuthenticated + " " +userContext.httpScheme + '://' + userContext.endpoint);
		         		console.log("SignInCallback");
		         		
				  	} 
				  	else 
				  	{
				  		// If the user is not logged in, open the profile list
				  		lsconnect.openProfileAuth();
				  		console.log("SignInCallback open profile auth");
				  	}

				
				
			}
		});		
			
})();
