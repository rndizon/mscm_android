 /***************************************************************
 *                                                             *
 *                           NOTICE                            * 
 *                                                             *
 *   THIS SOFTWARE IS THE PROPERTY OF AND CONTAINS             *
 *   CONFIDENTIAL INFORMATION OF LAWSON SOFTWARE AND/OR ITS    *
 *   AFFILIATES OR SUBSIDIARIES AND SHALL NOT BE DISCLOSED     *
 *   WITHOUT PRIOR WRITTEN PERMISSION. LICENSED CUSTOMERS MAY  *
 *   COPY AND ADAPT THIS SOFTWARE FOR THEIR OWN USE IN         *
 *   ACCORDANCE WITH THE TERMS OF THEIR SOFTWARE LICENSE       *
 *   AGREEMENT. ALL OTHER RIGHTS RESERVED.                     *
 *                                                             *
 *   (c) COPYRIGHT 2015 LAWSON SOFTWARE.  ALL RIGHTS RESERVED. *
 *   THE WORD AND DESIGN MARKS SET FORTH HEREIN ARE            *
 *   TRADEMARKS AND/OR REGISTERED TRADEMARKS OF LAWSON         *
 *   SOFTWARE AND/OR ITS AFFILIATES AND SUBSIDIARIES. ALL      *
 *   RIGHTS RESERVED.  ALL OTHER TRADEMARKS LISTED HEREIN ARE  *
 *   THE PROPERTY OF THEIR RESPECTIVE OWNERS.                  *
 *                                                             *
 ***************************************************************
 *                                                             *
 *  v1.0 - 8/6/2015 - Initial release                          *
 *                                                             *   
 ***************************************************************/
(function(){
	
	
	function AppDataObject() 
	{
		this.username = 'u19670';
		this.password = 'lawson';
		this.host = '';
		this.port = '';
		this.userObj = null;
		this.navigationControllerStack = [];
		this.useSecurityPlugin = true;
	}

	// AppDataObject constants
	AppDataObject.HOST_PROP = 'com.infor.mobile.host_prop';
	AppDataObject.PORT_PROP = 'com.infor.mobile.port_prop';
	AppDataObject.USERNAME_PROP = 'com.infor.mobile.username';
	AppDataObject.APPLICATION_NAME = "Infor MSCM";
	AppDataObject.VERSION_NUMBER = "10.0.1.0";
	AppDataObject.COPYRIGHT = "Copyright &copy 2015 Infor. All rights reserved. www.infor.com";
	AppDataObject.IS_CHROME = false;
	AppDataObject.Loader = null;
	
	appDataObject = new AppDataObject();
	
	
	signInCallBack = function() {

		var userContext = lsconnect.getSelectedProfile();
	  	if (userContext.isAuthenticated == 'true') 
	    {
	  		var url = userContext.httpScheme + '://' + userContext.endpoint;
	  
	  		UserManager.SERVER_URL = url;
	  		UserManager.PROFILE_URL_900 = url + "/servlet/Profile?_SECTION=attributes";
	  		UserManager.username = userContext.userName;
	  		UserManager.SYSTEM_CODE = "IC";
	  
	  		var xmlhttp = new XMLHttpRequest();
	  		appDataObject.userObj = new UserManager(UserManager.TECHNOLOGY_900, xmlhttp, true, true, function(){ 
	  			
	  			console.log("User manager response");
	  		    
	  		   // util.hideLoadingMsg();
	  			if (!appDataObject.userObj.success)
	  			{
	  			    console.log("UserManager Response not successful");
	  			    // Get error Status
	  				if (appDataObject.userObj.errorMsg)
	  			        appDataObject.userObj.handleError();	
	  				else
	  				{
	  				    // Check product line, user access, and connectivity
	  				    if (!appDataObject.userObj.prodline)
	  				    	console.log("User Information not found on this product line. Contact your administrator.")
	  			        else 
	  			        	console.log("There is a problem connecting to the server. Check your connection and try again.");
	  		  		} 
	  		  		  
	  		  		lsconnect.openProfileAuth();  
	  		        return;		
	  			}
	  			    		
	  			// Load on success
	  			if (appDataObject.userObj.login)
	  			{
	  				
	  			// Setup the loader
	  				AppDataObject.Loader.hide();
	  				
	  			    // Check if user has MobileUser=MobileAssets attribute
	  				if (false)
	  				{
	  				    var okFunction = function () { 
	  			          //  signOut(); 
	  			        };
	  		        
	  			      console.log("You do not have sufficient rights to access this application. Contact your administrator.", okFunction );
	  				    return;
	  		        } 
	  			
	  			    
	  		    	console.log("Change Page SIGN IN");	
	  		    }
	  		    else
	  		    {
	  		    	console.log("There is a problem logging in. Check your server settings and try again.");
	  		        lsconnect.openProfileAuth();
	  		    }
	  			
	  		
	  		}, true);
	  		appDataObject.username = userContext.userName;
	  		appDataObject.lawServerURL = url;
	  		console.log("Callback SignInCallback");
	  	} 
	  	else 
	  	{
	  		// If the user is not logged in, open the profile list
	  		lsconnect.openProfileAuth();
	  		console.log("SignInCallback Calll open profile auth");
	  	}

	}
	
	
	
	
	//Define Module Name
	angular
		.module("InforSecurityModule", [])
		.service("InforSecurityService", InforSecurityService);

		//Methods of InforSecurity Service
		function InforSecurityService($ionicLoading) {

			var vm = this;
			
			AppDataObject.Loader = $ionicLoading;
			
			var loginService = {
				signIn: signIn,
				signOut : signOut

			};

			return loginService;
	

			//Service functions
			function signIn() {
				
				// Setup the loader
				  $ionicLoading.show({
				    templateUrl: 'html/templates/spinner.html'
				  });
				
				var userAssertionContext = lsconnect.assertForAuth("signInCallBack");	
				//alert("userAssertionContext");
				if (userAssertionContext != null) {
					
					if (userAssertionContext.NETWORK_STATUS === "SUCCESS") {
						//alert("SUCCESS");
						/*var userContext = lsconnect.getSelectedProfile();
					  	if (userContext.isAuthenticated == 'true') 
					    {
					  		userContext.httpScheme + '://' + userContext.endpoint;
					  	  
					  		//alert(userContext.profileName+ " : " + userContext.userName+ " "+userContext.isAuthenticated);
			         		console.log("SignInCallback: "+userContext.profileName+ " : " + userContext.userName+ " "+userContext.isAuthenticated+ " URL"+userContext.httpScheme + '://' + userContext.endpoint);
			         		
					  	} 
					  	else 
					  	{
					  		// If the user is not logged in, open the profile list
					  		lsconnect.openProfileAuth();
					  		console.log("SignInCallback open profile auth");
					  	}*/
						 $ionicLoading.hide();
						signInCallBack();

					} 
					else {
						//alert("NETWORK_STATUS FAILED");
						console.log("signIn: Connectivity failure. NETWORK_STATUS[" + 
										userAssertionContext.NETWORK_STATUS + "]");
					}
				
					
				} 	
				else {
					
					console.log("signIn: User Assertion context is null.");
				}

			}

			function signOut() {
				event.preventDefault();
				//alert("SIGNOUT");
				var userAssertionContext = lsconnect.getSelectedProfile();
				lsconnect.logout(userAssertionContext.endpoint);

			}

		} 

})();