package com.infor.mscm.scanner;

import android.app.Activity;
import android.app.AlertDialog;
import android.view.KeyEvent;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;

import java.io.IOException;

import com.honeywell.decodemanager.DecodeManager;
import com.honeywell.decodemanager.barcode.DecodeResult;
import com.honeywell.decodemanager.barcode.CommonDefine;

import com.honeywell.decodemanager.SymbologyConfigs;
import com.honeywell.decodemanager.symbologyconfig.SymbologyConfigCodeEan13;
import com.honeywell.decodemanager.symbologyconfig.SymbologyConfigCodeEan8;
import com.honeywell.decodemanager.symbologyconfig.SymbologyConfigCodeUPCA;
import com.honeywell.decodemanager.symbologyconfig.SymbologyConfigCodeUPCE;


public class HoneywellScanner extends Scanner 
{
	private final int SCANKEY = 0x94;
	private final int SCANTIMEOUT = 5000;
	
	private static DecodeManager decodeManager = null;
	private boolean m_bIsKeyDown;	
	
	public static Activity m_activityMain;
	
	public HoneywellScanner() 
	{
		m_bIsKeyDown = true;
	}
	
	@Override
	public void initialize(Activity activity) 
	{

		Log.v("HONEYWELL",":initialize");
		m_activityMain = activity;
		if (decodeManager == null) 
		{
			Log.v("HONEYWELL","Decode Manager SIYA");
			decodeManager = new DecodeManager(activity, scanResultHandler);
			Log.v("HONEYWELL","Converted"+decodeManager.toString());
		}
	}
	
	@Override
	public void deinitialize() 
	{
		super.deinitialize();
		
		if (decodeManager != null) 
		{
			try 
			{
				decodeManager.release();
				decodeManager = null;
			} 
			catch (IOException e) 
			{
				Log.v("Honeywell", "HoneywellScanner::deinitialize IOException");
			}
		}				
	}
	
	@Override
	public boolean processScanKey(int keyCode, KeyEvent event) {
		
		Log.v("Honeywell", "ScannerManager processScanKey process");
		
		if (!m_IsEnabled)
		{
			Log.v("CordovaLog", "Scanner Inactive");
			return false;
		}
		
		Log.v("Honeywell", "Scanner Active" +keyCode+ " "+ KeyEvent.KEYCODE_UNKNOWN);
		
		if (keyCode == KeyEvent.KEYCODE_UNKNOWN) 
		{
        	if (event.getScanCode() == SCANKEY 
        	||  event.getScanCode() == 87 
        	||  event.getScanCode() == 88) 
        	{
        		if (event.getAction() == KeyEvent.ACTION_DOWN)
        		{	
        			if (m_bIsKeyDown) 
					{
						try
        				{
							executeScan();
							m_bIsKeyDown = false;
						}
        				catch (Exception e) 
						{
        					Log.v("CordovaLog", "HoneywellScanner::processScanKey KeyEvent.ACTION_UP RemoteException");
						}
        				
        				return true;
					}
	        	}
        		
        		if (event.getAction() == KeyEvent.ACTION_UP)
        		{
        			try 
					{
        				m_bIsKeyDown = true;
        				cancelScan();
        				
			        	return true;
					} 
					catch (Exception e) 
					{
						Log.v("CordovaLog", "HoneywellScanner::processScanKey KeyEvent.ACTION_UP RemoteException");
					}
        		}
			}
		}
		
		return false;
	}
	
	private void executeScan() throws Exception 
	{
		try 
		{
			Log.v("CordovaLog", "EXECUTE SCAN");
			decodeManager.doDecode(SCANTIMEOUT);
		} 
		catch (RemoteException e) 
		{
			Log.v("CordovaLog", "HoneywellScanner::executeScan RemoteException");
		}
	}
	
	private void cancelScan() throws Exception 
	{
		decodeManager.cancelDecode();
	}
	
	private static Handler scanResultHandler = new Handler() 
	{
		public void handleMessage(Message msg) 
		{
			Log.v("CordovaLog", "EXECUTE SCAN PUMASOK scanResultHandler");
			switch (msg.what) 
			{
			case DecodeManager.MESSAGE_DECODER_COMPLETE:
				{
					DecodeResult decodeResult = (DecodeResult) msg.obj;
					
					ScannerSound.playSuccessSound();
										 
					String barcodeId = "]" + (char)decodeResult.aimId + (char)decodeResult.aimModifier;
					
					scanSuccess(barcodeId + decodeResult.barcodeData);
				}
				break;
			case DecodeManager.MESSAGE_DECODER_FAIL: 
				{
					ScannerSound.playFailedSound();
					
					scanFailed();
				}
				break;
			case DecodeManager.MESSAGE_DECODER_READY:
				{
					try 
					{
						decodeManager.enableSymbology(CommonDefine.SymbologyID.SYM_ALL);
						
						SymbologyConfigCodeEan13 ean13 = new SymbologyConfigCodeEan13();
						ean13.enableCheckTransmit(true);
						ean13.enableAddenda2Digit(true);
						ean13.enableAddenda5Digit(true);
						ean13.enableSymbology(true);
						
						SymbologyConfigCodeEan8 ean8 = new SymbologyConfigCodeEan8();
						ean8.enableCheckTransmit(true);
						ean8.enableAddenda2Digit(true);
						ean8.enableAddenda5Digit(true);
						ean8.enableSymbology(true);
						
						SymbologyConfigCodeUPCA upca = new SymbologyConfigCodeUPCA();
						upca.enableCheckTransmit(true);
						upca.enableAddenda2Digit(true);
						upca.enableAddenda5Digit(true);
						upca.enableSendNumSys(true);
						upca.enableSymbology(true);
						
						SymbologyConfigCodeUPCE upce = new SymbologyConfigCodeUPCE();
						upce.enableCheckTransmit(true);
						upce.enableAddenda2Digit(true);
						upce.enableAddenda5Digit(true);
						upca.enableSendNumSys(true);
						upce.enableUPCE(true);
						upce.enableSymbology(true);
						
						SymbologyConfigs symconfig = new SymbologyConfigs();
						symconfig.addSymbologyConfig(ean13);
						symconfig.addSymbologyConfig(ean8);
						symconfig.addSymbologyConfig(upca);
						symconfig.addSymbologyConfig(upce);

						decodeManager.setSymbologyConfigs(symconfig);
					} 
					catch (RemoteException e) 
					{
						Log.v("CordovaLog", "HoneywellScanner::handleMessage RemoteException");
					}
				}
				break;
			default:
				super.handleMessage(msg);
				break;
			}
		}
	};
}

