/***************************************************************
 *                                                             *
 *                           NOTICE                            * 
 *                                                             *
 *   THIS SOFTWARE IS THE PROPERTY OF AND CONTAINS             *
 *   CONFIDENTIAL INFORMATION OF LAWSON SOFTWARE AND/OR ITS    *
 *   AFFILIATES OR SUBSIDIARIES AND SHALL NOT BE DISCLOSED     *
 *   WITHOUT PRIOR WRITTEN PERMISSION. LICENSED CUSTOMERS MAY  *
 *   COPY AND ADAPT THIS SOFTWARE FOR THEIR OWN USE IN         *
 *   ACCORDANCE WITH THE TERMS OF THEIR SOFTWARE LICENSE       *
 *   AGREEMENT. ALL OTHER RIGHTS RESERVED.                     *
 *                                                             *
 *   (c) COPYRIGHT 2015 LAWSON SOFTWARE.  ALL RIGHTS RESERVED. *
 *   THE WORD AND DESIGN MARKS SET FORTH HEREIN ARE            *
 *   TRADEMARKS AND/OR REGISTERED TRADEMARKS OF LAWSON         *
 *   SOFTWARE AND/OR ITS AFFILIATES AND SUBSIDIARIES. ALL      *
 *   RIGHTS RESERVED.  ALL OTHER TRADEMARKS LISTED HEREIN ARE  *
 *   THE PROPERTY OF THEIR RESPECTIVE OWNERS.                  *
 *                                                             *
 ***************************************************************
 *                                                             *
 *  v1.0 - 9/8/2015 - Initial release                          *
 *                                                             *   
 ***************************************************************/
 (function() {
	'use strict';
	
	angular
		.module('InforGlobalization')
		.config(function($translateProvider) {
			
			$translateProvider.translations('en', {

				/*Side Menu */
	            menu_par_count: "Par Count",
	            pick_for_par: "Pick For Par",
	            cycle_count: "Cycle Count",
	            logout: "Logout",
	            settings: "Settings",

	            /* Content Text */
	            parforms: "Par Forms",
	            home_parforms: "Par Forms",
	            home_pfp_tickets: "PFP TICKETS",
	            home_last: "Last",
	            home_counted: "Counted",
	            home_spent: "Spent",
	            home_time: "Time",
	            home_items: "Items",
	            home_picked: "Picked",
	            home_used: "Used",
	            home_select_id: "Select ID",
	            home_select_ids: "Select IDs",
	            home_last_par_count: "Last Par Count",
	            home_last_par_forms_used: "Last Par Forms Used",
	            home_last_par_count_status: "Last par count status",
	            home_cycle_count: "Cycle Count",
	            home_last_cycle_count: "Last Cycle Count",
	            home_last_select_id_used: "Last Select ID used",
	            home_pick_for_par: "Pick for Par",
	            qty: "Qty",
	            all_parforms: "All Par Forms",
	            my_parforms: "My Par Forms",
	            search_par_forms: "Search Par Forms",
	            search_item: "Search Items",
	            search_a_company: "Search a company",
	            search_a_location: "Search a location",
	            par_location: "Par Location",
	            search_companies: "Search companies",
	            lbl_of: "of",
				decimal_places:"4",
				round_up:"up",

	            /* Buttons Text */
	            search_button : "SEARCH",
	            lbl_download: "DOWNLOAD",
	            lbl_send_btn: "SEND",
	            lbl_send_all_btn: "Send All",
	            btn_ok: "OK",
	            btn_edit: "EDIT",
	            btn_delete: "DELETE",
	            btn_done: "DONE",
	            btn_cancel: "CANCEL",
	            btn_clear_all: "CLEAR ALL",

	            /* Sub header instructions */
	            lbl_download_parcount: "Scan barcode or tap + to add par forms",
	            select_parforms_to_download: "Select par forms to download.",
	            tap_to_delete: "Check the rows you want to delete.",
	            scan_or_enter_to_start_counting: "Enter par count. Tap Done when finished.",
	            lbl_pull_to_refresh: "Pull to refresh...",
	            scan_parforms_and_perform_picking: "Scan par forms or tap + to select par forms and perform picking.",
	            enter_pulled_quantity: "Enter pulled quantity for each item. Tap Done when finished picking.",
	            tap_item_enter_pull_quantity: "Tap on an item to see details or enter pull quantity.",

	            /* Instruction */
	            ins_scan_bar_code_or_tap_plus: "Scan barcode or tap plus button to Add Par Forms.",
	            ins_set_default_company: "Set Default Company",
	            ins_tap_plus_to_add_par_forms: "Tap plus button to add Parforms.",
	            ins_barcode_scan_tone_setting: "Set a different tone for Good and Fail barcode scan",

	            /* templates */
	            /* welcome */
	            lbl_welcome_to_mpc: "Home",
	            lbl_location_unknown: "Location unknown",
	            
	            /* settings */
	            lbl_select_options_to_change: "Select options to change",
	            lbl_application_settings: "Application Settings",
	            lbl_server: "Server",
	            lbl_barcode_scan_sound: "Barcode Scan Sound",
	            lbl_printer_defaults: "Printer Defaults",
	            lbl_logging: "Logging",
	            lbl_module_settings: "Module Settings",
	            lbl_par_count_defaults: "My Par Count Defaults",
	            lbl_cycle_count_defaults: "My Cycle Count Defaults",
	            lbl_pick_for_par_defaults: "My Pick for Par Defaults",
	            lbl_general_settings: "General Settings",
	            lbl_show_help: "Show Help",
	            lbl_airplane_mode: "Airplane Mode",
	            lbl_reset_defaults: "Reset Defaults",
	            lbl_about_mpc: "About MPC",
	            lbl_shelf: "Shelf",
	            lbl_gtin: "GTIN",
	            lbl_hibc: "HIBC",
	            lbl_mfr_no: "Mfg No.",
	            lbl_bin_no: "Bin No.",
	            lbl_serial_no: "Serial No",
	            lbl_lot_no: "Lot No",
	            lbl_pending_quantity: "Pending Quantity",
	            lbl_requested_quantity: "Requested Quantity",
	            lbl_enable_email: "Enable Email",
	            lbl_theme: "Theme",
	            lbl_reset_tutorials: "Reset Tutorials",
	            lbl_default_dashboard_setting: "Default dashboard setting",

	            title_server_settings: "Server Settings",
	            lbl_server_info: "Server Info",
	            lbl_profile_name: "Profile Name",
	            lbl_demo_server: "Demo Server",
	            lbl_lawson_server_address: "Lawson Server Address",
	            lbl_product_line: "Product Line",
	            lbl_mscm_server: "MSCM Server",
	            lbl_kiosk_mode: "Kiosk Mode",
	            lbl_server_request_timeout: "Server Request Timeout",
	            lbl_duration: "Duration",
	            text_kiosk_mode: "Kiosk mode locks this app to run alone on this device.",
	            lbl_kiosk: "kiosk",
	            text_timeout_idle_login_screen: "Increase timeout setting to wait longer for server to respond.",
	            lbl_timeout: "Timeout",
	            time_minutes: "minutes",
	            time_minute: "minute",
	            time_hour: "Hour",
	            time_seconds: "seconds",
	            text_default_good_scan: "Default Good Scan",
	            text_default_bad_scan: "Default Bad Scan",

	            title_printer_defaults: "Printer Defaults",
	            title_set_default_printer: "Set Default Printer",
	            title_select_label_printer: "Select Label Printer",
	            text_type_the_printer_name: "Type the printer name",
	            text_change_printer: "Tap to change printer.",
	            lbl_label_printer: "Label Printer",
	            lbl_report_printer: "Report Printer",
	            lbl_set_label_printer: "Set Label Printer",

	            /* Options */
	            lbl_select_all: "Select All",
	            lbl_clear_all: "Clear All",
	            lbl_change_company: "Change Company",
	            lbl_change_printer: "Change Printer",
	            lbl_single_sweep: "Single Sweep",
	            lbl_edit: "Edit",
	            lbl_all: "All",

	            /* modules */
	            title_my_par_count_default: "My Par Count Default",
	            title_my_cycle_count_default: "My Cycle Count Default",
	            title_my_pick_for_par_default: "My Pick for Par Default",
	            par_company_info: "Par Company Info",
	            par_counting_company: "Par Counting Company",
	            default_company: "Default Company",
	            title_home: "Home",
	            title_last_login: "Last Logged in",
	            title_par_form: "Parforms",
	            text_par_item: "Par Item Default View",
	            lbl_detail: "Detail",
	            opt_first_par_item: "First par item",
	            opt_blank_awaiting_scan: "Blank awaiting scan",
	            text_default_printer: "Default Printer",
	            text_cycle_count_company_info: "Cycle Count Company Info",
	            text_cycle_count_company: "Cycle Counting Company",
	            lbl_enable: "Enable",
	            lbl_validate_freeze_qty: "Validate Freeze Qty",
	            lbl_validate: "Validate",
	            text_type_id_or_company: "Type the ID or company name",
	            lbl_search_id_or_company_name: "Search ID or company name",


	            /* forms */
	            add_par_forms: "Add Par Forms",
	            my_par_forms: "My Par Forms",
	            text_change_item_detail_priority: "Change Item Detail Priority",
	            text_first_par_item_detail: "First Par Item Detail",
	            option_data_for_first_par_item: "Data for first par Item",
	            lbl_toggle_blind_counting: "Toggle Blind Counting",
	            lbl_blind_counting: "Blind Counting",
	            lbl_toggle_freeze_qty: "Toggle Freeze Qty",
	            lbl_validate_against_freeze_qty: "Validate against freeze qty",
	            signout: "Signout",
	            signoff: "Sign off",

	            /* logging */
	            title_logging: "Logging",
	            text_change_logging_level: "Logging contains events logged by the app. Consult the documentation.",
	            lbl_log_path: "Log File Path",
	            lbl_log_level: "Log Level",
	            lbl_info: "Info",
	            text_max_log_file_size: "Max Log File Size",
	            text_max_log_size: "Max Log Size",
	            text_max_log_size_mb: "Max Log Size (Mb)",
	            btn_delete_logs: "Delete Logs",
                btn_send_email: "Send to Email",
	            title_change_location: "Change Location",
	            btn_search: "SEARCH",
	            radio_location_transfer: "1 - Location 1 Transfer",
	            radio_location_intransit_transfer: "2 - Location 2 Intransit Transfer",
	            radio_dg_loc: "3 - DG Loc",

	            lbl_average_par: "Average Par",
	            lbl_suggested: "Suggested",
	            lbl_current: "Current",
	            lbl_utilization: "Utilization",
	            lbl_level: "Level",
	            lbl_par_level: "Par level",

	            opt_verbose: "Verbose",
	            opt_debug: "Debug",
	            opt_info: "Info",
	            opt_warning: "Warning",
	            opt_error: "Error",
	            opt_none: "None",

	            /* Company Location */
	            title_change_company_and_location: "Change Company and location",
	            lbl_company: "Company",
	            lbl_location: "Location",
	            title_change_company: "Change Company",

	            text_tap_to_change_scand_sound: "Tap to change scan sound",
	            text_default_success_scan: "Default Success Scan",
	            text_default_fail_scan: "Default Fail Scan",
	            opt_high_tone: "High Tone",
	            opt_low_tone: "Low Tone",
	            opt_high_beep: "High Beep",
	            opt_low_beep: "Low Beep",
	            opt_one_tone: "One Tone",
	            opt_two_tone: "Two Tone",
	            opt_three_tone: "Three Tone",

	            lbl_version_history: "Version history",
	            lbl_mobile_par_and_cycle_counting: "Mobile Par And Cycle Counting",
	            lbl_version: "Version",
	            lbl_rate_this_app: "Rate this App",

	            /*pick for par*/
	            lbl_selected: "Selected",
	            lbl_company_name: "Company Name",
	            text_pfps_to_download: "PFP to download",
	            lbl_original: "ORIGINAL",
	            lbl_par_level_ac: "PAR LEVEL",
	            lbl_remaining: "REMAINING",
	            lbl_pick: "PICK",
	            lbl_pulled: "PULLED",
	            lbl_pulled_qty: "PULLED QTY",
	            lbl_previous: "PREVIOUS",
	            lbl_next: "NEXT",
	            lbl_total: "Total",
	            lbl_count: "COUNT",
	            lbl_count_ea: "COUNT",
	            title_change_stock_location: "Change Stock Location",
	            lbl_printer: "Printer",
	            confirm_pfp_pending_unsent:"There are pending unsent pick for par tickets. Discard them ?",
	            confirm_pfp_changing_company:"Changing company removes pick for par tickets on the list. Proceed ?",
	            corfirm_pfp_no_printer_selected:"Select a printer to print Pick for Par report before sending to server.",
	            corfirm_pfp_changing_inventory_location:"App currently supports one inventory location at a time. Clear current list ?",
	            

	            /*par counting*/
	            lbl_continue: "CONTINUE",
	            par_counting: "Par Counting",
	            lbl_done: "DONE",
	            lbl_additional_qty: "ADD'L QTY",
	            lbl_qoo: "Q.O.O.",
	            text_select_forms_to_count: "Select forms to count",
	            text_count_forms_to_download: "Count forms to download",
	            text_tap_ok_to_add_or_x_to_cancel: "Tap OK to add or X to cancel",
	            text_select_parforms_to_count: "Select the par forms you would like to count and tap OK.",
	            text_enter_par_count_tap_done_when_finished: "Enter par count for each item. Tap Done when finished counting.",
	            total_selected_parforms:"Total Selected ",
	            total_par_locations:"Total Par Locations ",
	            lbl_default_view : "Show",
	            lbl_edit_par_level: "Edit Par level",
	            text_replenishment_par_level: "Replenishment shall be based on this new par level.",
	            lbl_average_par_utilization: "Average Par Utilization",
	            lbl_suggested_par_level: "Suggested Par Level",
	            lbl_current_par_level: "CURRENT PAR LEVEL",
	            lbl_new_par_level: "NEW PAR LEVEL",
	            type_par_form_id_or_description: "Type par form ID or description",
	            tap_item_to_see_details: "Tap on an item to see details or enter par count",
	            type_item_id_or_desc: "Type Item ID or description",
	            title_par_form_item: "Par Form Item",
	            confirm_par_count_change_module: "There are pending unsent par forms. Discard them ?",
	            confirm_par_count_change_company: "Changing company removes par forms on the list. Proceed ?",
	            confirm_par_count_refresh_list:"Refreshing this list resets all counted par forms. Proceed?",
	            confirm_par_count_check_minimum_order:"This item only accept minimum order quantity of (X). Set count to par level ?",
	            confirm_par_count_check_max_additional_quantity:"Maximum additional quantity for this item is (X). Set to maximum ?",
	            confirm_par_count_check_min_additional_quantity:"This item only accept minimum order quantity of X. Set additional quantity to minimum order ?",
	            confirm_par_count_check_if_there_is_changes_for_item_details:"There are entered and modified par form item counts. Discard changes ?",
	            
	            /* Cycle count add parform */
	            lbl_title_add_parform: "Add Par Forms",
	            tap_to_add_or_delete: "Tap Ok to add or X to cancel",
	            seleted_parform: "Selected ({0})",
	            choose_id_to_count: "Choose the select IDs you would like to count and tap OK",
	            tap_item_to_see_cycle_details: "Tap on an item to see details or enter count.",

	            /* Cycle count */
	            lbl_title_cycle_count: "Cycle Count",
	            title_count_form_item: "Count Form Item",
	            text_enter_count_for_each_item: "Enter count for each item. Tap Done when finished counting.",
	            select_forms_to_count: "Select forms to count",
	            count_forms_to_download: "Count forms to download",
	            search_select_id: "Search Select ID",
	            type_select_id: "Type Select ID",
	            add_select_ids: "Add Select IDs",
	            total_selected_ids: "Total Select IDs",
	           	freeze_qty: "Freeze Qty",
	           	text_tap_to_add_select_id: "Tap + to add Select IDs that you want to perform cycle counting",
	           	text_count_qty_warning: "Count quantity cannot be more than par level. Set to par level ?",
	           	text_unsent_parform_warning: "There are pending unsent par forms. Discard them and sign out ?",
	           	text_set_def_comp: "Set as default company ?",
	           	text_freeze_quantiy_not_match: "Freeze quantity does not match entered count. Ensure correct count is entered.",

	            /* Downloaded Cycle Count Details List*/
	            confirm_cycle_count_pending_unsent: "There are pending unsent select IDs. Discard them?",
	            confirm_cycle_count_change_company: "Changing company removes select IDs on the list. Proceed?",
	            confirm_cycle_count_refresh: "Refreshing this list resets all counted select IDs. Proceed?",
				confirm_cycle_count_close: "There are entered and modified item counts. Discard changes?",
				confirm_cycle_count_invalid_serial_input: "This is a serial item which can only take a count of 0 or 1.",
				confirm_cycle_count_entry_no_internet_connection: "Server cannot be reached at the moment. Use cached data?",
				confirm_cycle_count_connect_to_server_failed: "Connection to server failed. Sign out?",
				confirm_cycle_refresh_no_internet: "Connection to server failed. Select IDs are not updated.",
				confirm_cycle_send_no_internet: "Connection to server failed. Try again?",
				confirm_cycle_no_select_id_available: "You have no access to any select IDs. Contact your administrator.",
				confirm_cycle_no_select_id_available_for_company: "No select IDs associated with this company. Change company?",
				confirm_cycle_default_company_not_available : "Your default company is no longer available. Select a new company?"
	        });


	    });
	
})();	
