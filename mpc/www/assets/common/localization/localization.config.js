/***************************************************************
 *                                                             *
 *                           NOTICE                            * 
 *                                                             *
 *   THIS SOFTWARE IS THE PROPERTY OF AND CONTAINS             *
 *   CONFIDENTIAL INFORMATION OF LAWSON SOFTWARE AND/OR ITS    *
 *   AFFILIATES OR SUBSIDIARIES AND SHALL NOT BE DISCLOSED     *
 *   WITHOUT PRIOR WRITTEN PERMISSION. LICENSED CUSTOMERS MAY  *
 *   COPY AND ADAPT THIS SOFTWARE FOR THEIR OWN USE IN         *
 *   ACCORDANCE WITH THE TERMS OF THEIR SOFTWARE LICENSE       *
 *   AGREEMENT. ALL OTHER RIGHTS RESERVED.                     *
 *                                                             *
 *   (c) COPYRIGHT 2015 LAWSON SOFTWARE.  ALL RIGHTS RESERVED. *
 *   THE WORD AND DESIGN MARKS SET FORTH HEREIN ARE            *
 *   TRADEMARKS AND/OR REGISTERED TRADEMARKS OF LAWSON         *
 *   SOFTWARE AND/OR ITS AFFILIATES AND SUBSIDIARIES. ALL      *
 *   RIGHTS RESERVED.  ALL OTHER TRADEMARKS LISTED HEREIN ARE  *
 *   THE PROPERTY OF THEIR RESPECTIVE OWNERS.                  *
 *                                                             *
 ***************************************************************
 *                                                             *
 *  v1.0 - 9/8/2015 - Initial release                          *
 *                                                             *   
 ***************************************************************/
 (function() {
	'use strict';
	
	angular
		.module('InforGlobalization', ['pascalprecht.translate'])
		.config(function($translateProvider) {
			/* Set preffered and fallback locale */
	        $translateProvider.preferredLanguage("en");
	        $translateProvider.fallbackLanguage("en");
	    })
		.run(function($ionicPlatform, $translate) {
	        $ionicPlatform.ready(function() {
	            if(typeof navigator.globalization !== "undefined") {
	                navigator.globalization.getPreferredLanguage(function(language) {
	                    $translate.use((language.value).split("-")[0])
	                    .then(function(data) {
	                        console.log("SUCCESS -> " + data);
	                    }, function(error) {
	                        console.log("ERROR -> " + error);
	                    });
	                }, null);
	            }
	        });
	    });

		
	
})();	
