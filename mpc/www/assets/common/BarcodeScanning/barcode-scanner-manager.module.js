/***************************************************************
 *                                                             *
 *                           NOTICE                            * 
 *                                                             *
 *   THIS SOFTWARE IS THE PROPERTY OF AND CONTAINS             *
 *   CONFIDENTIAL INFORMATION OF LAWSON SOFTWARE AND/OR ITS    *
 *   AFFILIATES OR SUBSIDIARIES AND SHALL NOT BE DISCLOSED     *
 *   WITHOUT PRIOR WRITTEN PERMISSION. LICENSED CUSTOMERS MAY  *
 *   COPY AND ADAPT THIS SOFTWARE FOR THEIR OWN USE IN         *
 *   ACCORDANCE WITH THE TERMS OF THEIR SOFTWARE LICENSE       *
 *   AGREEMENT. ALL OTHER RIGHTS RESERVED.                     *
 *                                                             *
 *   (c) COPYRIGHT 2015 LAWSON SOFTWARE.  ALL RIGHTS RESERVED. *
 *   THE WORD AND DESIGN MARKS SET FORTH HEREIN ARE            *
 *   TRADEMARKS AND/OR REGISTERED TRADEMARKS OF LAWSON         *
 *   SOFTWARE AND/OR ITS AFFILIATES AND SUBSIDIARIES. ALL      *
 *   RIGHTS RESERVED.  ALL OTHER TRADEMARKS LISTED HEREIN ARE  *
 *   THE PROPERTY OF THEIR RESPECTIVE OWNERS.                  *
 *                                                             *
 ***************************************************************
 *                                                             *
 *  v1.0 - 8/7/2015 - Initial release                          *
 *                                                             *   
 ***************************************************************/
 (function() {
	'use strict';
	
	angular
		.module('InforScannerManager', [])
		.service("ScannerManager",ScannerManager);

		function ScannerManager(){

			var scannerManager = {
				enableScanner: enableBarcodeScan,
				disableScanner: disableBarcodeScan
			};

			return scannerManager;

			/*--------------------Implementation-------------------*/
			function enableBarcodeScan(successCallback, failCallback){
				//alert("Enabled");
				window.ionic.Platform.ready(function() {
					cordova.exec(successCallback,failCallback,"ScannerManager","enableScanner",[ "" ]); 
				});
			}

			function disableBarcodeScan(successCallback, failCallback) {
				window.ionic.Platform.ready(function() {
					cordova.exec(successCallback,failCallback,"ScannerManager","disableScanner",[ "" ]); 
				});
			}


		}
	
})();	
