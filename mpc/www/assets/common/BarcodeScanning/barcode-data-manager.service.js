/***************************************************************
 *                                                             *
 *                           NOTICE                            * 
 *                                                             *
 *   THIS SOFTWARE IS THE PROPERTY OF AND CONTAINS             *
 *   CONFIDENTIAL INFORMATION OF LAWSON SOFTWARE AND/OR ITS    *
 *   AFFILIATES OR SUBSIDIARIES AND SHALL NOT BE DISCLOSED     *
 *   WITHOUT PRIOR WRITTEN PERMISSION. LICENSED CUSTOMERS MAY  *
 *   COPY AND ADAPT THIS SOFTWARE FOR THEIR OWN USE IN         *
 *   ACCORDANCE WITH THE TERMS OF THEIR SOFTWARE LICENSE       *
 *   AGREEMENT. ALL OTHER RIGHTS RESERVED.                     *
 *                                                             *
 *   (c) COPYRIGHT 2015 LAWSON SOFTWARE.  ALL RIGHTS RESERVED. *
 *   THE WORD AND DESIGN MARKS SET FORTH HEREIN ARE            *
 *   TRADEMARKS AND/OR REGISTERED TRADEMARKS OF LAWSON         *
 *   SOFTWARE AND/OR ITS AFFILIATES AND SUBSIDIARIES. ALL      *
 *   RIGHTS RESERVED.  ALL OTHER TRADEMARKS LISTED HEREIN ARE  *
 *   THE PROPERTY OF THEIR RESPECTIVE OWNERS.                  *
 *                                                             *
 ***************************************************************
 *                                                             *
 *  v1.0 - 8/7/2015 - Initial release                          *
 *                                                             *   
 ***************************************************************/
  (function() {
	'use strict';
	
	angular
		.module('InforScannerManager')
		.service("BarcodeDataManager",BarcodeDataManager);

		function BarcodeDataManager(BarcodeParser,$cordovaNativeAudio,LocalData){

			var that = this;

			var BarcodeDataManager = {
				ScanBarCode: ScanBarCode,
				Error: Error
			};

			return BarcodeDataManager;

			function ScanBarCode(data,dataType){
		        $cordovaNativeAudio.preloadSimple('0', 'assets/common/BarcodeScanning/sounds/beep.wav');
		        $cordovaNativeAudio.preloadSimple('1', 'assets/common/BarcodeScanning/sounds/buzz.wav');

				/*
					Default Barcode Scan Sound
				*/
				
				var appObj = JSON.parse(LocalData.loadData('application'));

				var defGoodScan = appObj.goodScan;
				var defFailScan = appObj.badScan;


		       	var dataObject = BarcodeParser.parseBarcode(data);
		       	var dataPrefix = dataObject.codeId.substring(0,2);
		       	var return_data;


		       	if(dataType == 'location' && dataPrefix == 'LL') {

		       		var loc = dataObject.codeId.split(':');
		       		var companyId = loc[1];
		       		var locationId = loc[2];

		       		return_data =  {'companyID': companyId, 'locationID': locationId};

		       		$cordovaNativeAudio.play(defGoodScan);

		       	} else if (dataType == 'stock_item' && dataPrefix == 'IL') {

		       		var stock = dataObject.codeId.split(':');
		       		var stockItem = stock[1];
		       		return_data =  [{'item_number': stockItem}];

		       	} else {
		       		$cordovaNativeAudio.play(defFailScan);
		       		alert('Invalid search');
		       	}

		       	return return_data;

			}

			function Error(data) {
				alert('Error: ' + data );
			}


		}


	})();