/* 
	Service to get network information
		
	
 */
(function(){
	'use strict';
	
	angular
		.module('InforCommsManager')
		.factory('CI', Body);
		
		function Body(Popup){
			var service = {
				// connectionAvailable : connectionAvailable,
				connectionAvailable : connectionAvailable,
				connectionType : connectionType
			};
			
			return service;
			
			function connectionAvailable(){
				console.log('-connectionAvailable function-');
				return connectionType() !== 'none';
			}
			
			function connectionType(){
				console.log('-connectionType function-');
				return navigator.connection.type;
			}
		}
})();