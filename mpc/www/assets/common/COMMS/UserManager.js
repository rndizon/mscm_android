/*
***************************************************************
*                                                             *
*                           NOTICE                            * 
*                                                             *
*   THIS SOFTWARE IS THE PROPERTY OF AND CONTAINS             *
*   CONFIDENTIAL INFORMATION OF INFOR AND/OR ITS		      *
*   AFFILIATES OR SUBSIDIARIES AND SHALL NOT BE DISCLOSED     *
*   WITHOUT PRIOR WRITTEN PERMISSION. LICENSED CUSTOMERS MAY  *
*   COPY AND ADAPT THIS SOFTWARE FOR THEIR OWN USE IN         *
*   ACCORDANCE WITH THE TERMS OF THEIR SOFTWARE LICENSE       *
*   AGREEMENT. ALL OTHER RIGHTS RESERVED.                     *
*                                                             *
*   (c) COPYRIGHT 2012 INFOR.  ALL RIGHTS RESERVED.			  *
*   THE WORD AND DESIGN MARKS SET FORTH HEREIN ARE            *
*   TRADEMARKS AND/OR REGISTERED TRADEMARKS OF INFOR          *
*   AND/OR ITS AFFILIATES AND SUBSIDIARIES. ALL			      *
*   RIGHTS RESERVED.  ALL OTHER TRADEMARKS LISTED HEREIN ARE  *
*   THE PROPERTY OF THEIR RESPECTIVE OWNERS.                  *
*                                                             *
***************************************************************
*/

// Define global variables to JSLint.
// True indicates that the variable may be assigned to by this file.
// False indicates that the variable may not be assigned to by this file.
/*global SSOAuthObject: false,
		 cmnGetElementText: false,
		 Connection: false,
		 DOMParser: false,
		 util: false,
		 AppVersionObject: false,
		 window: false,
		 trxconnect: false
*/

//-----------------------------------------------------------------------------
//
//	NOTE:
//		this object is intended to be extended
//-----------------------------------------------------------------------------
//
//	DEPENDENCIES:
//		common.js
//		commonHTTP.js
//		/sso/sso.js		(for 9.0 tech only)
//-----------------------------------------------------------------------------
function UserManager(techVersion, httpRequest, bAppVersion, bShowErrors, funcAfterCall, bUseSecurityPlugin) {
	this.techVersion = techVersion || UserManager.TECHNOLOGY_900;
	this.httpRequest = httpRequest || null;
	this.profileDom = null;
	this.funcAfterCall = funcAfterCall;
	
	// user attributes
	this.acGrpCollect = null;
	this.activityList = null;
	this.approveCd = null;
	this.appVersion = null;
	this.longAppVersion = null;
	this.buyerCode = null;
	this.commaSeparator = ",";
	this.company = null;
	this.currencySymbol = "$";
	this.custGroup = null;
	this.customer = null;
	this.date = null;
	this.dateSeparator = "/";
	this.dateFormat = "MMDDYY";
	this.timeSeparator = ":";
	this.timeFormat = "12 HOUR";
	this.decimalSeparator = ".";
	this.employee = null;
	this.firstname = null;
	this.id = null;		// i.e. webuser
	this.name = null;
	this.defaultLanguage = "default";
	this.language = this.defaultLanguage;
	this.languageDom = null;
	this.login = null;
	this.mobileuser = [];
	this.mobileinventory = null;
	this.mobileassets = [];
	this.mobilefinancials = [];
	this.percentSymbol = "%";
	this.prodline = null;
	this.requester = null;
	this.time = null;
	this.vendor = null;
	this.vendorGroup = null;
	this.itemGroup = null;
	this.wfUser = null;
	
	this.useSecurityPlugin = (typeof (bUseSecurityPlugin) == "boolean") ? bUseSecurityPlugin : false;

	// errors with profile
	this.errorMsg = "";
	this.showErrors = (typeof (bShowErrors) == "boolean") ? bShowErrors : true;

	// set up time
	this.success = this.loadProfile();
	if (typeof (bAppVersion) != "boolean") {
		bAppVersion = true;
	}
	this.bAppVersion = bAppVersion;
}
//-- static methods -----------------------------------------------------------
UserManager.extendTo = function (subClass) {
	// *** DO NOT CHANGE THIS CODE! ***
	var baseClass = UserManager;
	for (var i in baseClass) {
		if (i == "extendTo") {
			continue;
		}
		subClass[i] = baseClass[i];
	}
	function Inheritance() {}
	Inheritance.prototype = baseClass.prototype;
	subClass.prototype = new Inheritance();
	subClass.prototype.constructor = subClass;
	subClass.baseConstructor = baseClass;
	subClass.superClass = baseClass.prototype;
};
//-- static variables ---------------------------------------------------------
UserManager.PROFILE_ERROR_803 = "ERRORMSG";
UserManager.PROFILE_URL_803 = "/servlet/Profile";
UserManager.PROFILE_URL_900 = "/servlet/Profile?_SECTION=attributes";
UserManager.SERVER_URL = "";  // Override this vaule (i.e. "http://pepsi.lawson.com:9486")
UserManager.SYSTEM_CODE = "";			// override this value (i.e. HR, RQ, etc...)
UserManager.TECHNOLOGY_803 = "8.0.3";
UserManager.TECHNOLOGY_900 = "9.0.0";
//-----------------------------------------------------------------------------
UserManager.prototype.isTimedOut = function () {
	if (this.techVersion == UserManager.TECHNOLOGY_803) {
		return false;
	}
	return !(new SSOAuthObject().ping());
};
//-----------------------------------------------------------------------------
UserManager.prototype.getPhrase = function (phraseId) {
	var phrase = phraseId;
	if (this.languageDom) {
		var lawxml = this.languageDom.documentElement;
		var len = lawxml.childNodes.length;
		for (var i = 0; i < len; i++) {
			var node = lawxml.childNodes.item(i);
			if ("phrase" == node.nodeName && phraseId == node.getAttribute("id")) {
				return cmnGetElementText(node);
			}
		}
	}
	return phrase;
};
//-----------------------------------------------------------------------------
UserManager.prototype.loadLanguageDom = function (lang) {
	// not implemented
};
//-----------------------------------------------------------------------------
UserManager.username = "";
UserManager.password = "";
UserManager.prototype.loadProfile = function () {
	var callingFunction = this;
	// return
	//		false - if errors are found
	//		true  - if everything is OK
	if (typeof navigator.network === 'undefined' || navigator.network.connection.type != Connection.NONE) {
		var parseResponse = function (responseText) {
			var parser = new DOMParser();
			var responseXML = parser.parseFromString(responseText, "text/xml");
			callingFunction.profileDom = responseXML;
			
			if (responseXML && responseXML.documentElement.nodeName == "ERROR") {
				var errorNode = responseXML.documentElement;
				var msgNode = errorNode.firstChild.firstChild;
				callingFunction.success = false;
				if ((errorNode.getAttribute("class") != null && errorNode.getAttribute("class") == "LawsonHttpClientException") || 
						(msgNode.nodeValue.toLowerCase().indexOf("login failed") >= 0) || 
						(msgNode.nodeValue.toLowerCase().indexOf("invalid session") >= 0)) {
					callingFunction.errorMsg = "Wrong username or password, please try again.";
				} else {
					var details = errorNode.firstChild.nextSibling.firstChild.nodeValue;
					if (details.length > 1024) {
						details = details.substring(0, 1024) + "...";
					}
					var msg = msgNode.nodeValue
						+ "\n\n" + UserManager.PROFILE_URL_900 + "  -  loadProfile() - UserManager.js"
						+ "\n\n" + details;
					util.alert(msg, 
						function () {
							if (callingFunction.funcAfterCall) {
								callingFunction.funcAfterCall();
							}
						});
					return;
				}			
			} else {
				callingFunction.parse900TechProfile();
			}
			if (callingFunction.bAppVersion) {
				callingFunction.setAppVersion();
			} else {
				if (callingFunction.funcAfterCall) {
					callingFunction.funcAfterCall();
				}
			}
		};
		var handleError = function (e) {
			callingFunction.success = false;
			var msg = 'There was a problem connecting to the server. Please verify your server status and host and port settings.\n\n';
			util.alert(msg, 
				function () {
					if (callingFunction.funcAfterCall) {
						callingFunction.funcAfterCall();
					}
				});
			return false;
		};
		
		var timestamp = new Date().getTime();
		// Append a query string parameter, "_=[TIMESTAMP]", to the URL to fix client caching problem.
		var url = UserManager.PROFILE_URL_900 + "&_=" + timestamp;
		//console.log("url = " + url);
		
		if (callingFunction.useSecurityPlugin == true) {
			var xhr = trxconnect.request({
				url: url,
				methodType: 'GET',
				contentType: 'text/plain',
				cache: false,
				success: function (responseText) {
					parseResponse(responseText);
				},
				fail: function (e) {
					return handleError(e);
				}
			});
		} else {
			this.httpRequest.onload = function () {
				if (this.status == 404) {
					callingFunction.success = false;
					callingFunction.errorMsg = "Http-404: URL [" + url +"] not found";
					if (callingFunction.funcAfterCall) {
						callingFunction.funcAfterCall();
					}
				} else
					parseResponse(this.responseText);
			};
			this.httpRequest.onerror = function (e) {
				return handleError(e);
			};		
			this.httpRequest.open('GET', url, true, '', '');
			this.httpRequest.setRequestHeader('Authorization', 'Basic ' + window.btoa(UserManager.username + ':' + UserManager.password));
			this.httpRequest.setRequestHeader('Cache-Control', 'no-cache');
			this.httpRequest.setRequestHeader('Pragma', 'no-cache');
			this.httpRequest.send();
		}
		return true;
	} else {
		util.alert('This application requires an internet connection.  Please check your connection and try again.',
			function () {
				if (callingFunction.funcAfterCall) {
					callingFunction.funcAfterCall();
				}
			});
		return false;
	}

	if (!this.httpRequest) {
		return false;
	}

	if (this.techVersion == UserManager.TECHNOLOGY_803) {
		this.profileDom = this.httpRequest(UserManager.PROFILE_URL_803);
		this.parse803TechProfile();
	} else {
		this.profileDom = this.httpRequest(UserManager.PROFILE_URL_900);
		if (!this.profileDom) {
			return false;
		}
		this.parse900TechProfile();
	}

	var bSuccess = this.checkForSuccess();
	return bSuccess;
};
//-----------------------------------------------------------------------------
UserManager.prototype.parse803TechProfile = function () {
	// Any errors from this method cannot be translated
	var webuserNodes = this.profileDom.getElementsByTagName("WEBUSER");
	if (!webuserNodes || webuserNodes.length == 0) {
		this.errorMsg = "Invalid Profile output";
		return;
	}

	var webuserNode = webuserNodes.item(0);
	for (var i = 0; i < webuserNodes.item(0).childNodes.length; i++) {
		var node = webuserNodes.item(0).childNodes.item(i);
		if (node.nodeType == 1) {
			var name = node.nodeName;
			var value = cmnGetElementText(node);
			switch (name) {
			case UserManager.PROFILE_ERROR_803:
				this.errorMsg = UserManager.PROFILE_ERROR_803 + cmnGetElementText(node).toUpperCase();
				break;
			case "ACGRPCOLLECT":
				this.acGrpCollect = value;
				break;
			case "ACTIVITYLIST":
				this.activityList = value;
				break;
			case "APPROVECD":
				this.approveCd = value;
				break;
			case "BUYERCODE":
				this.buyerCode = value;
				break;
			case "COMMASEP":
				if (value != "") {
					this.commaSeparator = value;
				}
				break;
			case "COMPANY":
				this.company = value;
				break;
			case "CURRENCYSYMBOL":
				if (value != "") {
					this.currencySymbol = value;
				}
				break;
			case "CUSTGROUP":
				this.custGroup = value;
				break;
			case "CUSTOMER":
				this.customer = value;
				break;
			case "DATE":
				this.date = value;
				break;
			case "DATEFMTSTR":
				if (value != "") {
					this.dateFormat = value;
				}
				break;
			case "TIMESEP":
				if (value != "") {
					this.timeSeparator = value;
				}
				break;
			case "TIMEFMT":
				if (value != "") {
					this.timeFormat = value;
				}
				break;
			case "DATESEP":
				if (value != "") {
					this.dateSeparator = value;
				}
				break;
			case "DECIMALSEP":
				if (value != "") {
					this.decimalSeparator = value;
				}
				break;
			case "EMPLOYEE":
				this.employee = value;
				break;
			case "LANGUAGE":
				if (value != "") {
					this.language = value;
				}
				this.loadLanguageDom(this.language);
				break;
			case "LOGIN":
				this.login = value;
				break;
			case "LONGNAME":
				this.name = value;
				break;
			case "PERCENTSYMBOL":
				if (value != "") {
					this.percentSymbol = value;
				}
				break;
			case "PRODLINE":
				this.prodline = value.toUpperCase();
				break;
			case "REQUESTER":
				this.requester = value;
				break;
			case "TIME":
				this.time = value;
				break;
			case "VENDOR":
				this.vendor = value;
				break;
			case "VENDORGROUP":
				this.vendorGroup = value;
				break;
			case "WEBUSER":
				this.id = value;
				break;
			}
		}
	}
};
//-----------------------------------------------------------------------------
UserManager.prototype.parse900TechProfile = function () {
	// Any errors from this method cannot be translated
	var attrNodes = this.profileDom.getElementsByTagName("ATTR");
	if (!attrNodes || attrNodes.length == 0) {
		this.errorMsg = "Invalid Profile output";
		return;
	}

	for (var i = 0; i < attrNodes.length; i++) {
		var name = attrNodes.item(i).getAttribute("name").toLowerCase();
		var value = attrNodes.item(i).getAttribute("value");
		switch (name) {
		case "acgrpcollect":
			this.acGrpCollect = value;
			break;
		case "activitylist":
			this.activityList = value;
			break;
		case "approvecd":
			this.approveCd = value;
			break;
		case "buyercode":
			this.buyerCode = value;
			break;
		case "commaseparator":
			if (value != "") {
				this.commaSeparator = value;
			}
			break;
		case "company":
			this.company = value;
			break;
		case "currencysymbol":
			if (value != "") {
				this.currencySymbol = value;
			}
			break;
		case "cust_group":
			this.custGroup = value;
			break;
		case "customer":
			this.customer = value;
			break;
		case "date":
			this.date = value;
			break;
		case "dateformat":
			if (value != "") {
				this.dateFormat = value;
			}
			break;
		case "dateseparator":
			if (value != "") {
				this.dateSeparator = value;
			}
			break;
		case "decimalseparator":
			if (value != "") {
				this.decimalSeparator = value;
			}
			break;
		case "employee":
			this.employee = value;
			break;
		case "firstname":
			this.firstname = value;
			break;				
		case "id":
			this.id = value;
			break;
		case "language":
			if (value != "") {
				this.language = value;
			}
			this.loadLanguageDom(this.language);
			break;
		case "lawsonuserlogin":
			this.login = value;
			break;
		case "mobileuser":
			this.mobileuser.push(value);
			break;	
		case "mobileinventory":
			this.mobileinventory = value;
			break;
		case "mobileassets":
			this.mobileassets.push(value);
			break;	
		case "mobilefinancials":
			this.mobilefinancials.push(value);
			break;	
		case "name":
			this.name = value;
			break;
		case "percentsymbol":
			if (value != "") {
				this.percentSymbol = value;
			}
			break;
		case "productline":
			this.prodline = value.toUpperCase();
			break;
		case "requester":
			this.requester = value;
			break;
		case "time":
			this.time = value;
			break;
		case "vendor":
			this.vendor = value;
			break;
		case "vendor_group":
			this.vendorGroup = value;
			break;
		case "itemgrp":
			this.itemGroup = value;
			break;
		case "wfuser":
			this.wfUser = value;
			break;				
		}
	}
};
//-----------------------------------------------------------------------------
UserManager.prototype.hasMultiValuedAttribute = function (attrName, attrValue) {
	var attrExists = false;
	try {
		var attr = this[attrName];
		if (attr && attr.length) {
			for (var i = 0; i < attr.length; i++) {
				if (attr[i] == attrValue) {
					attrExists = true;
					break;
				}
			}
		}
	} catch (e) {
		
	}
	return attrExists;
};

//-----------------------------------------------------------------------------
UserManager.prototype.getAttributeValues = function (attrName) {
	var values = [];
	if (this[attrName]) {
		values = this[attrName].slice(0);
	}
	return values;
};

//-----------------------------------------------------------------------------
UserManager.prototype.checkForSuccess = function () {
	// return
	//		false - if errors are found
	//		true  - if everything is OK

	// maybe populated already if response was invalid
	if (this.errorMsg) {
		this.handleError();
		return false;
	}

	// implement your own method to check
	// for required attributes (i.e. prodline)
	// populate this.errorMsg with a message

	return true;
};
//-----------------------------------------------------------------------------
UserManager.prototype.handleError = function () {
	// override this to show a nice error page with the message
	if (this.showErrors) {
		util.alert(this.errorMsg);
	}
};
//-----------------------------------------------------------------------------
UserManager.prototype.setAppVersion = function () {
console.log('zzzzzzz Begin UserManager.prototype.setAppVersion, this.appVersion = ' + this.appVersion);
	if (this.appVersion == null) {
		if (this.prodline == null) {
			this.success = this.loadProfile();
			if (!this.success) {
				return;
			}
		}
		
		var appObj = null;
		var callingFunction = this;
		var localFuncAfterCall = function () {
			callingFunction.appVersion = appObj.appVersion;
			callingFunction.longAppVersion = appObj.longAppVersion;
						
			if (callingFunction.appVersion == null) {
				// THIS CANNOT BE TRANSLATED!!!
				// this productline doesn't have the right system codes
				callingFunction.errorMsg = "The product line " + callingFunction.prodline + " is not supported.";
				callingFunction.handleError();
			}
			callingFunction.funcAfterCall();
		};

		AppVersionObject.username = UserManager.username;
		AppVersionObject.password = UserManager.password;
		appObj = new AppVersionObject(this.prodline, UserManager.SYSTEM_CODE, false, UserManager.SERVER_URL, localFuncAfterCall, this.useSecurityPlugin);
	}
};