(function(){
 	'use strict';
 	
		angular
		    .module('app')
			.config(function($stateProvider, $urlRouterProvider) {
		     
		     $stateProvider
		      .state("app.my-par-forms", {
			    url: "/my-par-forms",
                changeColor: 'bar-amber-7',
			    cache:false, 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/par_counting/my-par-forms-view.html",
					      controller: "ParCountController as vm"
					 }
				  }
			      
		      })
		      
		      .state("add-parforms-view", {
		      	  cache:false,
			      url: "/add-parforms-view",
			      changeColor: 'bar-amber-7',
			      templateUrl: "html/par_counting/add-par-forms-view.html",
			      controller: "AddParformsController as vm"
					
		      })

		      .state("app.all-parforms-view", {
			      url: "/all-parforms-view",
			      changeColor: 'bar-amber-7',
			      cache:false, 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/par_counting/all-par-forms-view.html",
					      controller: "AllParformsController as vm"
					 }
				  }
			      
		      })
		     
		     .state("app.downloaded-parcount", {
		     	cache:false,
			      url: "/downloaded-parcount", 
			      changeColor: 'bar-amber-7',
			      views :{
			      	'menu-content':{
					      templateUrl: "html/par_counting/downloaded-parcount.html",
					      controller: "ParCountDownloadController as vm"
					 }
				  }
			      
		      })	

		     .state("app.downloaded-parcount-edit", {
			      url: "/downloaded-parcount-edit", 
			      changeColor: 'bar-amber-7',
			      cache:false,
			      views :{
			      	'menu-content':{
					      templateUrl: "html/par_counting/downloaded-parcount-edit.html",
					      controller: "ParCountDownloadEditController as vm"
					 }
				  }
			      
		      })

		     .state("app.downloaded-parcount-details", {
			      url: "/downloaded-parcount-details/:LocationCode/:LocationID/:LocationName/:IndexView", 
			      changeColor: 'bar-amber-7',  
			      cache:false,   
			      nativeTransitions: {
				        "type": "flip",
				        "direction": "up"
				    },
			      views :{
			      	'menu-content':{
					      templateUrl: "html/par_counting/downloaded-parcount-details.html",
					      controller: "ParCountItemDetailController as vm"
					  }
					}
		      })

		     .state('app.downloaded-parcount-details-list',{
		     	url: "/downloaded-parcount-details-list/:LocationCode/:LocationID/:LocationName",
			    changeColor: 'bar-amber-7',
			    cache:false, 
			      views :{
			      	'menu-content':{
		     			templateUrl: "html/par_counting/downloaded-parcount-details-list.html",
		     			controller: "ParCountItemDetailListController as vm" 
		     		}
		     	}
		     })

		     .state('app.downloaded-parcount-edit-parlevel',{
		     	url: "/downloaded-parcount-edit-parlevel/:LocationCode/:LocationID/:LocationName/:IndexView/:UID",
			    changeColor: 'bar-amber-7',
			    cache:false, 
			      views :{
			      	'menu-content':{
		     			templateUrl: "html/par_counting/downloaded-parcount-edit-parlevel.html",
		     			controller: "ParCountItemDetailEditParlevel as vm" 
		     		}
		     	}
		     })

		     .state('app.downloaded-parcount-edit-parlevel-printer',{
		     	url: "/downloaded-parcount-edit-parlevel-printer",
			    changeColor: 'bar-amber-7',
			    cache:false, 
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/settings/settings-printer-list.html",
			      		controller: "ParCountItemDetailEditParlevel as vm"
		     		}
		     	}
		     });
		     
		     
		      
		  });
		
		
	
})();