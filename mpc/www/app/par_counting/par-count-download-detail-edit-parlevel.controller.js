(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('ParCountItemDetailEditParlevel', Body);

		function Body($state,
					$scope,
					$stateParams,
					LocalData,
					$ionicViewSwitcher,
					CRUD
					  ) 
		{
				
			 var vm = this;
			 vm.goTo = goTo;
			 vm.goToRowDetailsView = goToRowDetailsView;
			vm.currentUser = LocalData.loadSession('currentUser');
			var users = JSON.parse(LocalData.loadData('users'));
			vm.localStorShowHelp = users[vm.currentUser].isShowHelp;
			 vm.LocationCode = $stateParams.LocationCode;
			 vm.LocationID = $stateParams.LocationID;
			 vm.LocationName = $stateParams.LocationName;
			 vm.indexView = $stateParams.IndexView;
			 vm.UID = $stateParams.UID;
			 vm.itemDetails;
			vm.headerTitle = "title_select_label_printer";


			$scope.$on('$ionicView.afterEnter', function() {
					//alert($stateParams.ItemID);
					/*$ionicLoading.show({
						templateUrl: 'html/templates/spinner.html'
					});*/
					var queryParams = {
						UID:vm.UID
					};

					CRUD.select("Par_Item_Table","*", queryParams)
						.then(function(result) {
							if(result[0]){
								vm.itemDetails = result[0];
							}
					});

			});

			

			function goToRowDetailsView(){
				var params = {LocationCode:vm.LocationCode,LocationID:vm.LocationID,LocationName:vm.LocationName,IndexView:vm.indexView};
				$ionicViewSwitcher.nextDirection('slideDown');
				$state.go('app.downloaded-parcount-details', params);

			}

	    	function goTo(goToUrl,_locationCode,_locationID,_locationName) {
				if(_locationCode || _locationCode != ""){
					var params = {LocationCode:_locationCode,LocationID:_locationID,LocationName:_locationName};
					$ionicViewSwitcher.nextDirection('slideDown');
					$state.go(goToUrl, params);
				}else{
					$state.go(goToUrl);
				}
		  	}
			  
		
		}
		
		
		
})();
