(function(){
	'use strict';
	
	angular
		.module("app")
		.controller("ParCountItemDetailController", Body);
	
	function Body($state,$rootScope,
				  $stateParams,
				  $scope,DmeManager,
				  $ionicLoading,
				  $ionicViewSwitcher,
				  $ionicPopover,
				  $ionicSlideBoxDelegate,
				  $ionicPopup,
				  $q,
				  filterFilter, 
				  CommonInfoService,
				  CRUD,
				  LocalData,
				  ParCountService
				  ) 
	{
		
			var vm = this;
			vm.currentSlideNumber = 1;
			vm.companyHeader = LocalData.loadData("ParCountCompanyName");
			vm.companyHeaderID = LocalData.loadData("ParCountCompanyID");
			vm.slidestop = slidestop;
			vm.slidePrevious = slidePrevious;
			vm.slideNext = slideNext;
			vm.slideHasChanged = slideHasChanged;
			vm.goTo = goTo;
			vm.openEditParlevel = openEditParlevel;
			vm.LocationCode = $stateParams.LocationCode;
			vm.LocationID = $stateParams.LocationID;
			vm.LocationName = $stateParams.LocationName;
			vm.IndexView = ($stateParams.IndexView) ? parseInt($stateParams.IndexView) : 0;
			vm.parCounting = [];
			vm.parCountingPrinter = [];
			vm.slideParCounting = [];
			vm.fillQuantityField = CommonInfoService.fillQuantityField;
			vm.quantityValues = CommonInfoService.getQuantityValues();
			vm.TotalItemCounted = 0;
			vm.saveValues = ParCountService.saveValues;
			vm.onFocus = ParCountService.onFocus;
			
			vm.currentUser = LocalData.loadSession('currentUser');
			vm.users = JSON.parse(LocalData.loadData('users'));
			vm.localStorShowHelp = (vm.users[vm.currentUser].isShowHelp)?vm.users[vm.currentUser].isShowHelp:1;
			vm.isAllowAddtionalQty = (vm.users[vm.currentUser].parCountConfiguration.isAllowAddlQuantity == 1) ? true : false;
			vm.isPopulateItemDetail = (vm.users[vm.currentUser].parCountConfiguration.isPopulateItemScreen == 1) ? true : false;
			
			$scope.$on('$ionicView.beforeEnter',function() {

				$rootScope.isSavingInputs = false;

				var queryParams = {};
				queryParams.CompanyID = vm.companyHeaderID;
				CRUD.select("Par_Company_Table","*",queryParams)
				.then(function(result){
					vm.IsEditParLevel = result[0].IsEditParLevel;
					vm.IsPrintLabel = result[0].IsPrintLabel;
					vm.IsShowAPU = result[0].IsShowAPU;
					vm.IsShowQOO = result[0].IsShowQOO;
				});

				var queryParamsPrinter = {};
				queryParamsPrinter.CompanyID = vm.companyHeaderID;
				queryParamsPrinter.UserID = vm.currentUser;
				CRUD.select("Par_Printer_Table","*",queryParamsPrinter)
				.then(function(result){
					vm.parCountingPrinter = result;
				});

		    });
			$scope.$on('$ionicView.afterEnter', function() {
					

					var itemQuery = "SELECT *,item.LocationID as LocationID,item.ItemID as ItemID, Item.UID as UID, ";
						itemQuery +="(select AdditionalQuantityMultiplier from Par_Location_Table where LocationID=item.LocationID) as AdditionalQuantityMultiplier ";
						itemQuery +="FROM Par_Item_Table item left join Par_Transaction_table trans ";
						itemQuery += "USING(ItemID) WHERE item.LocationID ="+vm.LocationID+ " GROUP BY item.ItemID ORDER BY ItemSequence ASC"

						CRUD.inlineQuery(itemQuery)
				    	.then(function(result) {
				    		vm.parCounting = result;

							var curSlideLimit = (vm.parCounting.length < 3 ? vm.parCounting.length : 3);

							for(var i = 0; i < curSlideLimit; i++) {
							  vm.slideParCounting.push(vm.parCounting[i]);
							} 
					    	$ionicSlideBoxDelegate.update();
				    	});

					/*var queryParams = {
						LocationID:vm.LocationID
					};

					CRUD.select("Par_Item_Table","*", queryParams)
						.then(function(result) {
						vm.parCounting = result;

						var curSlideLimit = (vm.parCounting.length < 3 ? vm.parCounting.length : 3);

						for(var i = 0; i < curSlideLimit; i++) {
						  vm.slideParCounting.push(vm.parCounting[i]);
						} 
				    	$ionicSlideBoxDelegate.update();

					});*/
			});

			

	      	//navigate to Other Page
			function goTo(goToUrl,_locationCode,_locationID,_locationName) {
				
				//CommonInfoService.setQuantityValues('slide-box',vm.parCounting.length);
				
				
			 	if(goToUrl == 'app.my-par-forms') {
			   		$ionicViewSwitcher.nextDirection('slideDown');
			 	} else {
			   		$ionicViewSwitcher.nextDirection('swap');
			 	}

				if(_locationCode){
					var params = {LocationCode:_locationCode,LocationID:_locationID,LocationName:_locationName};
					$state.go(goToUrl, params);
				}else{
					$state.go(goToUrl);
				}
			  	
			  
			}

			function slideHasChanged(index){

				if($rootScope.isSavingInputs){
				//	$ionicSlideBoxDelegate.slide($ionicSlideBoxDelegate.currentIndex());
				}
				

			  var countParCount = vm.parCounting.length;
			  var countCurSlide = vm.slideParCounting.length;
			  var currentSlideIndex = $ionicSlideBoxDelegate.currentIndex();
			  var addIndex = currentSlideIndex + 2;

			  if(addIndex == countCurSlide) {

			      if(addIndex < countParCount) {
			        vm.slideParCounting.push(vm.parCounting[addIndex]);

			        $ionicSlideBoxDelegate.update();
			      }

			  }

				vm.currentSlideNumber =  $ionicSlideBoxDelegate.currentIndex() + 1;

				
			}

			
			/*
				Next and Previous button of slider content
			*/

			function slidestop(index) {
			    $ionicSlideBoxDelegate.enableSlide(false);
			}

			function slidePrevious() {
				if($rootScope.isSavingInputs){
					return false;
				}
				$ionicSlideBoxDelegate.previous();
				vm.currentSlideNumber =  $ionicSlideBoxDelegate.currentIndex() + 1;
				
			}

			function slideNext() {

				if($rootScope.isSavingInputs){
					return false;
				}

				if(vm.currentSlideNumber >= $ionicSlideBoxDelegate.slidesCount()) {
					//nextParLocationConfirmation();
				}

				$ionicSlideBoxDelegate.next();
				vm.currentSlideNumber =  $ionicSlideBoxDelegate.currentIndex() + 1;
				
			}

			function nextParLocationConfirmation() {
				var confirmPopup  = $ionicPopup.confirm({
					template: 'Previous Par forms will be saved',
					title: 'Continue to next Par Location?',
					scope: $scope
				});

				confirmPopup.then( function(res) {
				});
			};

			function openEditParlevel(item) {
				vm.item = item;
				var confirmPopup  = $ionicPopup.confirm({
                    cssClass: 'edit-parlevel-popup',
					templateUrl: 'html/par_counting/downloaded-parcount-edit-parlevel.html',
					title: 'Edit Par Level',
					scope: $scope,
       				cancelText: "Cancel",
        			okText: "Save"
				});


				confirmPopup.then(function(res) {
					console.log(res);
				});
		
			}

			vm.limitNumber = function(e) {
				if(e.target.value.length > 10) {
					e.currentTarget.value = e.currentTarget.value.slice(0,-1);
				}


				console.log(e.currentTarget.value);
			}




		
	}
	
	
	
	
	
	
})();