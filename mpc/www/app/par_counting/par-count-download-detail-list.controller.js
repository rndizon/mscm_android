(function(){
	'use strict';
	
	angular
		.module("app")
		.controller("ParCountItemDetailListController", Body);
	
	function Body($state,$q,$rootScope,
				  $scope,DmeManager,
				  $ionicLoading,
				  $ionicViewSwitcher,
				  $ionicPopover,
				  $stateParams, 
				  filterFilter,
				  $ionicSlideBoxDelegate,
				  $ionicPopup,
				  LocalData,
				  CommonInfoService,
				  CRUD,
				  ParCountService
				  ) 
	{
		
			var vm = this;
			vm.goTo = goTo;
			vm.companyHeader = LocalData.loadData("ParCountCompanyName");
			vm.companyHeaderID = LocalData.loadData("ParCountCompanyID");
			vm.LocationCode = $stateParams.LocationCode;
			vm.LocationName = $stateParams.LocationName;
			vm.LocationID = $stateParams.LocationID;
			vm.parCounting = [];
			vm.fillQuantityField = CommonInfoService.fillQuantityField;
			vm.quantityValues = CommonInfoService.getQuantityValues();
			vm.goToRowDetailsView = goToRowDetailsView;
			vm.TotalItemCounted = 0;
			vm.currentUser = LocalData.loadSession('currentUser');
			vm.users = JSON.parse(LocalData.loadData('users'));
			vm.localStorShowHelp = (vm.users[vm.currentUser].isShowHelp)?vm.users[vm.currentUser].isShowHelp:1;
			vm.saveValues = ParCountService.saveValues;
			vm.onFocus = ParCountService.onFocus;

			$scope.$on('$ionicView.afterEnter',function() {
					$rootScope.isSavingInputs = false;
					/*$ionicLoading.show({
						templateUrl: 'html/templates/spinner.html'
					});*/

					var itemQuery = "SELECT *,item.LocationID as LocationID,item.ItemID as ItemID, Item.UID as UID ";
						itemQuery +="FROM Par_Item_Table item left join Par_Transaction_table trans ";
						itemQuery += "USING(ItemID) WHERE item.LocationID ="+vm.LocationID+ " GROUP BY item.ItemID ORDER BY ItemSequence ASC";

						CRUD.inlineQuery(itemQuery)
				    	.then(function(result) {
				    		vm.parCounting = result;

				    	});	
						


			});
			$scope.$on('$ionicView.beforeLeave', function() {
				var quantityFields = document.getElementsByName("quantity-from-list");
				
				var promises = [];

				for(var i = 0; i < quantityFields.length;i++){
				   if(quantityFields[i].value.trim().length > 0){
						vm.TotalItemCounted++;
				   		promises.push(vm.TotalItemCounted);
				   }
				   
				}
				$q.all(promises).then(function() {
				    
				    var query = "UPDATE Par_Location_Table SET TotalItemCounted = "+vm.TotalItemCounted+" WHERE LocationID='"+vm.LocationID+"'";
					CRUD.inlineQuery(query);



				});
				
			});

	      	//navigate to Other Page
			function goTo(goToUrl,_locationCode,_locationID,_locationName) {
				
				
				//CommonInfoService.setQuantityValues('list',vm.parCounting.length);

			 	if(goToUrl == 'app.downloaded-parcount') {
			   		$ionicViewSwitcher.nextDirection('back');
			 	} else if (goToUrl == "app.my-par-forms") {
			   		$ionicViewSwitcher.nextDirection('slideDown');
			 	} else {
			   		$ionicViewSwitcher.nextDirection('swap');
			 	}
			 	
			  if(_locationCode){
					var params = {LocationCode:_locationCode,LocationID:_locationID,LocationName:_locationName};
					$state.go(goToUrl, params);
				}else{
					$state.go(goToUrl);
				}

			};
			

			function goToRowDetailsView(_locationCode,_locationID,_locationName, _indexView){
				var params = {LocationCode:_locationCode,LocationID:_locationID,LocationName:_locationName,IndexView:_indexView};
				$ionicViewSwitcher.nextDirection('back');
				$state.go('app.downloaded-parcount-details', params);

			}
			vm.clearSearch = function(){
				vm.searchText = '';
			};

			

	}
	
	
	
	
	
	
})();