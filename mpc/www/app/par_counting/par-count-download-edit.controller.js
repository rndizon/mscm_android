(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('ParCountDownloadEditController', Body);

		function Body($state,
					  $scope,
					  $ionicLoading,
					  $ionicPopover,
					  $ionicViewSwitcher,
					  filterFilter,
					  CommonInfoService,
					  ParCountService,
					  LocalData,
					  CRUD
					  ) 
		{
				
			 var vm = this;
			 vm.popover = null;
			 vm.CommonInfoService = CommonInfoService;
			 vm.downloadedParformList = [];
			 vm.parformItemLength = 0; //number of items in given parform
			 vm.ifCheckboxTrue = 0;
			 
			$scope.$on('$ionicView.afterEnter', function () {
				
				var queryParams = {};
				queryParams.CompanyID = LocalData.loadData("ParCountCompanyID");
				queryParams.IsSelectedForParCount = 1;
					    	  	
					CRUD.select("Par_Location_Table","*", queryParams)
						.then(function(result) {
						vm.downloadedParformList = result;
						vm.allParformsLength = result.length;
					});
				
			}); 

			vm.checkboxClicked = function() {
			    vm.ifCheckboxTrue = filterFilter( vm.downloadedParformList, {val:true}).length;
			    vm.selectedAllParforms  = filterFilter( vm.downloadedParformList, {val:true});
			    CommonInfoService.setSelectedParforms(vm.selectedAllParforms);	
			};
			vm.selectAllCheckbox = function() {
			    var parCountLen =  vm.downloadedParformList.length;

			    for(var x = 0; x < parCountLen; x++) {
			    	vm.downloadedParformList[x].val = true;
			    }

			    //vm.closePopover();
			    vm.checkboxClicked(); // Show subheader 
			};
			vm.unSelectAllCheckbox = function() {
			    var parCountLen =  vm.downloadedParformList.length;

			    for(var x = 0; x < parCountLen; x++) {
			    	vm.downloadedParformList[x].val = false;
			    }

			    vm.checkboxClicked(); 
			    $state.go('app.my-par-forms');
	    	};
			vm.deleteParForms = function(){
				
				var LocationCodeTobeDeleted = [];

				CommonInfoService.getSelectedParforms().forEach(function(v){
					LocationCodeTobeDeleted.push(v.LocationID);
				});

				

				var query = "UPDATE Par_Location_Table SET IsSelectedForParCount = 0 WHERE LocationID IN ('";
		 			query += LocationCodeTobeDeleted.join("','");
		 			query += "');";

				CRUD.inlineQuery(query)
				    .then(function(result) {
				    	$ionicViewSwitcher.nextDirection('back');
				    	$state.go('app.my-par-forms');
						
				},function errorCallback(response) {
					    //$ionicLoading.hide(); 	
							   		 
				});

			};

			$ionicPopover.fromTemplateUrl('html/templates/options-parcount.html', {
			        scope: $scope
			}).then(function(popover) {
			        vm.popover = popover;
			        vm.popUpType = "list";
			});

			vm.clearSearch = function(){
				vm.searchText = '';
			};
		}
		
})();
