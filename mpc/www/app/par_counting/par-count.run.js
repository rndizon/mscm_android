(function(){
 	'use strict';

	angular
		.module("app")
		.run(function($rootScope,ScannerManager,BarcodeParser,DmeManager,$ionicLoading) {
			
		  /* ------------------------
		   *  State Change Events 
		   * ------------------------ */
		  	$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){ 
				    
					
			});
			$rootScope.$on('$stateNotFound',function(event, unfoundState, fromState, fromParams){ 
			    console.log(unfoundState.to); // "lazy.state"
			    console.log(unfoundState.toParams); // {a:1, b:2}
			    console.log(unfoundState.options); // {inherit:false} + default options
			    alert("Error state");
			});
			$rootScope.$on('$stateChangeSuccess',function(event, toState, toParams, fromState, fromParams){ 
					
			});
			
		});		
			
})();