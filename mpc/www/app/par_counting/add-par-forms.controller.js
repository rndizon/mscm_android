(function(){
 	'use strict';
 	
		angular
		    .module('app')	
		    .controller('AddParformsController', Body);

		function Body($ionicLoading,
					  $state,
					  $scope,
					  $ionicPopover, 
					  filterFilter,
					  $ionicPopup,
					  $ionicViewSwitcher, 
					  $q,
					  CRUD, 
					  LocalData,
					  CommonInfoService,
					  ParCountService,
					  CONSTANT,
					  $ionicPlatform
					  ) 
		{
				
			var vm = this;
			vm.ParformsLength = 0;
    	  	vm.popover = null;
    	  	vm.Parforms = null;
    	  	vm.companyHeaderID = LocalData.loadData("ParCountCompanyID");
    	  	vm.optionMore = optionMore;
    	  	vm.selectParforms = selectParforms;
    	  	vm.selectAll = selectAll;
    	  	vm.clearAll = clearAll;
    	  	vm.showClearAll = showClearAll;
			vm.selectedParForm = selectedParForm;
			vm.doRefresh = doRefresh;
			vm.currentUser = LocalData.loadSession('currentUser');
			vm.users = JSON.parse(LocalData.loadData('users'));
			vm.localStorShowHelp = vm.users[vm.currentUser].isShowHelp;
			vm.isMyParFormFirst = vm.users[vm.currentUser].parCountConfiguration.isMyParFormFirst;

	    	var backBtn = $ionicPlatform.registerBackButtonAction(function(event) {
				    if (true) { // your check here
						    document.getElementById('backBtn').click();
				    }
				  }, 150);  

   			$scope.$on('$destroy', backBtn);

    	  	
		   $scope.$on('$ionicView.afterEnter', function() {

		   		if(!LocalData.loadData("defAddParformFilter")){
					
		   			vm.filtertype = (vm.isMyParFormFirst == 1) ? "My Par Forms" : "All Par Forms";
					vm.listParforms(vm.isMyParFormFirst);
	    	  	}else{
	    	  		vm.filtertype = LocalData.loadData("defAddParformFilter");
	    	  		if(vm.filtertype == "My Par Forms"){
	    	  			vm.listParforms(1);
	    	  		}else{
	    	  			vm.listParforms(0);
	    	  		}
	    	  	}
	    	  	
	   			 
			   
		   });


		   vm.listParforms = function(type){
					var queryParams = {};
				    if(type > 0){
				    	queryParams.IsMyParform = type;
				    }
				    queryParams.IsSelected = 0;
				    queryParams.CompanyID = vm.companyHeaderID;

				    	CRUD.select("Par_Location_Table","*", queryParams)
							.then(function(result){
					 		 	vm.Parforms = result;
					 		 	vm.ParformsLength = result.length;
					 	});

				$scope.$broadcast('scroll.refreshComplete');
				$ionicLoading.hide();
			};

			function doRefresh () {
	    	  	//show loading icon
				$ionicLoading.show({
					templateUrl: CONSTANT.SpinnerTemplate
				});
				CommonInfoService
		 			.clearTable('Par_Location_Table')
		 			.then(function(){
		 				ParCountService
						.getParformHeaders().then(function(response) {
				     			
			     				ParCountService
			     				.saveParCountLocation(response)
			     				.then(function(res){
			     					vm.users = JSON.parse(LocalData.loadData('users'));
			     					vm.isMyParFormFirst = vm.users[vm.currentUser].parCountConfiguration.isMyParFormFirst;
			     					if(!LocalData.loadData("defAddParformFilter")){
				
							   			vm.filtertype = (vm.isMyParFormFirst == 1) ? "My Par Forms" : "All Par Forms";
										vm.listParforms(vm.isMyParFormFirst);
						    	  	}else{
						    	  		vm.filtertype = LocalData.loadData("defAddParformFilter");
						    	  		if(vm.filtertype == "My Par Forms"){
						    	  			vm.listParforms(1);
						    	  		}else{
						    	  			vm.listParforms(0);
						    	  		}
						    	  	}
									
			     				});	
			     			
						 });
		 			});
				
			}

			function selectParforms($parform) {
		            //LocalData.saveData("defAddParformFilter",$parform);
		            //$state.go($state.current, {}, {reload: true});
		            if($parform == "My Par Forms"){
	    	  			vm.listParforms(1);
	    	  		}else{
	    	  			vm.listParforms(0);
	    	  		}
	    	  		vm.filtertype = $parform;
		            vm.popover.hide();

			}
	    	

	    	vm.goTo = function(goToUrl,direction,_locationCode) {
				if(_locationCode){
					var params = {LocationCode:_locationCode};
					$state.go(goToUrl, params);
				}else if(direction){
					$ionicViewSwitcher.nextDirection(direction);
					$state.go(goToUrl,direction);
			
				}else{
					$state.go(goToUrl);
				}
			  	
			  
			};

			function optionMore($event,popupType) {

				$ionicPopover.fromTemplateUrl('html/templates/options-more.html', {
				        scope: $scope
				}).then(function(popover) {
				        vm.popover = popover;
				        vm.popUpType = (popupType) ? popupType : "add-par-form-settings";
				        popover.show($event);
				});
			}

			vm.checkboxClicked = function() {

			    vm.selectedAllParforms  = filterFilter( vm.Parforms, {val:true});
			    CommonInfoService.setSelectedParforms(vm.selectedAllParforms);	
			};

			function showClearAll() {

			    if(filterFilter( vm.Parforms, {val:true}).length == vm.Parforms.length) {
			    	return true;
			    } else {
			    	return false;
			    }
			}

	    	function selectedParForm(){
		    	if(filterFilter( vm.Parforms, {val:true})) {
		    		return filterFilter( vm.Parforms, {val:true}).length;
		    	}
		    	else {
		    		return 0;
		    	}
		    };

			function selectAll() {
			    var parCountLen =  vm.Parforms.length;

			    for(var x = 0; x < parCountLen; x++) {
			    	vm.Parforms[x].val = true;
			    }

			    vm.checkboxClicked();
			};

			function clearAll() {
			    var parCountLen =  vm.Parforms.length;

			    for(var x = 0; x < parCountLen; x++) {
			    	vm.Parforms[x].val = false;
			    }

			    vm.checkboxClicked();
	    	};

	    	vm.addParforms = function(){
	    		var SelectedParforms = CommonInfoService.getSelectedParforms();
	    		var promises = [];
		    	var deffered = $q.defer();
	    		//show loading icon
				/*$ionicLoading.show({
					templateUrl: CONSTANT.SpinnerTemplate
				});

	    		
	    		var requestString = "<getparformdetails>";
				Object.keys(SelectedParforms).forEach(function(index) {

					requestString += '<par locid="'+ SelectedParforms[index].LocationID +'"/>';
				});
				requestString += "</getparformdetails>";

				ParCountService
				.getParformDetails(requestString)
				.then(function(response) {
				
					ParCountService
						.saveParformItems(response)
						.then(function(res){*/
			     					
	     					Object.keys(SelectedParforms).forEach(function(index) {

			    				var queryParams = {};
						    	queryParams.LocationID = SelectedParforms[index].LocationID;
			    				CRUD.update("Par_Location_Table","IsSelected",1,queryParams)
			    					.then(function(result){
			    						deffered.resolve(result);
			    						promises.push(deffered.promise);
			    					});

								$q.all(promises)
								  .then(function(){
								  		$ionicLoading.hide();
								  		vm.goTo("app.my-par-forms","slideDown");
								 });
								
							});
									
			     	//});	
			 	//});
	    		

    			

				

	    	};

	    	vm.clearSearch = function(){
				vm.searchText = '';
			};

		}
		
})();
