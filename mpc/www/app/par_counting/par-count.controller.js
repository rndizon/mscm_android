(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('ParCountController', Body);

		function Body($state,
					  $scope,
					  $ionicLoading,
					  $ionicViewSwitcher, 
					  $ionicPopover, 
					  $ionicHistory,
					  filterFilter,
					  $cordovaToast, 
					  DmeManager,
					  CommonInfoService,
					  CRUD, 
					  ParCountService, 
					  LocalData,
					  ScannerManager,
					  BarcodeDataManager,
					  CONSTANT,
					  $ionicPlatform,
					  $ionicPopup
					  ) 
		{
				
			var vm = this;
			vm.parCount = [];
			vm.allParformsLength = 0;
			vm.showOptionToDelete = false;
			vm.selectedAllParforms = [];
			vm.searchText = "";
			vm.popover = null;
			vm.ifThereAreDownloaded = false;
			vm.companyHeader = null;
		    vm.companyHeaderID = null;
		    vm.goTo = goTo;
		    vm.ifCheckboxTrue = 0;
		    vm.showHideDeleteButtons = false;
		    vm.optionMore = optionMore;
		    vm.selectAll = selectAll;
		    vm.clearAll = clearAll;
		    vm.showClearAll = showClearAll;
			vm.firstEnter = LocalData.loadSession('firstEnterParcount');
			vm.currentUser = LocalData.loadSession('currentUser');
			vm.users = JSON.parse(LocalData.loadData('users'));
			


		    ScannerManager.enableScanner(searchByBarcode,BarcodeDataManager.Error); 



	    	var backBtn = $ionicPlatform.registerBackButtonAction(function(event) {
				    if (true) { // your check here

				    	if(vm.showSendButton == true) {
				    		vm.popUpMsg = "unsent-parform";
							$ionicPopup.confirm({
                    			cssClass: 'custom-popup',
								templateUrl: 'html/templates/pop-up.html',
								okText: 'Yes',
								cancelText: 'No',
								scope: $scope,
							}).then(function(res) {
								if (res) {

									/*
										Clear all unsent
									*/

									ionic.Platform.exitApp();
								}
							});

				    	} else if(vm.showOptionToDelete == false) {
							$ionicPopup.confirm({
								template: 'Are you sure you want to sign out ?'
							}).then(function(res) {
								if (res) {
									ionic.Platform.exitApp();
								}
							});
				    	} else {
						    document.getElementById('closeOption').click();
				    	}
				    }
				  }, 150);  

   			$scope.$on('$destroy', backBtn);



		    function searchByBarcode(data) {
		    	var barCodeDetails = BarcodeDataManager.ScanBarCode(data,'location');
		    	console.log(barCodeDetails.companyID);

		     	var queryParams = {};
				queryParams.CompanyID = barCodeDetails.companyID;
				// Disabled temporarily since no data matching on same location ID
		//		queryParams.LocationID = barCodeDetails.locationID;	

					CRUD.select("Par_Location_Table","*", queryParams)
						.then(function(result) {
							vm.parCount = result;
					});

		    }

			
		    $scope.$on('$ionicView.beforeEnter', function() {

		    	if(!LocalData.loadData("DownloadedItems")){ 
		    		LocalData.saveData("DownloadedItems",[]); //This session storage is for checking items if it is already exists in Par_Item_Table
		    	}

				if(LocalData.loadSession('firstEnterParcount') == "1") {
		    		removeAllFirstLoad();
				} 

		    	var SQLiteFlag = LocalData.loadData("SQLiteLoaded");

		     	
		     	if(!SQLiteFlag) {
		     		//show loading icon
					$ionicLoading.show({
						templateUrl: CONSTANT.SpinnerTemplate
					});
		     		vm.getHeadersCall();
					
				} else {
					vm.getCompanyList();
				}
		    });
		     $scope.$on('$ionicView.afterEnter', function() {
		     		vm.localStorShowHelp = (vm.users[vm.currentUser].isShowHelp)?vm.users[vm.currentUser].isShowHelp:1;
		     });

	      	function removeAllFirstLoad() {

		     	var queryParams = {};
				queryParams.CompanyID = LocalData.loadData("ParCountCompanyID");
				queryParams.IsSelected = 1;

				var LocationCodeTobeDeleted = [];

		    	CRUD.select("Par_Location_Table as parLoc","*", queryParams).then(function(result){

		    		result.forEach(function(v){
						LocationCodeTobeDeleted.push(v.LocationID);
		    		})

			    	var query = "UPDATE Par_Location_Table SET IsSelected = 0, IsItemDownloaded = 0 WHERE LocationID IN ('";
			 			query += LocationCodeTobeDeleted.join("','");
			 			query += "');";

					CRUD.inlineQuery(query)
					    .then(function(result) {
					    	vm.clearAll();
					    	CommonInfoService
				 			.clearTable('Par_Item_Table')
				 			.then(function(){
				 				CommonInfoService
					 			.clearTable('Par_Transaction_Table')
					 			.then(function(){
					 				$state.go($state.current, {}, {reload: true}); //refresh screen to update list view
					 			});
				 			});
					    	
							
					},function errorCallback(response) {
						    //$ionicLoading.hide(); 	
								   		 
					}); 

		    	})


			    LocalData.saveSession('firstEnterParcount','0')

		    }
		    vm.changeStart = 0;
		    $scope.$on('$stateChangeStart', 
			      function(event, toState, toParams, fromState, fromParams){ 
			          // transitionTo() promise will be rejected with 
			          // a 'transition prevented' error
			          
			          //console.log("Object is empty: "+Object.keys($state.params).length); //0 add-parforms-view
			          	if(!toParams.LocationID && vm.changeStart < 1 && toState.name == "app.company-change-view" && vm.parCount.length > 0){ //Go to change company without counted items
			          		event.preventDefault();
			          		vm.popUpMsg = "par-count-change-company";
			          		$ionicPopup.confirm({
		            			cssClass: 'custom-popup',
								templateUrl: 'html/templates/pop-up.html',
								okText: 'Yes',
								cancelText: 'No',
								scope: $scope,
							}).then(function(res) {
								if(res) {
									vm.changeStart++;
									$state.go(toState.name,{"deletelist":"1"});
												
								} else {
									event.preventDefault(); 
								}
							});
			          	} else if(!toParams.LocationID && vm.showSendButton && vm.changeStart < 1 && toState.name != "add-parforms-view"){
							
							event.preventDefault(); 
							
							if(toState.name == "app.company-change-view"){
								vm.popUpMsg = "par-count-change-company";
							}else {
								vm.popUpMsg = "par-count-change-module";
							}

							$ionicPopup.confirm({
		            			cssClass: 'custom-popup',
								templateUrl: 'html/templates/pop-up.html',
								okText: 'Yes',
								cancelText: 'No',
								scope: $scope,
							}).then(function(res) {
								if(res) {
									vm.changeStart++;

									if(toState.name != "app.company-change-view"){ // If is not change company screen
										
									 	ParCountService
									 		.deleteCurrentParforms()
									 		.then(function(){
									 			if(LocalData.loadData("DownloadedItems")){ 
										    		LocalData.saveData("DownloadedItems",[]); //This session storage is for checking items if it is already exists in Par_Item_Table
										    	}
								 				$state.go(toState.name);
									 		});

									} else{ // Set Flag delete par form list if it is going to change company screen. Deletion of Parfor list should be after selecting new company
										$state.go(toState.name,{"deletelist":"1"});
									}

											
								} else {
									//alert("NO");
									event.preventDefault(); 
								}
							});


						}else{
							//console.log("NOt details "+$state.params);
						}

			 });

		    $scope.$on('$ionicView.beforeLeave',function() {
			
				

		    	LocalData.saveSession('firstEnterParcount','0');

		    	if(vm.showSendButton == true) {
		    		console.log($state);
		    	}
		    });

		     vm.getCompanyList = function () {

		     	CRUD.select("Par_Company_Table","*")
					.then(function(result){
					 console.log('Callback: select result:' + result[0].CompanyDesc);

					//vm.companyList = result;
					

					if(vm.firstEnter == "1") {
						
						vm.companyHeader = (vm.users[vm.currentUser].defaultParCompanyDescription ? vm.users[vm.currentUser].defaultParCompanyDescription : LocalData.loadData("ParCountCompanyName"));
						vm.companyHeaderID = (vm.users[vm.currentUser].defaultParCompanyID ? vm.users[vm.currentUser].defaultParCompanyID : LocalData.loadData("ParCountCompanyID"));
						LocalData.saveData("ParCountCompanyID",vm.companyHeaderID);
						LocalData.saveData("ParCountCompanyName",vm.companyHeader);
					} else {

						vm.companyHeader = LocalData.loadData("ParCountCompanyName");
						vm.companyHeaderID = LocalData.loadData("ParCountCompanyID");
				//		LocalData.saveData("ParCountCompanyID", result[0].CompanyID);
				//		LocalData.saveData("ParCountCompanyName",result[0].CompanyDesc);
					}


					vm.getParformsList();
					
				});
		     
		     };

		     vm.getParformsList = function() {
		     	var queryParams = {};
				queryParams.CompanyID = LocalData.loadData("ParCountCompanyID");
				queryParams.IsSelected = 1;

				var columns = "parLoc.IsMyParform,parLoc.LocationCode,parLoc.LocationID,parLoc.LocationName,parLoc.IsItemDownloaded,";
					columns	+="(select COUNT(*) from Par_Location_Table left join Par_Item_Table ";
					columns	+="on Par_Location_Table.LocationID = Par_Item_Table.LocationID ";
					columns +="where Par_Item_Table.LocationID = parLoc.LocationID) as ItemCount,";

					columns	+="(select COUNT(*) from Par_Location_Table left join Par_Transaction_Table ";
					columns	+="on Par_Location_Table.LocationID = Par_Transaction_Table.LocationID ";
					columns +="where Par_Transaction_Table.LocationID = parLoc.LocationID) as TotalItemCounted,";
					
					columns	+="(select Timestamp from Par_Location_Table left join Par_TransHistory_Table ";
					columns	+="on Par_Location_Table.LocationID = Par_TransHistory_Table.CountedParforms ";
					columns +="where Par_TransHistory_Table.CountedParforms = parLoc.LocationID ORDER BY Par_TransHistory_Table.Timestamp DESC LIMIT 1) as Timestamp ";

				CRUD.select("Par_Location_Table as parLoc",columns, queryParams)
					.then(function(result) {
					vm.parCount = result;
					vm.allParformsLength = result.length;
					vm.showHideSendButton(result);
					$ionicLoading.hide();
					$scope.$broadcast('scroll.refreshComplete');

					//This loop statement is for getting items from SOAP call and save it to Par_Item_Table asynchronously
					for(var i = 0;i < vm.allParformsLength; i++){
						if(parseInt(vm.parCount[i].IsItemDownloaded) < 1){

							var requestString = "<getparformdetails>";
							requestString += '<par locid="'+ vm.parCount[i].LocationID +'"/>';
							requestString += "</getparformdetails>";

							ParCountService
							.getParformDetails(requestString)
							.then(function(response) {
								ParCountService.saveParformItems(response);
					     	});
						}
						
					}

				});
		     };

		     vm.showHideSendButton = function(result) {
		     	var stopLoop = false;
		     	result.forEach(function(index) {
		    		
		    		if(!stopLoop){
		    			if(index.TotalItemCounted <= index.ItemCount && index.TotalItemCounted > 0) {
			     			vm.showSendButton = true;
			     			stopLoop = true;
			     		} else {
			     			vm.showSendButton = false;
			     		}
		    		}
		     		
		     	});
		     };
		     vm.getHeadersCall = function() {
		    	 	
		     		ParCountService.getParformHeaders().then(function(response) {
		     			//alert(response);
		     				ParCountService.saveParforms(response).
		     				then(function(res){
		     					vm.getCompanyList();
								
		     				});	
		     			
					 });
		     };
		        
		      		//navigate to Download ParCount page.
		     vm.goToParCountDownload = function () {
			    console.log("button clicked");
			    vm.ifCheckboxTrue = false;
			    $state.go('app.downloaded-parcount');
			};
			 
			vm.goToAddParforms = function(){
				$ionicViewSwitcher.nextDirection('slideUp');
				$state.go("add-parforms-view");
			};

			vm.checkboxClicked = function() { 
			    vm.showHideDeleteButtons = false;
			    vm.showOptionToDelete = false;
			    vm.selectedAllParforms  = filterFilter( vm.parCount, {val:true});
			    CommonInfoService.setSelectedParforms(vm.selectedAllParforms);
			    	
			};
		    vm.selectedParCount = function(){
		    	return filterFilter( vm.parCount, {val:true}).length;
		    };

			function selectAll() {
			    var parCountLen =  vm.parCount.length;

			    for(var x = 0; x < parCountLen; x++) {
			    	vm.parCount[x].val = true;
			    }
			}

			function clearAll() {
			    var parCountLen =  vm.parCount.length;

			    for(var x = 0; x < parCountLen; x++) {
			    	vm.parCount[x].val = false;
			    }
			}

			function showClearAll() {

			    if(filterFilter( vm.parCount, {val:true}).length == vm.parCount.length) {
			    	return true;
			    } else {
			    	return false;
			    }
			}


	    	function optionMore($event,popupType) {
		    	$ionicPopover.fromTemplateUrl('html/templates/options-more.html', {
				        scope: $scope
				}).then(function(popover) {
				        vm.popover = popover;
				        vm.popUpType = (popupType) ? popupType : "parcount-edit";
				        popover.show($event);
				});
			}

	    	/* 
	    	    Ionic Popover
	    	*/
	    	vm.closePopover = function() {
	    		vm.popover.hide();
	    	};


	    	vm.doRefresh = function(){

	    	  	//show loading icon
				$ionicLoading.show({
					templateUrl: CONSTANT.SpinnerTemplate
				});
	 			
		 		CommonInfoService
		 			.clearTable('Par_Item_Table')
		 			.then(function(){

		 				LocalData.saveData("DownloadedItems",[]); //Clear Downloaded Items

		 				CommonInfoService
			 			.clearTable('Par_Transaction_Table')
			 			.then(function(){
			 				var LocationCodeTobeDeleted = [];

						    	angular.forEach(vm.parCount, function(item,key){
						    		LocationCodeTobeDeleted.push(item.LocationID);
						    	});

								var query = "UPDATE Par_Location_Table SET IsItemDownloaded = 0 WHERE LocationID IN ('";
					 			query += LocationCodeTobeDeleted.join("','");
					 			query += "');";

								CRUD.inlineQuery(query)
								    .then(function(result) {
								    	//$state.go($state.current, {}, {reload: true}); //refresh screen to update list view
								    	vm.getParformsList();
								    	if(LocalData.loadData("DownloadedItems")){ 
								    		LocalData.saveData("DownloadedItems",[]); //This session storage is for checking items if it is already exists in Par_Item_Table
								    	}
								    	
								});
			 			});
		 			});		
						

				
	    	};

	    	function goTo(goToUrl,_locationCode,_locationID,_locationName) {
				if(_locationCode || _locationCode != ""){
					var params = {LocationCode:_locationCode,LocationID:_locationID,LocationName:_locationName};
					
					if(goToUrl == "app.downloaded-parcount-details") {
						$ionicViewSwitcher.nextDirection('slideUp');
					} else {
						$ionicViewSwitcher.nextDirection('forward');
					}
					
					$state.go(goToUrl, params);
				}else{
					$state.go(goToUrl);
				}
			  	
			  
			};

			vm.clearSearch = function(){
				vm.searchText = '';
			};

			vm.showHideDelete = function(){
				vm.showOptionToDelete = !vm.showOptionToDelete;
				vm.showHideDeleteButtons = !vm.showHideDeleteButtons;
				vm.ifCheckboxTrue = 0;
				//vm.unSelectAllCheckbox();
				vm.popover.hide();
			};

			vm.deleteParForms = function(){
				var selectedAllParforms  = filterFilter( vm.parCount, {val:true});
			    CommonInfoService.setSelectedParforms(selectedAllParforms);
				var LocationCodeTobeDeleted = [];

				CommonInfoService.getSelectedParforms().forEach(function(v){
					LocationCodeTobeDeleted.push(v.LocationID);
				});

				

				var query = "UPDATE Par_Location_Table SET IsSelected = 0, IsItemDownloaded = 0 WHERE LocationID IN ('";
		 			query += LocationCodeTobeDeleted.join("','");
		 			query += "');";

				CRUD.inlineQuery(query)
				    .then(function(result) {
				    	vm.clearAll();
				    	CommonInfoService
			 			.clearTable('Par_Item_Table')
			 			.then(function(){
			 				CommonInfoService
				 			.clearTable('Par_Transaction_Table')
				 			.then(function(){
				 				//$state.go($state.current, {}, {reload: true}); //refresh screen to update list view
				 				vm.getParformsList();
				 				vm.showHideDelete();
				 				if(LocalData.loadData("DownloadedItems")){ 
						    		LocalData.saveData("DownloadedItems",[]); //This session storage is for checking items if it is already exists in Par_Item_Table
						    	}
				 			});
			 			});
				    	
						
				},function errorCallback(response) {
					    //$ionicLoading.hide(); 	
							   		 
				});

			};

			vm.SendFormToServer = function(_LocationID){
				
				var LocationID = _LocationID || 0;

				vm.users[vm.currentUser].lastParformsUsed = []; //Clear last parform used

				//show loading icon
				$ionicLoading.show({
					templateUrl: CONSTANT.SpinnerTemplate
				});

				var ItemQuery = "select * from Par_Transaction_Table";
                if(LocationID != "0"){
                	ItemQuery += " where LocationID ="+LocationID;
                }

                CRUD.inlineQuery(ItemQuery)
                    .then(function(itemResult) {

                    var requestString = "<syncparcounts>";
                    requestString += '<username>'+LocalData.loadSession('SoapUsername')+'</username>';

                    var currentLocationID = 0;
                    var nextLocationID = 0;
                    var locIDArr = [];
                    		angular.forEach(itemResult, function(item,key){
                            currentLocationID = item.LocationID;      

                            if(locIDArr.indexOf(item.LocationID) == -1) {
                                    console.log(key);

                                    if(key != 0) {
                                        requestString += "</par>";
                                    }
                                    requestString += '<par locid="'+ item.LocationID +'" lblprnid="'+item.PrinterID+'">';
                                    vm.users[vm.currentUser].lastParformsUsed.push(item.LocationID); //Save LocationID for Last Par Form used flow
                            } 
                            requestString += '<item itemid="'+item.ItemID+'" count="'+(item.ItemQuantity == null)?"":item.ItemQuantity+'" addqty="'+(item.AdditionalQuantity == null)?"":item.AdditionalQuantity+'" timestamp="04/18/2016 09:07:17 PM"/>';
                            locIDArr[key] = item.LocationID;

                            if(locIDArr.indexOf(item.LocationID) == -1) {
                                    requestString += '</par>';
                            } 
                                            

				        });
				        requestString += "</par></syncparcounts>";
				        

				        console.log(requestString);

				        ParCountService
				        	.syncParCount(requestString)
				        	.then(function(data){
				        		var xmlString = (new XMLSerializer()).serializeToString(data);
				        		//alert(xmlString);
				        		if(xmlString.indexOf("resultsuccess") != -1){

				        			
		        					//Update Par_TransHistory_Table
								    locIDArr.sort();
									var currentLoc = null;
								    var itemCount = 0;
								    for (var i = 0; i < locIDArr.length; i++) {
								        if (locIDArr[i] != currentLoc) {
								            if (itemCount > 0) {
								                console.log(currentLoc + ' comes1 --> ' + itemCount + ' times<br>');
								                ParCountService.saveTransactionHistory(currentLoc,itemCount);

								            }
								            currentLoc = locIDArr[i];
								            itemCount = 1;
								        } else {
								            itemCount++;
								        }
								    }
								    if (itemCount > 0) {
								        console.log(currentLoc + ' comes2 --> ' + itemCount + ' times');
								        ParCountService.saveTransactionHistory(currentLoc,itemCount);
								    }

								    
								    //Clear Transaction Table and refresh the list
								    CommonInfoService
				        				.clearTable("Par_Transaction_Table")
				        				.then(function(){
				        					$cordovaToast.showLongCenter('Par form counts sent successfully to server.');
				        					LocalData.saveData('users',JSON.stringify(vm.users)); // Update Users Localstorage data
				        					vm.getParformsList();
				        				});

		        						

				        			
				        		}else{    
				        			vm.getParformsList();
				        			//alert("failed");
				        		}
				        	});


                    });


			};
    	  
		}
		
		
		
})();
