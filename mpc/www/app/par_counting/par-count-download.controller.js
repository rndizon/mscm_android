(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('ParCountDownloadController', Body);

		function Body($state,
					  $scope,
					  $ionicLoading,
					  $ionicPopover,
					  CommonInfoService,
					  ParCountService,
					  LocalData
					  ) 
		{
				
			 var vm = this;
			 vm.popover = null;
			 vm.companyID = CommonInfoService.getSelectedCompany();
			 vm.companyHeader = LocalData.loadData("ParCountCompanyName");
			 vm.companyHeaderID = LocalData.loadData("ParCountCompanyID");
			 vm.selectedParforms = CommonInfoService.getSelectedParforms();
			 vm.CommonInfoService = CommonInfoService;
			 vm.downloadedParformList = [];
			 vm.parformItemLength = 0; //number of items in given parform
			 vm.downloadedParformDetails = [];
			 vm.downloadedParformList = [];
			 vm.goTo = goTo;
			 
			 $scope.$on('$ionicView.beforeEnter', function() {
				if(!LocalData.loadData("DownloadedParformDetails")){
				 	LocalData.saveData("DownloadedParformDetails",[]);
				}
			});

			$scope.$on('$ionicView.afterEnter', function () {
				
			 	vm.downloadedParformDetails = LocalData.loadData("DownloadedParformDetails");

			 	var requestString = "<getparformdetails>";
			 	var needToRequestData = 0;
				Object.keys(vm.selectedParforms).forEach(function(index) {

					if(vm.downloadedParformDetails.indexOf(vm.selectedParforms[index].LocationID) == -1){

						needToRequestData++;
						vm.downloadedParformDetails.push(vm.selectedParforms[index].LocationID);
						requestString += '<par locid="'+ vm.selectedParforms[index].LocationID +'"/>';
					}
					
				});
				requestString += "</getparformdetails>";

				/*if(needToRequestData > 0){
					$ionicLoading.show({
						templateUrl: 'html/templates/spinner.html'
					});
					ParCountService
						.getParformDetails(requestString)
						.then(function(response) {
						
							ParCountService
								.saveParformItems(response)
								.then(function(res){
					     					
			     					ParCountService
			     						.getDownloadedParforms(vm.downloadedParformDetails)
			     						.then(function(result){
											vm.downloadedParformList = result;
											console.log(vm.downloadedParformList.length);
											$ionicLoading.hide();
									});
											
					     	});	
					 });
				}else{*/
					ParCountService
						.getDownloadedParforms(vm.downloadedParformDetails)
						.then(function(result){
									vm.downloadedParformList = result;
									console.log(vm.downloadedParformList.length);
									$ionicLoading.hide();
						});
				//}
			 	

				/*var fieldList = "COMPANY;LOCATION;ITEM;UOM;REORDER-POINT;ITEMMAST.DESCRIPTION;" +
 				 "ITEMMAST.MANUF_NBR";
 
				var strCond = "COMPANY="+ vm.companyID + "&ACTIVE-STATUS=A";
				 
				DmeManager.init(vm.getDMEResult);
				
				Object.keys(vm.selectedParforms).forEach(function(idx) {
					
					if (strCond.indexOf("LOCATION") > - 1 ){
						 strCond += "|LOCATION=" + vm.selectedParforms[idx].LOCATION;
					}else {
						 strCond += "&LOCATION=" + vm.selectedParforms[idx].LOCATION;
					}
				});
						 
				 DmeManager.setParam("FIELD", fieldList);
		    	 DmeManager.setParam("INDEX", "ITLSET1");
				 DmeManager.setParam("PROD", "APPS9017"); // is this hard coded?
				 DmeManager.setParam("SELECT", strCond);
				 DmeManager.setParam("MAX", 100);
				 DmeManager.setParam("FILE", "ITEMLOC"); 
				 DmeManager.send();
				 
				  //show loading icon
				 $ionicLoading.show({
					   templateUrl: 'html/templates/spinner.html'
				 });*/


				  
			 });
			 
		    /*vm.getDMEResult = function(result) {
				 
				 $ionicLoading.hide();
				 
				 if (result) {
					vm.downloadedParformList = result;
					vm.downloadParformLength = vm.downloadedParformList.length;
					console.log('ITEM -->  ' + result[0].ITEM);
				} else {
					console.log('No data');
				}
				 
			 };*/

			 function goTo(goToUrl,_locationCode) {
				if(_locationCode){
					var params = {LocationCode:_locationCode};
					$state.go(goToUrl, params);
				}else{
					$state.go(goToUrl);
				}
			  	
			  
			};

			$ionicPopover.fromTemplateUrl('html/templates/options-parcount.html', {
			        scope: $scope
			}).then(function(popover) {
			        vm.popover = popover;
			        vm.popUpType = "edit";
			});

	    	/* 
	    	    Ionic Popover
	    	*/
	    	vm.closePopover = function() {
	    		vm.popover.hide();
	    	};
		
		}
		
		
		
})();
