(function(){
 	'use strict';
 	
		angular
		    .module('app')	
		    .controller('AllParformsController', Body);

		function Body($ionicLoading,
					  $state,
					  $scope,
					  $ionicPopover, 
					  filterFilter, 
					  CRUD, 
					  LocalData,
					  CommonInfoService
					  ) 
		{
				
			var vm = this;
			vm.allParformsLength = 0;
			vm.allParformsList = [];
			vm.companyHeader = LocalData.loadData("ParCountCompanyName");
    	  	vm.companyHeaderID = LocalData.loadData("ParCountCompanyID");
    	  	vm.popover = null;
    	  	vm.ifThereAreDownloaded = false;
			   $scope.$on('$ionicView.afterEnter', function() {


			   	if(LocalData.loadData("DownloadedParformDetails") && LocalData.loadData("DownloadedParformDetails").length > 0){
				 	vm.ifThereAreDownloaded = true;
				}
				

				//show loading icon
				/*$ionicLoading.show({
					templateUrl: 'html/templates/spinner.html'
				}); */  
				var queryParams = {};
			    queryParams.IsMyParform = 0;
			    queryParams.CompanyID = vm.companyHeaderID;

			    	CRUD.select("Par_Location_Table","*", queryParams)
						.then(function(result){
				 		 	vm.parCount = result;
				 		 	vm.allParformsLength = result.length;
				 		 	$ionicLoading.hide();
				 	});
				   
			   });

			$ionicPopover.fromTemplateUrl('html/templates/options-parcount.html', {
			        scope: $scope
			}).then(function(popover) {
			        vm.popover = popover;
			        vm.popUpType = "list";
			});

	    	/* 
	    	    Ionic Popover
	    	*/
	    	vm.closePopover = function() {
	    		vm.popover.hide();
	    	}; 
			
			   		//navigate to Download ParCount page.
		     vm.goToParCountDownload = function () {
			    console.log("button clicked");
			    vm.ifCheckboxTrue = false;
			    $state.go('app.downloaded-parcount');
			};
			      
			vm.checkboxClicked = function() {
			    vm.ifCheckboxTrue = filterFilter( vm.parCount, {val:true}).length;
			    vm.selectedAllParforms  = filterFilter( vm.parCount, {val:true});
			    CommonInfoService.setSelectedParforms(vm.selectedAllParforms);	
			};
		      
			vm.selectAllCheckbox = function() {
			    var parCountLen =  vm.parCount.length;

			    for(var x = 0; x < parCountLen; x++) {
			    	vm.parCount[x].val = true;
			    }

			    vm.closePopover();
			    vm.checkboxClicked(); // Show subheader 
			};

			vm.unSelectAllCheckbox = function() {
			    var parCountLen =  vm.parCount.length;

			    for(var x = 0; x < parCountLen; x++) {
			    	vm.parCount[x].val = false;
			    }

			vm.checkboxClicked(); // Hide subheader 

	    	};

	    	vm.allParForm = function(){
	    		vm.closePopover();	

		    	$state.go("app.all-parforms-view");
	    	};

	    	vm.goTo = function(goToUrl,_locationCode) {
				if(_locationCode){
					var params = {LocationCode:_locationCode};
					$state.go(goToUrl, params);
				}else{
					$state.go(goToUrl);
				}
			  	
			  
			};
			vm.searchParLocation = function () {
	    	  	
	    	  	/*var queryParams = {};
	    	  	queryParams.CompanyID = LocalData.loadData("ParCountCompanyID");
	    		queryParams.IsMyParform = 1; 

	    	  	if (vm.searchText) {
	    	  		queryParams.LocationCode = vm.searchText;	
	    	  			
	    	  	}

	    	  	CRUD.select("Location_Table","*", queryParams)
					.then(function(result) {
			 		 	vm.parCount = result;
			 		 	vm.allParformsLength = result.length;
			 		});*/
					vm.unSelectAllCheckbox();

			 		var query = "SELECT * FROM Location_Table WHERE LocationCode LIKE '%"+vm.searchText+"%' AND IsMyParform = 0 AND CompanyID='"+LocalData.loadData("ParCountCompanyID")+"'";

					CRUD.inlineQuery(query)
				     	 .then(function(result) {

				     	 	vm.parCount = result;
			 		 		vm.allParformsLength = result.length;

				     	 },function errorCallback(response) {
						   		 
					});

	    	  };

	    	  vm.doRefresh = function(){

		    	  	var queryParams = {};
				    queryParams.IsMyParform = 0;
				    queryParams.CompanyID = vm.defCompanyID;

				    CRUD.select("Location_Table","*", queryParams)
							.then(function(result){
					 		 	vm.parCount = result;
					 		 	vm.allParformsLength = result.length;
					 		 	$scope.$broadcast('scroll.refreshComplete');
					});
	    	  };
			
		}
		
})();
