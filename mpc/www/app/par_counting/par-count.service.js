(function(){
    'use strict';
	 
	angular
	    .module('app')
	    .factory('ParCountService', Body);

	    function Body($q,$ionicPopup,$rootScope,$ionicSlideBoxDelegate,
	    			  CRUD,
	    			  LocalData,
	    			  CM,
	    			  CommonInfoService
	    			  ) 
	    {
		  
		    var service = {
		        getParformDetails:getParformDetails,
		        //getDownloadedParforms: getDownloadedParforms,
		        getParformHeaders:getParformHeaders,
		        saveParforms:saveParforms,
		        saveParformItems:saveParformItems,
		        syncParCount:syncParCount,
		        saveTransactionHistory:saveTransactionHistory,
		        deleteCurrentParforms:deleteCurrentParforms,
		        saveParCountCompany:saveParCountCompany,
		        saveParCountLocation:saveParCountLocation,
		        onFocus:onFocus,
		        saveValues:saveValues
		    },
		    userObject = JSON.parse(LocalData.loadData('users')),
		    currentUser = LocalData.loadSession('SoapUsername'),
			requestToken = LocalData.loadSession("SoapToken");

		    return service;  	
		 	
		 	function getParformHeaders(){
				var args = {
		 			URL:    "ParformService",
		 			Action: "getParformHeaders",
		 			Params: {"requestString":"<getparformheaders></getparformheaders>","token":requestToken}
		 		};

		 		return CM.doCommsCall("soap",args);
		 	}

		 	function saveParforms(response){
		 		

		    	var promises = [];
		    	var deffered = $q.defer();
		 		var value = xmlToJson(response);
				var xmlResponse = value.root.body.getParformHeadersResponse.getParformHeadersReturn;
				var object = ReturnDataObject(xmlResponse);
				var companyResponse = object.getparformheadersresponse.parforms.fac;
				var configResponse = object.getparformheadersresponse.parconfig.attributes;
			  	var arryCompanies = [];

			  	userObject[currentUser].parCountConfiguration.isMyParFormFirst = (configResponse.myparforms == "true") ? 1 : 0;
			  	userObject[currentUser].parCountConfiguration.isPopulateItemScreen = (configResponse.populateitemscreen == "true") ? 1 : 0;
			  	userObject[currentUser].parCountConfiguration.isAllowAddlQuantity = (configResponse.allowaddqty == "true") ? 1 : 0;
			  	LocalData.saveData('users',JSON.stringify(userObject)); // Update Users Localstorage data

			  	if(companyResponse instanceof Array){
				  arryCompanies = companyResponse;
				}else{
				  arryCompanies.push(companyResponse);  
				}

			  	if(!LocalData.loadData("ParCountCompanyID")){
			  		LocalData.saveData("ParCountCompanyID", arryCompanies[0].attributes.facid);
			  		LocalData.saveData("ParCountCompanyName",arryCompanies[0].attributes.facdesc);
			  	}
			  	
			  	
			  	var CompanyQuery = "insert into Par_Company_Table (CompanyID,CompanyDesc,UserID,IsShowQOO,IsEditParLevel,IsPrintLabel) " ;
			  	var CompanyQueryCount = 0;

			  	

			  	angular.forEach(arryCompanies, function(company, key) {
			  	
				  	    var CompanyID = checkValue(company.attributes.facid);
					    var CompanyDesc = checkValue(company.attributes.facdesc,"text");
					    var IsQOODisplay = (company.attributes.qoo === 'true' ?  1 :  0 );
					    var IsEditParLevel = (company.attributes.editpar === 'true' ?  1 :  0 );
				  	    var IsPrintLabel = (company.attributes.prnlbl === 'true' ?  1 :  0 );
				  	   	
				  	    saveParCountPrinter(CompanyID);

				  	    if(CompanyQueryCount < 1){
				  	  		CompanyQueryCount++;

				  	  		CompanyQuery += "select "+CompanyID+" as CompanyID, ";
				  	  		CompanyQuery += '"'+CompanyDesc+'" as CompanyDesc, ';
				  	  		CompanyQuery += '"'+currentUser+'" as UserID, ';
				  	  		CompanyQuery += IsQOODisplay+" as IsShowQOO, ";
				  	  		CompanyQuery += IsEditParLevel+" as IsEditParLevel, ";
				  	  		CompanyQuery += IsPrintLabel+" as IsPrintLabel ";

				  	    }else{
				  	  		CompanyQuery += "union all select "+CompanyID+", ";
				  	  		CompanyQuery += '"'+CompanyDesc+'", ';
				  	  		CompanyQuery += '"'+currentUser+'", ';
				  	  		CompanyQuery += IsQOODisplay+", ";
				  	  		CompanyQuery += IsEditParLevel+", ";
				  	  		CompanyQuery += IsPrintLabel+" ";
				  	    }

				     	var parlocationsArray = company.par;
				     	var LocationQuery = "insert into Par_Location_Table (LocationCode,LocationName,UserID,LocationID,CompanyID,IsMyParform,IsPickForPar) " ;
				  		var LocationQueryCount = 0;
						var LimitCount = 0;
				     	angular.forEach(parlocationsArray, function(loc, key) {	
				     		
					     		var _LocationCode,_LocationName,_LocationID,_CompanyID,_IsMyParform,_ItemCount,_LastMod,_AddQtyMult,_IsPickForPar;
					     		if (loc.attributes != undefined) {
						     		_LocationCode = checkValue(loc.attributes.loccode,"text");
							  		_LocationName = checkValue(loc.attributes.locdesc,"text");
							  		_LocationID = checkValue(loc.attributes.locid);
							  		_CompanyID = checkValue(company.attributes.facid);
							  		_IsMyParform = (loc.attributes.mypar != undefined ? 1 : 0); 
							  		//_ItemCount = parseInt(loc.attributes.itemcount);
							  		//_LastMod =  loc.attributes.lastmod;
							  		//_AddQtyMult = 0; // user input
							  		_IsPickForPar = 0; //to be asked
						     	}	else {
						     		_LocationCode = checkValue(loc.loccode,"text");
							  		_LocationName = checkValue(loc.locdesc,"text");
							  		_LocationID = checkValue(loc.locid);
							  		_CompanyID = checkValue(company.attributes.facid);
							  		_IsMyParform = (loc.mypar != undefined ? 1 : 0); 
							  		//_ItemCount = parseInt(loc.itemcount);
							  		//_LastMod =  loc.lastmod;
							  		//_AddQtyMult = 0; // user input
							  		_IsPickForPar = 0; //to be asked
						     	}
						     	if(LimitCount < 500){ //Temporary SQL insertion limit 
						     		if(LocationQueryCount < 1){
						     			LocationQueryCount++;
						     			LocationQuery += "select '"+_LocationCode+"' as LocationCode, ";
						     			LocationQuery += '"'+_LocationName+'" as LocationName, ';
						     			LocationQuery += '"'+currentUser+'" as UserID, ';
						     			LocationQuery += '"'+_LocationID+'" as LocationID, ';
						     			LocationQuery += _CompanyID+" as CompanyID, ";
						     			LocationQuery += _IsMyParform+" as IsMyParform, ";
						     			//LocationQuery += _ItemCount+" as ItemCount, ";
						     			//LocationQuery += "'"+_LastMod+"' as LastMod, ";
						     			//LocationQuery += _AddQtyMult+" as AddQtyMult, ";
						     			LocationQuery += _IsPickForPar+" as IsPickForPar ";
						     		}else{
						     			LocationQuery += "union all select '"+_LocationCode+"', ";
						     			LocationQuery += '"'+_LocationName+'", ';
						     			LocationQuery += '"'+currentUser+'", ';
						     			LocationQuery += '"'+_LocationID+'", ';
						     			LocationQuery += _CompanyID+", ";
						     			LocationQuery += _IsMyParform+", ";
						     			//LocationQuery += _ItemCount+", ";
						     			//LocationQuery += "'"+_LastMod+"', ";
						     			//LocationQuery += _AddQtyMult+", ";
						     			LocationQuery += _IsPickForPar+" ";
						     		}
						     	}
						     	
					  		  LimitCount++;
				  	});
					
			  		//console.log(LocationQuery);
				  	CRUD.inlineQuery(LocationQuery)
				     	 .then(function(result) {
				     	 	deffered.resolve(result);
				    		console.log("successful location insert "+company.attributes.facid+" count "+LimitCount);
				     	 },function errorCallback(response) {
						   	deffered.resolve(response); 
					});
				     console.log(company.attributes.facid+" count "+LimitCount);
			  	});

				

				console.log(CompanyQuery);
				CRUD.inlineQuery(CompanyQuery).
			  	then(function(result){
			  			deffered.resolve(result);
						LocalData.saveData("SQLiteLoaded", true);
						console.log("successful company insert");
			  	},function(res){
			  		deffered.resolve(res);
			  	});

			  	
			  	
			  	promises.push(deffered.promise);
				
				return $q.all(promises);
		 	}
		 	function saveParCountPrinter(CompanyID){

		 		var args = {
		 			URL:    "ParformService",
		 			Action: "getParPrinters",
		 			Params: {"token":requestToken,"strCompanyNo":CompanyID}
		 		};

		 		CM.doCommsCall("soap",args)
		 		  .then(function(response){
		 		  		var value = xmlToJson(response);
						var xmlResponse = value.root.body.getParPrintersResponse.getParPrintersReturn;
						var object = ReturnDataObject(xmlResponse);
						var printersResponse = object.getparprinters.printer;
						var printersResponseArray = [];


						if(printersResponse instanceof Array){
							printersResponseArray = printersResponse;
						}else{
							printersResponseArray.push(printersResponse);
						}

						var printerQuery = "INSERT INTO Par_Printer_Table (CompanyID,UserID,PrinterID,PrinterName) VALUES ";

						angular.forEach(printersResponseArray, function(printer, key) {
					  		var PrinterID = checkValue(printer.attributes.id);
					  		var PrinterName = checkValue(printer.attributes.name,"text");

					  		printerQuery += "('" + CompanyID + "','" + currentUser + "','" + PrinterID + "','" + PrinterName + "'),";

					  	});
						console.log("Printer "+printerQuery);
					  	CRUD.inlineQuery(printerQuery.slice(0,-1));
		 		  });

		 	}
		 	function saveParCountCompany(response){
		 		var promises = [];
		    	var deffered = $q.defer();
		 		var value = xmlToJson(response);
				var xmlResponse = value.root.body.getParformHeadersResponse.getParformHeadersReturn;
				var object = ReturnDataObject(xmlResponse);
				var companyResponse = object.getparformheadersresponse.parforms.fac;
				var configResponse = object.getparformheadersresponse.parconfig.attributes;
			  	var arryCompanies = [];

			  	userObject[currentUser].parCountConfiguration.isMyParFormFirst = (configResponse.myparforms == "true") ? 1 : 0;
			  	userObject[currentUser].parCountConfiguration.isPopulateItemScreen = (configResponse.populateitemscreen == "true") ? 1 : 0;
			  	userObject[currentUser].parCountConfiguration.isAllowAddlQuantity = (configResponse.allowaddqty == "true") ? 1 : 0;
			  	LocalData.saveData('users',JSON.stringify(userObject)); // Update Users Localstorage data

			  	if(companyResponse instanceof Array){
				  arryCompanies = companyResponse;
				}else{
				  arryCompanies.push(companyResponse);  
				}
			  	
			  	
			  	var CompanyQuery = "insert into Par_Company_Table (CompanyID,CompanyDesc,UserID,IsShowQOO,IsEditParLevel,IsPrintLabel) " ;
			  	var CompanyQueryCount = 0;

			  	

			  	angular.forEach(arryCompanies, function(company, key) {
			  	
				  	    var CompanyID = checkValue(company.attributes.facid);
					    var CompanyDesc = checkValue(company.attributes.facdesc,"text");
					    var IsQOODisplay = (company.attributes.qoo === 'true' ?  1 :  0 );
					    var IsEditParLevel = (company.attributes.editpar === 'true' ?  1 :  0 );
				  	    var IsPrintLabel = (company.attributes.prnlbl === 'true' ?  1 :  0 );
				  	    
				  	    saveParCountPrinter(CompanyID);
				  	    
				  	    if(CompanyQueryCount < 1){
				  	  		CompanyQueryCount++;

				  	  		CompanyQuery += "select "+CompanyID+" as CompanyID, ";
				  	  		CompanyQuery += '"'+CompanyDesc+'" as CompanyDesc, ';
				  	  		CompanyQuery += '"'+currentUser+'" as UserID, ';
				  	  		CompanyQuery += IsQOODisplay+" as IsShowQOO, ";
				  	  		CompanyQuery += IsEditParLevel+" as IsEditParLevel, ";
				  	  		CompanyQuery += IsPrintLabel+" as IsPrintLabel ";

				  	    }else{
				  	  		CompanyQuery += "union all select "+CompanyID+", ";
				  	  		CompanyQuery += '"'+CompanyDesc+'", ';
				  	  		CompanyQuery += '"'+currentUser+'", ';
				  	  		CompanyQuery += IsQOODisplay+", ";
				  	  		CompanyQuery += IsEditParLevel+", ";
				  	  		CompanyQuery += IsPrintLabel+" ";
				  	    }

				     	
			  	});

				
				CRUD.inlineQuery(CompanyQuery).
			  	then(function(result){
			  			deffered.resolve(result);
			  	},function(res){
			  		deffered.resolve(res);
			  	});
			  	
			  	promises.push(deffered.promise);
				
				return $q.all(promises);
		 	}
		 	function saveParCountLocation(response){
		 		var promises = [];
		    	var deffered = $q.defer();
		 		var value = xmlToJson(response);
				var xmlResponse = value.root.body.getParformHeadersResponse.getParformHeadersReturn;
				var object = ReturnDataObject(xmlResponse);
				var companyResponse = object.getparformheadersresponse.parforms.fac;
				var configResponse = object.getparformheadersresponse.parconfig.attributes;
			  	var arryCompanies = [];

			  	userObject[currentUser].parCountConfiguration.isMyParFormFirst = (configResponse.myparforms == "true") ? 1 : 0;
			  	userObject[currentUser].parCountConfiguration.isPopulateItemScreen = (configResponse.populateitemscreen == "true") ? 1 : 0;
			  	userObject[currentUser].parCountConfiguration.isAllowAddlQuantity = (configResponse.allowaddqty == "true") ? 1 : 0;
			  	LocalData.saveData('users',JSON.stringify(userObject)); // Update Users Localstorage data

			  	if(companyResponse instanceof Array){
				  arryCompanies = companyResponse;
				}else{
				  arryCompanies.push(companyResponse);  
				}
			  	
			  	angular.forEach(arryCompanies, function(company, key) {
			  	
				     	var parlocationsArray = company.par;
				     	var LocationQuery = "insert into Par_Location_Table (LocationCode,LocationName,UserID,LocationID,CompanyID,IsMyParform,IsPickForPar) " ;
				  		var LocationQueryCount = 0;
						var LimitCount = 0;
				     	angular.forEach(parlocationsArray, function(loc, key) {	
				     		
					     		var _LocationCode,_LocationName,_LocationID,_CompanyID,_IsMyParform,_ItemCount,_LastMod,_AddQtyMult,_IsPickForPar;
					     		if (loc.attributes != undefined) {
						     		_LocationCode = checkValue(loc.attributes.loccode,"text");
							  		_LocationName = checkValue(loc.attributes.locdesc,"text");
							  		_LocationID = checkValue(loc.attributes.locid);
							  		_CompanyID = checkValue(company.attributes.facid);
							  		_IsMyParform = (loc.attributes.mypar != undefined ? 1 : 0); 
							  		//_ItemCount = parseInt(loc.attributes.itemcount);
							  		//_LastMod =  loc.attributes.lastmod;
							  		//_AddQtyMult = 0; // user input
							  		_IsPickForPar = 0; //to be asked
						     	}	else {
						     		_LocationCode = checkValue(loc.loccode,"text");
							  		_LocationName = checkValue(loc.locdesc,"text");
							  		_LocationID = checkValue(loc.locid);
							  		_CompanyID = checkValue(company.attributes.facid);
							  		_IsMyParform = (loc.mypar != undefined ? 1 : 0); 
							  		//_ItemCount = parseInt(loc.itemcount);
							  		//_LastMod =  loc.lastmod;
							  		//_AddQtyMult = 0; // user input
							  		_IsPickForPar = 0; //to be asked
						     	}
						     	if(LimitCount < 500){ //Temporary SQL insertion limit 
						     		if(LocationQueryCount < 1){
						     			LocationQueryCount++;
						     			LocationQuery += "select '"+_LocationCode+"' as LocationCode, ";
						     			LocationQuery += '"'+_LocationName+'" as LocationName, ';
						     			LocationQuery += '"'+currentUser+'" as UserID, ';
						     			LocationQuery += '"'+_LocationID+'" as LocationID, ';
						     			LocationQuery += _CompanyID+" as CompanyID, ";
						     			LocationQuery += _IsMyParform+" as IsMyParform, ";
						     			//LocationQuery += _ItemCount+" as ItemCount, ";
						     			//LocationQuery += "'"+_LastMod+"' as LastMod, ";
						     			//LocationQuery += _AddQtyMult+" as AddQtyMult, ";
						     			LocationQuery += _IsPickForPar+" as IsPickForPar ";
						     		}else{
						     			LocationQuery += "union all select '"+_LocationCode+"', ";
						     			LocationQuery += '"'+_LocationName+'", ';
						     			LocationQuery += '"'+currentUser+'", ';
						     			LocationQuery += '"'+_LocationID+'", ';
						     			LocationQuery += _CompanyID+", ";
						     			LocationQuery += _IsMyParform+", ";
						     			//LocationQuery += _ItemCount+", ";
						     			//LocationQuery += "'"+_LastMod+"', ";
						     			//LocationQuery += _AddQtyMult+", ";
						     			LocationQuery += _IsPickForPar+" ";
						     		}
						     	}
						     	
					  		  LimitCount++;
				  	});
					
			  		//console.log(LocationQuery);
				  	CRUD.inlineQuery(LocationQuery)
				     	 .then(function(result) {
				     	 	deffered.resolve(result);
				    		console.log("add par form successful location insert ");
				     	 },function errorCallback(response) {
						   	deffered.resolve(response); 
					});
			  	});

			  	
			  	promises.push(deffered.promise);
				
				return $q.all(promises);
		 	}

		 	function getParformDetails(requestString){

				var args = {
		 			URL:    "ParformService",
		 			Action: "getParformDetails",
		 			Params: {"requestString":requestString,"token":requestToken}
		 		};

		 		return CM.doCommsCall("soap",args);

		 	}
		 	function saveParformItems(response){

		     	var promises = [];
		    	var deffered = $q.defer();
		 		var value = xmlToJson(response);
				var xmlResponse = value.root.body.getParformDetailsResponse.getParformDetailsReturn;
				var object = ReturnDataObject(xmlResponse);
			  	var parlocationsResponse = object.getparformdetailsresponse.fac.par;
			  	var queryParamsUpdate = {};
			  	var arryParlocations = [];
			  	
			  	if(parlocationsResponse instanceof Array){
				  arryParlocations = parlocationsResponse;
				}else{
				  arryParlocations.push(parlocationsResponse);  
				}


			  	console.log(object);

				angular.forEach(arryParlocations, function(location, key) {	
					
					var queryParamsUpdate = {};
		    		queryParamsUpdate.LocationID = location.attributes.locid;
					CRUD.update("Par_Location_Table","AdditionalQuantityMultiplier",parseInt(location.attributes.addqtymult),queryParamsUpdate); //Update Additional Quantity Multiplier

	  				deffered.resolve(saveParformItemsInsertSQL(location.item,location));
	  				promises.push(deffered.promise);
	  			});
		  		

				
				
				return $q.all(promises);
			}

			/* PRIVATE METHODS*/
			function saveParformItemsInsertSQL(items,location){

					if(typeof items === 'undefined' || typeof location === 'undefined'){
						return "";
					}

					var itemArray = [];

					if(items instanceof Array){
						itemArray = items;
					}else{
						itemArray.push(items);
					}
					var addedItems = LocalData.loadData("DownloadedItems"); // This is to avoid duplicate item insertion to Par_Item_Table

					var ItemQuery = "insert into Par_Item_Table (ItemID,ItemDesc,ItemSequence,LocationID,AverageParUtilization,GTIN,HIBC,QuantityOnOrder,ManufacturerCatalogueNumber,ItemNo,MinimumOrder,Parlevel,Shelf,SuggestedParlevel,UnitOfMeasure) " ;

					angular.forEach(itemArray, function(item,item_key){
						
						console.log("Item desc: "+item.attributes.itemdesc);
						if (addedItems.indexOf(checkValue(item.attributes.itemid)+"_"+checkValue(location.attributes.locid)) == -1 ){ // Check if already exists in DownloadedItems array (session storage)

							var _ItemID 				= checkValue(item.attributes.itemid),
								_ItemDesc				= checkValue(item.attributes.itemdesc,"text"),
								_ItemSequence			= checkValue(item.attributes.seq),
								_LocationID 			= checkValue(location.attributes.locid),
								_AverageParUtilization	= checkValue(item.attributes.apu),
								_GTIN 					= checkValue(item.attributes.gtin,"text"),
								_HIBC 					= checkValue(item.attributes.hibc,"text"),
								_QuantityOnOrder 		= checkValue(item.attributes.qoo,"text"),
								_ItemNo 				= checkValue(item.attributes.itemno,"text"),
								_ManufacturerCatalogueNo= checkValue(item.attributes.mfrcatno,"text"),
								_MinimumOrder			= checkValue(item.attributes.minorder),
								_Parlevel 				= checkValue(item.attributes.parlevel),
								_Shelf 					= checkValue(item.attributes.shelf,"text"),
								_SuggestedParlevel 		= checkValue(item.attributes.spl),
								_UnitOfMeasure			= checkValue(item.attributes.uom,"text");

								
								if(item_key < 1){

									ItemQuery += "select "+_ItemID+" as ItemID, ";
									ItemQuery += "'"+_ItemDesc+"' as ItemDesc, ";
									ItemQuery += _ItemSequence+" as ItemSequence, ";
									ItemQuery += _LocationID+" as LocationID, ";
									ItemQuery += "'"+_AverageParUtilization+"' as AverageParUtilization, ";
									ItemQuery += "'"+_GTIN+"' as GTIN, ";
									ItemQuery += "'"+_HIBC+"' as HIBC, ";
									ItemQuery += "'"+_QuantityOnOrder+"' as QuantityOnOrder, ";
									ItemQuery += "'"+_ManufacturerCatalogueNo+"' as ManufacturerCatalogueNumber, ";
									ItemQuery += "'"+_ItemNo+"' as ItemNo, ";
									ItemQuery += _MinimumOrder+" as MinimumOrder, ";
									ItemQuery += _Parlevel+" as Parlevel, ";
									ItemQuery += "'"+ _Shelf +"' as Shelf, ";
									ItemQuery += _SuggestedParlevel+" as SuggestedParlevel, ";
									ItemQuery += "'"+_UnitOfMeasure+"' as UnitOfMeasure ";
									
								}else{
									ItemQuery += "union all select "+_ItemID+", ";
				     				ItemQuery += "'"+_ItemDesc+"', ";
				     				ItemQuery += _ItemSequence+", ";
				     				ItemQuery += _LocationID+", ";
				     				ItemQuery += "'"+_AverageParUtilization+"', ";
									ItemQuery += "'"+_GTIN+"', ";
									ItemQuery += "'"+_HIBC+"', ";
									ItemQuery += "'"+_QuantityOnOrder+"', ";
									ItemQuery += "'"+_ManufacturerCatalogueNo+"', ";
									ItemQuery += "'"+_ItemNo+"', ";
									ItemQuery += _MinimumOrder+", ";
									ItemQuery += _Parlevel+", ";
									ItemQuery += "'"+ _Shelf +"', ";
									ItemQuery += _SuggestedParlevel+", ";
									ItemQuery += "'"+_UnitOfMeasure+"' ";

								}

								addedItems.push(_ItemID+"_"+_LocationID);
						}
						
						
					});
					
					LocalData.saveData("DownloadedItems",addedItems);

					CRUD.inlineQuery(ItemQuery)
			     	 .then(function(result) {
				     	 	console.log("successful Item insert "+ItemQuery);
				     	 	return result;
				     	},function errorCallback(response) {
				     		return response;
						   		 
					});
  					

			}
			function syncParCount(requestString){
				var args = {
					 			URL:    "ParformService",
					 			Action: "syncParCounts",
					 			Params: {"requestString":requestString,"token":requestToken}
					 		};

		 		return CM.doCommsCall("soap",args);
			}

			function saveTransactionHistory(locationID,ItemCount){

				var query = "insert into Par_TransHistory_Table (UserID,CountedParforms,CountedItems,TimeSpent) VALUES(";
					query += "'"+LocalData.loadSession("SoapUsername")+"',";
					query += locationID+",";
					query += ItemCount+",";
					query += "(SELECT ";
					query +="ROUND(cast( ";
					query +="( ";
					query +="strftime('%s',(SELECT TimeStamp from Par_Transaction_Table ORDER BY TimeStamp DESC LIMIT 1))";	
					query +="-strftime('%s',(SELECT TimeStamp from Par_Transaction_Table ORDER BY TimeStamp ASC LIMIT 1))";
					query +=") as real";
					query +=")/60,2) as TimeSpent "; // Devided by 60 to get Minutes
					query +="FROM Par_Transaction_Table WHERE LocationID = "+locationID;
					query +=" ORDER BY TimeStamp DESC LIMIT 1))";

				CRUD.inlineQuery(query).then(function(res){
					console.log("Success: "+res);
				},function(res){
					console.log("Error: "+res);
				});
			}
			function deleteCurrentParforms(){
				var query = "UPDATE Par_Location_Table SET IsSelected = 0, IsItemDownloaded = 0";

					return CRUD.inlineQuery(query)
					    .then(function(result) {
					    	CommonInfoService
				 			.clearTable('Par_Item_Table')
				 			.then(function(){
				 				CommonInfoService
					 			.clearTable('Par_Transaction_Table');
				 			});
				 	});
			}
			function onFocus(){
				$rootScope.isSavingInputs = true;
				$ionicSlideBoxDelegate.enableSlide(false);
			}
			function saveValues(item,input,type){
				if(input.target.value.length < 1){
					return;
				}

				var quantityValue;
				if(input.target.value.length < 1){
					quantityValue = 0;
				}else{
					quantityValue = parseInt(input.target.value);
				}

				var parLevel = parseInt(item.Parlevel);
				var minOrder = parseInt(item.MinimumOrder);
				var AddQtyMult = parseInt(item.AdditionalQuantityMultiplier);
				var AddQtyMultMaxCompare = parLevel * AddQtyMult;
				var minOrderCompare = parLevel - quantityValue;
//				
				if(type=="quantity"){ /* For Quantity Field */

					if(quantityValue > parLevel) { /*User tapped the Count field, entered value > par level and pressed previous or next, or anywhere within the screen.*/
							
							var confirmPopup  = $ionicPopup.confirm({
								cssClass: 'custom-popup',
								template: '<div>{{ "text_count_qty_warning" | translate }}</div>',
								cancelText: "No",
								okText: "Yes"
							});

							confirmPopup.then( function(res) {
							if(res == true) {
							/*
							Update Parlevel to new 
							*/
								input.target.value = parLevel;

								var query = "SELECT EXISTS(SELECT ItemID FROM Par_Transaction_Table WHERE ItemID="+item.ItemID+" AND LocationID = "+item.LocationID+" LIMIT 1) as Item";
									CRUD.inlineQuery(query)
										.then(function(result){
											if(result[0].Item < 1){
												var params = {
												 	"LocationID":parseInt(item.LocationID),
												 	"ItemID":parseInt(item.ItemID),
												 	"ItemQuantity": parLevel
												 };	
											     CRUD.insert("Par_Transaction_Table",params)
											     	 .then(function(){
											     	 	$rootScope.isSavingInputs = false;
											     	 	$ionicSlideBoxDelegate.enableSlide(true);
											     	 });
											}else{
												var params = {ItemID:item.ItemID};
							    				CRUD.update("Par_Transaction_table","ItemQuantity",quantityValue,params)
							    					.then(function(){
											     	 	$rootScope.isSavingInputs = false;
											     	 	$ionicSlideBoxDelegate.enableSlide(true);
											     	});
											}

										});

							} else {
								$rootScope.isSavingInputs = false;
								$ionicSlideBoxDelegate.enableSlide(true);
								input.target.value = "";
							}
						});
					} else if(minOrder > 0 && minOrder > minOrderCompare){/* Popup message display "This item only accept minimum order quantity of X. Set count to par level ?" If yes, set count to par level; otherwise, just dismiss popup. */		
						
						var confirmPopup  = $ionicPopup.confirm({
								cssClass: 'custom-popup',
								template: '<div>{{ "confirm_par_count_check_minimum_order" | translate }}</div>',
								cancelText: "No",
								okText: "Yes"
							});

							confirmPopup.then( function(res) {
							if(res == true) {
							/*
							Update Parlevel to new 
							*/
								input.target.value = parLevel;

								var query = "SELECT EXISTS(SELECT ItemID FROM Par_Transaction_Table WHERE ItemID="+item.ItemID+" AND LocationID = "+item.LocationID+" LIMIT 1) as Item";
									CRUD.inlineQuery(query)
										.then(function(result){
											if(result[0].Item < 1){
												var params = {
												 	"LocationID":parseInt(item.LocationID),
												 	"ItemID":parseInt(item.ItemID),
												 	"ItemQuantity": quantityValue
												 };	
											     CRUD.insert("Par_Transaction_Table",params)
											     	 .then(function(){
											     	 	$rootScope.isSavingInputs = false;
											     	 	$ionicSlideBoxDelegate.enableSlide(true);
											     	 });
											}else{
												var params = {ItemID:item.ItemID};
							    				CRUD.update("Par_Transaction_table","ItemQuantity",quantityValue,params)
							    					.then(function(){
											     	 	$rootScope.isSavingInputs = false;
											     	 	$ionicSlideBoxDelegate.enableSlide(true);
											     	 });
											}

										});

							} else {
								$rootScope.isSavingInputs = false;
								$ionicSlideBoxDelegate.enableSlide(true);
								input.target.value = "";
							}
						});

					}else{
						var query = "SELECT EXISTS(SELECT ItemID FROM Par_Transaction_Table WHERE ItemID="+item.ItemID+" AND LocationID = "+item.LocationID+" LIMIT 1) as Item";
							CRUD.inlineQuery(query)
								.then(function(result){
									if(result[0].Item < 1){
										var params = {
										 	"LocationID":parseInt(item.LocationID),
										 	"ItemID":parseInt(item.ItemID),
										 	"ItemQuantity": quantityValue
										 };	
									     CRUD.insert("Par_Transaction_Table",params)
									     	 .then(function(){
									     	 	$rootScope.isSavingInputs = false;
									     	 	$ionicSlideBoxDelegate.enableSlide(true);
									     	 });
									}else{
										var params = {ItemID:item.ItemID};
					    				CRUD.update("Par_Transaction_table","ItemQuantity",quantityValue,params)
					    					.then(function(){
									     	 	$rootScope.isSavingInputs = false;
									     	 	$ionicSlideBoxDelegate.enableSlide(true);
									     	 });
									}

								});
					}

					

				}else if(type=="add-quantity"){ /* For Additional Quantity Field */

					if(quantityValue > AddQtyMultMaxCompare){ /* User tapped the Add'l Qty field, entered value > (par level times the addl qty multiplier) and tapped previous or next, or anywhere within the screen. */
						var confirmPopup  = $ionicPopup.confirm({
								cssClass: 'custom-popup',
								template: '<div>{{ "confirm_par_count_check_max_additional_quantity" | translate }}</div>',
								cancelText: "No",
								okText: "Yes"
							});

							confirmPopup.then( function(res) {
							if(res == true) {
								
								input.target.value = AddQtyMultMaxCompare;

								var query = "SELECT EXISTS(SELECT ItemID FROM Par_Transaction_Table WHERE ItemID="+item.ItemID+" AND LocationID = "+item.LocationID+" LIMIT 1) as Item";
								CRUD.inlineQuery(query)
									.then(function(result){
										if(result[0].Item < 1){
											var params = {
											 	"LocationID":parseInt(item.LocationID),
											 	"ItemID":parseInt(item.ItemID),
											 	"AdditionalQuantity": AddQtyMultMaxCompare
											 };	
										     CRUD.insert("Par_Transaction_Table",params)
											     .then(function(){
										     	 	$rootScope.isSavingInputs = false;
										     	 	$ionicSlideBoxDelegate.enableSlide(true);
										     	 });
										}else{
											var params = {ItemID:item.ItemID};
						    				CRUD.update("Par_Transaction_table","AdditionalQuantity",quantityValue,params)
						    					.then(function(){
										     	 	$rootScope.isSavingInputs = false;
										     	 	$ionicSlideBoxDelegate.enableSlide(true);
										     	});
										}

									});	

							} else {
								$rootScope.isSavingInputs = false;
								$ionicSlideBoxDelegate.enableSlide(true);
								input.target.value = "";
							}
						});
					}else if(minOrder > quantityValue){ /* User tapped Add'l Qty field, entered value < minimum order quantity and tapped anywhere within the screen. */
						var confirmPopup  = $ionicPopup.confirm({
								cssClass: 'custom-popup',
								template: '<div>{{ "confirm_par_count_check_min_additional_quantity" | translate }}</div>',
								cancelText: "No",
								okText: "Yes"
							});

							confirmPopup.then( function(res) {
							if(res == true) {
								
								input.target.value = minOrder;

								var query = "SELECT EXISTS(SELECT ItemID FROM Par_Transaction_Table WHERE ItemID="+item.ItemID+" AND LocationID = "+item.LocationID+" LIMIT 1) as Item";
								CRUD.inlineQuery(query)
									.then(function(result){
										if(result[0].Item < 1){
											var params = {
											 	"LocationID":parseInt(item.LocationID),
											 	"ItemID":parseInt(item.ItemID),
											 	"AdditionalQuantity": minOrder
											 };	
										     CRUD.insert("Par_Transaction_Table",params)
											     .then(function(){
										     	 	$rootScope.isSavingInputs = false;
										     	 	$ionicSlideBoxDelegate.enableSlide(true);
										     	 });
										}else{
											var params = {ItemID:item.ItemID};
						    				CRUD.update("Par_Transaction_table","AdditionalQuantity",quantityValue,params)
						    					.then(function(){
										     	 	$rootScope.isSavingInputs = false;
										     	 	$ionicSlideBoxDelegate.enableSlide(true);
										     	 });
										}

									});	

							} else {
								$rootScope.isSavingInputs = false;
								input.target.value = "";
							}
						});	
					}else{
						var query = "SELECT EXISTS(SELECT ItemID FROM Par_Transaction_Table WHERE ItemID="+item.ItemID+" AND LocationID = "+item.LocationID+" LIMIT 1) as Item";
						CRUD.inlineQuery(query)
							.then(function(result){
								//alert(result[0].Item);
								if(result[0].Item < 1){
									var params = {
									 	"LocationID":parseInt(item.LocationID),
									 	"ItemID":parseInt(item.ItemID),
									 	"AdditionalQuantity": quantityValue
									 };	
								     CRUD.insert("Par_Transaction_Table",params)
								     	 .then(function(){
								     	 	$rootScope.isSavingInputs = false;
								     	 	$ionicSlideBoxDelegate.enableSlide(true);
								     	 });
								}else{
									var params = {ItemID:item.ItemID};
				    				CRUD.update("Par_Transaction_table","AdditionalQuantity",quantityValue,params)
				    					.then(function(){
								     	  $rootScope.isSavingInputs = false;
								     	  $ionicSlideBoxDelegate.enableSlide(true);
								     	});
								}

							});	
					}

					
				}else if(type=="new-par-level"){ /* For New Par Level field*/
					var query = "SELECT EXISTS(SELECT ItemID FROM Par_Transaction_Table WHERE ItemID="+item.ItemID+" AND LocationID = "+item.LocationID+" LIMIT 1) as Item";
					CRUD.inlineQuery(query)
						.then(function(result){
							//alert(result[0].Item);
							if(result[0].Item < 1){
								var params = {
								 	"LocationID":parseInt(item.LocationID),
								 	"ItemID":parseInt(item.ItemID),
								 	"NewParLevel": quantityValue
								 };	
							     CRUD.insert("Par_Transaction_Table",params)
								     .then(function(){
							     	 	$rootScope.isSavingInputs = false;
							     	 });
							}else{
								var params = {ItemID:item.ItemID};
			    				CRUD.update("Par_Transaction_table","AdditionalQuantity",quantityValue,params)
			    					.then(function(){
							     	  $rootScope.isSavingInputs = false;
							     	});
							}

						});
				}


				

				
			}
			function checkValue(val,type){

				if(type){
					return (val == undefined || val == "" ? "0" : val.replace(/["']/g, ""));
				}else{
					return (val == undefined || val == "" ? 0 : parseInt(val));
				}

			}

		}
})();

