(function(){
 	'use strict';

	angular
		.module("app", [
			'ionic',
			'ui.router',
			'InforCommsManager',
			'InforScannerManager',
			'InforSecurityModule',
			'InforCOMMSManager',
			'InforOfflineStorageManager',
			'InforGlobalization',
			'motionIconsDirectives',
			'ngCordova',
			'StorageManagerModule'
		])
		.constant('CONSTANT', {
	        SpinnerTemplate: 'html/templates/spinner.html'
	    });
	
})();

