(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('PickForParSettingsController', PickForParSettingsController);

		function PickForParSettingsController($state,
					  $scope,
					  LocalData){
					
			var vm = this;
			vm.headTitle = "lbl_pick_for_par_defaults";
			vm.page = "pickForPar";

		    $scope.$on('$ionicView.afterEnter', function() {
				vm.currentUser = LocalData.loadSession('currentUser');
				var users = JSON.parse(LocalData.loadData('users'));
				
		    	vm.defaultPrinter = users[vm.currentUser].defaultPickForParReportPrinter;	
			});



		}
				
		
})();
