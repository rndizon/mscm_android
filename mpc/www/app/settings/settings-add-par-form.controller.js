(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('SettingsAddParForm', Body);

		function Body($state,
					  $scope,
					  $ionicViewSwitcher,
					  $ionicPopover,
					  filterFilter,
					  LocalData){
					
			var vm = this;
			vm.goTo = goTo;
			vm.addParFormSettingsOption = addParFormSettingsOption;
			vm.selectAll = selectAll;
			vm.clearAll = clearAll;
			vm.showClearAll = showClearAll;
			vm.currentUser = LocalData.loadSession('currentUser');
			var users = JSON.parse(LocalData.loadData('users'));


			vm.localStorShowHelp = users[vm.currentUser].isShowHelp;


			/* Sample Data */

			vm.parCount = [
				{name: 'Parforms 1 ER 7369', loc: 'Tower A, Makati Med', val: false},
				{name: 'Parforms 2 ER 7369', loc: 'Tower A, Makati Med', val: false},
				{name: 'Parforms 3 ER 7369', loc: 'Tower A, Makati Med', val: false},
				{name: 'Parforms 4 ER 7369', loc: 'Tower A, Makati Med', val: false},
				{name: 'Parforms 5 ER 7369', loc: 'Tower A, Makati Med', val: false},
				{name: 'Parforms 6 ER 7369', loc: 'Tower A, Makati Med', val: false},
				{name: 'Parforms 7 ER 7369', loc: 'Tower A, Makati Med', val: false}
			]



	    	function goTo(goToUrl,_locationCode) {
				if(_locationCode){
					var params = {LocationCode:_locationCode};
					$state.go(goToUrl, params);
				}else{
					$state.go(goToUrl);
				}
			  
			};

	    	function addParFormSettingsOption($event) {
		    	$ionicPopover.fromTemplateUrl('html/templates/options-more.html', {
				        scope: $scope
				}).then(function(popover) {
				        vm.popover = popover;
				        vm.popUpType = 'add-par-form-settings';
				        popover.show($event)
				}); 
	    	};


	    	function selectAll() {
			    var parCountLen =  vm.parCount.length;

			    for(var x = 0; x < parCountLen; x++) {
			    	vm.parCount[x].val = true;
			    }
			};

			function clearAll() {
			    var parCountLen =  vm.parCount.length;

			    for(var x = 0; x < parCountLen; x++) {
			    	vm.parCount[x].val = false;
			    }

			}

			function showClearAll() {

			    if(filterFilter( vm.parCount, {val:true}).length == vm.parCount.length) {
			    	return true;
			    } else {
			    	return false;
			    }
			}
	    	
	    	


		}
				
		
})();
