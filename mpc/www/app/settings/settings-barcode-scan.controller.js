(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('SettingsBarcodeScanController', Body);

		function Body($state,
					  $scope,
					  LocalData)
		{
					
			var vm = this;
			vm.changeDefScanSound = changeDefScanSound;

			vm.currentUser = LocalData.loadSession('currentUser');
			var users = JSON.parse(LocalData.loadData('users'));
			var appObj = JSON.parse(LocalData.loadData('application'));


			vm.localStorShowHelp = users[vm.currentUser].isShowHelp;

			/*
				Default Barcode Scan Sound
			*/
			
			vm.defGoodScan = appObj.goodScan;
			vm.defFailScan = appObj.badScan;

			console.log(appObj);



			function changeDefScanSound() {

				appObj.goodScan = vm.defGoodScan;
				appObj.badScan = vm.defFailScan;

				LocalData.saveData('application',JSON.stringify(appObj));
			}


		}
				
		
})();
