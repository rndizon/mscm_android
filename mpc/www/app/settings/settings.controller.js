(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('SettingsController', Body);

		function Body($state,
					  $scope,
					  $ionicPopup,
					  LocalData){
					
			var vm = this;
			vm.resetDefaultsConfirmation = resetDefaultsConfirmation;
			vm.showHelpChange = showHelpChange;

			vm.currentUser = LocalData.loadSession('currentUser');
		    var usersObj = JSON.parse(LocalData.loadData('users'));
		    var appObj = JSON.parse(LocalData.loadData('application'));

			vm.localStorShowHelp = usersObj[vm.currentUser].isShowHelp;

			if(vm.localStorShowHelp == "1") {
				vm.modelShowHelp = true;
			} else {
				vm.modelShowHelp = false;
			}
			
			function showHelpChange(val) {
				if(val  == true) {
					usersObj[vm.currentUser].isShowHelp = 1;
				} else {
					usersObj[vm.currentUser].isShowHelp = 0;
				} 

				LocalData.saveData('users',JSON.stringify(usersObj));
			}


			function resetDefaultsConfirmation() {

				var confirmPopup  = $ionicPopup.confirm({
                    cssClass: 'custom-popup',
					template: 'Are you sure you want to reset to defaults?',
					title: '',
					scope: $scope,
       				cancelText: 'No',
        			okText: 'Reset'
				});

				confirmPopup.then( function(res) {


			 		usersObj[vm.currentUser] = { 
						lastLoggedIn: "",
						lastAccessLocation: "",
						defaultParCompanyID: "",
						defaultParCompanyDescription: "",
						defaultParLabelPrinter: "",
						defaultCycleCompanyID: "",
						defaultCycleCompanyDescription: "",
						defaultPickForParReportPrinter: "",
						isShowHelp: 1,
						lastParCount: "",
						lastParformsUsed: "",
						lastCycleCount: "",
						lastSelectIDUsed: ""
			 		}

			 		appObj = {
				        isKioskMode: "0",
				        kioskPasskey: "OBF:kajsdqweoi1923n1s",
				        goodScan: "0",
				        badScan: "1",
				        logLevel: "3",
				        maxLogFileSize: 2,
				        logFilePath: "/sdcard",
				        serverRequestTimeOut: "60000",
				    };

					vm.modelShowHelp = true;
				
	 				LocalData.saveData('application',JSON.stringify(appObj));
	 				LocalData.saveData('users',JSON.stringify(usersObj));
				});
			};


		}
				
		
})();
