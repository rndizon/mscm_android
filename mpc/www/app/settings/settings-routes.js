(function(){
 	'use strict';
 	
		angular
		    .module('app')
			.config(function($stateProvider, $urlRouterProvider) {
		     
		     $stateProvider.state("app.settings", {
			      url: "/settings",
			      changeColor: 'bar-slate-6',
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/settings/settings.html",
			      		controller: 'SettingsController as vm'
			      	}
			      }
			      
			      
		      })

		      .state("app.server-settings", {
			      url: "/server-settings", 
			      changeColor: 'bar-slate-6',
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/settings/server-settings.html",
			      		controller: 'ServerSettingsController as vm'
			      	}
			      }
			      
			      
		      })

		      .state("app.barcode-scan-settings", {
			      url: "/barcode-scan-settings", 
			      changeColor: 'bar-slate-6',
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/settings/barcode-scan-settings.html",
			      		controller: 'SettingsBarcodeScanController as vm'
			      	}
			      }
			      
			      
		      })

		      .state("app.settings-printer-list", {
			      url: "/settings-printer-list", 
			      changeColor: 'bar-slate-6',
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/settings/settings-printer-list.html",
			      		controller: "SettingsPrinterList as vm"
			      	}
			      }
			      
			      
		      })

		      .state("app.logging-settings", {
			      url: "/logging-settings", 
			      changeColor: 'bar-slate-6',
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/settings/logging-settings.html",
			      		controller: "LoggingSettingsController as vm"
			      	}
			      }
			      
			      
		      })

		      .state("app.par-count-settings", {
			      url: "/par-count-settings", 
			      changeColor: 'bar-slate-6',
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/settings/module-settings.html",
			      		controller: "ParCountSettingsController as vm"
			      	}
			      }
		      })

		      .state("app.settings-my-par-form-view", {
			      url: "/settings-my-par-form-view", 
			      changeColor: 'bar-slate-6',
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/settings/settings-my-par-form-view.html",
			      		controller: "SettingsMyParForm as vm"
			      	}
			      }
		      })

		      .state("settings-add-par-form-view", {
			      url: "/settings-add-par-form-view", 
			      changeColor: 'bar-slate-6',
		//	      views :{
		//	      	'menu-content':{
			      		templateUrl: "html/settings/settings-add-par-form-view.html",
			      		controller: "SettingsAddParForm as vm"
		//	      	}
		//	      }
		      })


		      .state("app.cycle-count-settings", {
			      url: "/cycle-count-settings", 
			      changeColor: 'bar-slate-6',
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/settings/module-settings.html",
			      		controller: "CycleCountSettingsController as vm"
			      	}
			      }
			      
			      
		      })

		      .state("app.pick-for-par-settings", {
			      url: "/pick-for-par-settings", 
			      changeColor: 'bar-slate-6',
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/settings/module-settings.html",
			      		controller: "PickForParSettingsController as vm"
			      	}
			      }
			      
			      
		      })

		      .state("app.about-mpc", {
			      url: "/about-mpc", 
			      changeColor: 'bar-slate-6',
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/settings/about-mpc.html"
			      	}
			      }
			      
			      
		      })
		      .state("app.settings-company-change-view", {
			     url: "/settings-company-change-view", 
			     changeColor: 'bar-slate-6',
	      		 views: {
	      		 	'menu-content': {
	      		 		templateUrl: "html/settings/settings-company-change-view.html",
	      				controller: "SettingsCompanyChangeController as vm"
	      		 	}
	      		 }
			      	
		      })
		      .state("settings-location-change-view", {
		      	url: "/settings-location-change-view",
			      changeColor: 'bar-slate-6',
		      	templateUrl: "html/settings/settings-location-change-view.html",
		      	controller: "PickForParController as vm"
		      });

		  //    $urlRouterProvider.otherwise('/app/welcome');
		  });
		
		
		
})();