(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('CycleCountSettingsController', CycleCountSettingsController);

		function CycleCountSettingsController($state,
					  $scope,
					  LocalData){
					
			var vm = this;
			vm.headTitle = "lbl_cycle_count_defaults";
			vm.page = "cycleCount";
			vm.futureRelease = false; // Hide Toggle Blind count and validate freeze qty


		    $scope.$on('$ionicView.afterEnter', function() {
		    	
				vm.currentUser = LocalData.loadSession('currentUser');
				var users = JSON.parse(LocalData.loadData('users'));
				vm.companyHeader = users[vm.currentUser].defaultCycleCompanyDescription;
				vm.companyHeaderID = users[vm.currentUser].defaultCycleCompanyID;
			});


		}
				
		
})();
