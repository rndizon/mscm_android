(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('SettingsPrinterList', Body);

		function Body($state,
					  $scope,
					  $ionicHistory,
					  LocalData,
					  $ionicLoading,
					  CONSTANT){
					
			var vm = this;
			vm.selectedItem = "No Item Selected";
			vm.headerTitle = "title_set_default_printer";
			vm.savePrinter = savePrinter;
			vm.defaultPrinter;
			vm.doRefresh = doRefresh;
			vm.currentUser = LocalData.loadSession('currentUser');
			var users = JSON.parse(LocalData.loadData('users'));
			
			vm.localStorShowHelp = users[vm.currentUser].isShowHelp;


			vm.printerList = [
				{ name: "Printer 1"},
				{ name: "Printer 2"},
				{ name: "Printer 3"},
			];

			vm.prevStateId = $ionicHistory.viewHistory().backView.stateId;

			if(vm.prevStateId == "app.par-count-settings") {
				vm.defaultPrinter = users[vm.currentUser].defaultParLabelPrinter;
			} else if(vm.prevStateId == "app.pick-for-par-settings") {
				vm.defaultPrinter = users[vm.currentUser].defaultPickForParReportPrinter;
			} else if(vm.prevStateId == "app.pick-for-par") {
				vm.defaultPrinter = LocalData.loadData("pickForParSelectedPrinter");
			}

			function savePrinter() {

				if(vm.prevStateId == "app.par-count-settings") {

					users[vm.currentUser].defaultParLabelPrinter = vm.defaultPrinter;
					LocalData.saveData("users",JSON.stringify(users));
				} else if(vm.prevStateId == "app.pick-for-par-settings") {

					users[vm.currentUser].defaultPickForParReportPrinter = vm.defaultPrinter;
					LocalData.saveData("users",JSON.stringify(users));
				}
				else if(vm.prevStateId == "app.pick-for-par") {

					vm.defaultPrinter = LocalData.saveData("pickForParSelectedPrinter",vm.defaultPrinter);
				}
				
				$state.go(vm.prevStateId);

			}


			function doRefresh(){

	    	  	//show loading icon
				$ionicLoading.show({
					templateUrl: CONSTANT.SpinnerTemplate
				});

				$scope.$broadcast('scroll.refreshComplete');
				$ionicLoading.hide();

	    	};

		}
				
		
})();
