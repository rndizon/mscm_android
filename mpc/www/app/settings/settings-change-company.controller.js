(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('SettingsCompanyChangeController', Body);

		function Body($state,
					  $scope,
					  $ionicLoading,
					  $ionicViewSwitcher,
					  $ionicModal, 
					  filterFilter,
					  $ionicHistory,
					  CRUD, 
					  DmeManager,
					  LocalData,
					  CONSTANT
					  ) 
		{
					
			var vm = this;
			vm.companyList = []; 
			vm.searchText = ""; //initialize
			vm.doRefresh = doRefresh;
			vm.currentUser = LocalData.loadSession('currentUser');
			var users = JSON.parse(LocalData.loadData('users'));

			if(LocalData.loadSession("PrevStateName") == "app.par-count-settings") {
				vm.settingPage = "Parcount"; 
				vm.companyTable = "Par_Company_Table";
			} else if (LocalData.loadSession("PrevStateName") == "app.cycle-count-settings") {
				vm.settingPage = "Cyclecount";
				vm.companyTable = "Cycle_Company_Table";
			} else {
				vm.settingPage = "PickForPar";
				vm.companyTable = "PFP_Company_Table";
			}

			     	
			$scope.$on('$ionicView.afterEnter', function() {
			    
			    if(vm.settingPage == "Parcount") {

					vm.defaultCompanyID = users[vm.currentUser].defaultParCompanyID;
					vm.defaultCompanyName = users[vm.currentUser].defaultParCompanyDescription;
			    } else if (vm.settingPage == "Cyclecount"){

					vm.defaultCompanyID = users[vm.currentUser].defaultCycleCompanyID;
					vm.defaultCompanyName = users[vm.currentUser].defaultCycleCompanyDescription;

			    }
			    
				vm.showCompanyList(vm.companyTable);
				
			});

			vm.goBack = function(){

					$ionicHistory.goBack();
			}

			vm.showCompanyList = function(tableName) {
				var companyParams = {};

				if (vm.searchText.trim()) {
					companyParams.CompanyID = vm.searchText.trim();
				}

				CRUD.select(tableName,"*",companyParams)
				 .then(function(result){
	 		 	 	console.log('Callback: select result:' + result[0].CompanyDesc);
	 		 	 	vm.companyList = result;
		 		});
				

			};

			vm.saveCompanySelected = function () {

				Object.keys(vm.companyList).forEach(function(idx){
					if (vm.companyList[idx].CompanyID == vm.defaultCompanyID) {

						if(vm.settingPage == "Parcount") {
							users[vm.currentUser].defaultParCompanyID = vm.defaultCompanyID;	
							users[vm.currentUser].defaultParCompanyDescription = vm.companyList[idx].CompanyDesc;	
						} else if (vm.settingPage == "Cyclecount") {
							users[vm.currentUser].defaultCycleCompanyID = vm.defaultCompanyID;	
							users[vm.currentUser].defaultCycleCompanyDescription = vm.companyList[idx].CompanyDesc;
						}
					}
				});


				LocalData.saveData("users", JSON.stringify(users));	
				$state.go(LocalData.loadSession("PrevStateName"));
				
			};
		     vm.clearSearch = function(){
				vm.searchText = '';
			}; 

			function doRefresh () {
	    	  	//show loading icon
				$ionicLoading.show({
					templateUrl: CONSTANT.SpinnerTemplate
				});

       			vm.showCompanyList();

				$scope.$broadcast('scroll.refreshComplete');
				$ionicLoading.hide();
			}

		}
				
		
})();
