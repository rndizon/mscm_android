(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('SettingsMyParForm', Body);

		function Body($state,
					  $scope,
					  $ionicViewSwitcher,
					  $ionicPopover,
					  filterFilter,
					  CommonInfoService,
					  LocalData){
					
			var vm = this;
			vm.showOptionToDelete = false;
			vm.OptionToDeleteIcons = "-blank";
			vm.addParForm = addParForm;
			vm.selectAll = selectAll;
			vm.clearAll = clearAll;
			vm.showClearAll = showClearAll;
			vm.showData = showData;
			vm.myParFormSettingsOption = myParFormSettingsOption;
			vm.showOptionToDelete = false;
			vm.selectedParCount = selectedParCount;
			vm.currentUser = LocalData.loadSession('currentUser');
			var users = JSON.parse(LocalData.loadData('users'));


			vm.localStorShowHelp = users[vm.currentUser].isShowHelp;

			vm.parCount = [
				{id: '12345', name:"Locations 1", val: false},
				{id: '23456', name:"Locations 2", val: false}
			]

			function showData() {  
				vm.selectedAllParforms  = filterFilter( vm.parCount, {val:true});
			    CommonInfoService.setSelectedParforms(vm.selectedAllParforms);	

			    alert(CommonInfoService.getSelectedParforms());

			    console.log(CommonInfoService.getSelectedParforms());
			}


			function addParForm() {
				$ionicViewSwitcher.nextDirection('slideUp');
				$state.go('settings-add-par-form-view');
			}

			function selectAll() {
			    var parCountLen =  vm.parCount.length;

			    for(var x = 0; x < parCountLen; x++) {
			    	vm.parCount[x].val = true;
			    }
			}


			function clearAll() {
			    var parCountLen =  vm.parCount.length;

			    for(var x = 0; x < parCountLen; x++) {
			    	vm.parCount[x].val = false;
			    }
			}

			function showClearAll() {

			    if(filterFilter( vm.parCount, {val:true}).length == vm.parCount.length) {
			    	return true;
			    } else {
			    	return false;
			    }
			}

			function selectedParCount() {
				console.log(filterFilter( vm.parCount, {val:true}).length);
				return filterFilter( vm.parCount, {val:true}).length;
			}


	    	function myParFormSettingsOption($event,popUpType) {
		    	$ionicPopover.fromTemplateUrl('html/templates/options-more.html', {
				        scope: $scope
				}).then(function(popover) {
				        vm.popover = popover;
				        vm.popUpType = (popUpType) ? popUpType : 'my-par-form-settings';
				        popover.show($event)
				}); 
	    	}



		}
		
})();
