(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('ServerSettingsController', Body);

		function Body($state,
					  $scope,
					  LocalData){
					
			var vm = this;
			vm.futureRelease = false; // Hide Kiosk mode
			vm.saveTimeDuration = saveTimeDuration;

			var appObj = JSON.parse(LocalData.loadData('application'));

		    $scope.$on('$ionicView.afterEnter', function() {

				vm.timeDuration = appObj.serverRequestTimeOut;
		    });	

		    function saveTimeDuration() {
		    	appObj.serverRequestTimeOut = vm.timeDuration
		    	LocalData.saveData("application",JSON.stringify(appObj));
		    }


		}
				
		
})();
