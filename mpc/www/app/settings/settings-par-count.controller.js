(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('ParCountSettingsController', ParCountSettingsController);

		function ParCountSettingsController($state,
					  $scope,
					  LocalData){
					
			var vm = this;
			vm.headTitle = "lbl_par_count_defaults";
			vm.page = "parCount";
			vm.futureRelease = false; // Hide my parforms and par item defauls

		    $scope.$on('$ionicView.afterEnter', function() {
				vm.currentUser = LocalData.loadSession('currentUser');
				var users = JSON.parse(LocalData.loadData('users'));
				
				vm.companyHeader = users[vm.currentUser].defaultParCompanyDescription;
				vm.companyHeaderID = users[vm.currentUser].defaultParCompanyID;
		    	vm.defaultPrinter = users[vm.currentUser].defaultParLabelPrinter;		   

		    });

		    ;



		}
				
		
})();
