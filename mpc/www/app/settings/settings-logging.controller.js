(function(){
 	'use strict';
 	
		angular 
		    .module('app')
		    .controller('LoggingSettingsController', Body);

		function Body($state,
					  $scope,
                      $ionicPopover,
					  LocalData){
					
			var vm = this;
            vm.optionMore = optionMore;
			vm.currentUser = LocalData.loadSession('currentUser');
			var users = JSON.parse(LocalData.loadData('users'));
			var appObj = JSON.parse(LocalData.loadData('application'));
			vm.localStorShowHelp = users[vm.currentUser].isShowHelp;

			vm.saveFilePath = saveFilePath;
			vm.saveMaxLogSize = saveMaxLogSize;
			vm.savelogLevel = savelogLevel;


			vm.maxLogSize = appObj.maxLogFileSize;
			vm.logFilePath = appObj.logFilePath;
			vm.logLevel = appObj.logLevel;

			$scope.$watch('vm.maxLogSize',function(newValue,oldValue){
				if(newValue > 2048) {
					vm.maxLogSize = oldValue;
				}
			});

			function savelogLevel() {
				appObj.logLevel = vm.logLevel;
 				LocalData.saveData('application',JSON.stringify(appObj));
			}

			function saveMaxLogSize() {
				appObj.maxLogFileSize = vm.maxLogSize;
				LocalData.saveData('application',JSON.stringify(appObj));
			}

			function saveFilePath() {
				appObj.logFilePath = vm.logFilePath;
	 			LocalData.saveData('application',JSON.stringify(appObj));
			}


            
            function optionMore($event,popupType) {

				$ionicPopover.fromTemplateUrl('html/templates/options-more.html', {
				        scope: $scope
				}).then(function(popover) {
				        vm.popover = popover;
				        vm.popUpType = (popupType) ? popupType : "logging-setting";
				        popover.show($event);
				});
			}
            
		}
				
		
})();
