(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('cycleCountDownload', Body);

		function Body(
				$state,
				$scope,
				filterFilter,
				$ionicPopover,
				$ionicViewSwitcher,
				LocalData
				) {
			
			var vm = this;
			vm.downloadedCycleCountOption = downloadedCycleCountOption;
			vm.companyHeader = LocalData.loadData("CycleCompanyName");
			vm.companyHeaderID = LocalData.loadData("CycleCompanyID");
			vm.currentUser = LocalData.loadSession('currentUser');
			var users = JSON.parse(LocalData.loadData('users'));
			vm.localStorShowHelp = users[vm.currentUser].isShowHelp;


	    	function downloadedCycleCountOption($event) {
		    	$ionicPopover.fromTemplateUrl('html/templates/options-more.html', {
				        scope: $scope
				}).then(function(popover) {
				        vm.popover = popover;
				        vm.popUpType = 'edit';
				        popover.show($event)
				}); 
	    	}

	    	vm.goTo = function(url) {
			  $state.go(url);
			}
		}
		
		
		
})();
