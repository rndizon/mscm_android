(function(){
 	'use strict';
 	
		angular
		    .module('app')
			.config(function($stateProvider, $urlRouterProvider) {
		     
		     $stateProvider
		      .state("app.cycle-count", {
		      	cache: false,
			    url: "/cycle-count",
                changeColor: 'bar-amethyst-8', 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/cycle_counting/cycle-count.html",
					      controller: "CycleCountController as vm"
					 }
				  }
			      
		      })

		      .state("cycle-count-add-parform", {
		      	url: "/cycle-count-add-parform",
		      	cache: false,
                changeColor: 'bar-amethyst-8',
  				templateUrl: "html/cycle_counting/cycle-count-add-parform.html",
  				controller: "CycleCountAddSelectIds as vm"
		      })

		      .state("app.downloaded-cycle-count", {
		      	url: "/downloaded-cycle-count",
                changeColor: 'bar-amethyst-8',
                cache: false,
		      		views: {
		      			'menu-content': {
		      				templateUrl: "html/cycle_counting/downloaded-cycle-count.html",
		      				controller: "cycleCountDownload as vm"
		      			}
		      		}
		      })

		      .state("app.downloaded-cycle-count-details", {
		      	url: "/downloaded-cycle-count-details/:CountFormID/:SelectID/:LocationCodeList",
		      	cache: false,
                changeColor: 'bar-amethyst-8',
		      		views: {
		      			'menu-content': {
		      				templateUrl: "html/cycle_counting/downloaded-cycle-count-details.html",
		      				controller: "CycleCountItemDetailsController as vm"
		      			}
		      		}
		      })

		      .state("app.downloaded-cycle-count-details-list", {
		      	url: "/downloaded-cycle-count-details-list/:CountFormID/:SelectID/:LocationCodeList",
		      	cache: false,
                changeColor: 'bar-amethyst-8',
		      		views: {
		      			'menu-content': {
		      				templateUrl: "html/cycle_counting/downloaded-cycle-count-details-list.html",
		      				controller: "CycleCountItemDetailListController as vm"
		      			}
		      		}
		      });

		     });
		     
})();