(function(){
	'use strict';
	
	angular
		.module("app")
		.controller("CycleCountItemDetailListController", Body);
	
	function Body($state,$q,
				$ionicViewSwitcher,
				$ionicPopover,
				$scope,
				$stateParams,
				CRUD,
				LocalData,
				CycleCountService, 
				$ionicPopup,
				$ionicPlatform){
		
			var vm = this;
			vm.cycleCount = CycleCountService.currentCycleCount;
			vm.CountFormID = $stateParams.CountFormID;
			vm.goTo = goTo;
			vm.currentUser = LocalData.loadSession('currentUser');
			var users = JSON.parse(LocalData.loadData('users'));
			vm.localStorShowHelp = users[vm.currentUser].isShowHelp;
			vm.TotalItemCounted = 0;
			vm.isBlindCount = (users[vm.currentUser].cycleCountConfiguration.isBlindCount == 1) ? true : false;
			vm.isValidateFreeze = (users[vm.currentUser].cycleCountConfiguration.isValidateFreeze == 1) ? true : false;
			vm.saveValues = CycleCountService.saveValues;
			vm.highlightField = highlightField;
			vm.setMaxDecimal = setMaxDecimal;
			vm.itemHasCount = itemHasCount;
			vm.totalCounted = 0;
			//vm.checkItemType = checkItemType;
			vm.SelectID = $stateParams.SelectID;
			vm.LocationCodeList = $stateParams.LocationCodeList;
			vm.CheckForDirtyItemCounts = CheckForDirtyItemCounts;
			vm.clickedDoneBtn = clickedDoneBtn;
			vm.checkValidInput = CycleCountService.checkValidInput;
			vm.backBtn = backBtn;


			$scope.$on('$ionicView.afterEnter', function() {

				/*var query = "select *,cycleItem.CountFormID as CountFormID from Cycle_Item_Table as cycleItem left join Cycle_Transaction_Table as trans ";
		 			query +="using(CountFormItemID) where cycleItem.CountFormId = "+vm.CountFormID+" GROUP BY cycleItem.CountFormItemID";

				CRUD.inlineQuery(query)
				    .then(function(result) {
						
						vm.cycleCount = result;
					});*/

					vm.itemHasCount();
		    	
		    });

		    $scope.$on('$ionicView.beforeLeave', function() {
				/*var quantityFields = document.getElementsByName("quantity-from-list");
				
				var promises = [];

				for(var i = 0; i < quantityFields.length;i++){
				   if(quantityFields[i].value.trim().length > 0){
						vm.TotalItemCounted++;
				   		promises.push(vm.TotalItemCounted);
				   		
				   }
				   
				}
				$q.all(promises).then(function() {
				    
				    var query = "UPDATE Cycle_Count_Form_Table SET TotalItemCounted = "+vm.TotalItemCounted+" WHERE CountFormID='"+vm.CountFormID+"'";
					CRUD.inlineQuery(query);



				});*/
				
			});
			
			function itemHasCount() {
					var query = "select *,cycleItem.CountFormID as CountFormID from Cycle_Item_Table as cycleItem left join Cycle_Transaction_Table as trans ";
		 			query +="using(CountFormItemID) where cycleItem.CountFormId = "+vm.CountFormID+" and trans.ItemQuantity > '0' GROUP BY cycleItem.CountFormItemID";

				CRUD.inlineQuery(query)
				    .then(function(result) {
						vm.totalCounted =  result.length;
					});
		    	
			}

	      	//navigate to Other Page
			function goTo(goToUrl,_CountFormID,_SelectID,_LocationCodeList) {
				CycleCountService.currentCycleCount = vm.cycleCount;
			  	if(_CountFormID && _CountFormID.length > 0){
			 		var params = {CountFormID:_CountFormID,SelectID:_SelectID,LocationCodeList:_LocationCodeList};
			 		$state.go(goToUrl,params);
			 	}else{
					$ionicViewSwitcher.nextDirection('slideDown');
					$state.go(goToUrl);	
			 	}
			}

			function highlightField($event) {
		    	$event.target.select();
		    }

			vm.clearSearch = function(){
				vm.searchText = '';
			};

			function setMaxDecimal(currentModel,currslideModel) {
				vm.pulledQty[currslideModel] = parseFloat(toFixed(currentModel,4));
			}   	

			function toFixed(value, precision) {
			    var precision = precision || 0,
			        power = Math.pow(10, precision),
			        absValue = Math.abs(Math.round(value * power)),
			        result = (value < 0 ? '-' : '') + String(Math.floor(absValue / power));

			    if (precision > 0) {
			        var fraction = String(absValue % power),
			            padding = new Array(Math.max(precision - fraction.length, 0) + 1).join('0');
			        result += '.' + padding + fraction;
			    }
			    return result;
			}
			
			
			/*function checkItemType(item, elementValue){
				var value = parseInt(elementValue);
				if(item.SerialNumber !== "" && item.SerialNumber !== null && !(value == 0 || value == 1)){
					
					vm.popUpMsg = "cycle-count-invalid-serial-input"
					var confirmPopup = $ionicPopup.alert({
							cssClass: 'custom-popup',
							templateUrl: 'html/templates/pop-up.html',
							scope: $scope
					});
				}
				
			}*/

			function CheckForDirtyItemCounts() {
				if(CycleCountService.isItemsDirty) {
				    vm.popUpMsg = "cycle-count-close";
					$ionicPopup.confirm({
						cssClass: 'custom-popup',
						templateUrl: "html/templates/pop-up.html",
	        			okText: "Discard",
	       				cancelText: "No",
						scope: $scope
					}).then( function(res) {
						if(!res) {
							clickedDoneBtn();
						} else {
							vm.goTo('app.cycle-count');
						}
					});
				} else {
					vm.goTo('app.cycle-count');
				}

			}

			function clickedDoneBtn() {
				CycleCountService.saveAllValues(vm.cycleCount, function () {
					vm.goTo('app.cycle-count');
				});				
			}

	    	var backBtn = $ionicPlatform.registerBackButtonAction(function(event) {
			    CheckForDirtyItemCounts();
			}, 150);  

   			$scope.$on('$destroy', backBtn);


	}
	
	
	
	
	
	
})();