(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('CycleCountAddSelectIds', Body);

		function Body($q,
				$state,$scope,
				filterFilter,
				$ionicPopover,
				$ionicViewSwitcher,
				LocalData,
				CRUD,
				CommonInfoService,
				$ionicPlatform) {
			
			var vm = this;
			vm.cycleCountOption = cycleCountOption;
			vm.goTo = goTo;
			vm.goBackToCycleCount = goBackToCycleCount;
			vm.companyHeader = LocalData.loadData("CycleCompanyName");
			vm.companyHeaderID = LocalData.loadData("CycleCompanyID");
			vm.selectAll = selectAll;
			vm.clearAll = clearAll;
			vm.showClearAll = showClearAll;
			vm.optionMore = optionMore;
			vm.getFormList = getFormList;
			vm.selectIds = [];
			vm.currentUser = LocalData.loadSession('currentUser');
			var users = JSON.parse(LocalData.loadData('users'));
			vm.localStorShowHelp = users[vm.currentUser].isShowHelp;
			vm.selectedIDs = selectedIDs;

	    	var backBtn = $ionicPlatform.registerBackButtonAction(function(event) {
				    if (true) { // your check here
						    document.getElementById('backBtn').click();
				    }
				  }, 150);  

   			$scope.$on('$destroy', backBtn);


			$scope.$on('$ionicView.beforeEnter', function() {
				vm.getFormList();
			});


			function getFormList() {
				var queryParams = {};
				queryParams.IsSelected = 0;
				queryParams.CompanyID = vm.companyHeaderID;
				CRUD.select("Cycle_Count_Form_Table","*",queryParams)
				.then(function(result){
					console.log(result);
					vm.selectIds = result;
				});

			};

			function goTo(url) {
			  $state.go(url);
			}

			function goBackToCycleCount() {
				$ionicViewSwitcher.nextDirection('slideDown');
				$state.go("app.cycle-count");
			}


	    	function cycleCountOption($event) {
		    	$ionicPopover.fromTemplateUrl('html/templates/options-more.html', {
				        scope: $scope
				}).then(function(popover) {
				        vm.popover = popover;
				        vm.popUpType = 'cycle-count';
				        popover.show($event)
				}); 
	    	}

	    	function optionMore($event,popupType) {
		    	$ionicPopover.fromTemplateUrl('html/templates/options-more.html', {
				        scope: $scope
				}).then(function(popover) {
				        vm.popover = popover;
				        vm.popUpType = (popupType) ? popupType : "parcount-edit";
				        popover.show($event);
				});
			}

			function selectAll() {
			    var parformLen =  vm.selectIds.length;

			    for(var x = 0; x < parformLen; x++) {
			    	vm.selectIds[x].val = true;
			    }

			    vm.checkboxClicked();

			}
			vm.checkboxClicked = function() {

			    CommonInfoService.setSelectedParforms(filterFilter( vm.selectIds, {val:true}));	
			};	    	

			function selectedIDs(){
				if(filterFilter( vm.selectIds, {val:true})) {
		    		return filterFilter( vm.selectIds, {val:true}).length;
				}
				else {
					return 0;
				}
		    };


			function clearAll() {
			    var parformLen =  vm.selectIds.length;

			    for(var x = 0; x < parformLen; x++) {
			    	vm.selectIds[x].val = false;
			    }
			}

			function showClearAll() {

			    if(filterFilter( vm.selectIds, {val:true}).length == vm.selectIds.length) {
			    	return true;
			    } else {
			    	return false;
			    }
			}


	    	function closePopOver() {
	    		vm.popover.hide();
	    	};
	    	vm.clearSearch = function(){
				vm.searchText = '';
			};

			vm.addSelectId = function(){
	    		var SelectedIds = CommonInfoService.getSelectedParforms();

	    		var promises = [];
		    	var deffered = $q.defer();

    			Object.keys(SelectedIds).forEach(function(index) {

    				var queryParams = {};
			    	queryParams.CountFormID = SelectedIds[index].CountFormID;
    				CRUD.update("Cycle_Count_Form_Table","IsSelected",1,queryParams)
    					.then(function(result){
    						deffered.resolve(result);
    						promises.push(deffered.promise);
    					});

					
				});

				$q.all(promises)
				  .then(function(){
				  		vm.goTo("app.cycle-count","slideDown");
				 });

	    	};
		}
		
		
		
})();
