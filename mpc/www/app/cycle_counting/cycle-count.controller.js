(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('CycleCountController', Body);

		function Body(
			$state,
			$scope,
			filterFilter,
			$ionicPopover,
			$ionicViewSwitcher,
			$cordovaToast,
			$ionicLoading,
			LocalData,
			CycleCountService,
			CommonInfoService,
			CRUD,
			CONSTANT,
			$ionicPlatform,
			$ionicPopup,
			$timeout, 
			CI,
			Popup) {
			
			var vm = this;
			vm.selectAllCheckbox = selectAllCheckbox;
			vm.selectedAllParforms = [];
			vm.cycleCount = [];
			vm.unSelectAllCheckbox = unSelectAllCheckbox;
			vm.cycleCountOption = cycleCountOption;
			vm.closePopOver = closePopOver;
			vm.optionMore = optionMore;
			vm.goTo = goTo;
			vm.goBackToPickforPar = goBackToPickforPar;
			vm.goToAddParCount = goToAddParCount;
			vm.getCycleSelectIds = getCycleSelectIds;
			vm.currentUser = LocalData.loadSession('currentUser');
			vm.users = JSON.parse(LocalData.loadData('users'));
			vm.localStorShowHelp = vm.users[vm.currentUser].isShowHelp;
			vm.ifCheckboxTrue = 0;
		    vm.showHideDeleteButtons = false;
		    vm.showOptionToDelete = false;
		    vm.selectAll = selectAll;
		    vm.clearAll = clearAll;
		    vm.showClearAll = showClearAll;
		    vm.doRefresh = doRefresh;
		    vm.firstEnter = LocalData.loadSession('firstEnterCycleCount');
			vm.companyHeader = LocalData.loadData("CycleCompanyName");
			vm.companyHeaderID = LocalData.loadData("CycleCompanyID");	    	
			vm.popupDisplayed = false;
			vm.checkIfSelectIDIsAvailable = checkIfSelectIDIsAvailable;
			vm.isDisabled = false;
			
		    var backBtn = $ionicPlatform.registerBackButtonAction(function(event) {
				    if (true) { // your check here
				    	if(vm.showOptionToDelete == false) {
							$ionicPopup.confirm({
								template: 'Are you sure you want to sign out ?'
							}).then(function(res) {
								if (res) {
									ionic.Platform.exitApp();
								}
							});
				    	} else {
						    document.getElementById('closeOption').click();
				    	}
				    }
				  }, 150);  

   			$scope.$on('$destroy', backBtn);

			$scope.$on('$ionicView.beforeEnter', function() {
				CycleCountService.currentCycleCount = [];
				
				if(!LocalData.loadSession("DownloadedCycleItems")){ 
		    		LocalData.saveSession("DownloadedCycleItems",[]); //This session storage is for checking items if it is already exists in Cycle_Item_Table
		    	}
		    	if(!LocalData.loadSession("DownloadedCycleLocations")){ 
		    		LocalData.saveSession("DownloadedCycleLocations",[]); //This session storage is for checking items if it is already exists in Par_Location_Table
		    	}

				var SQLiteFlag = LocalData.loadData("SQLiteCycleLoaded");

		     	
		     	if(!SQLiteFlag) {
		     		//show loading icon
					$ionicLoading.show({
						templateUrl: CONSTANT.SpinnerTemplate
					});
		     		vm.getHeadersCall();
				} else {
					/*if(!CI.connectionAvailable()){
						showPopup({
							message : 'cycle-count-entry-no-internet-connection',
							type : 'confirm',
							scope : $scope,
							cancelText : 'No',
							okText : 'Yes',
							callbackForYes : function y(){
								vm.getCompanyList();
							},
							callbackForNo : function n(){
								showPopup(getOptionsForNoInternetUponLoad());
							}
						});
					} else {*/
						vm.getCompanyList();
					//}
	     			
				}
				
			});

			$scope.$on('$ionicView.afterEnter',function(){
				vm.isDisabled = false;
				if(LocalData.loadSession('firstEnterCycleCount') == "1") {
					$timeout(function(){
						goToAddParCount();
					},1500);
				}
				getCycleSelectIds();
			});
			
		    $scope.$on('$ionicView.beforeLeave',function() {
		    	LocalData.saveSession('firstEnterCycleCount','0');
		    });

			function doRefresh(){
				vm.isDisabled = true;
				console.log('cycle-count.controller --- doRefresh');
				
				
				/*if(!CI.connectionAvailable()){
					vm.popUpMsg = 'cycle-count-refresh-no-internet';
					Popup.displayPopup({
						scope: $scope,
						alertCallback : function a(){
							$scope.$broadcast('scroll.refreshComplete');
							console.log('cycle-count.controller --- doRefresh - no internet connection');
						}
					});
				} else {*/
					vm.popUpMsg = "cycle-count-refresh";
					var confirmPopup  = $ionicPopup.confirm({
						cssClass: 'custom-popup',
						templateUrl: "html/templates/pop-up.html",
						scope: $scope,
						cancelText: "No",
						okText: "Yes"
					});

					console.log('cycle-count.controller --- doRefresh 1');

					confirmPopup.then( function(res) {
						if(res == true) {
							//show loading icon
							$ionicLoading.show({
								templateUrl: CONSTANT.SpinnerTemplate
							});

							/*
								Ask Ryan about the correct implementation
							*/
							// vm.getCycleSelectIds();
							vm.getHeadersCall();

							$scope.$broadcast('scroll.refreshComplete');
							$ionicLoading.hide();
						}
					});
				//}
				

				
	    	};

			
			vm.getCompanyList = function () {
				$timeout(function(){
					vm.checkIfSelectIDIsAvailable();
				},1000);
				
		     	// CRUD.select("Cycle_Company_Table","*")
					// .then(function(result){
						 // console.log('Callback: select result:' + result[0].CompanyDesc);
						
						//vm.companyList = result;
						
						if(vm.firstEnter == "1"){
							vm.companyHeader = (vm.users[vm.currentUser].defaultCycleCompanyDescription ? vm.users[vm.currentUser].defaultCycleCompanyDescription : LocalData.loadData('CycleCompanyName'));
							vm.companyHeaderID = (vm.users[vm.currentUser].defaultCycleCompanyID ? vm.users[vm.currentUser].defaultCycleCompanyID : LocalData.loadData('CycleCompanyID'));
							LocalData.saveData("CycleCompanyID", vm.companyHeaderID);
							LocalData.saveData("CycleCompanyName",vm.companyHeader);
						} else {
							vm.companyHeader = LocalData.loadData("CycleCompanyName");
							vm.companyHeaderID = LocalData.loadData("CycleCompanyID");
						}
						vm.getCycleSelectIds();	
									 
				// });
		     
		     };


			function getCycleSelectIds() {
				
				var query = "SELECT *,";
					query +="(select count(*) from Cycle_Count_Form_Table left join Cycle_Item_Table USING(CountFormID) ";
					query +="where Cycle_Item_Table.CountFormID = cycleCount.CountFormID) as ItemCount,";
					query +="(select COUNT(*) from Cycle_Count_Form_Table left join Cycle_Transaction_Table USING(CountFormID) ";
					query +="where Cycle_Transaction_Table.CountFormID = cycleCount.CountFormID) as TotalItemCounted	";	
		 			query +="FROM Cycle_Count_Form_Table as cycleCount ";
		 			query +="WHERE cycleCount.IsSelected = 1 AND cycleCount.CompanyID="+ vm.companyHeaderID;

				CRUD.inlineQuery(query)
				    .then(function(result) {
				    	$ionicLoading.hide();
						vm.cycleCount = result;
						vm.showHideSendButton(result);

						
						for(var i = 0;i < vm.cycleCount.length; i++){
							if(parseInt(vm.cycleCount[i].IsItemDownloaded) < 1){
								var curIndex = i;
								var countFormID = vm.cycleCount[curIndex].CountFormID;
								var requestString = "<get_cycle_details>";
									requestString += '<countform countformid="'+ countFormID +'"/>';
									requestString += "</get_cycle_details>";

								CycleCountService
					     		.getCountFormDetails(requestString)
					     		.then(function(response) {
					     			var queryParamsUpdate = {};
                                      queryParamsUpdate.CountFormID = countFormID;
                                    CRUD.update("Cycle_Count_Form_Table","IsItemDownloaded",1,queryParamsUpdate);

					     			CycleCountService
					     			.saveCountFormDetails(response);
					     			
								});
					     	}
				     	}

					});

			}

		     vm.showHideSendButton = function(result) {
		     	var stopLoop = false;
		     	result.forEach(function(index) {
		    		
		    		if(!stopLoop){
		    			if(index.TotalItemCounted <= index.ItemCount && index.TotalItemCounted > 0) {
			     			vm.showSendButton = true;
			     			stopLoop = true;
			     		} else {
			     			vm.showSendButton = false;
			     		}
		    		}
		     		
		     	});
		     };
		     vm.getHeadersCall = function() {
		    	 	
		     		CycleCountService
		     		.getCycleHeaders()
		     		.then(function(response) {
		     			if(response.length == undefined){
							CycleCountService
								.saveCycleHeaders(response)
								.then(function(){
									vm.getCompanyList();
								});
						} else {
							showPopup(getOptionsForNoInternetUponLoad());
							$ionicLoading.hide();
						}
						
		     			
		     				
		     			
					});

				
		     };
			 
			function getOptionsForNoInternetUponLoad(){
				return  {
							message : 'cycle-count-connect-to-server-failed',
							type : 'confirm',
							scope : $scope,
							cancelText : 'Try Again',
							okText : 'Sign Out',
							callbackForYes : function y(){
								ionic.Platform.exitApp();
							},
							callbackForNo : function n(){
								if($state.current.name == 'app.cycle-count'){
									$state.go($state.current, {}, {reload: true});
								} else {
									$state.go('app.cycle-count');
								}
							}
						};
			}
			
			function showPopup(options){
				var _options = {
					type : options.type,
					scope : $scope,
					cancelText : options.cancelText,
					okText : options.okText,
					callbackForYes : options.callbackForYes,
					callbackForNo : options.callbackForNo
				};
				
				vm.popUpMsg = options.message;
				Popup.displayPopup(_options);
			 }

			function goTo(url,_CountFormID,_SelectID,_LocationCodeList) {
				if(_CountFormID || _CountFormID != ""){
					var params = {CountFormID:_CountFormID,SelectID:_SelectID,LocationCodeList:_LocationCodeList};
					$ionicViewSwitcher.nextDirection('slideUp');
					$state.go(url, params);
				} else {
			  		$state.go(url);
				}
			}

			function goBackToPickforPar() {
				$ionicViewSwitcher.nextDirection('slideDown');
				$state.go("app.pick-for-par");
			}

			function goToAddParCount() {
		    	LocalData.saveSession('firstEnterCycleCount','0');
				$ionicViewSwitcher.nextDirection('slideUp');
				$state.go("cycle-count-add-parform");
			}

			function selectAllCheckbox() {
			    var cycleCountLen =  vm.cycleCount.length;

			    for(var x = 0; x < cycleCountLen; x++) {
			    	vm.cycleCount[x].val = true;
			    }

			    vm.popover.hide();
			    vm.checkboxClicked(); // Show subheader 
			}

			function unSelectAllCheckbox() {
			    var cycleCountLen =  vm.cycleCount.length;

			    for(var x = 0; x < cycleCountLen; x++) {
			    	vm.cycleCount[x].val = false;
			    }

				vm.checkboxClicked(); // Hide subheader 

	    	}
	    	vm.checkboxClicked = function() { 
			    vm.showHideDeleteButtons = false;
			    vm.showOptionToDelete = false;
			    vm.selectedAllParforms  = filterFilter( vm.cycleCount, {val:true});
			    CommonInfoService.setSelectedParforms(vm.selectedAllParforms);
			    	
			};
		    vm.selectedParCount = function(){
		    	return filterFilter( vm.cycleCount, {val:true}).length;
		    };

			function selectAll() {
			    var parCountLen =  vm.cycleCount.length;

			    for(var x = 0; x < parCountLen; x++) {
			    	vm.cycleCount[x].val = true;
			    }
			}

			function clearAll() {
			    var parCountLen =  vm.cycleCount.length;

			    for(var x = 0; x < parCountLen; x++) {
			    	vm.cycleCount[x].val = false;
			    }
			}

			function showClearAll() {

			    if(filterFilter( vm.cycleCount, {val:true}).length == vm.cycleCount.length) {
			    	return true;
			    } else {
			    	return false;
			    }
			}
	    	function cycleCountOption($event) {
		    	$ionicPopover.fromTemplateUrl('html/templates/options-more.html', {
				        scope: $scope
				}).then(function(popover) {
				        vm.popover = popover;
				        vm.popUpType = 'cycle-count';
				        popover.show($event);
				}); 
	    	}
	    	function optionMore($event,popupType) {
		    	$ionicPopover.fromTemplateUrl('html/templates/options-more.html', {
				        scope: $scope
				}).then(function(popover) {
				        vm.popover = popover;
				        vm.popUpType = (popupType) ? popupType : "parcount-edit";
				        popover.show($event);
				});
			}
	    	function closePopOver() {
	    		vm.popover.hide();
	    	};

	    	vm.clearSearch = function(){
				vm.searchText = '';
			};
			vm.showHideDelete = function(){
				vm.showOptionToDelete = !vm.showOptionToDelete;
				vm.showHideDeleteButtons = !vm.showHideDeleteButtons;
				vm.ifCheckboxTrue = 0;
				//vm.unSelectAllCheckbox();
				vm.popover.hide();
			};
			vm.deleteParForms = function(){
				var selectedAllParforms  = filterFilter( vm.cycleCount, {val:true});
			    CommonInfoService.setSelectedParforms(selectedAllParforms);
				var LocationCodeTobeDeleted = [];

				CommonInfoService.getSelectedParforms().forEach(function(v){
					LocationCodeTobeDeleted.push(v.CountFormID);
				});

				

				var query = "UPDATE Cycle_Count_Form_Table SET IsSelected = 0 WHERE CountFormID IN ('";
		 			query += LocationCodeTobeDeleted.join("','");
		 			query += "');";

				CRUD.inlineQuery(query)
				    .then(function(result) {
				    	vm.clearAll();
				    	$state.go($state.current, {}, {reload: true}); //refresh screen to update list view
						
				},function errorCallback(response) {
					    //$ionicLoading.hide(); 	
							   		 
				});

			};
			vm.SendFormToServer = function(){
				
				/*if(!CI.connectionAvailable()){
					showPopup({
						message : 'cycle-count-send-no-internet',
						type : 'confirm',
						scope : $scope,
						cancelText : 'No',
						okText : 'Yes',
						callbackForYes : function y(){
							vm.SendFormToServer()
						},
						callbackForNo : function n(){
							
						}
					});
				} else {*/
					vm.users[vm.currentUser].lastSelectIDUsed = []; //Clear last selectID used

					//show loading icon
					$ionicLoading.show({
						templateUrl: CONSTANT.SpinnerTemplate
					});
					
					var ItemQuery = "select * from Cycle_Transaction_Table";
					CRUD.inlineQuery(ItemQuery)
						.then(function(itemResult) {

							var requestString = "<sync_cycle_counts>";
							requestString += '<username>'+LocalData.loadSession('SoapUsername')+'</username>';

							var locIDArr = [];
									angular.forEach(itemResult, function(item,key){   

									if(locIDArr.indexOf(item.CountFormID) == -1) {
											console.log(key);

											if(key != 0) {
												requestString += "</countform>";
											}
											requestString += '<countform countformid="'+ item.CountFormID +'" selectid="">';
											vm.users[vm.currentUser].lastSelectIDUsed.push(item.CountFormID); //Save LocationID for Last Last selctIDs used flow
											
									} 
									requestString += '<countitem countformitemid="'+item.CountFormItemID+'" countqty="'+item.ItemQuantity+'" timestamp=""/>';
									locIDArr[key] = item.CountFormID;

									if(locIDArr.indexOf(item.CountFormID) == -1) {
											requestString += '</countform>';
									} 
													

								});

								requestString += "</countform></sync_cycle_counts>";
								console.log(requestString);

								CycleCountService
									.syncCycleCounts(requestString)
									.then(function(data){

										var xmlString = (new XMLSerializer()).serializeToString(data);
										//alert(xmlString);
										if(xmlString.indexOf("resultsuccess") != -1){
											
												//Update Cycle_TransHistory_Table
											locIDArr.sort();
											var currentCountFormID = null;
											var itemCount = 0;
											for (var i = 0; i < locIDArr.length; i++) {
												if (locIDArr[i] != currentCountFormID) {
													if (itemCount > 0) {
														console.log(currentCountFormID + ' comes1 --> ' + itemCount + ' times<br>');
														CycleCountService.saveTransactionHistory(currentCountFormID,itemCount);

													}
													currentCountFormID = locIDArr[i];
													itemCount = 1;
												} else {
													itemCount++;
												}
											}
											if (itemCount > 0) {
												console.log(currentCountFormID + ' comes2 --> ' + itemCount + ' times');
												CycleCountService.saveTransactionHistory(currentCountFormID,itemCount);
											}

											
											//Clear Transaction Table and refresh the list
											CommonInfoService
												.clearTable("Cycle_Transaction_Table")
												.then(function(){
													$cordovaToast.showLongCenter('Counts sent successfully to server.');
													LocalData.saveData('users',JSON.stringify(vm.users)); // Update Users Localstorage data
													vm.getCycleSelectIds();
												});


										}else{
											vm.getCycleSelectIds();
										}

									});

						});
				//}
				

			};
		
			function checkIfSelectIDIsAvailable(){
				 
				var query = "SELECT *,";
					query +="(select count(*) from Cycle_Count_Form_Table left join Cycle_Item_Table USING(CountFormID) ";
					query +="where Cycle_Item_Table.CountFormID = cycleCount.CountFormID) as ItemCount,";
					query +="(select COUNT(*) from Cycle_Count_Form_Table left join Cycle_Transaction_Table USING(CountFormID) ";
					query +="where Cycle_Transaction_Table.CountFormID = cycleCount.CountFormID) as TotalItemCounted	";	
		 			query +="FROM Cycle_Count_Form_Table as cycleCount ";
		 			//query +="WHERE cycleCount.CompanyID="+ vm.companyHeaderID;
					
					
				CRUD.inlineQuery(query).then(function(result) {
					
					var isCompanyAvailable = false;
					var defaultID = parseInt((vm.users[vm.currentUser].defaultCycleCompanyID || LocalData.loadData('CycleCompanyID')));
					for(var i = 0; i < result.length; i++){
						if(result[i].CompanyID == defaultID){
							isCompanyAvailable = true;
							break;
						}
					}
					
					
						if(!(result.length !== undefined && result.length > 0)){ //No select ID available for all companies
							vm.isDisabled = true;
							vm.popUpMsg = 'cycle-count-no-select-id-available';
							Popup.displayPopup({
								message : 'cycle-count-no-select-id-available',
								type : 'alert',
								scope : $scope,
								okText : 'Ok',
								alertCallback : function y(){
									
								}
							});
						} else { //select IDs available for company
						
							CRUD.select("Cycle_Company_Table","*").then(function(resultForCompanyList){
								var defaultCompanyAvailable = false;
								for(var i = 0; i < resultForCompanyList.length; i++){
									if(resultForCompanyList[i].CompanyID == parseInt(vm.companyHeaderID)){
										defaultCompanyAvailable = true;
										break;
									}
								}
								
								if(!defaultCompanyAvailable){
									//default company selected is not available anymore
									vm.popUpMsg = 'cycle-count-default-company-not-available';
									Popup.displayPopup({
										message : 'cycle-count-default-company-not-available',
										type : 'confirm',
										scope : $scope,
										cancelText : 'No',
										okText : 'Yes',
										callbackForYes : function y(){
											//go to change-company screen
											LocalData.saveData("CycleCompanyID", '');
											LocalData.saveData("CycleCompanyName",'');
											$state.go('app.cycle-count-change-company');
										},
										callbackForNo : function n(){
											//set first company to default
											vm.companyHeader = result[0].CompanyDesc;
											vm.companyHeaderID = result[0].CompanyID;
											LocalData.saveData("CycleCompanyID", vm.companyHeaderID);
											LocalData.saveData("CycleCompanyName",vm.companyHeader);
											vm.getCycleSelectIds();	
											
											checkIfSelectIDIsAvailableForCompany(result);
										}
									});
								} else {
									checkIfSelectIDIsAvailableForCompany(result);
								}
								 
							});
						}
					
					
					
				    
				});
			}
			
			function checkIfSelectIDIsAvailableForCompany(result){
				if(result.length > 0){
										
					//check if company contains select ID
					var isSelectIDAvailableForCompany = false;
					
					for(var i = 0; i < result.length; i++){
						if(result[i].CompanyID == parseInt(vm.companyHeaderID)){
							isSelectIDAvailableForCompany = true;
							break;
						}
					}
					
					if(!isSelectIDAvailableForCompany){
						vm.popUpMsg = 'cycle-count-no-select-id-available-for-company';
						Popup.displayPopup({
							message : 'cycle-count-no-select-id-available-for-company',
							type : 'confirm',
							scope : $scope,
							cancelText : 'No',
							okText : 'Yes',
							callbackForYes : function y(){
								//go to change-company screen
								console.log('show company selection');
								$state.go('app.cycle-count-change-company');
							},
							callbackForNo : function n(){
								//do nothing
							}
						});
					}
					
				}
			}
		}
		
		
		
})();
