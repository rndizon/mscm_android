(function(){
	'use strict';
	
	angular
		.module("app")
		.controller("CycleCountItemDetailsController", Body);
	
	function Body($state,$q,
				  $scope,
				  filterFilter,
				  $ionicSlideBoxDelegate,
				  $ionicPopup,
				  $ionicViewSwitcher,
				  $ionicPopover,
				  $stateParams,
				  CRUD,
				  LocalData,
				  CycleCountService,
				  $ionicPlatform,
				  $ionicLoading,
				  CommonInfoService,
				  $filter){
		
			var vm = this;
			vm.currentSlideNumber = 1;
			vm.slidestop = slidestop;
			vm.slidePrevious = slidePrevious;
			vm.slideNext = slideNext;
			vm.slideHasChanged = slideHasChanged;
			vm.goTo = goTo;
			vm.CountFormID = $stateParams.CountFormID;
			vm.SelectID = $stateParams.SelectID;
			vm.LocationCodeList = $stateParams.LocationCodeList;
			vm.checkFreezeQuantity = checkFreezeQuantity;
			vm.pickForParDetailsOption = pickForParDetailsOption;
			vm.slideCycleCount = [];
			vm.cycleCount = CycleCountService.currentCycleCount;
			vm.currentUser = LocalData.loadSession('currentUser');
			vm.users = JSON.parse(LocalData.loadData('users'));
			vm.localStorShowHelp = vm.users[vm.currentUser].isShowHelp;
			vm.isBlindCount = (vm.users[vm.currentUser].cycleCountConfiguration.isBlindCount == 1) ? true : false;
			vm.isValidateFreeze = (vm.users[vm.currentUser].cycleCountConfiguration.isValidateFreeze == 1) ? true : false;
			vm.TotalItemCounted = 0;
			vm.highlightField = highlightField;
			vm.setMaxDecimal = setMaxDecimal;
			vm.getMfgDisplayString = getMfgDisplayString;
			vm.CheckForDirtyItemCounts = CheckForDirtyItemCounts;
			vm.clickedDoneBtn = clickedDoneBtn;
			vm.checkValidInput = CycleCountService.checkValidInput;
			vm.backBtn = backBtn;

	    	var backBtn = $ionicPlatform.registerBackButtonAction(function(event) {
			    CheckForDirtyItemCounts();
			}, 150);  

   			$scope.$on('$destroy', backBtn);

			$scope.$on('$ionicView.afterEnter', function() {
					CycleCountService.isItemsDirty = 0;
					getItemList();
		    });

		    $scope.$on('$ionicView.beforeLeave', function() {
				/*var quantityFields = document.getElementsByName("quantity-from-details");
				
				var promises = [];

				for(var i = 0; i < quantityFields.length;i++){
				   if(quantityFields[i].value.trim().length > 0){
						vm.TotalItemCounted++;
				   		promises.push(vm.TotalItemCounted);
				   		
				   }
				   
				}
				$q.all(promises).then(function() {
				    
				    var query = "UPDATE Cycle_Count_Form_Table SET TotalItemCounted = "+vm.TotalItemCounted+" WHERE CountFormID='"+vm.CountFormID+"'";
					CRUD.inlineQuery(query);



				});*/
				
			});

			function highlightField($event) {
		    	$event.target.select();
		    }

	    	function pickForParDetailsOption($event) {
		    	$ionicPopover.fromTemplateUrl('html/templates/options-more.html', {
				        scope: $scope
				}).then(function(popover) {
				        vm.popover = popover;
				        vm.popUpType = "pick-for-par-details";
				        popover.show($event);
				}); 
	    	}
			

			function getItemList(){

				if(vm.cycleCount == undefined || vm.cycleCount == "" || vm.cycleCount == [])
				{
					var query = "select *,cycleItem.CountFormID as CountFormID, 0 as isDirty, trans.ItemQuantity as previousQuantity from Cycle_Item_Table as cycleItem left join Cycle_Transaction_Table as trans ";
			 			query +="using(CountFormItemID) where cycleItem.CountFormId = "+vm.CountFormID+" GROUP BY cycleItem.CountFormItemID";


					CRUD.inlineQuery(query)
					    .then(function(result) {

							vm.cycleCount = result;
							var curSlideLimit = (vm.cycleCount.length < 3 ? vm.cycleCount.length : 3);

							for(var i = 0; i < curSlideLimit; i++) {
							  vm.slideCycleCount.push(vm.cycleCount[i]);
							} 
					    	$ionicSlideBoxDelegate.update();
						});

				} else {
					var curSlideLimit = (vm.cycleCount.length < 3 ? vm.cycleCount.length : 3);

					for(var i = 0; i < curSlideLimit; i++) {
					  vm.slideCycleCount.push(vm.cycleCount[i]);
					} 
			    	$ionicSlideBoxDelegate.update();
				}
			}

	      	//navigate to Other Page
			function goTo(goToUrl,_CountFormID,_SelectID,_LocationCodeList) {
				CycleCountService.currentCycleCount = vm.cycleCount;
			 	if(_CountFormID && _CountFormID.length > 0){
			 		var params = {CountFormID:_CountFormID,SelectID:_SelectID,LocationCodeList:_LocationCodeList};
					$ionicViewSwitcher.nextDirection('none');
			 		$state.go(goToUrl,params);
			 	}else{
					$ionicViewSwitcher.nextDirection('slideDown');
			 		$state.go(goToUrl);	
			 	}
			 	
			}
			
			/*
				Next and Previous button of slider content
			*/

			function slideHasChanged(){

			  var countParCount = vm.cycleCount.length;
			  var countCurSlide = vm.slideCycleCount.length;
			  var currentSlideIndex = $ionicSlideBoxDelegate.currentIndex();
			  var addIndex = currentSlideIndex + 2;

			  if(addIndex == countCurSlide) {

			      if(addIndex < countParCount) {
			        vm.slideCycleCount.push(vm.cycleCount[addIndex]);

			        $ionicSlideBoxDelegate.update();
			      }

			  }
			  vm.currentSlideNumber = $ionicSlideBoxDelegate.currentIndex() + 1;

			}

			function slidestop(index) {
			    $ionicSlideBoxDelegate.enableSlide(false);
			}

			function slidePrevious() {
				$ionicSlideBoxDelegate.previous();
				vm.currentSlideNumber =  $ionicSlideBoxDelegate.currentIndex() + 1;
				
			}

			function slideNext() {
				if(vm.currentSlideNumber >= $ionicSlideBoxDelegate.slidesCount()) {
					//nextParLocationConfirmation();
				}

				$ionicSlideBoxDelegate.next();
				vm.currentSlideNumber =  $ionicSlideBoxDelegate.currentIndex() + 1;
				
			}  
			function checkFreezeQuantity(e,freezeQty) {

				if(parseInt(e.srcElement.value) > freezeQty && (vm.isBlindCount && vm.isValidateFreeze)) {
						vm.popUpMsg = "check-freeze-quantity";
						var confirmPopup  = $ionicPopup.alert({
							cssClass: 'custom-popup',
							templateUrl: "html/templates/pop-up.html",
							scope: $scope
						});

				}
			}
			

			function setMaxDecimal(currentModel,currslideModel) {
				vm.pulledQty[currslideModel] = parseFloat(toFixed(currentModel,4));
			}   	

			function toFixed(value, precision) {
			    var precision = precision || 0,
			        power = Math.pow(10, precision),
			        absValue = Math.abs(Math.round(value * power)),
			        result = (value < 0 ? '-' : '') + String(Math.floor(absValue / power));

			    if (precision > 0) {
			        var fraction = String(absValue % power),
			            padding = new Array(Math.max(precision - fraction.length, 0) + 1).join('0');
			        result += '.' + padding + fraction;
			    }
			    return result;
			}

			function getMfgDisplayString(mfgNum) {
				if (mfgNum == "" || mfgNum == undefined)
					return "-";
				else
					return mfgNum;
			}

			function getDisplayHibcGtin(hibc,gtin) {
				var hibcgtin = "";
				if (hibc !== "" || hibc !== undefined)
					hibcgtin = hibc;
				if (gtin !== ""  || gtin !== undefined) {
					if(hibcgtin !== "")
						hibcgtin += "/";
					hibcgtin += gtin;
				}
				return hibcgtin;
			}

			function CheckForDirtyItemCounts() {
				if(CycleCountService.isItemsDirty) {
				    vm.popUpMsg = "cycle-count-close";
					$ionicPopup.confirm({
						cssClass: 'custom-popup',
						templateUrl: "html/templates/pop-up.html",
	        			okText: "Discard",
	       				cancelText: "No",
						scope: $scope
					}).then( function(res) {
						if(!res) {
							clickedDoneBtn();
						} else {
							vm.goTo('app.cycle-count');
						}
					});
				} else {
					vm.goTo('app.cycle-count');
				}

			}
			
			function clickedDoneBtn() {
				CycleCountService.saveAllValues(vm.cycleCount, function () {
					vm.goTo('app.cycle-count');
				});
			}
		
	}
	
})();