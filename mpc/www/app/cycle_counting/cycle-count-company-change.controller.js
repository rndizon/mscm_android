(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('CycleCompanyChangeController', Body);

		function Body($state,
					  $scope,
					  $ionicLoading,
					  $ionicViewSwitcher,
					  $ionicModal, 
					  filterFilter,
					  $ionicHistory,
					  CRUD, 
					  DmeManager,
					  LocalData,
					  CONSTANT,
					  $ionicPopup
					  ) 
		{
					
			var vm = this;
			vm.prevDefCompanyID;
			vm.currentUser = LocalData.loadSession('currentUser');
			var usersObj = JSON.parse(LocalData.loadData('users'));
			vm.companyList = []; 
			vm.searchText = ""; //initialize
			vm.doRefresh = doRefresh;

			$scope.$on('$ionicView.beforeEnter', function() {
				
				vm.popUpMsg = "cycle-count-change-company";
				var confirmPopup  = $ionicPopup.confirm({
					cssClass: 'custom-popup',
					templateUrl: "html/templates/pop-up.html",
					scope: $scope,
       				cancelText: "No",
        			okText: "Yes"
				});

				confirmPopup.then( function(res) {
					if(res == false) {
						$ionicHistory.goBack();
					}
				});

			});

			     	
			$scope.$on('$ionicView.afterEnter', function() {
			    	
				vm.defaultCompanyID = LocalData.loadData("CycleCompanyID");
				vm.defaultCompanyName = LocalData.loadData("CycleCompanyName");
				vm.showCompanyList();
				vm.prevDefCompanyID = vm.defaultCompanyID;
				console.log('initial prevDefCompanyID = ' + vm.prevDefCompanyID);
				
			});

			vm.goBack = function(){

					$ionicHistory.goBack();
			};

			vm.showCompanyList = function() {
				var companyParams = {};

				if (vm.searchText.trim()) {
					companyParams.CompanyID = vm.searchText.trim();
				}

				CRUD.select("Cycle_Company_Table","*",companyParams)
				 .then(function(result){
		 		 	 	console.log('Callback: select result:' + result[0].CompanyDesc);
		 		 	 	vm.companyList = result;
		 		});

			};

			vm.saveCompanySelected = function () {
				var prevDefCompanyID = "";
				if(vm.defaultCompanyID !== vm.prevDefCompanyID)
				{
					LocalData.saveData("CycleCompanyID", vm.defaultCompanyID);

					Object.keys(vm.companyList).forEach(function(idx){
						if (vm.companyList[idx].CompanyID == vm.defaultCompanyID) {
							LocalData.saveData("CycleCompanyName", vm.companyList[idx].CompanyDesc);

							if(!usersObj[vm.currentUser].defaultCycleCompanyID) {
								saveParCountDefCompany(vm.companyList[idx].CompanyID,vm.companyList[idx].CompanyDesc)
							} else {
								var queryParams = {};
			   			    	queryParams.IsSelected = 1;
			       				CRUD.update("Cycle_Count_Form_Table","IsSelected",0,queryParams)
				       				.then(function(result){
				       					 CRUD.remove("Cycle_Transaction_Table")
					        				.then(function(){
	    										$state.go('app.cycle-count');
				        				});
	    							});
							}
						}

					});
				} else {
					$state.go('app.cycle-count');
				}			
			};

			function saveParCountDefCompany(compID,compName) {

					vm.popUpMsg = "set-def-company";
					var confirmPopup  = $ionicPopup.confirm({
	                    cssClass: 'custom-popup',
						templateUrl: "html/templates/pop-up.html",
						scope: $scope,
	       				cancelText: "No",
	        			okText: "Yes"
					});

					confirmPopup.then( function(res) {
						if(res == true) {
							usersObj[vm.currentUser].defaultCycleCompanyID = compID;
							usersObj[vm.currentUser].defaultCycleCompanyDescription = compName;
							LocalData.saveData('users',JSON.stringify(usersObj));
							var queryParams = {};
							queryParams.IsSelected = 1;
							CRUD.update("Cycle_Count_Form_Table","IsSelected",0,queryParams)
			       					.then(function(result){
	    							$state.go('app.cycle-count');
	    						});
						} else {
							$state.go('app.cycle-count');
						}
					});
			}

		     vm.clearSearch = function(){
				vm.searchText = '';
			}; 

			function doRefresh() {

				$ionicLoading.show({
					templateUrl: CONSTANT.SpinnerTemplate
				});

				// TODO: perform call to server before refresh

				vm.showCompanyList();

				$scope.$broadcast('scroll.refreshComplete');
				$ionicLoading.hide();

			}

		}
				
		
})();
