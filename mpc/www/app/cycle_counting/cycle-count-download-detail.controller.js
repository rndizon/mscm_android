(function(){
	'use strict';
	
	angular
		.module("app")
		.controller("CycleCountItemDetailController", Body);
	
	function Body($state,
				  $scope,
				  filterFilter,
				  $ionicSlideBoxDelegate,
				  $ionicPopup,
				  $ionicViewSwitcher,
				  $ionicPopover,
				  $stateParams,
				  CRUD,
				  LocalStorageManagerService
				  ) 
	{
		
			var vm = this;
			vm.currentSlideNumber = 1;
			vm.slidestop = slidestop;
			vm.slidePrevious = slidePrevious;
			vm.slideNext = slideNext;
			vm.slideHasChanged = slideHasChanged;
			vm.goTo = goTo;
			vm.CountFormID = $stateParams.CountFormID;
			vm.openEditParlevel = openEditParlevel;
			vm.pickForParDetailsOption = pickForParDetailsOption;
			vm.slideCycleCount = [];
			vm.cycleCount = [];
			vm.currentUser = LocalData.loadSession('currentUser');
			var users = JSON.parse(LocalData.loadData('users'));
			vm.localStorShowHelp = users[vm.currentUser].isShowHelp;

			$scope.$on('$ionicView.afterEnter', function() {
		    	
				getItemList();
		    	
		    });

	    	function pickForParDetailsOption($event) {
		    	$ionicPopover.fromTemplateUrl('html/templates/options-more.html', {
				        scope: $scope
				}).then(function(popover) {
				        vm.popover = popover;
				        vm.popUpType = "pick-for-par-details";
				        popover.show($event);
				}); 
	    	}
			

			function getItemList(){

				if(vm.cycleCount == undefined || vm.cycleCount == "" || vm.cycleCount == []) {

					var query = "SELECT Cycle_Location_Table.LocationID,";
						query +="Cycle_Location_Table.LocationCode,";
						query +="Cycle_Location_Table.LocationName,"; 
						query +="Cycle_Item_Table.* ";
			 			query +="FROM Cycle_Location_Table INNER JOIN Cycle_Item_Table ";
			 			query +="ON Cycle_Location_Table.LocationID = Cycle_Item_Table.LocationID ";
			 			query +="WHERE Cycle_Location_Table.CountFormID = "+vm.CountFormID; 
			 			//query +=" LIMIT 11";

					CRUD.inlineQuery(query)
					    .then(function(result) {

							vm.cycleCount = result;
							var curSlideLimit = (vm.cycleCount.length < 3 ? vm.cycleCount.length : 3);

							for(var i = 0; i < curSlideLimit; i++) {
							  vm.slideCycleCount.push(vm.cycleCount[i]);
							} 
					    	$ionicSlideBoxDelegate.update();
						});
				} else {
					var curSlideLimit = (vm.cycleCount.length < 3 ? vm.cycleCount.length : 3);

					for(var i = 0; i < curSlideLimit; i++) {
					  vm.slideCycleCount.push(vm.cycleCount[i]);
					} 
			    	$ionicSlideBoxDelegate.update();

				}


					

			}
	      	//navigate to Other Page
			function goTo(goToUrl,_CountFormID) {

			 	if(_CountFormID && _CountFormID.length > 0){
			 		var params = {CountFormID:_CountFormID};
			 		$state.go(goToUrl,params);
			 	}else{
			 		$state.go(goToUrl);	
			 	}
			 	
			}
			
			/*
				Next and Previous button of slider content
			*/


			function slideHasChanged(){

			  var countParCount = vm.cycleCount.length;
			  var countCurSlide = vm.slideCycleCount.length;
			  var currentSlideIndex = $ionicSlideBoxDelegate.currentIndex();
			  var addIndex = currentSlideIndex + 2;

			  if(addIndex == countCurSlide) {

			      if(addIndex < countParCount) {
			        vm.slideCycleCount.push(vm.cycleCount[addIndex]);

			        $ionicSlideBoxDelegate.update();
			      }

			  }
			  vm.currentSlideNumber = currentSlideIndex;

			};

			function slidestop(index) {
			    $ionicSlideBoxDelegate.enableSlide(false);
			}

			function slidePrevious() {
				$ionicSlideBoxDelegate.previous();
				vm.currentSlideNumber =  $ionicSlideBoxDelegate.currentIndex() + 1;
				
			};

			function slideNext() {
				if(vm.currentSlideNumber >= $ionicSlideBoxDelegate.slidesCount()) {
					//nextParLocationConfirmation();
				}

				$ionicSlideBoxDelegate.next();
				vm.currentSlideNumber =  $ionicSlideBoxDelegate.currentIndex() + 1;
				
			};  

			/*
				Change this to native popup
			*/
			function nextParLocationConfirmation() {
				var confirmPopup  = $ionicPopup.confirm({
					template: 'Previous Par forms will be saved',
					title: 'Continue to next Par Location?',
					scope: $scope
				});

				confirmPopup.then( function(res) {
				});
			};

			function openEditParlevel() {
				var confirmPopup  = $ionicPopup.confirm({
					templateUrl: 'html/templates/edit-par-level.html',
					title: 'Edit Par Level',
					scope: $scope
				});
			}
		
	}
	
	
	
	
	
	
})();