(function(){
    'use strict';
	 
	angular
	    .module('app')
	    .factory('CycleCountService', Body);

	    function Body($q,
	    			  CRUD,
	    			  LocalData,
	    			  CM,
	    			  CI,
	    			  $ionicPopup
	    			  ) 
	    {		  
		    var service = {
		        getCycleHeaders:getCycleHeaders,
		        saveCycleHeaders:saveCycleHeaders,
		        getCountFormDetails:getCountFormDetails,
		        saveCountFormDetails:saveCountFormDetails,
		        syncCycleCounts:syncCycleCounts,
		        saveTransactionHistory:saveTransactionHistory,
		        saveValues:saveValues,
		        saveAllValues:saveAllValues,
		        currentCycleCount:currentCycleCount,
		        isItemsDirty:isItemsDirty,
		        checkValidInput:checkValidInput
		    },
		    userObject = JSON.parse(LocalData.loadData('users')),
		    currentUser = LocalData.loadSession('SoapUsername'),
			requestToken = LocalData.loadSession("SoapToken");

		    return service;  	

		    var currentCycleCount = [];
		    var isItemsDirty = 0;
		 	
		 	function getCycleHeaders(){
				//if(CI.connectionAvailable()){
					var args = {
						URL:    "CycleCountService",
						Action: "getCycleHeaders",
						Params: {"requestString":"<get_cycle_headers/>","token":requestToken}
					};
					return CM.doCommsCall("soap",args);
				/*} else {
					var deffered = $q.defer();
					var promises = [];
					deffered.resolve({"connectionAvailable": false});
					promises.push(deffered.promise);
					return $q.all(promises);
				}*/
		 	}
		 	function saveCycleHeaders(response){
		 		var promises = [];
		    	var deffered = $q.defer();
		 		var value = xmlToJson(response);
		 		var xmlResponse = value.root.body.getCycleHeadersResponse.getCycleHeadersReturn;
		 		var object = ReturnDataObject(xmlResponse);
		 		var companyResponse = object.get_cycle_headers_response.fac;
		 		var configResponse = object.get_cycle_headers_response.cyclecountconfig;
				var arryCompanies = [];
				
				userObject[currentUser].cycleCountConfiguration.isBlindCount = (configResponse.blindcount == "true") ? 1 : 0;
			  	userObject[currentUser].cycleCountConfiguration.isValidateFreeze = (configResponse.validatecount == "true") ? 1 : 0;
			  	LocalData.saveData('users',JSON.stringify(userObject)); // Update Users Localstorage data

				if(companyResponse instanceof Array){
				  arryCompanies = companyResponse;
				}else{
				  arryCompanies.push(companyResponse);  
				}

				if(!LocalData.loadData("CycleCompanyID")){
			  		LocalData.saveData("CycleCompanyID", arryCompanies[0].attributes.facid);
			  		LocalData.saveData("CycleCompanyName", arryCompanies[0].attributes.facdesc);
			  	}

			  	var CompanyQuery = "insert into Cycle_Company_Table (CompanyID,CompanyDesc) " ;
			  	var CompanyQueryCount = 0;

				angular.forEach(arryCompanies, function(company, key) {
					console.log(key+" : "+company.attributes.facdesc);
					var _CompanyID = checkValue(company.attributes.facid);
					var _CompanyDesc = checkValue(company.attributes.facdesc,"text");

					if(CompanyQueryCount < 1){
			  	  		CompanyQueryCount++;

			  	  		CompanyQuery += "select "+_CompanyID+" as CompanyID, ";
			  	  		CompanyQuery += '"'+_CompanyDesc+'" as CompanyDesc ';

			  	    }else{
			  	  		CompanyQuery += " union all select "+_CompanyID+", ";
			  	  		CompanyQuery += ' "'+_CompanyDesc+'" ';
			  	    }

					var countformResponse = company.countform;
					var countformArray = [];

					if(countformResponse instanceof Array){
						countformArray = countformResponse;
					}else{
						countformArray.push(countformResponse);
					}

					var CountformQuery = "insert into Cycle_Count_Form_Table (CompanyID,CountFormID,SelectID,LocationCodeList) " ;
					var countformLimitCount = 0;
					angular.forEach(countformArray, function(countform, key) {
						console.log(key+" : "+countform.attributes.selectid);

						var _CountFormID = checkValue(countform.attributes.countformid),
							_SelectID	 = checkValue(countform.attributes.selectid,'text'),
							_CodeList	 = checkValue(countform.attributes.loccodelist,'text');

							if(countformLimitCount < 1){
								countformLimitCount++;
							
								CountformQuery +="select "+_CompanyID+" as CompanyID, ";
								CountformQuery +=_CountFormID+' as CountFormID, ';
								CountformQuery +='"'+_SelectID+'" as SelectID, ';
								CountformQuery +='"'+_CodeList+'" as LocationCodeList ';
							} else {
								CountformQuery +=" union all select "+_CompanyID+", ";
								CountformQuery +=_CountFormID+", ";
								CountformQuery +='"'+_SelectID+'", ';
								CountformQuery +='"'+_CodeList+'"';
							}

					});
					CRUD.inlineQuery(CountformQuery)
			     	 	.then(function(result) {
				     	 	console.log("successful Countform insert "+CountformQuery);
				     	 	return result;
				     	},function errorCallback(response) {
				     		return response;
						   		 
						});


				});

				CRUD.inlineQuery(CompanyQuery).
			  	then(function(result){
			  			deffered.resolve(result);
						LocalData.saveData("SQLiteCycleLoaded", true);
						console.log("successful company insert");
			  	});

			  	promises.push(deffered.promise);
				
				return $q.all(promises);

		 	}
		 	function getCountFormDetails(requestString){
		 		var args = {
		 			URL:    "CycleCountService",
		 			Action: "getCycleDetails",
		 			Params: {"requestString":requestString,"token":requestToken}
		 		};

		 		return CM.doCommsCall("soap",args);
		 	}
		    function saveCountFormDetails(response){
		    	var promises = [];
		    	var deffered = $q.defer();
		 		var value = xmlToJson(response);
				var xmlResponse = value.root.body.getCycleDetailsResponse.getCycleDetailsReturn;
				var object = ReturnDataObject(xmlResponse);
				var CountFormID = checkValue(object.get_cycle_details_response.countform.attributes.countformid);
				var locationResponse = object.get_cycle_details_response.countform.loc;
				var locationArray = [];

				if(locationResponse instanceof Array){
				  locationArray = locationResponse;
				}else{
				  locationArray.push(locationResponse);  
				}

				var addedLocations = LocalData.loadSession("DownloadedCycleLocations");

				var LocationQuery = "insert into Cycle_Location_Table (CountFormID,LocationID,LocationCode,LocationName) " ;
			  	var LocationQueryCount = 0;

			  	var _LocationID = "",
			  		_LocationCode = "",
			  		_LocationDesc = "";

			  	angular.forEach(locationArray, function(location, key) {
					var locationData = location.attributes;
					if (addedLocations.indexOf(checkValue(locationData.locid)+"_"+checkValue(CountFormID)) == -1 ){ // Check if already exists in DownloadedCycleItems array (session storage)
						
						_LocationID	 = checkValue(locationData.locid),
						_LocationCode	= checkValue(locationData.loccode,'text'),
						_LocationDesc	= checkValue(locationData.locdesc,'text');

						if(LocationQueryCount < 1){
							LocationQueryCount++;
							
							LocationQuery +="select "+CountFormID+" as CountFormID, ";
							LocationQuery +=_LocationID+" as LocationID, ";
							LocationQuery +="'"+_LocationCode+"' as LocationCode, ";
							LocationQuery +="'"+_LocationDesc+"' as LocationName ";
						}else{
							LocationQuery +="union all select "+CountFormID+" , ";
							LocationQuery +=_LocationID+", ";
							LocationQuery +="'"+_LocationCode+"', ";
							LocationQuery +="'"+_LocationDesc+"' ";
						}

						addedLocations.push(_LocationID+"_"+checkValue(CountFormID));
					}
					
					var countformItemResponse = location.countitem;
					var countformItemArray = [];

					if(countformItemResponse instanceof Array){
						countformItemArray = countformItemResponse;
					}else{
						countformItemArray.push(countformItemResponse);
					}

					var addedItems = LocalData.loadSession("DownloadedCycleItems");

					var CountformItemQuery = "insert into Cycle_Item_Table (CountFormID,LocationID,CountFormItemID,ItemID,ItemNo,ItemDesc,GTIN,HIBC,ManufacturerCatalogueNumber,StockQuantityOnHand,UnitOfMeasure,BinNumber,SerialNumber,LotNumber) " ;
			  		var CountformItemQueryCount = 0;
					angular.forEach(countformItemArray, function(countformItem, key) {
							var countFormData 	 = countformItem.attributes;
							if (addedItems.indexOf(CountFormID+"_"+checkValue(countFormData.countformitemid)) == -1 ){ // Check if already exists in DownloadedCycleItems array (session storage)

							var	_CountFormItemID = checkValue(countFormData.countformitemid),
								_LocationID	 = checkValue(countFormData.locid),
								_ItemID 		 = checkValue(countFormData.itemid),
								_ItemNo			 = checkValue(countFormData.itemno,'text'),
								_ItemDesc		 = checkValue(countFormData.itemdesc,'text'),
								_GTIN			 = checkValue(countFormData.gtin,'text'),
								_HIBC			 = checkValue(countFormData.hibc,'text'),
								_ManufacturerCatalogueNumber = checkValue(countFormData.mfrcatno,'text'),
								_StockQuantityOnHand = checkValue(countFormData.sqoh,'text'),
								_UnitOfMeasure   = checkValue(countFormData.uom,'text'),
								_BinNumber 		 = checkValue(countFormData.binno,'text'),
								_SerialNumber	 = checkValue(countFormData.serno,'text'),
								_LotNumber	 = checkValue(countFormData.lotno,'text');
							console.log("Counform item: "+countFormData.itemid+ " BIN:"+ countFormData.binno+ " "+ countFormData.itemdesc);

							if(CountformItemQueryCount < 1){
								CountformItemQueryCount++;

								CountformItemQuery +="select "+CountFormID+" as CountFormID, ";
								CountformItemQuery +=_LocationID+" as LocationID, ";
								CountformItemQuery +=_CountFormItemID+" as CountFormID, ";
								CountformItemQuery +='"'+_ItemID+'" as ItemID,';
								CountformItemQuery +='"'+_ItemNo+'" as ItemNo,';
								CountformItemQuery +='"'+_ItemDesc+'" as ItemDesc,';
								CountformItemQuery +='"'+_GTIN+'" as GTIN,';
								CountformItemQuery +='"'+_HIBC+'" as HIBC,';
								CountformItemQuery +='"'+_ManufacturerCatalogueNumber+'" as ManufacturerCatalogueNumber,';
								CountformItemQuery +='"'+_StockQuantityOnHand+'" as StockQuantityOnHand,';
								CountformItemQuery +='"'+_UnitOfMeasure+'" as UnitOfMeasure,';
								CountformItemQuery +='"'+_BinNumber+'" as BinNumber, ';
								CountformItemQuery +='"'+_SerialNumber+'" as SerialNumber, ';
								CountformItemQuery +='"'+_LotNumber+'" as LotNumber ';
							} else {
								CountformItemQuery +=" union all select "+CountFormID+", ";
								CountformItemQuery +=_LocationID+", ";
								CountformItemQuery +=_CountFormItemID+", ";
								CountformItemQuery +='"'+_ItemID+'", ';
								CountformItemQuery +='"'+_ItemNo+'", ';
								CountformItemQuery +='"'+_ItemDesc+'", ';
								CountformItemQuery +='"'+_GTIN+'", ';
								CountformItemQuery +='"'+_HIBC+'", ';
								CountformItemQuery +='"'+_ManufacturerCatalogueNumber+'", ';
								CountformItemQuery +='"'+_StockQuantityOnHand+'", ';
								CountformItemQuery +='"'+_UnitOfMeasure+'", ';
								CountformItemQuery +='"'+_BinNumber+'", ';
								CountformItemQuery +='"'+_SerialNumber+'", ';
								CountformItemQuery +='"'+_LotNumber+'" ';
							}

							addedItems.push(CountFormID+"_"+_CountFormItemID);
						}
						
					});
					CRUD.inlineQuery(CountformItemQuery).
					  	then(function(result){
					  			deffered.resolve(result);
								console.log("successful company insertxxxx");
					  	},function(result){
					  		deffered.resolve(result);
							console.log("error inserting CounformFormItem:"+CountformItemQuery);
					  	});

				});

				CRUD.inlineQuery(LocationQuery).
			  	then(function(result){
			  			deffered.resolve(result);
						console.log("successful location insertyyyy");
			  	},function(result){
			  		deffered.resolve(result);
					console.log("error inserting Countform locations:"+LocationQuery);
			  	});

			  	promises.push(deffered.promise);
				
				return $q.all(promises);

		    }
		    function syncCycleCounts(requestString){
				var args = {
					 			URL:    "CycleCountService",
					 			Action: "syncCycleCounts",
					 			Params: {"requestString":requestString,"token":requestToken}
					 		};

		 		return CM.doCommsCall("soap",args);
			}

			function saveTransactionHistory(CountformID,ItemCount){

				var query = "insert into Cycle_TransHistory_Table (UserID,CountedSelectIDs,CountedItems,TimeSpent) VALUES(";
					query += "'"+LocalData.loadSession("SoapUsername")+"',";
					query += CountformID+",";
					query += ItemCount+",";
					query += "(SELECT ";
					query +="ROUND(cast( ";
					query +="( ";
					query +="strftime('%s',(SELECT TimeStamp from Cycle_Transaction_Table ORDER BY TimeStamp DESC LIMIT 1))";	
					query +="-strftime('%s',(SELECT TimeStamp from Cycle_Transaction_Table ORDER BY TimeStamp ASC LIMIT 1))";
					query +=") as real";
					query +=")/60,2) as TimeSpent "; // Devided by 60 to get Minutes
					query +="FROM Cycle_Transaction_Table WHERE CountformID = "+CountformID;
					query +=" ORDER BY TimeStamp DESC LIMIT 1))";

				CRUD.inlineQuery(query).then(function(res){
					console.log("Success: "+res);
				},function(res){
					console.log("Error: "+res);
				});
			}

		    function saveValues(item,input,type){
		    	input = input + "";
		    	if(input != "")
				{
					var quantityValue;
					
					if(input.indexOf('.') > -1) {
						quantityValue = parseFloat(input);
					} else {
						quantityValue = parseInt(input);
					}
					
					var query = "SELECT EXISTS(SELECT CountFormItemID FROM Cycle_Transaction_Table WHERE CountFormID="+item.CountFormID+" AND CountFormItemID = "+item.CountFormItemID+" LIMIT 1) as Item";
					CRUD.inlineQuery(query)
						.then(function(result){
							//alert(result[0].Item);
							if(result[0].Item < 1){
								var params = {
								 	"CountFormID":parseInt(item.CountFormID),
								 	"CountFormItemID":parseInt(item.CountFormItemID),
								 	"ItemQuantity": quantityValue
								 };	
							     CRUD.insert("Cycle_Transaction_Table",params);
							}else{
								var params = {CountFormItemID:item.CountFormItemID};
			    				CRUD.update("Cycle_Transaction_Table","ItemQuantity",quantityValue,params);
							}

						});
				} else {
					var query = "DELETE FROM Cycle_Transaction_Table WHERE CountFormID="+item.CountFormID+" AND CountFormItemID = "+item.CountFormItemID+"";
					CRUD.inlineQuery(query);
				}
			}

			function saveAllValues(cycleCount, callback) {
				angular.forEach(cycleCount, function(item, key) {
					if(item.isDirty == 1) {
						if(item.ItemQuantity != undefined || item.ItemQuantity != "") {
							saveValues(item, item.ItemQuantity);
						} else {
							saveValues(item, "");
						}

					}
				});
				callback();
			}

			function checkValidInput(item, input, isBlindCount, isValidateFreeze) {
				var inputValue;

				if(input.target.value == "") {
					inputValue = "";
				} else {
					
					if(input.target.value.indexOf('.') > -1) {
						inputValue = parseFloat(input.target.value);
					} else {
						inputValue = parseInt(input.target.value);
					}
					
					if(item.SerialNumber !== "" && item.SerialNumber !== null && !(inputValue == 0 || inputValue == 1)){
						
						var confirmPopup = $ionicPopup.alert({
								cssClass: 'custom-popup',
								template: '<div>{{ "confirm_cycle_count_invalid_serial_input" | translate }}</div>' 
						});

						input.target.value = "";

						return;
					}

					if(inputValue != item.StockQuantityOnHand && (isBlindCount && isValidateFreeze)) {
						var confirmPopup  = $ionicPopup.alert({
							cssClass: 'custom-popup',
							templateUrl: '<div>{{ "text_freeze_quantiy_not_match" | translate }}</div>'
						});

						input.target.value = "";

						return;
					}
				}

				item.ItemQuantity = inputValue;


				if(inputValue != item.previousQuantity) {
					service.isItemsDirty = 1;
					item.isDirty = 1;
				} else {
					item.isDirty = 0;
				}

			}

		 	/***** PRIVATE METHODS *******/
		 	function checkValue(val,type){

				if(type){
					return (val == undefined || val == "" ? "" : val.replace(/["']/g, ""));
				}else{
					return (val == undefined || val == "" ? 0 : parseInt(val));
				}

			}

		}
})();

