(function(){
 	'use strict';

	angular
		.module("app")
		.run(function(LocalData,CRUD,$cordovaToast,$ionicViewService,$ionicPlatform,$rootScope,ScannerManager,BarcodeParser,DmeManager,$ionicLoading,$ionicViewSwitcher, $ionicPopup) {
			
			$rootScope.Modules = null; //This is for manipulating side menu panel based on the User Profile
			$rootScope.isSavingInputs = false; // This is a flag when saving user inputs on Item details screen
			$rootScope.isOpenAddScreen = 1; // This is a flag to determine if Add Screen will dispaly first (for Cycle count and PFP module)

			$ionicPlatform.ready(function() {
			    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
			    // for form inputs)
			    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
			      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
			      cordova.plugins.Keyboard.disableScroll(true);
			    }
			    if (window.StatusBar) {
			      // org.apache.cordova.statusbar required
			      StatusBar.styleLightContent();
			    }
			 });


		  /* ------------------------
		   *  State Change Events 
		   * ------------------------ */
		    var headerBarElement;
		  	$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){ 
				
				 LocalData.saveSession("PrevStateName", fromState.name);

				// Disable BACK button on home
				  $ionicPlatform.registerBackButtonAction(function(event) {
				    if (true) { // your check here

				    	if(toState.name == "app.my-par-forms" ||
				    	toState.name == "app.pick-for-par" ||
				    	toState.name == "app.cycle-count" ||
				    	toState.name == "app.welcome" ||
				    	toState.name == "app.settings" ||
				    	toState.name == "app"){
				    	
				    	 $ionicPopup.confirm({
					        template: 'Are you sure you want to sign out ?'
					      }).then(function(res) {
					        if (res) {
					          ionic.Platform.exitApp();
					        }
					      });
				    	}else{
				    		$ionicViewService.getBackView().go();
				    	}
				    }
				  }, 100);  

				    //  event.preventDefault(); 
				      // transitionTo() promise will be rejected with 
				      // a 'transition prevented' error
		  		//console.log(toState.name + " "+ function() {}; romState.name);
		  		var direction = null;
			    if (fromState.stateUpward === toState.name) direction = 'back';
			    if (toState.stateUpward === fromState.name) direction = 'forward';
			    if (direction) $ionicViewSwitcher.nextDirection(direction);


			    headerBarElement = angular.element(document.querySelectorAll('ion-side-menu-content ion-header-bar'));
			    /* Nav Bar Header Color per module */
		/*		if (toState.name.toLowerCase().indexOf("pick-for-par") >= 0){
					headerBarElement.removeClass('bar-amethyst-8');
					headerBarElement.removeClass('bar-energized');
                    headerBarElement.addClass('bar-turquoise-8');
                }else if (toState.name.toLowerCase().indexOf("cycle-count") >= 0){  
                  	 headerBarElement.removeClass('bar-energized');
                  	 headerBarElement.removeClass('bar-turquoise-8');
                  	 headerBarElement.addClass('bar-amethyst-8');
				}else{
					headerBarElement.removeClass('bar-amethyst-8');
					headerBarElement.removeClass('bar-turquoise-8');
                    headerBarElement.addClass('bar-energized');
				} */

                if (toState.changeColor) {
                    $rootScope.setNavColor = toState.changeColor;
                    console.log($rootScope.setNavColor + 'Larrys <----');
                } else {
                    $rootScope.setNavColor = false;
                    console.log($rootScope.setNavColor + 'Larrys <----');
                }

				console.log(fromState.name+" "+toState.name);

					
			});

			$rootScope.$on('$stateNotFound',function(event, unfoundState, fromState, fromParams){ 
			    console.log(unfoundState.to); // "lazy.state"
			    console.log(unfoundState.toParams); // {a:1, b:2}
			    console.log(unfoundState.options); // {inherit:false} + default options
			    alert("Error state: "+fromState);
			});
			$rootScope.$on('$stateChangeSuccess',function(event, toState, toParams, fromState, fromParams){ 
			
				 /*$cordovaToast.showLongCenter('Here is a message').then(function(success) {
				    // success
				  }, function (error) {
				    // error
				  });*/
				

			});
			
		});		
			
})();
