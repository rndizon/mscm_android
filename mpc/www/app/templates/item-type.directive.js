(function(){
    'use strict';
   
  angular
      .module('app')
      .directive('itemType', function() {
        return {
          require: '?ngModel',
		  scope: { itemTypeCallback: '&itemTypeCallback'},
          link: function(scope, element, attrs, ngModelCtrl) {
			
            element.bind('blur',function() {
				var elementData = angular.element(element)[0].dataset;
				if(!(elementData.serial == "")){
					if(!(element.value == 0 || element.value == 1)){
						scope.itemTypeCallback({elementValue: element.val()});
						element.val('');
						element.parent().css('border-color', 'red')
					}
				} 
                
            });
			
			element.bind('focus',function() {
			
				if(element.parent().css('border-color') == 'red'){
					element.parent().css('border-color', '');
				}
				
                
            });
          }
        };
      });
      
})();