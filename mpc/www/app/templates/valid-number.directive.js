(function(){
    'use strict';
   
  angular
      .module('app')
      .directive('validNumber', function() {
        return {
          require: '?ngModel',
          link: function(scope, element, attrs, ngModelCtrl) {
           
           element.bind('keyup', function (blurEvent) {
                var oldValue = element.val().slice(0,-1);
                var curVal = element.val();
                var reg = /^[0-9.]+$/;

                console.log(element.val());
                if(element.val() == "") {
                  element.val("");
                  sessionStorage.setItem('currentInputVal',"");

                }  else if(reg.test(curVal) == false) {
                  element.val(sessionStorage.getItem('currentInputVal'));

                } else if(element.val().length > 10) {
                  element.val(oldValue);

                } else {
                  sessionStorage.setItem('currentInputVal',element.val());

                }


            });

            element.bind('blur',function() {

                var output = 0;
				 
				var elementData = angular.element(element)[0].dataset;
				var multiplier = 1;
				
				for(var i = 0; i < elementData.decimal; i++){
						multiplier *= 10;
				}
				
				var roundedValue = Math.floor(parseFloat(element.val()) * multiplier);
				
				/*if(elementData.round.trim().toLowerCase() == "up"){
					roundedValue =  Math.ceil(parseFloat(element.val()) * multiplier);
				} */
				
				output = roundedValue/multiplier;
				
				element.val(output);
				
                sessionStorage.setItem('currentInputVal',"");
            });

            element.bind('click', function(e) {
              e.target.select();
            });
          }
        };
      });
      
})();