/* 
	Service to get network information
		
	
 */
(function(){
	'use strict';
	
	angular
		.module('app')
		.factory('Popup', Body);
		
		function Body($ionicPopup){
			var service = {
				displayPopup : displayPopup
			};
			
			return service;
			
			
			function displayPopup(options){
				var _popupOptions = {
					cssClass: 'custom-popup',
					templateUrl: "html/templates/pop-up.html",
					scope: options.scope,
					callbackForYes : options.callbackForYes,
					callbackForNo : options.callbackForNo,
					cancelText: options.cancelText,
					okText: options.okText,
					alertCallback : options.alertCallback
				}
				
				if(options.type == "confirm"){
					var confirmPopup  = $ionicPopup.confirm(_popupOptions);
					
					confirmPopup.then( function(res) {
						if(res == true) {
							_popupOptions.callbackForYes();
						} else {
							_popupOptions.callbackForNo();
						}
					});
				} else {
					var alertPopup = $ionicPopup.alert(_popupOptions);
					alertPopup.then(function(){
						_popupOptions.alertCallback();
					})
				}
			}
		}
})();