(function(){
    'use strict';
   
  angular
      .module('app')
      .directive('stopEvent', function () {
          function stopEvent(e) {
            e.stopPropagation();
          }
          return {
            restrict: 'A',
            link: function (scope, element, attr) {
              element.bind(attr.stopEvent, stopEvent);
            }
          };
        });
      
})();