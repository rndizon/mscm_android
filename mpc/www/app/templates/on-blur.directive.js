(function(){
    'use strict';
   
  angular
      .module('app')
      .directive('onBlur', function () {
        return {
          restrict: 'EA',
          link: function (scope, element, attrs) {
            
                element.bind('blur', function () {
                    scope.$apply(attrs.ngBlur);
                });

          
          }
        };
      });
      
})();