(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('pickForParDownload', Body);

		function Body($state,$scope,CRUD,LocalData) {
			
			var vm = this;

			vm.companyHeader = LocalData.loadData("PFPCompanyName");
			vm.companyHeaderID = LocalData.loadData("PFPCompanyID");
			vm.currentUser = LocalData.loadSession('currentUser');
			var users = JSON.parse(LocalData.loadData('users'));
			vm.localStorShowHelp = users[vm.currentUser].isShowHelp;

			$scope.$on('$ionicView.afterEnter', function() {


				//show loading icon
				/*$ionicLoading.show({
					templateUrl: 'html/templates/spinner.html'
				}); */  
				var queryParams = {};
			    queryParams.CompanyID = vm.companyHeaderID;

			    	CRUD.select("PFP_Location_Table","*", queryParams)
						.then(function(result){
				 		 	vm.pickForPar = result;
				 		 	
				 	});
				   
			   });

			vm.goTo = function(url) {
			  $state.go(url);
			}
		}
		
		
		
})();
