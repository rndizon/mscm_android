(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('PFPLocationChangeController', Body);

		function Body($state,
					  $scope,
					  $ionicLoading,
					  $ionicViewSwitcher,
					  $ionicModal, 
					  filterFilter,
					  $ionicHistory,
					  CRUD, 
					  DmeManager,
					  LocalData
					  ) 
		{
					
			var vm = this;
			vm.itemList = []; 
			vm.defaultCompanyID = LocalData.loadData("PFPCompanyID");
			vm.defaultCompanyName = LocalData.loadData("PFPCompanyName");
			vm.inventoryLocationID = LocalData.loadData("defInventoryLocationID");
			vm.inventoryLocationDesc = LocalData.loadData("defInventoryLocationDesc");
			vm.searchText = ""; //initialize
			vm.currentUser = LocalData.loadSession('currentUser');
			var users = JSON.parse(LocalData.loadData('users'));
			vm.localStorShowHelp = users[vm.currentUser].isShowHelp;

			     	
			$scope.$on('$ionicView.afterEnter', function() {
			    	
				var itemParams = {};

				itemParams.CompanyID = vm.defaultCompanyID;

				CRUD.select("PFP_Inventory_Location_Table","*",itemParams)
				 .then(function(result){
		 		 	 	vm.itemList = result;
		 		});
				
			});

			vm.goBack = function(){

					$ionicHistory.goBack();
			};

			vm.saveLocationSelected = function () {
				
				LocalData.saveData("defInventoryLocationID", vm.inventoryLocationID);

				Object.keys(vm.itemList).forEach(function(idx){
					if (vm.itemList[idx].InventoryLocationID == vm.inventoryLocationID) {
						LocalData.saveData("defInventoryLocationDesc", vm.itemList[idx].InventoryLocationDesc);
					}

				});
				$state.go('app.pick-for-par');
				
				
			};
		     vm.clearSearch = function(){
				vm.searchText = '';
			}; 

		}
				
		
})();
