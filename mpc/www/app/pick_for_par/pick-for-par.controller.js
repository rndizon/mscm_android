(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('PickForParController', Body);

		function Body($state,$rootScope,
					  $scope,
					  filterFilter,
					  $ionicPopover,$ionicLoading,
					  $ionicViewSwitcher,$timeout,
					  $cordovaToast,
					  PFPService,
					  LocalData,
					  CommonInfoService,
					  CRUD,CONSTANT,
					  $ionicPlatform,$ionicPopup) 
		{

			var vm = this;
			vm.checkboxClicked = checkboxClicked;
			vm.selectAllCheckbox = selectAllCheckbox;
			vm.unSelectAllCheckbox = unSelectAllCheckbox;
			vm.pickForParOption = pickForParOption;
			vm.closePopOver = closePopOver;
			vm.goTo = goTo;
			vm.pickForPar = [];
			vm.goBackToPickforPar = goBackToPickforPar;
			vm.goToAddParCount = goToAddParCount;
			vm.companyHeader = LocalData.loadData("PFPCompanyName");
			vm.companyHeaderID = LocalData.loadData("PFPCompanyID");
			vm.inventoryLocationID = LocalData.loadData("defInventoryLocationID");
			vm.inventoryLocationDesc = LocalData.loadData("defInventoryLocationDesc");
			vm.showOptionToDelete = false;
			vm.showOptionSingleSweeping = false;
			vm.selectedAllParforms = [];
			vm.ifCheckboxTrue = 0;
		    vm.showHideDeleteButtons = false;
		    vm.optionMore = optionMore;
		    vm.selectAll = selectAll;
		    vm.clearAll = clearAll;
		    vm.showClearAll = showClearAll;
		    vm.goToPickForParDetails = goToPickForParDetails;
		    vm.pickForParSelectedPrinter = LocalData.loadData("pickForParSelectedPrinter");
			vm.currentUser = LocalData.loadSession('currentUser');
			vm.users = JSON.parse(LocalData.loadData('users'));
			vm.localStorShowHelp = vm.users[vm.currentUser].isShowHelp;		    

			var backBtn = $ionicPlatform.registerBackButtonAction(function(event) {
				    if (true) { // your check here
				    	if(vm.showOptionToDelete == false) {
							$ionicPopup.confirm({
								template: 'Are you sure you want to sign out ?'
							}).then(function(res) {
								if (res) {
									ionic.Platform.exitApp();
								}
							});
				    	} else {
						    document.getElementById('closeOption').click();
				    	}
				    }
				  }, 150);  

   			$scope.$on('$destroy', backBtn);

		    /*if(vm.firstEnter == "1") {
		    	vm.pickForParSelectedPrinter = vm.users[vm.currentUser].defaultPickForParReportPrinter;
		    	LocalData.saveData("pickForParSelectedPrinter",vm.pickForParSelectedPrinter);
		    }*/

		    $scope.$on('$ionicView.beforeLeave',function() {
		    })



			$scope.$on('$ionicView.afterEnter', function() {

				var SQLiteFlag = LocalData.loadData("SQLitePFPLoaded");

				if(!SQLiteFlag) {
		     		//show loading icon
					$ionicLoading.show({
						templateUrl: CONSTANT.SpinnerTemplate
					});
		     		vm.getHeadersCall();
					
				} else {
					getCompanyList();
				}

		     	
				
				   
			});
			vm.changeStart = 0;
			$scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){ 

					if(toState.name.indexOf("pick-for-par") === -1){// Reset $rootScope.isOpenAddScreen if the redirection is not related to PFP module
						$rootScope.isOpenAddScreen = 1;
					}
					if(toState.name == "app.pick-for-par-printer-list"){
						$state.go(toState.name);
					}else if(!toParams.ParLocationID && vm.changeStart < 1 && toState.name == "app.pick-for-par-change-company" && vm.pickForPar.length > 0){ //Go to change company without counted items
			          		event.preventDefault();
			          		vm.popUpMsg = "pfp-change-company";
			          		$ionicPopup.confirm({
		            			cssClass: 'custom-popup',
								templateUrl: 'html/templates/pop-up.html',
								okText: 'Yes',
								cancelText: 'No',
								scope: $scope,
							}).then(function(res) {
								if(res) {
									vm.changeStart++;
									$state.go(toState.name,{"deletelist":"1"});
												
								} else {
									event.preventDefault(); 
								}
							});
			          	} else if(!toParams.ParLocationID && vm.changeStart < 1 && toState.name == "app.pick-for-par-change-location" && vm.pickForPar.length > 0){ //Go to change location without counted items
			          		event.preventDefault();
			          		vm.popUpMsg = "pfp-change-inventory-location";
			          		$ionicPopup.confirm({
		            			cssClass: 'custom-popup',
								templateUrl: 'html/templates/pop-up.html',
								okText: 'Yes',
								cancelText: 'No',
								scope: $scope,
							}).then(function(res) {
								if(res) {
									vm.changeStart++;
									$state.go(toState.name,{"deletelist":"1"});
												
								} else {
									event.preventDefault(); 
								}
							});
			          	} else if(!toParams.ParLocationID && vm.showSendButton && vm.changeStart < 1 && toState.name != "pick-for-par-add-parform"){
							
							event.preventDefault(); 
							
							if(toState.name == "app.pick-for-par-change-company"){
								vm.popUpMsg = "pfp-change-company";
							} else if(toState.name == "app.pick-for-par-change-location"){
								vm.popUpMsg = "pfp-change-inventory-location";
							} else {
								vm.popUpMsg = "pfp-pending-unsent";
							}
							
							$ionicPopup.confirm({
		            			cssClass: 'custom-popup',
								templateUrl: 'html/templates/pop-up.html',
								okText: 'Yes',
								cancelText: 'No',
								scope: $scope,
							}).then(function(res) {
								if(res) {
									vm.changeStart++;

									if(toState.name != "app.pick-for-par-change-company" || toState.name != "app.pick-for-par-change-location"){ // If is not change company/location screen
										
									 	/*ParCountService
									 		.deleteCurrentParforms()
									 		.then(function(){*/
									 			/*if(LocalData.loadData("DownloadedItems")){ 
										    		LocalData.saveData("DownloadedItems",[]); //This session storage is for checking items if it is already exists in Par_Item_Table
										    	}*/
								 				$state.go(toState.name);
									 		//});

									} else{ // Set Flag delete par form list if it is going to change company/location screen. Deletion of Parfor list should be after selecting new company/location
										$state.go(toState.name,{"deletelist":"1"});
									}

											
								} else {
									//alert("NO");
									event.preventDefault(); 
								}
							});


						}
			});
			function getSelectedPar(){

				var query = "select *,";
					query+= "(select count(ItemID) FROM PFP_Task_Table where ParLocationID = p_table.ParLocationID) as ItemCount, ";
					query+= "(select count(*) FROM PFP_Transaction_Table where ParLocationID = p_table.ParLocationID) as TotalItemCounted ";
					query+= "from PFP_Par_Table p_table ";
					query+= "where IsSelected = 1 AND ";
					query+= "InventoryLocationID = "+vm.inventoryLocationID;

				CRUD.inlineQuery(query)
				.then(function(data){
					console.log(data);
					vm.pickForPar = data;
					vm.showHideSendButton(data);
					$ionicLoading.hide();

					if(vm.pickForPar.length < 1 && $rootScope.isOpenAddScreen > 0){ //Open add PFP parforms if list is empty
						$timeout(function(){
								goToAddParCount();
							},1000);
					}

				});

			}

			function getCompanyList() {

		     	CRUD.select("PFP_Company_Table","*")
					.then(function(result){
					 console.log('Callback: select result:' + result[0].CompanyDesc);
					
					if(!LocalData.loadData("PFPCompanyName")){
						vm.companyHeader = result[0].CompanyDesc;
						vm.companyHeaderID = result[0].CompanyID;
						LocalData.saveData("PFPCompanyID", vm.companyHeaderID);
						LocalData.saveData("PFPCompanyName",vm.companyHeader);

						
					}

					if(!LocalData.loadData("defInventoryLocationDesc")){

						var queryParams = {};
						queryParams.CompanyID = vm.companyHeaderID;
						CRUD.select("PFP_Inventory_Location_Table","*")
						.then(function(res){
							vm.inventoryLocationID = res[0].InventoryLocationID;
							vm.inventoryLocationDesc = res[0].InventoryLocationDesc;
							LocalData.saveData("defInventoryLocationID", vm.inventoryLocationID);
							LocalData.saveData("defInventoryLocationDesc",vm.inventoryLocationDesc);
							getSelectedPar();
						});
					}else{
						getSelectedPar();
					}

					
				});
		     
		     }
		    vm.showHideSendButton = function(result) {
		     	var stopLoop = false;
		     	result.forEach(function(index) {
		    		
		    		if(!stopLoop){
		    			if(index.TotalItemCounted <= index.ItemCount && index.TotalItemCounted > 0) {
			     			vm.showSendButton = true;
			     			stopLoop = true;

			     		} else {
			     			vm.showSendButton = false;
			     		}
		    		}
		     		
		     	});
		    };
		 	vm.getHeadersCall = function(){

		 			PFPService
		 			.getPFPHeaders()
		 			.then(function(res){
		 				$ionicLoading.hide();
		 				PFPService
		 				.savePFPHeaders(res)
		 				.then(function(data){
		 					getCompanyList();
		 				});

		 				
		 			});
		 	};

			function goTo(goToUrl,_locationCode) {
				if(_locationCode || _locationCode != ""){
					var params = {LocationCode:_locationCode};
					$ionicViewSwitcher.nextDirection('slideUp');
					$state.go(goToUrl, params);
				}else{
					$state.go(goToUrl);
				}
			}

			function goToPickForParDetails(_ParLocationID,_ParLocationDesc) {

				var params = {ParLocationID:_ParLocationID,ParLocationDesc:_ParLocationDesc};
				$ionicViewSwitcher.nextDirection('slideUp');
				$state.go('app.downloaded-pick-for-par-details', params);
			}

			function goBackToPickforPar() {
				$ionicViewSwitcher.nextDirection('slideDown');
				$state.go("app.pick-for-par");
			}

			function goToAddParCount() {
				$rootScope.isOpenAddScreen = 0;
				$ionicViewSwitcher.nextDirection('slideUp');
				$state.go("pick-for-par-add-parform");
			}

			function checkboxClicked() {
			    vm.ifCheckboxTrue = filterFilter( vm.pickForPar, {val:true}).length;
			    vm.selectedAllParforms  = filterFilter( vm.pickForPar, {val:true});
			}

			function selectAllCheckbox() {
			    var pickForParLen = vm.pickForPar.length;

			    for(var x = 0; x < pickForParLen; x++) {
			    	vm.pickForPar[x].val = true;
			    }
			    
			    vm.popover.hide();
			    vm.checkboxClicked(); // Show subheader 
			}

			function unSelectAllCheckbox() {
			    var pickForParLen =  vm.pickForPar.length;

			    for(var x = 0; x < pickForParLen; x++) {	
			    	vm.pickForPar[x].val = false;
			    }

				vm.checkboxClicked(); // Hide subheader 

	    	}

	    	function pickForParOption($event) {

		    	$ionicPopover.fromTemplateUrl('html/templates/options-more.html', {
				        scope: $scope
				}).then(function(popover) {
				        vm.popover = popover;
				        vm.popUpType = "pick-for-par";
				        popover.show($event);
				}); 

	    	}

	    	function closePopOver() {
	    		vm.popover.hide();
	    	}

	    	vm.clearSearch = function(){
				vm.searchText = '';
			};
			vm.showHideDelete = function(isForSingleSweep){

				if(isForSingleSweep || typeof isForSingleSweep != "undefined"){
					vm.showOptionSingleSweeping = !vm.showOptionSingleSweeping;
				}else{
					vm.showOptionToDelete = !vm.showOptionToDelete;
					vm.showHideDeleteButtons = !vm.showHideDeleteButtons;
					vm.ifCheckboxTrue = 0;
					//vm.unSelectAllCheckbox();
				}
				
				vm.popover.hide();
			};
			function optionMore($event,popupType) {
		    	$ionicPopover.fromTemplateUrl('html/templates/options-more.html', {
				        scope: $scope
				}).then(function(popover) {
				        vm.popover = popover;
				        vm.popUpType = (popupType) ? popupType : "parcount-edit";
				        popover.show($event);
				});
			}
			vm.selectedParCount = function(){
		    	return filterFilter( vm.pickForPar, {val:true}).length;
		    };

			function selectAll() {
			    var parCountLen =  vm.pickForPar.length;

			    for(var x = 0; x < parCountLen; x++) {
			    	vm.pickForPar[x].val = true;
			    }
			}

			function clearAll() {
			    var parCountLen =  vm.pickForPar.length;

			    for(var x = 0; x < parCountLen; x++) {
			    	vm.pickForPar[x].val = false;
			    }
			}

			function showClearAll() {

			    if(filterFilter( vm.pickForPar, {val:true}).length == vm.pickForPar.length) {
			    	return true;
			    } else {
			    	return false;
			    }
			}
			vm.continueToSingleSweeping = function(){
				var selectedAllParforms  = filterFilter( vm.pickForPar, {val:true});
			    CommonInfoService.setSelectedParforms(selectedAllParforms);
			    vm.goToPickForParDetails(selectedAllParforms[0].ParLocationID,selectedAllParforms[0].ParLocationDesc);
			};
			vm.deleteParForms = function(){
				var selectedAllParforms  = filterFilter( vm.pickForPar, {val:true});
			    CommonInfoService.setSelectedParforms(selectedAllParforms);
				var LocationCodeTobeDeleted = [];

				CommonInfoService.getSelectedParforms().forEach(function(v){
					LocationCodeTobeDeleted.push(v.ParLocationID);
				});

				

				var query = "UPDATE PFP_Par_Table SET IsSelected = 0 WHERE ParLocationID IN ('";
		 			query += LocationCodeTobeDeleted.join("','");
		 			query += "');";

				CRUD.inlineQuery(query)
				    .then(function(result) {
				    	vm.clearAll();
				    	$state.go($state.current, {}, {reload: true}); //refresh screen to update list view
						
				},function errorCallback(response) {
					    //$ionicLoading.hide(); 	
							   		 
				});

			};
			vm.SendFormToServer = function(_ParLocationID){
				if(!vm.pickForParSelectedPrinter || typeof vm.pickForParSelectedPrinter == "undefined"){
					vm.popUpMsg = "pfp-no-printer"
					var confirmPopup = $ionicPopup.alert({
							cssClass: 'custom-popup',
							templateUrl: 'html/templates/pop-up.html',
							scope: $scope
					});

					return false;
				}
				

				var ParLocationID = _ParLocationID || 0;

				//show loading icon
				$ionicLoading.show({
					templateUrl: CONSTANT.SpinnerTemplate
				});
				
				var ItemQuery = "select * from PFP_Transaction_Table";
				if(ParLocationID != "0"){
                	ItemQuery += " where ParLocationID ="+ParLocationID;
                }
				CRUD.inlineQuery(ItemQuery)
                    .then(function(itemResult) {

                    	var requestString = "<syncPFP>";
                    		requestString += '<username>'+LocalData.loadSession('SoapUsername')+'</username>';	

	                    var locIDArr = [];
	                    	angular.forEach(itemResult, function(item,key){
	                            
	                            if(locIDArr.indexOf(item.ParLocationID) == -1) {
	                                    console.log(key);

	                                    if(key != 0) {
	                                        requestString += "</par>";
	                                    }
	                                    requestString += '<par parlocid="'+ item.ParLocationID +'" invlocid="'+vm.inventoryLocationID+'">';
	                                   // vm.users[vm.currentUser].lastParformsUsed.push(item.LocationID); //Save LocationID for Last Par Form used flow
	                            } 
	                            requestString += '<pfp taskid="'+item.TaskID+'" binno="" uom="" timestamp="" gtin="" hibc="" pickedqty="'+item.PickedQuantity+'"/>';
	                            locIDArr[key] = item.ParLocationID;

	                            if(locIDArr.indexOf(item.ParLocationID) == -1) {
	                                    requestString += '</par>';
	                            } 
	                                            

					        });

	                    	requestString += '</par><printer><document id=""/></printer>';	
					        requestString += "</syncPFP>";
					        

					        console.log(requestString);

					        PFPService
				        	.syncPFP(requestString)
				        	.then(function(data){
				        		var xmlString = (new XMLSerializer()).serializeToString(data);
				        		//alert(xmlString);
				        		if(xmlString.indexOf("resultsuccess") != -1){

				        			//alert("success");
		        					//Update PFP_TransHistory_Table
								    locIDArr.sort();
									var currentLoc = null;
								    var itemCount = 0;
								    for (var i = 0; i < locIDArr.length; i++) {
								        if (locIDArr[i] != currentLoc) {
								            if (itemCount > 0) {
								                console.log(currentLoc + ' comes1 --> ' + itemCount + ' times<br>');
								                PFPService.saveTransactionHistory(currentLoc,itemCount);

								            }
								            currentLoc = locIDArr[i];
								            itemCount = 1;
								        } else {
								            itemCount++;
								        }
								    }
								    if (itemCount > 0) {
								        console.log(currentLoc + ' comes2 --> ' + itemCount + ' times');
								        PFPService.saveTransactionHistory(currentLoc,itemCount);
								    }

								    
								    //Clear Transaction Table and refresh the list
								    CommonInfoService
				        				.clearTable("PFP_Transaction_Table")
				        				.then(function(){
				        					$cordovaToast.showLongCenter('Tickets sent successfully to server.');
				        					//LocalData.saveData('users',JSON.stringify(vm.users)); // Update Users Localstorage data
				        					getSelectedPar();
				        				});

		        						

				        			
				        		}else{    
				        			getSelectedPar();
				        			//alert("failed");
				        		}
				        	});



                    });

				/*$cordovaToast.showLongCenter('Successfully sent to Server').then(function(success) {
				    
						var query = "UPDATE PFP_Par_Table SET TotalItemCounted = 0";

						CRUD.inlineQuery(query)
						    .then(function(result) {
						    	$state.go($state.current, {}, {reload: true}); //refresh screen to update list view
								
						},function errorCallback(response) {
							    //$ionicLoading.hide(); 	
									   		 
						});


				  }, function (error) {
				    // error
				  });*/
			};

		}
		
		
		
})();
