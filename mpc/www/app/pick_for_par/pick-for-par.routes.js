(function(){
 	'use strict';
 	
		angular
		    .module('app')
			.config(function($stateProvider, $urlRouterProvider) {
		     
		     $stateProvider
		      .state("app.pick-for-par", {
		      	cache: false,
			    url: "/pick-for-par",
                changeColor: 'bar-turquoise-8', 
			      views :{
			      	'menu-content':{
					      templateUrl: "html/pick_for_par/pick-for-par.html",
					      controller: "PickForParController as vm"
					 }
				  }
			      
		      })

		      .state("pick-for-par-add-parform", {
		      	url: "/pick-for-par-add-parform",
		      	cache: false,
                changeColor: 'bar-turquoise-8', 
		     // 	views: {
		     // 		'menu-content': {
		      			templateUrl: "html/pick_for_par/pick-for-par-add-parform.html",
		      			controller: "PickForParAddParForm as vm"
		     // 		}
		     // 	}
		      })

		      .state("app.downloaded-pick-for-par-details", {
		      	url: "/downloaded-pick-for-par-details/:ParLocationID/:ParLocationDesc",
                changeColor: 'bar-turquoise-8', 
                cache: false,
                views: {
		      		'menu-content': {
		      			templateUrl: "html/pick_for_par/downloaded-pick-for-par-details.html",
		      			controller: "PickForParItemDetailController as vm"
		      		}
		      	}
		      		
		      })

		      .state("app.downloaded-pick-for-par-details-list", {
		      	url: "/downloaded-pick-for-par-details-list/:ParLocationID/:ParLocationDesc",
                changeColor: 'bar-turquoise-8',
                cache: false,
                views: {
		      		'menu-content': {
		      			templateUrl: "html/pick_for_par/downloaded-pick-for-par-details-list.html",
		      			controller: "PickForParItemDetailListController as vm"
		      		}
		      	}
		      		
		      })

		      .state("app.pick-for-par-printer-list", {
		      	url: "/pick-for-par-printer-list",
                changeColor: 'bar-turquoise-8',
                cache: false,
                views: {
		      		'menu-content': {
		      			templateUrl: "html/settings/settings-printer-list.html",
		      			controller: "PickForParChangePrinter as vm"
		      		}
		      	}
		      		
		      });

		     });
		     
})();