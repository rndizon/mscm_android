(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('PFPCompanyChangeController', Body);

		function Body($state,
					  $scope,
					  $ionicLoading,
					  $ionicViewSwitcher,
					  $ionicModal, 
					  filterFilter,
					  $ionicHistory,
					  CRUD, 
					  DmeManager,
					  LocalData,
					  CONSTANT
					  ) 
		{
					
			var vm = this;
			vm.companyList = []; 
			vm.searchText = ""; //initialize
			vm.currentUser = LocalData.loadSession('currentUser');
			var users = JSON.parse(LocalData.loadData('users'));
			vm.localStorShowHelp = users[vm.currentUser].isShowHelp;
			vm.doRefresh = doRefresh;

			     	
			$scope.$on('$ionicView.afterEnter', function() {
			    	
				vm.defaultCompanyID = LocalData.loadData("PFPCompanyID");
				vm.defaultCompanyName = LocalData.loadData("PFPCompanyName");
				vm.showCompanyList();
				
			});

			vm.goBack = function(){

					$ionicHistory.goBack();
			}

			vm.showCompanyList = function() {
				var companyParams = {};

				if (vm.searchText.trim()) {
					companyParams.CompanyID = vm.searchText.trim();
				}

				CRUD.select("PFP_Company_Table","*",companyParams)
				 .then(function(result){
		 		 	 	console.log('Callback: select result:' + result[0].CompanyDesc);
		 		 	 	vm.companyList = result;
		 		});

			};

			vm.saveCompanySelected = function () {
				
				LocalData.saveData("PFPCompanyID", vm.defaultCompanyID);

				Object.keys(vm.companyList).forEach(function(idx){
					if (vm.companyList[idx].CompanyID == vm.defaultCompanyID) {
						LocalData.saveData("PFPCompanyName", vm.companyList[idx].CompanyDesc);
					}

				});
				$state.go('app.pick-for-par');
				
				
			};
		     vm.clearSearch = function(){
				vm.searchText = '';
			}; 

			function doRefresh() {

				$ionicLoading.show({
					templateUrl: CONSTANT.SpinnerTemplate
				});

				vm.showCompanyList();

				$scope.$broadcast('scroll.refreshComplete');
				$ionicLoading.hide();

			}

		}
				
		
})();
