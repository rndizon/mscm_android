(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('PickForParAddParForm', Body);

		function Body($state,$q,
					  $scope,
					  filterFilter,
					  $ionicPopover,
					  $ionicViewSwitcher,
					  CRUD,
					  CommonInfoService,
					  LocalData,
					  $ionicPlatform) {
			
			var vm = this;
			vm.pickForParOption = pickForParOption;
			vm.goTo = goTo;
			vm.goBackToPickforPar = goBackToPickforPar;
			vm.companyHeader = LocalData.loadData("PFPCompanyName");
			vm.companyHeaderID = LocalData.loadData("PFPCompanyID");
			vm.selectAll = selectAll;
			vm.clearAll = clearAll;
			vm.showClearAll = showClearAll;
			vm.optionMore = optionMore;
			vm.selectedPickForPar = selectedPickForPar;
			vm.currentUser = LocalData.loadSession('currentUser');
			var users = JSON.parse(LocalData.loadData('users'));
			vm.localStorShowHelp = users[vm.currentUser].isShowHelp;

	    	var backBtn = $ionicPlatform.registerBackButtonAction(function(event) {
				    if (true) { // your check here
						    document.getElementById('backBtn').click();
				    }
				  }, 150);  

   			$scope.$on('$destroy', backBtn);


			$scope.$on('$ionicView.afterEnter', function() {

				var queryParamsPFP = {};
				queryParamsPFP.IsSelected = 0;
				queryParamsPFP.InventoryLocationID = LocalData.loadData("defInventoryLocationID");
				CRUD.select("PFP_Par_Table","*",queryParamsPFP)
				.then(function(data){
					console.log(data);
					vm.pickForPar = data;
				});
				   
			});


			function goTo(url) {
			  $state.go(url);
			}
			
			function goBackToPickforPar() {
				$ionicViewSwitcher.nextDirection('slideDown');
				$state.go("app.pick-for-par");
			}
	    	function pickForParOption($event) {

		    	$ionicPopover.fromTemplateUrl('html/templates/options-more.html', {
				        scope: $scope
				}).then(function(popover) {
				        vm.popover = popover;
				        vm.popUpType = "pick-for-par";
				        popover.show($event);
				}); 
	    	}


	    	function optionMore($event,popupType) {
		    	$ionicPopover.fromTemplateUrl('html/templates/options-more.html', {
				        scope: $scope
				}).then(function(popover) {
				        vm.popover = popover;
				        vm.popUpType = (popupType) ? popupType : "parcount-edit";
				        popover.show($event);
				});
			}
			vm.checkboxClicked = function() {

			    CommonInfoService.setSelectedParforms(filterFilter( vm.pickForPar, {val:true}));	
			};
			
			function selectAll() {
			    var parformLen =  vm.pickForPar.length;

			    for(var x = 0; x < parformLen; x++) {
			    	vm.pickForPar[x].val = true;
			    }
			    vm.checkboxClicked();
			}

			function selectedPickForPar(){
		    	return filterFilter( vm.pickForPar, {val:true}).length;
		    };


			function clearAll() {
			    var parformLen =  vm.pickForPar.length;

			    for(var x = 0; x < parformLen; x++) {
			    	vm.pickForPar[x].val = false;
			    }
			}

			function showClearAll() {

			    if(filterFilter( vm.pickForPar, {val:true}).length == vm.pickForPar.length) {
			    	return true;
			    } else {
			    	return false;
			    }
			}

	    	function closePopOver() {
	    		vm.popover.hide();
	    	}
	    	vm.clearSearch = function(){
				vm.searchText = '';
			};


			vm.addSelectPar = function(){
	    		var SelectedPar = CommonInfoService.getSelectedParforms();

	    		var promises = [];
		    	var deffered = $q.defer();

    			Object.keys(SelectedPar).forEach(function(index) {

    				var queryParams = {};
			    	queryParams.UID = SelectedPar[index].UID;
    				CRUD.update("PFP_Par_Table","IsSelected",1,queryParams)
    					.then(function(result){
    						deffered.resolve(result);
    						promises.push(deffered.promise);
    					});

					
				});

				$q.all(promises)
				  .then(function(){
				  		vm.goTo("app.pick-for-par","slideDown");
				 });

	    	};

		}
		
		
		
})();
