(function(){
	'use strict';
	
	angular
		.module("app")
		.controller("PickForParItemDetailListController", Body);
	
	function Body($state,$q,
				$ionicViewSwitcher,
				$ionicPopover,
				$ionicPopup,
				$scope,
				$stateParams,
				CRUD,
				LocalData,PFPService,CommonInfoService) {
		
			var vm = this;
			vm.goTo = goTo;
			vm.pickForParDetailsOption = pickForParDetailsOption;
			vm.pickForPar = [];
			vm.parLocations = [];
			vm.companyHeader = LocalData.loadData("PFPCompanyName");
			vm.companyHeaderID = LocalData.loadData("PFPCompanyID");
			vm.inventoryLocationID = LocalData.loadData("defInventoryLocationID");
			vm.optionParLocation = optionParLocation;
			vm.saveCurrentLocation = saveCurrentLocation;
			vm.ParLocationID = $stateParams.ParLocationID;
			vm.ParLocationDesc = $stateParams.ParLocationDesc;
			vm.CurrentParLocation = '';
			vm.CurrentParLocationPopUp = null;
			vm.TotalItemCounted = 0;
			vm.highlightField = highlightField;
			vm.currentUser = LocalData.loadSession('currentUser');
			var users = JSON.parse(LocalData.loadData('users'));
			vm.localStorShowHelp = users[vm.currentUser].isShowHelp;
			vm.saveValues = PFPService.saveValues;

	      	$scope.$on('$ionicView.afterEnter', function() {

				getTasks(vm.ParLocationID);
				//Populate drop down		
				getSelectedPar();
				   
			});

	      	

			function getTasks(_ParLocationID){

				var query = "select *,pfpItem.TaskID as TaskID,pfpItem.ItemID as ItemID,pfpItem.BinNumber as BinNumber,pfpItem.ParLocationID as ParLocationID from PFP_Task_Table as pfpItem left join PFP_Transaction_Table as trans ";
		 			query +="on pfpItem.ParLocationID = trans.ParLocationID where pfpItem.ParLocationID = "+_ParLocationID+" GROUP BY pfpItem.TaskID";
		 			CRUD.inlineQuery(query)
				    .then(function(result) {
				    	vm.pickForPar = result;
				    });

			}
			function getSelectedPar(){

				var LocationCodeForSweepingCount = [];
				CommonInfoService.getSelectedParforms().forEach(function(v){
					LocationCodeForSweepingCount.push(v.ParLocationID);
				});

				var query = "select ParLocationID,ParLocationCode,ParLocationDesc ";
					query+= "from PFP_Par_Table ";
					query+= "where IsSelected = 1 AND ";

					if(LocationCodeForSweepingCount.length > 0){
						query+= "ParLocationID IN ("+LocationCodeForSweepingCount.join(', ')+") AND ";
					}

					query+= "InventoryLocationID = "+vm.inventoryLocationID;
				CRUD.inlineQuery(query)
				.then(function(data){
					vm.parLocations = data;
					vm.CurrentParLocation = (LocationCodeForSweepingCount.length > 0) ? data[0].ParLocationDesc : vm.ParLocationDesc;
				});

			}
			function saveCurrentLocation(_ParLocationID,_Currentlocation){
				vm.CurrentParLocation = _Currentlocation;
				getTasks(_ParLocationID);
				//vm.CurrentParLocationPopUp.close();
			}
	      	//navigate to Other Page
			function goTo(goToUrl,_ParLocationID,_ParLocationDesc) {

			  	if(_ParLocationID && _ParLocationID > 0){
					var params = {ParLocationID:_ParLocationID,ParLocationDesc:_ParLocationDesc};
					$state.go(goToUrl,params);
				}else{

					$ionicViewSwitcher.nextDirection('slideDown');
					$state.go(goToUrl);
				}
			}

		    function highlightField($event) {
		    	$event.target.select();
		    }
			

	    	function pickForParDetailsOption($event) {
		    	$ionicPopover.fromTemplateUrl('html/templates/options-more.html', {
				        scope: $scope
				}).then(function(popover) {
				        vm.popover = popover;
				        vm.popUpType = "pick-for-par-details";
				        popover.show($event);
				}); 
	    	}
	    	function optionParLocation($event,popupType) {
			  // An elaborate, custom popup
			/*  vm.CurrentParLocationPopUp = $ionicPopup.show({
			//    templateUrl: 'html/templates/options-more.html',
				templateUrl: 'html/pick_for_par/par-location-pop-up.html',
			    title: 'Change Par Location',scope: $scope,
			    buttons: null
			  }); */

                $ionicPopover.fromTemplateUrl('html/templates/options-more.html', {
                        scope: $scope
                }).then(function(popover) {
                        vm.popover = popover;
                        vm.popUpType = (popupType) ? popupType : "par-location-pop-up";
                        popover.show($event);
                });

			 }
			 vm.clearSearch = function(){
				vm.searchText = '';
			};


	}
	
	
	
	
	
	
})();