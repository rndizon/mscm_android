(function(){
    'use strict';
	 
	angular
	    .module('app')
	    .factory('PFPService', Body);

	    function Body($q,
	    			  CRUD,
	    			  LocalData,
	    			  CM
	    			  ) 
	    {
		  
		    var service = {
		        getPFPHeaders:getPFPHeaders,
		        savePFPHeaders:savePFPHeaders,
		        getPFPDetails:getPFPDetails,
		        savePFPDetails:savePFPDetails,
		        syncPFP:syncPFP,
		        saveTransactionHistory:saveTransactionHistory,
		        saveValues:saveValues
		    },
			requestToken = LocalData.loadSession("SoapToken");

		    return service;
		 	
		 	

		 	function getPFPHeaders(){

		 		var args = {
		 			URL:    "PFPService",
		 			Action: "getPFPHeaders",
		 			Params: {"requestString":"<getPFPheaders />","token":requestToken}
		 		};

		 		return CM.doCommsCall("soap",args);
		 	}
		 	function savePFPHeaders(response){
		 		var promises = [];
		    	var deffered = $q.defer();
		 		var value = xmlToJson(response);
		 		var xmlResponse = value.root.body.getPFPHeadersResponse.getPFPHeadersReturn;
		 		var object = ReturnDataObject(xmlResponse);
		 		var taskResponse = object.getPFPheadersresponse.tasks;
				var arryTasks = [];
				var UserID = LocalData.loadSession("SoapUsername");

				if(taskResponse instanceof Array){
				  arryTasks = taskResponse;
				}else{
				  arryTasks.push(taskResponse);  
				}

				/*Insert Companies*/
				var companyExists = [];
				var insertCompanyQuery = "insert into PFP_Company_Table (CompanyID,CompanyDesc,UserID) values ";
				angular.forEach(arryTasks, function(task, index) {
					var CompanyID = checkValue(task.invloc.attributes.facid);
					if(companyExists.indexOf(CompanyID) == -1){
						var CompanyDesc = checkValue(task.invloc.attributes.facdesc,"text");
					    var UserID = LocalData.loadSession("SoapUsername");

					    insertCompanyQuery += "("+CompanyID+", ";
			  	  		insertCompanyQuery += "'"+CompanyDesc+"', ";
			  	  		insertCompanyQuery += "'"+UserID+"'),";

					    /*if(index < 1){

					    	insertCompanyQuery += "select "+CompanyID+" as CompanyID, ";
				  	  		insertCompanyQuery += "'"+CompanyDesc+"' as CompanyDesc, ";
				  	  		insertCompanyQuery += "'"+UserID+"' as UserID ";
					    }else{
					    	insertCompanyQuery += "union all select "+CompanyID+", ";
				  	  		insertCompanyQuery += "'"+CompanyDesc+"', ";
				  	  		insertCompanyQuery += "'"+UserID+"' ";
					    }*/

					    companyExists.push(CompanyID);
					}



				});
				CRUD.inlineQuery(insertCompanyQuery.slice(0,-1))
		     	 	.then(function(result) {
		     	 		deffered.resolve(result);
			     	 	LocalData.saveData("SQLitePFPLoaded", true);
			     	});
			    /*End of Company insertion*/ 	

			    //promises.push(deffered.promise);

			    var invertoryLocationQuery = "insert into PFP_Inventory_Location_Table (CompanyID,InventoryLocationID,InventoryLocationCode,InventoryLocationDesc) values ";

			    angular.forEach(arryTasks, function(task, index) {
					var CompanyID = checkValue(task.invloc.attributes.facid),
						InventoryLocationID = checkValue(task.invloc.attributes.locid),
						InventoryLocationCode = checkValue(task.invloc.attributes.loccode,"text"),
						InventoryLocationDesc = checkValue(task.invloc.attributes.locdesc,"text");			    	

						savePrinter(CompanyID,InventoryLocationID); //Call getPrinter SOAP call and save it to SQLite

						invertoryLocationQuery += "("+CompanyID+", ";
			  	  		invertoryLocationQuery += InventoryLocationID+", ";
			  	  		invertoryLocationQuery += "'"+InventoryLocationCode+"', ";
			  	  		invertoryLocationQuery += "'"+InventoryLocationDesc+"'),";
						/*if(index < 1){

					    	invertoryLocationQuery += "select "+CompanyID+" as CompanyID, ";
				  	  		invertoryLocationQuery += InventoryLocationID+" as InventoryLocationID, ";
				  	  		invertoryLocationQuery += "'"+InventoryLocationCode+"' as InventoryLocationCode, ";
				  	  		invertoryLocationQuery += "'"+InventoryLocationDesc+"' as InventoryLocationDesc ";
					    }else{
					    	invertoryLocationQuery += "union all select "+CompanyID+", ";
				  	  		invertoryLocationQuery += InventoryLocationID+", ";
				  	  		invertoryLocationQuery += "'"+InventoryLocationCode+"', ";
				  	  		invertoryLocationQuery += "'"+InventoryLocationDesc+"' ";
					    }*/

					var parlocationsResponse = task.par;
				  	var arryParlocations = [];
				  	
					  	if(parlocationsResponse instanceof Array){
						  arryParlocations = parlocationsResponse;
						}else{
						  arryParlocations.push(parlocationsResponse);  
						}
    					
					var parLocationQuery = "insert into PFP_Par_Table (InventoryLocationID,ParLocationID,ParLocationCode,ParLocationDesc,IsPreviousDownloaded) values ";

						angular.forEach(arryParlocations, function(par, par_index) {
							
							var InvLocationID = InventoryLocationID,
								ParLocationID = checkValue(par.attributes.locid),
								ParLocationCode = checkValue(par.attributes.loccode,"text"),
								ParLocationDesc = checkValue(par.attributes.locdesc,"text"),
								IsPreviousDownloaded = (checkValue(par.attributes.locid) == "TRUE") ? 1 : 0;
								
								parLocationQuery += "("+InvLocationID+", ";
					  	  		parLocationQuery += ParLocationID+", ";
					  	  		parLocationQuery += "'"+ParLocationCode+"', ";
					  	  		parLocationQuery += "'"+ParLocationDesc+"', ";
					  	  		parLocationQuery += IsPreviousDownloaded+"),";
								/*if(par_index < 1){

							    	parLocationQuery += "select "+InvLocationID+" as InventoryLocationID, ";
						  	  		parLocationQuery += ParLocationID+" as ParLocationID, ";
						  	  		parLocationQuery += "'"+ParLocationCode+"' as ParLocationCode, ";
						  	  		parLocationQuery += "'"+ParLocationDesc+"' as ParLocationDesc, ";
						  	  		parLocationQuery += IsPreviousDownloaded+" as ParLocationID ";
							    }else{
							    	parLocationQuery += "union all select "+InvLocationID+", ";
							    	parLocationQuery += ParLocationID+", ";
						  	  		parLocationQuery += "'"+ParLocationCode+"', ";
						  	  		parLocationQuery += "'"+ParLocationDesc+"', ";
						  	  		parLocationQuery += IsPreviousDownloaded+" ";
							    }*/

						});
						CRUD.inlineQuery(parLocationQuery.slice(0,-1))
					  	.then(function(result){
					  			deffered.resolve(result);
					  	},function(res){
					  		deffered.resolve(res);
					  	});

			    });

			    CRUD.inlineQuery(invertoryLocationQuery.slice(0,-1))
				  	.then(function(result){
				  			deffered.resolve(result);
				  	},function(res){
				  		deffered.resolve(res);
				  	});

			  	promises.push(deffered.promise);
				
				return $q.all(promises);

			}
			function savePrinter(CompanyID,LocationID){
				var Userid = requestToken.split(":"),
					Userid = Userid[0];
		 		var args = {
		 			URL:    "PFPService",
		 			Action: "getPrinters",
		 			Params: {"strUserId":Userid,"strCompanyNo":CompanyID,"strLocationId":LocationID}
		 		};

		 		CM.doCommsCall("soap",args)
		 		  .then(function(response){
		 		  		var value = xmlToJson(response);
						var xmlResponse = value.root.body.getPrintersResponse.getPrintersReturn;
						var object = ReturnDataObject(xmlResponse);
						var printersResponse = object.getprinters.printer;
						var printersResponseArray = [];


						if(printersResponse instanceof Array){
							printersResponseArray = printersResponse;
						}else{
							printersResponseArray.push(printersResponse);
						}

						var printerQuery = "INSERT INTO PFP_Printer_Table (CompanyID,UserID,PrinterID,PrinterName) VALUES ";

						angular.forEach(printersResponseArray, function(printer, key) {
					  		var PrinterID = checkValue(printer.attributes.id);
					  		var PrinterName = checkValue(printer.attributes.name,"text");

					  		printerQuery += "('" + CompanyID + "','" + Userid + "','" + PrinterID + "','" + PrinterName + "'),";

					  	});
						console.log("Printer "+printerQuery);
					  	CRUD.inlineQuery(printerQuery.slice(0,-1));
		 		  });

		 	}

			function getPFPDetails(requestString){

		 		var args = {
		 			URL:    "PFPService",
		 			Action: "getPFPDetails",
		 			Params: {"requestString":requestString,"token":requestToken}
		 		};

		 		return CM.doCommsCall("soap",args);
		 	}
		 	function savePFPDetails(response){
		 		var promises = [];
		    	var deffered = $q.defer();
		 		var value = xmlToJson(response);
		 		var xmlResponse = value.root.body.getPFPDetailsResponse.getPFPDetailsReturn;
		 		var object = ReturnDataObject(xmlResponse);
		 		var taskResponse = object.getPFPdetailsresponse.task;
				var arryTasks = [];

				if(taskResponse instanceof Array){
					arryTasks = taskResponse;
				}else{
					arryTasks.push(taskResponse);
				}

				var taskQuery = "insert into PFP_Task_Table (ParLocationID,TaskID,ItemID,ItemNo,ItemDesc,ManufacturerNumber,GTIN,HIBC,BinNumber,UnitOfMeasure,PFPQuantity,ParLevel) values ";

				angular.forEach(arryTasks, function(task, index) {

					var ParLocationID = checkValue(task.attributes.parlocid),
						TaskID = checkValue(task.attributes.id),
						ItemID = checkValue(task.attributes.itemid),
						ItemNo = checkValue(task.attributes.itemno),
						ItemDesc = checkValue(task.attributes.itemdesc,"text"),
						ManufacturerNumber = checkValue(task.attributes.mfrno,"text"),
						GTIN = checkValue(task.attributes.gtin,"text"),
						HIBC = checkValue(task.attributes.hibc,"text"),
						UnitOfMeasure = checkValue(task.attributes.uom,"text"),
						PFPQuantity = checkValue(task.attributes.pfpqty,"text"),
						ParLevel = checkValue(task.attributes.parlevel,"text"),
						BinNumber = checkValue(task.attributes.binno,"text");

						taskQuery += "("+ParLocationID+", ";
			  	  		taskQuery += TaskID+", ";
			  	  		taskQuery += ItemID+", ";
			  	  		taskQuery += ItemNo+", ";
			  	  		taskQuery += "'"+ItemDesc+"', ";
			  	  		taskQuery += "'"+ManufacturerNumber+"', ";
			  	  		taskQuery += "'"+GTIN+"', ";
			  	  		taskQuery += "'"+HIBC+"', ";
			  	  		taskQuery += "'"+BinNumber+"', ";
			  	  		taskQuery += "'"+UnitOfMeasure+"', ";
			  	  		taskQuery += "'"+PFPQuantity+"', ";
			  	  		taskQuery += "'"+ParLevel+"'),";

						/*if(index < 1){

					    	taskQuery += "select "+ParLocationID+" as ParLocationID, ";
				  	  		taskQuery += TaskID+" as TaskID, ";
				  	  		taskQuery += ItemID+" as ItemID, ";
				  	  		taskQuery += ItemNo+" as ItemNo, ";
				  	  		taskQuery += "'"+ItemDesc+"' as ItemDesc, ";
				  	  		taskQuery += "'"+ManufacturerNumber+"' as ManufacturerNumber, ";
				  	  		taskQuery += "'"+GTIN+"' as GTIN, ";
				  	  		taskQuery += "'"+HIBC+"' as HIBC, ";
				  	  		taskQuery += "'"+BinNumber+"' as BinNumber, ";
				  	  		taskQuery += "'"+UnitOfMeasure+"' as UnitOfMeasure, ";
				  	  		taskQuery += "'"+PFPQuantity+"' as PFPQuantity, ";
				  	  		taskQuery += "'"+ParLevel+"' as ParLevel ";
				  	  		
					    }else{
					    	taskQuery += "union all select "+ParLocationID+", ";
					    	taskQuery += TaskID+", ";
					    	taskQuery += ItemID+", ";
					    	taskQuery += ItemNo+", ";
				  	  		taskQuery += "'"+ItemDesc+"', ";
				  	  		taskQuery += "'"+ManufacturerNumber+"', ";
				  	  		taskQuery += "'"+GTIN+"', ";
				  	  		taskQuery += "'"+HIBC+"', ";
				  	  		taskQuery += "'"+BinNumber+"', ";
				  	  		taskQuery += "'"+UnitOfMeasure+"', ";
				  	  		taskQuery += "'"+PFPQuantity+"', ";
				  	  		taskQuery += "'"+ParLevel+"' ";
					    }*/

				});

				CRUD.inlineQuery(taskQuery.slice(0,-1))
				  	.then(function(result){
				  			deffered.resolve(result);
				  	},function(res){
				  		deffered.resolve(res);
				  	});

			  	promises.push(deffered.promise);
				
				return $q.all(promises);

		 	}

		 	function syncPFP(requestString){
				var args = {
					 			URL:    "PFPService",
					 			Action: "syncPFP",
					 			Params: {"requestString":requestString,"token":requestToken}
					 		};

		 		return CM.doCommsCall("soap",args);
			}
			function saveTransactionHistory(locationID,ItemCount){

				var query = "insert into PFP_TransHistory_Table (UserID,CountedPFPTickets,PulledItems,TimeSpent) VALUES(";
					query += "'"+LocalData.loadSession("SoapUsername")+"',";
					query += locationID+",";
					query += ItemCount+",";
					query += "(SELECT ";
					query +="ROUND(cast( ";
					query +="( ";
					query +="strftime('%s',(SELECT TimeStamp from Par_Transaction_Table ORDER BY TimeStamp DESC LIMIT 1))";	
					query +="-strftime('%s',(SELECT TimeStamp from Par_Transaction_Table ORDER BY TimeStamp ASC LIMIT 1))";
					query +=") as real";
					query +=")/60,2) as TimeSpent "; // Devided by 60 to get Minutes
					query +="FROM PFP_Transaction_Table WHERE ParLocationID = "+locationID;
					query +=" ORDER BY TimeStamp DESC LIMIT 1))";

				CRUD.inlineQuery(query).then(function(res){
					console.log("Success: "+res);
				},function(res){
					console.log("Error: "+res);
				});
			}

		 	function saveValues(item,input,type){
				var quantityValue;
				if(input.target.value.length < 1){
					quantityValue = 0;
				}else{
					quantityValue = parseInt(input.target.value);
				}
//				
				

				var query = "SELECT EXISTS(SELECT TaskID FROM PFP_Transaction_Table WHERE TaskID="+item.TaskID+" AND ParLocationID = "+item.ParLocationID+" LIMIT 1) as Item";
				CRUD.inlineQuery(query)
					.then(function(result){
						//alert(result[0].Item);
						if(result[0].Item < 1){
							var params = {
							 	"ParLocationID":parseInt(item.ParLocationID),
							 	"TaskID":parseInt(item.TaskID),
							 	"ItemID":parseInt(item.ItemID),
							 	"PickedQuantity": quantityValue
							 };	
						     CRUD.insert("PFP_Transaction_Table",params);
						}else{
							var params = {"TaskID":item.TaskID};
		    				CRUD.update("PFP_Transaction_Table","PickedQuantity",quantityValue,params);
						}

					});
					



				

				
			}

		 	function checkValue(val,type){

				if(type){
					return (val == undefined || val == "" ? "0" : val.replace(/["']/g, ""));
				}else{
					return (val == undefined || val == "" ? 0 : parseInt(val));
				}

			}
		    
		}
})();

