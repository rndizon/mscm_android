(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('PickForParChangePrinter', Body);

		function Body($state,
					  $scope,
					  $ionicHistory,
					  LocalData,
					  $ionicLoading,
					  CONSTANT,CRUD){
					
			var vm = this;
			vm.selectedItem = "No Item Selected";
			vm.headerTitle = "title_set_default_printer";
			vm.savePrinter = savePrinter;
			vm.defaultPrinter;
			vm.doRefresh = doRefresh;
			vm.companyHeader = LocalData.loadData("PFPCompanyName");
			vm.companyHeaderID = LocalData.loadData("PFPCompanyID");
			vm.currentUser = LocalData.loadSession('currentUser');
			var users = JSON.parse(LocalData.loadData('users'));
			vm.localStorShowHelp = users[vm.currentUser].isShowHelp;


			var queryParams = {};
			queryParams.CompanyID = vm.companyHeaderID;
			CRUD.select("PFP_Printer_Table","*",queryParams)
			.then(function(res){
				vm.printerList = res;
			});

			vm.prevStateId = LocalData.loadSession("PrevStateName");

			vm.defaultPrinter = LocalData.loadData("pickForParSelectedPrinter");

			function savePrinter() {

				LocalData.saveData("pickForParSelectedPrinter",vm.defaultPrinter);				
				$state.go(vm.prevStateId);

			}


			function doRefresh(){

	    	  	//show loading icon
				$ionicLoading.show({
					templateUrl: CONSTANT.SpinnerTemplate
				});

				$scope.$broadcast('scroll.refreshComplete');
				$ionicLoading.hide();

	    	};

		}
				
		
})();
