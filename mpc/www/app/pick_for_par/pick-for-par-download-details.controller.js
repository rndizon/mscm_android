(function(){
	'use strict';
	
	angular
		.module("app")
		.controller("PickForParItemDetailController", Body);
	
	function Body($state,$q,
				  $scope,
				  filterFilter,
				  $ionicSlideBoxDelegate,
				  $ionicPopup,
				  $ionicViewSwitcher,
				  $ionicPopover,
				  $stateParams,
				  CRUD,
				  LocalData,
				  PFPService,
				  CommonInfoService
				  ) 
	{
		
			var vm = this;
			vm.currentSlideNumber = 1;
			vm.slidestop = slidestop;
			vm.slidePrevious = slidePrevious;
			vm.slideNext = slideNext;
			vm.slideHasChanged = slideHasChanged;
			vm.goTo = goTo;
			vm.openEditParlevel = openEditParlevel;
			vm.pickForParDetailsOption = pickForParDetailsOption;
			vm.pickForPar = [];
			vm.parLocations = [];
			vm.optionParLocation = optionParLocation;
			vm.saveCurrentLocation = saveCurrentLocation;
			vm.ParLocationID = $stateParams.ParLocationID;
			vm.ParLocationDesc = $stateParams.ParLocationDesc;
			vm.inventoryLocationID = LocalData.loadData("defInventoryLocationID");
			vm.CurrentParLocation = '';
			vm.CurrentParLocationPopUp = null;
            vm.closePopOver = closePopOver;
			vm.TotalItemCounted = 0;
			vm.highlightField = highlightField;
			vm.currentUser = LocalData.loadSession('currentUser');
			var users = JSON.parse(LocalData.loadData('users'));
			vm.localStorShowHelp = users[vm.currentUser].isShowHelp;
			vm.saveValues = PFPService.saveValues;

	    	$scope.$on('$ionicView.afterEnter', function() {

				getTasks(vm.ParLocationID);
				//Populate drop down		
				getSelectedPar();

			});

			
			function getTasks(_ParLocationID){

				vm.slidePickForPar = [];

			    var query = "select *,pfpItem.TaskID as TaskID,pfpItem.ItemID as ItemID,pfpItem.BinNumber as BinNumber,pfpItem.ParLocationID as ParLocationID from PFP_Task_Table as pfpItem left join PFP_Transaction_Table as trans ";
		 			query +="on pfpItem.ParLocationID = trans.ParLocationID where pfpItem.ParLocationID = "+_ParLocationID+" GROUP BY pfpItem.TaskID";
		 			CRUD.inlineQuery(query)
				    .then(function(result) {
				    	vm.pickForPar = result;
				 		 	
				 		 	var curSlideLimit = (vm.pickForPar.length < 3 ? vm.pickForPar.length : 3);

							for(var i = 0; i < curSlideLimit; i++) {
							  vm.slidePickForPar.push(vm.pickForPar[i]);
							} 
							$ionicSlideBoxDelegate.update();
				    });
			}
			function getSelectedPar(){

				var LocationCodeForSweepingCount = [];
				CommonInfoService.getSelectedParforms().forEach(function(v){
					LocationCodeForSweepingCount.push(v.ParLocationID);
				});
				

				var query = "select ParLocationID,ParLocationCode,ParLocationDesc ";
					query+= "from PFP_Par_Table ";
					query+= "where IsSelected = 1 AND ";

					if(LocationCodeForSweepingCount.length > 0){
						query+= "ParLocationID IN ("+LocationCodeForSweepingCount.join(', ')+") AND ";
					}
					
					query+= "InventoryLocationID = "+vm.inventoryLocationID;
				CRUD.inlineQuery(query)
				.then(function(data){
					vm.parLocations = data;
					vm.CurrentParLocation = (LocationCodeForSweepingCount.length > 0) ? data[0].ParLocationDesc : vm.ParLocationDesc;
				});

			}

			function saveCurrentLocation(_ParLocationID,_Currentlocation){
				vm.CurrentParLocation = _Currentlocation;
				getTasks(_ParLocationID);
				//vm.CurrentParLocationPopUp.close();
			}
	      	//navigate to Other Page
			function goTo(goToUrl,_ParLocationID,_ParLocationDesc) {

				if(_ParLocationID && _ParLocationID > 0){
					var params = {ParLocationID:_ParLocationID,ParLocationDesc:_ParLocationDesc};
					$state.go(goToUrl,params);
				}else{

					$ionicViewSwitcher.nextDirection('slideDown');
					$state.go(goToUrl);
				}
			 	
			}
			
			/*
				Next and Previous button of slider content
			*/


			function slideHasChanged(xxx){

			  var countParCount = vm.pickForPar.length;
			  var countCurSlide = vm.slidePickForPar.length;
			  var currentSlideIndex = $ionicSlideBoxDelegate.currentIndex();
			  var addIndex = currentSlideIndex + 2;

			  if(addIndex == countCurSlide) {

			      if(addIndex < countParCount) {
			        vm.slidePickForPar.push(vm.pickForPar[addIndex]);

			        $ionicSlideBoxDelegate.update();
			      }

			  }
			  vm.currentSlideNumber =  $ionicSlideBoxDelegate.currentIndex() + 1;

			  console.log(xxx);

			};

			function slidestop(index) {
			    $ionicSlideBoxDelegate.enableSlide(false);
			}

			function slidePrevious() {
				$ionicSlideBoxDelegate.previous();
				vm.currentSlideNumber =  $ionicSlideBoxDelegate.currentIndex() + 1;
				
			};

			function slideNext() {
				if(vm.currentSlideNumber >= $ionicSlideBoxDelegate.slidesCount()) {
					//nextParLocationConfirmation();
				}

				$ionicSlideBoxDelegate.next();
				vm.currentSlideNumber =  $ionicSlideBoxDelegate.currentIndex() + 1;
				
			};  

			/*
				Change this to native popup
			*/
			function nextParLocationConfirmation() {
				var confirmPopup  = $ionicPopup.confirm({
					template: 'Previous Par forms will be saved',
					title: 'Continue to next Par Location?',
					scope: $scope
				});

				confirmPopup.then( function(res) {
				});
			};

		    function highlightField($event) {
		    	$event.target.select();
		    }

			function openEditParlevel() {
				var confirmPopup  = $ionicPopup.confirm({
					templateUrl: 'html/templates/edit-par-level.html',
					title: 'Edit Par Level',
					scope: $scope
				});
			}

			function optionParLocation($event,popupType) {
                $ionicPopover.fromTemplateUrl('html/templates/options-more.html', {
                            scope: $scope
                    }).then(function(popover) {
                            vm.popover = popover;
                            vm.popUpType = (popupType) ? popupType : "par-location-pop-up";
                            popover.show($event);
                    });
			  // An elaborate, custom popup
			  //vm.CurrentParLocationPopUp = $ionicPopup.show({
			   //templateUrl: 'html/templates/options-more.html',
				//templateUrl: 'html/pick_for_par/par-location-pop-up.html',
			    //title: 'Change Par Location',scope: $scope,
			    //buttons: null
			  //});
            }
            function closePopOver() {
	    		vm.popover.hide();
	    	}
        
			 function pickForParDetailsOption($event) {
		    	$ionicPopover.fromTemplateUrl('html/templates/options-more.html', {
				        scope: $scope
				}).then(function(popover) {
				        vm.popover = popover;
				        vm.popUpType = "pick-for-par-details";
				        popover.show($event);
				}); 
	    	}


		
		
	}
	
	
	
	
	
	
})();