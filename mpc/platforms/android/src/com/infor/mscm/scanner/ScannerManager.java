package com.infor.mscm.scanner;

import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaInterface;

import org.json.JSONArray;
import org.json.JSONException;
import android.util.Log;

public class ScannerManager extends CordovaPlugin 
{
	private static final String MANUFACTURER_FOXCOM = "Foxconn";
	private static final String MANUFACTURER_HONEYWELL_D75E = "D75E";
	private static final String MANUFACTURER_HONEYWELL = "Honeywell";
    private static final String MANUFACTURER_MOTOROLA = "Motorola";
    private static final String MANUFACTURER_MOTOROLA_MC40 = "MC40";
    private static final String MANUFACTURER_ZEBRA = "Zebra"; //Zebra Technologies
    private static final String TAG = "ScannerManager Plugin";
	
	public static Scanner m_scanner = null;
	public void initialize(CordovaInterface cordova, CordovaWebView webView) {
	super.initialize(cordova, webView);
		Log.v(TAG,"Init ScannerManager");
	}	
	public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException 
	{
        
		Log.v("Scanner", "Action "+action);
		
		if (action.equals("enableScanner")) 
        {
			Log.v("Scanner", "enableScanner execute call");
        	m_scanner.enableScanner(callbackContext);
        }
        else if (action.equals("disableScanner")) 
        {
        	try {
        		m_scanner.disableScanner();
			} catch (Exception e) {
				// TODO: handle exception
				Log.v("Scanner", "disableScanner execute call :"+ e.getMessage());
			}
        	
        }

        return true;
    }
	
	public static void createScannerInstance() 
	{
		String strDeviceManufacturer = android.os.Build.MANUFACTURER;
		String strDeviceModel = android.os.Build.MODEL;
		Log.v("Device Scanner Checker",strDeviceManufacturer+" "+strDeviceModel);
		if (strDeviceManufacturer.contains(MANUFACTURER_MOTOROLA) || strDeviceManufacturer.contains(MANUFACTURER_ZEBRA) || strDeviceModel.contains(MANUFACTURER_MOTOROLA_MC40))
		{
			Log.v("Device Scanner Checker", "Motorola");
			m_scanner = new MotorolaScanner();
		}
		else if (strDeviceManufacturer.contains(MANUFACTURER_FOXCOM))
		{
			Log.v("Device Scanner Checker", "Honeywell D70E");
			m_scanner = new HoneywellScanner();
		}else if(strDeviceModel.contains(MANUFACTURER_HONEYWELL_D75E)) {
			Log.v("Device Scanner Checker", "Honeywell D75E");
			m_scanner = new DolphinScanner();
		}
		else
		{
			Log.v("Device Scanner Checker", "Scanner Default Class");
			m_scanner = new Scanner();
		}
	}
	
	public static Scanner getScannerInstance() 
	{
		return ScannerManager.m_scanner;
	}
	
	
}