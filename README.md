Hi Team,
 
Git repository is now available. Here is our project repo: https://<your username>@bitbucket.org/projectf4/mscm_android.git
 
Please follow steps below to start working with Bitbucket.
 
Via command line:
 
-          Open command prompt
-          Type mkdir /path/to/your/project
-          Type cd /path/to/your/project
-          Copy and paste: git clone https://<your username>@bitbucket.org/projectf4/mscm_android.git
-          Once done. Go to mpc\platforms\android and new folder “assets”.
-          Type cordova build android
-          Type cordova run android. The App should be running fine.
 
Via SourceTree
-          Download and install sourcetree here https://www.sourcetreeapp.com/download/
-          Open Sourcetree and Click Clone/New
-          Copy and Paste git clone https://<your username>@bitbucket.org/projectf4/mscm_android.git to Source path/URL and wait to validate the url
-          Add your working folder to Destination path field
-          Once done, click Pull
-          Go to mpc\platforms\android and new folder “assets”.
-          Type cordova build android
-          Type cordova run android. The App should be running fine.
 
Let me know if you have problems setting up your local branch. Thanks.
 
 
Regards,
 
Ryan Niño G. Dizon  | Software Engineer, Senior, Development 
office: +632 976 3600  x779314 | mobile: +63 933 512 9930
ryannino.dizon@infor.com | http://www.infor.com