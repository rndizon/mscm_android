(function(){
 	'use strict';
 	
		angular
		    .module('app')
			.config(function($stateProvider, $urlRouterProvider) {		     
		     $stateProvider		      
			  .state("app.build-case", {
			      url: "/build-case",
		          changeColor: 'bar-turquoise-8',
		          cache:false, 
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/build_case/build-case.html",
			      		controller: "BuildCaseController as vm"
			      	}
			      }
			})
			.state("app.build-case-load-case", {
			      url: "/build-case-load-case",
		          changeColor: 'bar-turquoise-8',
		          cache:true,
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/build_case/build-case-load-case.html",
			      		controller: "BuildCaseLoadCaseController as vm"
			      	}
			      }
		    })
			.state("app.build-case-load-case-filter", {
			      url: "/build-case-load-case-filter",
		          changeColor: 'bar-turquoise-8',
		          cache:true,
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/build_case/build-case-load-case-filter.html",
			      		controller: "BuildCaseLoadCaseFilterController as vm"
			      	}
			      }
		    })
			.state("app.build-case-new-case", {
			      url: "/build-case-new-case",
		          changeColor: 'bar-turquoise-8',
		          cache:true,
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/build_case/build-case-new-case.html",
			      		controller: "BuildCaseNewCaseController as vm"
			      	}
			      }
		    })
			.state("app.build-case-change-procedure-room", {
			      url: "/build-case-change-procedure-room",
		          changeColor: 'bar-turquoise-8',
		          cache:true,
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/build_case/build-case-change-procedure-room.html",
			      		controller: "BuildCaseChangeProcedureRoomController as vm"
			      	}
			      }
		    })
			.state("app.build-case-select-practitioner", {
			      url: "/build-case-select-practitioner",
		          changeColor: 'bar-turquoise-8',
		          cache:true,
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/build_case/build-case-select-practitioner.html",
			      		controller: "BuildCaseSelectPractitionerController as vm"
			      	}
			      }
		    })
			.state("app.build-case-additional-case-details", {
			      url: "/build-case-additional-case-details",
		          changeColor: 'bar-turquoise-8',
		          cache:true,
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/build_case/build-case-additional-case-details.html",
			      		controller: "BuildCaseAdditionalCaseDetailsController as vm"
			      	}
			      }
		    })
			.state("app.build-case-add-procedure", {
			      url: "/build-case-add-procedure",
		          changeColor: 'bar-turquoise-8',
		          cache:true,
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/build_case/build-case-add-procedure.html",
			      		controller: "BuildCaseAddProcedureController as vm"
			      	}
			      }
		    })
			.state("app.build-case-select-procedures", {
			      url: "/build-case-select-procedures",
		          changeColor: 'bar-turquoise-8',
		          cache:true,
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/build_case/build-case-select-procedures.html",
			      		controller: "BuildCaseSelectProceduresController as vm"
			      	}
			      }
		    })
			.state("app.build-case-add-procedure-item", {
			      url: "/build-case-add-procedure-item",
		          changeColor: 'bar-turquoise-8',
		          cache:true,
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/build_case/build-case-add-procedure-item.html",
			      		controller: "BuildCaseAddProcedureItemController as vm"
			      	}
			      }
		    })
			.state("app.build-case-add-procedure-modifier", {
			      url: "/build-case-add-procedure-modifier",
		          changeColor: 'bar-turquoise-8',
		          cache:true,
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/build_case/build-case-add-procedure-modifier.html",
			      		controller: "BuildCaseAddProcedureModifierController as vm"
			      	}
			      }
		    })
			.state("app.build-case-procedure-item-edit", {
			      url: "/build-case-procedure-item-edit",
		          changeColor: 'bar-turquoise-8',
		          cache:true,
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/build_case/build-case-procedure-item-edit.html",
			      		controller: "BuildCaseProcedureItemEditController as vm"
			      	}
			      }
		    })
			
			.state("app.build-case-summary", {
			      url: "/build-case-summary",
		          changeColor: 'bar-turquoise-8',
		          cache:true,
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/build_case/build-case-summary.html",
			      		controller: "BuildCaseSummaryController as vm"
			      	}
			      }
		    })
			
			
			
		});
})();