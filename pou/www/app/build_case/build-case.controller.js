(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('BuildCaseController', Body);

			function Body($state,
					$scope,
					filterFilter,
					$ionicPopover,
					$ionicViewSwitcher,
					$ionicActionSheet,
					$timeout,
					$ionicLoading,
					$cordovaToast) {

			var vm = this;
			vm.goTo = goTo;
			vm.goToWithID = goToWithID;
			vm.pullOption = pullOption;
			vm.showactionsheet = showSendActionSheet;
			vm.sendcounts = sendCounts;
			
			//	GOTO URL
			function goTo($url,$navDirection) {
				if(vm.popover) {
					vm.popover.hide();
				}
				if($navDirection) {
		   			$ionicViewSwitcher.nextDirection($navDirection);
				}				
				$state.go($url);
			};
			
			//	GOTO URL
			function goToWithID($url,setid) {
				if(vm.popover) {
					vm.popover.hide();
				}				
				$state.go($url, {"ciID":setid});
			};
			
			//	SHOW ACTION SHEET
			//<button class="button button-icon icon"><mxi-icon icon="icon-nav-send"></mxi-icon>Send</button>
			function showSendActionSheet(){
				// Show the action sheet
				$ionicActionSheet.show({
					buttons: [
						{ text: '<i class="icon ion-ios-information" disabled></i> Release'},						
						{ text: '<i class="icon ion-android-create"></i> Edit Case Header' },
						{ text: '<i class="icon ion-android-add"></i> Add Procedures' }
					],
					destructiveText: '',
					titleText: '',
					cancelText: '',
					cancel: function() {
						// add cancel code..
					},
					buttonClicked: function(index) {
						if (index==0){
							//vm.sendcounts();							
							vm.goTo('app.build-case-summary');
						}
						else if (index==1){
							//vm.sendcounts();							
							vm.goTo('app.build-case-new-case');
						}
						else if (index==2){
							vm.goTo('app.build-case-add-procedure');
						}						
						return true;
					},
					cssClass : 'actionsheetsend'
					
				});							
			}
			
			 /*
			   * if given group is the selected group, deselect it
			   * else, select the given group
			   */
			  function toggleGroup(group) {

			    if (vm.isGroupShown(group)) {
			      vm.shownGroup = null;
			    } else {
			      vm.shownGroup = group;
			    }
			  };

			 function isGroupShown(group) {
			    return vm.shownGroup === group;
			  };
			
			function sendCounts(){
				/*$ionicLoading.show({
					template: 'Item Added!',
					templateUrl : 'html/templates/show-toast.html',
					noBackdrop: true,
					duration: 0
				});*/
				ShowToastMessage("Item Added", 'center', 'long');
			}
			//	SHOW MENU POPOVER OPTIONS
	    	function pullOption($event) {				
		    	$ionicPopover.fromTemplateUrl('html/templates/options-more.html', {
					scope: $scope
				}).then(function(popover) {
				    vm.popover = popover;
				    vm.popUpType = 'build-case';				    
				    popover.show($event);
				}); 
	    	};
			
			//ShowToastMessage("test of toast message", 'center', 'long');

			//	SHOWS NATIVE TOAST MESSAGE
			function ShowToastMessage(messagetxt, messageposition, duration){
				//	messageposition : 'top', 'center', 'bottom'
				//	duration :	'short', 'long' 
				
				/*if (window){
					if (window.plugins){
						if (window.plugins.toast){
							window.plugins.toast.show(messagetxt, duration, messageposition);
						}
						else{
							console.log('window.plugins.toast is false');
						}
					}
					else{
							console.log('window.plugins is false');
					}
					
				}*/
				if (typeof $cordovaToast != 'undefined'){					
					$cordovaToast.show(messagetxt, duration, messageposition);
				}
				else{
					//	USE NATIVE LOADING
					$ionicLoading.show({
						template: 'Item Added!',
						templateUrl : 'html/templates/show-toast.html',
						noBackdrop: true,
						duration: 0
					});
				}
				
				/*if (typeof window.plugins.toast != 'undefined'){
				//if (window.plugins.toast!='undefined'){
					window.plugins.toast.show(messagetxt, duration, messageposition);
					//$cordovaToast.show(messagetxt, duration, messageposition);
				}
				else{
					//	USE NATIVE LOADING
					$ionicLoading.show({
						template: 'Item Added!',
						templateUrl : 'html/templates/show-toast.html',
						noBackdrop: true,
						duration: 0
					});
				}*/
				
				
				/*$cordovaToast.showLongCenter(messagetxt);
				$cordovaToast.showShortTop(messagetxt);
				$cordovaToast.showLongBottom(messagetxt)
				$cordovaToast.showLongTop(messagetxt)
				$cordovaToast.showShortBottom(messagetxt)
				$cordovaToast.showShortCenter(messagetxt)
				$cordovaToast.showShortTop(messagetxt)*/
			}			
		
		};	
		
})();
