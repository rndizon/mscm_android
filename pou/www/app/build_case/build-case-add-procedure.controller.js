(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('BuildCaseAddProcedureController', Body);

			function Body($state,
					$scope,
					filterFilter,
					$ionicPopover,
					$ionicViewSwitcher,
					$ionicActionSheet,
					$timeout,
					$ionicLoading,
					$cordovaToast,
					$ionicHistory) {

			var vm = this;
			vm.goTo = goTo;
			vm.goToWithID = goToWithID;
			vm.pullOption = pullOption;
			vm.showactionsheet = showSendActionSheet;
			vm.sendcounts = sendCounts;
			
			vm.goBack = function(){				
				$ionicHistory.goBack();
			};
			
			//	GOTO URL
			function goTo($url,$navDirection) {
				if(vm.popover) {
					vm.popover.hide();
				}
				if($navDirection) {
		   			$ionicViewSwitcher.nextDirection($navDirection);
				}				
				$state.go($url);
			};
			
			//	GOTO URL
			function goToWithID($url,setid) {
				if(vm.popover) {
					vm.popover.hide();
				}				
				$state.go($url, {"ciID":setid});
			};
			
			//	SHOW ACTION SHEET
			//<button class="button button-icon icon"><mxi-icon icon="icon-nav-send"></mxi-icon>Send</button>
			function showSendActionSheet(){
				// Show the action sheet
				$ionicActionSheet.show({
					buttons: [
						{ text: '<use x="100" y="100" xlink:href="#icon-build-case-edit" /><i class="icon ion-ios-information" disabled></i> Release'},						
						{ text: '<i class="icon ion-edit"></i> Edit Case Header' },
						{ text: '<i class="icon ion-android-add"></i> Add Procedures' }
					],
					destructiveText: '',
					titleText: '',
					cancelText: '',
					cancel: function() {
						// add cancel code..
					},
					buttonClicked: function(index) {
						vm.sendcounts();
						return true;
					},
					cssClass : 'actionsheetsend'
					
				});							
			}
			
			
			function sendCounts(){
				/*$ionicLoading.show({
					template: 'Item Added!',
					templateUrl : 'html/templates/show-toast.html',
					noBackdrop: true,
					duration: 0
				});*/
				ShowToastMessage("Item Added", 'center', 'long');
			}
			//	SHOW MENU POPOVER OPTIONS
	    	function pullOption($event) {				
		    	$ionicPopover.fromTemplateUrl('html/templates/options-more.html', {
					scope: $scope
				}).then(function(popover) {
				    vm.popover = popover;
				    vm.popUpType = 'pull';				    
				    popover.show($event);
				}); 
	    	};
			
			//ShowToastMessage("test of toast message", 'center', 'long');

			//	SHOWS NATIVE TOAST MESSAGE
			function ShowToastMessage(messagetxt, messageposition, duration){
				//	messageposition : 'top', 'center', 'bottom'
				//	duration :	'short', 'long' 
				
				/*if (window){
					if (window.plugins){
						if (window.plugins.toast){
							window.plugins.toast.show(messagetxt, duration, messageposition);
						}
						else{
							console.log('window.plugins.toast is false');
						}
					}
					else{
							console.log('window.plugins is false');
					}
					
				}*/
				if (typeof $cordovaToast != 'undefined'){					
					$cordovaToast.show(messagetxt, duration, messageposition);
				}
				else{
					//	USE NATIVE LOADING
					$ionicLoading.show({
						template: 'Item Added!',
						templateUrl : 'html/templates/show-toast.html',
						noBackdrop: true,
						duration: 0
					});
				}
				
				/*if (typeof window.plugins.toast != 'undefined'){
				//if (window.plugins.toast!='undefined'){
					window.plugins.toast.show(messagetxt, duration, messageposition);
					//$cordovaToast.show(messagetxt, duration, messageposition);
				}
				else{
					//	USE NATIVE LOADING
					$ionicLoading.show({
						template: 'Item Added!',
						templateUrl : 'html/templates/show-toast.html',
						noBackdrop: true,
						duration: 0
					});
				}*/
				
				
				/*$cordovaToast.showLongCenter(messagetxt);
				$cordovaToast.showShortTop(messagetxt);
				$cordovaToast.showLongBottom(messagetxt)
				$cordovaToast.showLongTop(messagetxt)
				$cordovaToast.showShortBottom(messagetxt)
				$cordovaToast.showShortCenter(messagetxt)
				$cordovaToast.showShortTop(messagetxt)*/
			}			
		
		};	
		
})();
