(function(){
 	'use strict';
 	
		angular
		    .module('app')
			.config(function($stateProvider, $urlRouterProvider) {
		     
		     $stateProvider
		        .state("app.company-change-view", {
		         changeColor: 'bar-amber-7',
			     url: "/company-change-view", 
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/templates/company-change-view.html",
			      		controller: "CompanyChangeController as vm"
			      	}
			      }
		      })

		      .state("app.pick-for-par-change-company", {
			     url: "/pick-for-par-change-company", 
		         changeColor: 'bar-turquoise-8',
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/templates/company-change-view.html",
			      		controller: "PFPCompanyChangeController as vm"
			      	}
			      }
		      })

		      .state("app.pick-for-par-change-location", {
			     url: "/pick-for-par-change-location", 
		         changeColor: 'bar-turquoise-8',
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/pick_for_par/location-change-view.html",
			      		controller: "PFPLocationChangeController as vm"
			      	}
			      }
		      })

		      .state("app.cycle-count-change-company", {
			     url: "/cycle-count-change-company", 
		         changeColor: 'bar-amethyst-8',
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/templates/company-change-view.html",
			      		controller: "CycleCompanyChangeController as vm"
			      	}
			      }
		      })

		      .state("app.cycle-count-change-location", {
			     url: "/cycle-count-change-location", 
		         changeColor: 'bar-amethyst-8',
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/templates/location-change-view.html",
			      		controller: "CompanyChangeController as vm"
			      	}
			      }
		      })

		      .state("company-location-change-view", {
		      	cache:false,
		      	url: "/company-location-change-view",
                changeColor: 'bar-turquoise-8', 
		      	templateUrl: "html/templates/company-location-change-view.html",
		      	controller: "CompanyChangeController as vm"
		      })

		      .state("cycle-count-change-company", {
		      	cache:false,
		      	url: "/cycle-count-company-location-change-view",
                changeColor: 'bar-amethyst-8', 
		      	templateUrl: "html/templates/company-change-view.html",
		      	controller: "CompanyChangeController as vm"
		      })
		      

		      .state("app.settings-company-view", {
			      url: "/settings-company-view", 
			      changeColor: 'bar-slate-6',
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/settings/settings-company-change-view.html",
			      		controller: "CompanyChangeController as vm"
			      	}
			      }
		      });
		      
		  });
		
		
		
})();



