(function(){
    'use strict';
   
  angular
      .module('app')
      .directive('validNumber', function() {
        return {
          require: '?ngModel',
          link: function(scope, element, attrs, ngModelCtrl) {
           
           element.bind('keyup', function (blurEvent) {
                var oldValue = element.val().slice(0,-1);
                var curVal = element.val();
                var reg = /^[0-9.]+$/;

                if(reg.test(curVal) == false) {
                  element.val(sessionStorage.getItem('currentInputVal'));
                } else if(element.val().length > 10) {
                  element.val(oldValue);
                } else {
                  sessionStorage.setItem('currentInputVal',element.val());
                }


            });

            element.bind('blur',function() {

                var precision = 4,
                    power = Math.pow(10, precision),
                    absValue = Math.abs(Math.round(element.val() * power)),
                    result = (element.val() < 0 ? '-' : '') + String(Math.floor(absValue / power));

                    var fraction = String(absValue % power),
                        padding = new Array(Math.max(precision - fraction.length, 0) + 1).join('0');
                    result += '.' + padding + fraction;

                    element.val(parseFloat(result));
            });

            element.bind('click', function(e) {
              e.target.select();
            })
          }
        };
      });
      
})();