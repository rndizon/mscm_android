(function(){
    'use strict';
	 
	angular
	    .module('app')
	    .factory('AuthService', Body);


	    function Body($q,CM,LocalData) {
	    	
	    	
	    	return {
	    		
	    		doLogin : doLogin
	    		
	    	};

	    	function doLogin(){

	    		var promises = [];
		    	var deffered = $q.defer();
		    	var deffered2 = $q.defer();

	    		var args = {
		 			URL:    "SystemManagementService",
		 			Action: "getTime",
		 			Params: {}
		 		};

		 		CM.doCommsCall("soap",args).then(function(response){
		 				var value = xmlToJson(response);
		 				var object = ReturnDataObject(value.root.body.getTimeResponse.getTimeReturn);
		 				LocalData.saveSession("getTime_time", object.timeresponse.return_success.time);
		 				LocalData.saveSession("getTime_format", object.timeresponse.return_success.format);
		 				
		 				deffered.resolve(response);
		 				var loginArgs = {
				 			URL:    "SystemManagementService",
				 			Action: "login",
				 			Params: {"user":"u12665","password":"lawson"}
				 		};
		 				CM.doCommsCall("soap",loginArgs).then(function(loginResponse){

		 					var loginValue = xmlToJson(loginResponse);
		 					var loginObject =  ReturnDataObject(loginValue.root.body.loginResponse.loginReturn);
		 					LocalData.saveSession("SoapToken", loginObject.loginresponse.return_success.token);
		 					var userNameFromToken = loginObject.loginresponse.return_success.token.split(":"); 
		 					LocalData.saveSession("SoapUsername", userNameFromToken[1]);
		 					var modules = [];
		 					if(loginObject.loginresponse.products.product instanceof Array) {
			 					var moduleReponse = loginObject.loginresponse.products.product[0].module;
			 					for(var i = 0;i < moduleReponse.length; i++){
			 						 console.log(moduleReponse[i].attributes.name);
			 						 modules.push(moduleReponse[i].attributes.name);
			 						 deffered2.resolve(moduleReponse[i].attributes.name);
			 					}
			 				}else{
			 					console.log(loginObject.loginresponse.products.product.module.attributes.name);
			 					modules.push(loginObject.loginresponse.products.product.module.attributes.name);
			 					deffered2.resolve(loginObject.loginresponse.products.product.module.attributes.name);
		 						
			 				}
			 				LocalData.saveSession("Modules", modules);
		 					

			 				/**
			 					Create LocalStorage for USERS and APPLICATION defaults per account
			 				*/
			 				var usersObj = (LocalData.loadData('users') ? JSON.parse(LocalData.loadData('users')) : {});
						    var appObj = (LocalData.loadData('application') ? JSON.parse(LocalData.loadData('application')) : {});
						    
						 	if(!usersObj[userNameFromToken[1]]) {
						 		
						 		/*
									Set Default Value for account
						 		*/
						 		usersObj[userNameFromToken[1]] = {
									lastLoggedIn: "",
									lastAccessLocation: "",
									defaultParCompanyID: "",
									defaultParCompanyDescription: "",
									defaultParLabelPrinter: "",
									defaultCycleCompanyID: "",
									defaultCycleCompanyDescription: "",
									defaultPickForParReportPrinter: "",
									isShowHelp: 1,
									lastParCount: "",
									lastParformsUsed: [],
									lastCycleCount: "",
									lastSelectIDUsed: "",
									parCountConfiguration:{
										isMyParFormFirst:1,
										isPopulateItemScreen:0,
										isAllowAddlQuantity:1

									},
									cycleCountConfiguration:{
										isBlindCount:0,
										isValidateFreeze:0
									}
						 		};

						 		appObj = {
							        isKioskMode: "0",
							        kioskPasskey: "OBF:kajsdqweoi1923n1s",
							        goodScan: "0",
							        badScan: "1",
							        logLevel: "3",
							        maxLogFileSize: 2,
							        logFilePath: "/sdcard",
							        serverRequestTimeOut: "60000",
							    };


						 	} else {
						 		 /* Update every login */
						 		usersObj[userNameFromToken[1]].lastLoggedIn = "";

						 	}

						 	LocalData.saveSession('currentUser',userNameFromToken[1]);

			 				LocalData.saveData('application',JSON.stringify(appObj));
			 				LocalData.saveData('users',JSON.stringify(usersObj));

		 				
		 				});


		 		});
				
				return $q.all([deffered.promise,deffered2.promise]);

	    	}
	    	
	    	
	    	
		}
})();
