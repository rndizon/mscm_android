(function(){
 	'use strict';
 	
		angular
		    .module('app')
			.config(function($stateProvider, $urlRouterProvider) {		     
		     $stateProvider		      
			  .state("app.count-inventory", {
			      url: "/count-inventory",
		          changeColor: 'bar-turquoise-8',
		          cache:false, 
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/count_inventory/count-inventory.html",
			      		controller: "CountInventoryController as vm"
			      	}
			      }
			})			  
			.state("app.count-inventory-add-items", {
			      url: "/count-inventory-add-items/:ciID/",
		          changeColor: 'bar-turquoise-8',
		          cache:true, 
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/count_inventory/count-inventory-add-items.html",
			      		controller: "CountInventoryAddItemsController as vm"
			      	}
			      }
		    })			  
			.state("app.count-inventory-add-items-config", {
			      url: "/count-inventory-add-items-config/:ciID/:ciID2/",
		          changeColor: 'bar-turquoise-8',
		          cache:true, 
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/count_inventory/count-inventory-add-items-config.html",
			      		controller: "CountInventoryAddItemsConfigController as vm"
			      	}
			      }
		    })
		});
})();