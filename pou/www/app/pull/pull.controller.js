(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('PullController', Body);

			function Body($state,
					$scope,
					filterFilter,
					$ionicPopover,
					$ionicViewSwitcher) {

			var vm = this;
			vm.goTo = goTo;
			vm.pullOption = pullOption;
			
			//	GOTO URL
			function goTo($url,$navDirection) {
				if(vm.popover) {
					vm.popover.hide();
				}
				if($navDirection) {
		   			$ionicViewSwitcher.nextDirection($navDirection);
				}
				$state.go($url);
			}
			
			//	SHOW MENU POPOVER OPTIONS
	    	function pullOption($event) {				
		    	$ionicPopover.fromTemplateUrl('html/templates/options-more.html', {
					scope: $scope
				}).then(function(popover) {
				    vm.popover = popover;
				    vm.popUpType = 'pull';				    
				    popover.show($event);
				}); 
	    	};
		};
				
		
})();
