(function(){
 	'use strict';
 	
		angular
		    .module('app')
			.config(function($stateProvider, $urlRouterProvider) {		     
		     $stateProvider		      
			  .state("app.pull", {
			      url: "/pull",
		          changeColor: 'bar-turquoise-8',
		          cache:false, 
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/pull/pull.html",
			      		controller: "PullController as vm"
			      	}
			      }
			})
			  .state("app.pull-load-draft", {
			      url: "/pull-load-draft",
		          changeColor: 'bar-turquoise-8',
		          cache:true, 
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/pull/pull-load-draft.html",
			      		controller: "PullLoadDraftController as vm"
			      	}
			      }
		      })
			  .state("app.pull-filter", {
			      url: "/pull-filter",
		          changeColor: 'bar-turquoise-8',
		          cache:true, 
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/pull/pull-filter.html",
			      		controller: "PullFilterController as vm"
			      	}
			      }
		      })
			  .state("app.pull-add-items", {
			      url: "/pull-add-items",
		          changeColor: 'bar-turquoise-8',
		          cache:true, 
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/pull/pull-add-items.html",
			      		controller: "PullAddItemsController as vm"
			      	}
			      }
		      })
			  .state("app.pull-add-items-pull", {
			      url: "/pull-add-items-pull",
		          changeColor: 'bar-turquoise-8',
		          cache:true, 
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/pull/pull-add-items-pull.html",
			      		controller: "PullAddItemsPullController as vm"
			      	}
			      }
		      })
			  .state("app.pull-search-patient", {
			      url: "/pull-search-patient",
		          changeColor: 'bar-turquoise-8',
		          cache:true, 
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/pull/pull-search-patient.html",
			      		controller: "PullSearchPatientController as vm"
			      	}
			      }
		      })
			  
			  
		     });
		     
})();