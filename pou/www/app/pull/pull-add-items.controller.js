(function(){
 	'use strict';
 	
		angular
		    .module('app')
		    .controller('PullAddItemsController', Body);

			function Body($state,
					$scope,
					filterFilter,
					$ionicPopover,
					$ionicViewSwitcher,
					$ionicHistory) {

			var vm = this;
			vm.goTo = goTo;
			vm.PullOption = pullOption;
			vm.clearSearch = clearSearch;
			
			vm.goBack = function(){				
				$ionicHistory.goBack();
			};
			
			function clearSearch(th){
				th.value="";
			}

			function goTo($url,$navDirection) {
				if(vm.popover) {
					vm.popover.hide();
				}
				if($navDirection) {
		   			$ionicViewSwitcher.nextDirection($navDirection);
				}
				$state.go($url);
			}

	    	function pullOption($event,$popupType) {
		    	$ionicPopover.fromTemplateUrl('html/templates/option-more.html', {
				        scope: $scope
				}).then(function(popover) {
				        vm.popover = popover;
				        if($popupType) {
				       		vm.popUpType = $popupType;
				        } else {
				        	vm.popUpType = 'issues';
				        }
				        popover.show($event)
				}); 
	    	};
		};
				
		
})();
