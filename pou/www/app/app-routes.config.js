(function(){
 	'use strict';
 	
		angular
		    .module('app')
			.config(function($stateProvider, $urlRouterProvider) {		     
		    $stateProvider
		    .state('app', {
		        url: '/app',
		        abstract: true,
		        templateUrl: "html/templates/menu-template.html",
		        controller: "MenuController as vm"
		      
		    })
		    .state("app.welcome", {
			      url: "/welcome",
		          changeColor: 'bar-turquoise-8',
		          cache:false, 
			      views :{
			      	'menu-content':{
			      		templateUrl: "html/templates/welcome.html",
			      		controller: "IndexController as vm"
			      	}
			      }
		    })
			.state("log-in", {
			      url: "/log-in",
		          changeColor: 'no-header',
				  templateUrl: "html/templates/log-in.html",	
	      		  controller: "IndexController as vm"	      
		    });
		    $urlRouterProvider.otherwise('/app/welcome');
		  });
		
		
		
})();