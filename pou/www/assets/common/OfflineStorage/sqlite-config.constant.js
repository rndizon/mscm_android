/***************************************************************
 *                                                             *
 *                           NOTICE                            * 
 *                                                             *
 *   THIS SOFTWARE IS THE PROPERTY OF AND CONTAINS             *
 *   CONFIDENTIAL INFORMATION OF LAWSON SOFTWARE AND/OR ITS    *
 *   AFFILIATES OR SUBSIDIARIES AND SHALL NOT BE DISCLOSED     *
 *   WITHOUT PRIOR WRITTEN PERMISSION. LICENSED CUSTOMERS MAY  *
 *   COPY AND ADAPT THIS SOFTWARE FOR THEIR OWN USE IN         *
 *   ACCORDANCE WITH THE TERMS OF THEIR SOFTWARE LICENSE       *
 *   AGREEMENT. ALL OTHER RIGHTS RESERVED.                     *
 *                                                             *
 *   (c) COPYRIGHT 2015 LAWSON SOFTWARE.  ALL RIGHTS RESERVED. *
 *   THE WORD AND DESIGN MARKS SET FORTH HEREIN ARE            *
 *   TRADEMARKS AND/OR REGISTERED TRADEMARKS OF LAWSON         *
 *   SOFTWARE AND/OR ITS AFFILIATES AND SUBSIDIARIES. ALL      *
 *   RIGHTS RESERVED.  ALL OTHER TRADEMARKS LISTED HEREIN ARE  *
 *   THE PROPERTY OF THEIR RESPECTIVE OWNERS.                  *
 *                                                             *
 ***************************************************************
 *                                                             *
 *  v1.0 - 9/7/2015 - Initial release                          *
 *                                                             *   
 ***************************************************************/
 (function() {
	'use strict';
	
	angular
	.module('InforOfflineStorageManager', [])
	.constant('DB_CONFIG', {
	    name: 'pou_DB',
	    tables: [
	       {
	            name: 'Company_Table', /* FROM ICCOMPANY table (back office)*/
	            columns: [
	            	{name: 'UID', type: 'INTEGER PRIMARY KEY AUTOINCREMENT'}, /*From Backoffice*/
	                {name: 'CompanyID', type: 'INTEGER'}, /*From Backoffice*/
	                {name: 'CompanyDesc', type: 'TEXT'}, /*From Backoffice*/
	                {name: 'IsQOODisplay', type: 'INTEGER'}, /*From MSCM server*/
	                {name: 'IsEditParLevel', type: 'INTEGER'}, /*From MSCM server*/
	                {name: 'IsPrintLabel', type: 'INTEGER'} /*From MSCM server*/
	            ]
	        },
	        {
	            name: 'Location_Table', /* FROM MSCM server */
	            columns: [
	                {name: 'UID', type: 'INTEGER PRIMARY KEY AUTOINCREMENT'}, 
	                {name: 'LocationID', type: 'INTEGER'},
	                {name: 'LocationCode', type: 'TEXT'},
	                {name: 'LocationName', type: 'TEXT'}, /*From Backoffice*/
	                {name: 'CompanyID', type: 'INTEGER'}, /*From Backoffice - Foreignkey*/
	                {name: 'IsMyParform', type: 'INTEGER'}, /*From MSCM server*/
	                {name: 'ItemCount', type: 'INTEGER'},
	                {name: 'LastMod', type: 'TEXT'},
	                {name: 'AddQtyMult', type: 'INTEGER'},
	                {name: 'IsPickForPar', type: 'INTEGER'}
	            ]
	        },
	        {
	            name: 'Item_Table', /* FROM MSCM server */
	            columns: [
	            	{name: 'UID', type: 'INTEGER PRIMARY KEY AUTOINCREMENT'},
	                {name: 'ItemID', type: 'INTEGER'}, /*From Backoffice*/
	                {name: 'ItemNo', type: 'TEXT'}, /*From Backoffice*/
	                {name: 'ItemDesc', type: 'TEXT'}, /*From Backoffice*/
	                {name: 'ItemDesc2', type: 'TEXT'}, /*From Backoffice*/
	                {name: 'ItemDesc3', type: 'TEXT'}, /*From Backoffice*/
	                {name: 'ItemImage', type: 'TEXT'}, /*From Backoffice*/
	                {name: 'ItemSequence', type: 'INTEGER'}, /*From Backoffice*/
	                {name: 'LocationCode', type: 'TEXT'}, /*From Backoffice*/
	                {name: 'ManufacturerCatalogueNo', type: 'TEXT'}, /*From Backoffice*/
	                {name: 'MinimumOrder', type: 'INTEGER'}, /*From Backoffice -  to be confirmed*/
	                {name: 'Shelf', type: 'TEXT'}, /*From Backoffice*/
	                {name: 'QuantityOnOrder', type: 'INTEGER'}, /*From Backoffice*/
	                {name: 'UnitOfMeasure', type: 'TEXT'}, /*From Backoffice*/
	                {name: 'Quantity', type: 'INTEGER'},
	                {name: 'AdditionalQuantity', type: 'INTEGER'},
	                {name: 'PendingQuantity', type: 'INTEGER'},
	                {name: 'RequestedQuantity', type: 'INTEGER'},
	                {name: 'GTIN', type: 'TEXT'}, /*From Backoffice*/
	                {name: 'HIBC', type: 'TEXT'}, /*From Backoffice*/
	                {name: 'AverageParUtilization', type: 'INTEGER'}, /*From MSCM server*/
	                {name: 'SuggestedParlevel', type: 'INTEGER'}, /*From MSCM server*/
	                {name: 'Parlevel', type: 'INTEGER'} /*From Backoffice*/
	            ]
	        },
	        {
	            name: 'ParCount_Transaction_Table', /* FROM MSCM server */
	            columns: [
	            	{name: 'UID', type: 'INTEGER PRIMARY KEY AUTOINCREMENT'},
	                {name: 'Quantity', type: 'INTEGER'}, 
	                {name: 'AdditionalQuantity', type: 'INTEGER'}, 
	                {name: 'ItemID', type: 'INTEGER'}, 
	                {name: 'LocationCode', type: 'INTEGER'}, 
	                {name: 'NewParLevel', type: 'INTEGER'}, 
	                {name: 'PrinterID', type: 'TEXT'},
	                {name: 'TimeStamp', type: 'DATETIME'}
	            ]
	        },
	        {
	            name: 'PFP_Location_Table', 
	            columns: [
	            	{name: 'UID', type: 'INTEGER PRIMARY KEY AUTOINCREMENT'},
	                {name: 'LocationCode', type: 'TEXT'}, 
	                {name: 'LocationID', type: 'TEXT'},
	                {name: 'LocationDesc', type: 'TEXT'}, /*From Backoffice*/
	                {name: 'CompanyID', type: 'TEXT'}
	            ]
	        },
	        {
	            name: 'PFP_Item_Table', 
	            columns: [
	            	{name: 'UID', type: 'INTEGER PRIMARY KEY AUTOINCREMENT'},
	                {name: 'LocationCode', type: 'TEXT'}, 
	                {name: 'LocationDesc', type: 'TEXT'}, /*From Backoffice*/
	                {name: 'ItemDesc', type: 'TEXT'}, /*From Backoffice*/
	                {name: 'ItemID', type: 'TEXT'}, /*From Backoffice*/
	                {name: 'ItemNo', type: 'TEXT'}, /*From Backoffice*/
	                {name: 'GTIN', type: 'TEXT'}, /*From Backoffice*/
	                {name: 'HIBC', type: 'TEXT'}, /*From Backoffice*/
	                {name: 'ManufacturerCatalogueNo', type: 'TEXT'}, /*From Backoffice*/
	                {name: 'UnitOfMeasure', type: 'TEXT'}, /*From Backoffice*/
	                {name: 'Parlevel', type: 'INTEGER'}, /*From Backoffice*/
	                {name: 'PFPQuantity', type: 'INTEGER'} /*From Backoffice*/
	            ]
	        }
	       
	    ]
	});
	
})();	