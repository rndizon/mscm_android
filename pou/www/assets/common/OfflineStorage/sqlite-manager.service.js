/***************************************************************
 *                                                             *
 *                           NOTICE                            * 
 *                                                             *
 *   THIS SOFTWARE IS THE PROPERTY OF AND CONTAINS             *
 *   CONFIDENTIAL INFORMATION OF LAWSON SOFTWARE AND/OR ITS    *
 *   AFFILIATES OR SUBSIDIARIES AND SHALL NOT BE DISCLOSED     *
 *   WITHOUT PRIOR WRITTEN PERMISSION. LICENSED CUSTOMERS MAY  *
 *   COPY AND ADAPT THIS SOFTWARE FOR THEIR OWN USE IN         *
 *   ACCORDANCE WITH THE TERMS OF THEIR SOFTWARE LICENSE       *
 *   AGREEMENT. ALL OTHER RIGHTS RESERVED.                     *
 *                                                             *
 *   (c) COPYRIGHT 2015 LAWSON SOFTWARE.  ALL RIGHTS RESERVED. *
 *   THE WORD AND DESIGN MARKS SET FORTH HEREIN ARE            *
 *   TRADEMARKS AND/OR REGISTERED TRADEMARKS OF LAWSON         *
 *   SOFTWARE AND/OR ITS AFFILIATES AND SUBSIDIARIES. ALL      *
 *   RIGHTS RESERVED.  ALL OTHER TRADEMARKS LISTED HEREIN ARE  *
 *   THE PROPERTY OF THEIR RESPECTIVE OWNERS.                  *
 *                                                             *
 ***************************************************************
 *                                                             *
 *  v1.0 - 9/7/2015 - Initial release                          *
 *                                                             *   
 ***************************************************************/
  (function() {
	'use strict';
	
	angular
		.module('InforOfflineStorageManager')
		.service("DBA",SQLiteManager);

		function SQLiteManager($cordovaSQLite, $q, $ionicPlatform){

			var DBA = {
				execute: execute,
				getAll: getAll,
				getById: getById
			};
			return DBA;

			/*--------------------Implementation-------------------*/
			function execute(query, parameters) {
			    parameters = parameters || [];
			    var q = $q.defer();
			    $ionicPlatform.ready(function () {
			      $cordovaSQLite.execute(SQLiteDb, query, parameters)
			        .then(function (result) {
			        	 console.log("Result: "+result);
			          q.resolve(result);
			        }, function (error) {
			        	console.warn('error is '+ error.message);
			          console.warn('I found an error');
			          console.warn(error+ " "+query+ " "+parameters[0]);
			          q.reject(error);
			        });
			    });
			    return q.promise;
			 }

			 function getAll(result) {
			    var output = [];

			    for (var i = 0; i < result.rows.length; i++) {
			      output.push(result.rows.item(i));
			    }
			    return output;
			 }
			 function getById(result) {
			    var output = null;
			    output = angular.copy(result.rows.item(0));
			    return output;
			  }

		}	
	
})();