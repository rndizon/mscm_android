/***************************************************************
 *                                                             *
 *                           NOTICE                            * 
 *                                                             *
 *   THIS SOFTWARE IS THE PROPERTY OF AND CONTAINS             *
 *   CONFIDENTIAL INFORMATION OF LAWSON SOFTWARE AND/OR ITS    *
 *   AFFILIATES OR SUBSIDIARIES AND SHALL NOT BE DISCLOSED     *
 *   WITHOUT PRIOR WRITTEN PERMISSION. LICENSED CUSTOMERS MAY  *
 *   COPY AND ADAPT THIS SOFTWARE FOR THEIR OWN USE IN         *
 *   ACCORDANCE WITH THE TERMS OF THEIR SOFTWARE LICENSE       *
 *   AGREEMENT. ALL OTHER RIGHTS RESERVED.                     *
 *                                                             *
 *   (c) COPYRIGHT 2015 LAWSON SOFTWARE.  ALL RIGHTS RESERVED. *
 *   THE WORD AND DESIGN MARKS SET FORTH HEREIN ARE            *
 *   TRADEMARKS AND/OR REGISTERED TRADEMARKS OF LAWSON         *
 *   SOFTWARE AND/OR ITS AFFILIATES AND SUBSIDIARIES. ALL      *
 *   RIGHTS RESERVED.  ALL OTHER TRADEMARKS LISTED HEREIN ARE  *
 *   THE PROPERTY OF THEIR RESPECTIVE OWNERS.                  *
 *                                                             *
 ***************************************************************
 *                                                             *
 *  v1.0 - 9/10/2015 - Initial release                          *
 *                                                             *   
 ***************************************************************/
  (function() {
	'use strict';
	
	angular
		.module('InforOfflineStorageManager')
		.service("CRUD",CRUDhelper);

		function CRUDhelper(DBA){

			var CRUD = {
				inlineQuery: inlineQuery,
				insert: insert,
				insertBatch:insertBatch,
				remove: remove,
				update: update,
				select:select
			};
			return CRUD;

			/*--------------------Implementation-------------------*/
			 /*
			INSERT HELPER:


			 var params = {
			 	"Quantity":1,
			 	"AdditionalQuantity":2
			 };	
		     CRUD.insert("ParCount_Transaction_Table",params)
		     	 .then(function(result){
		    		console.log(result);
		    	 });
  			*/
			function insert(table,params) {	
    			var parameters = [];
		        var query = "INSERT INTO "+table+" (";    

        		angular.forEach(params, function(v,k) {
			        query += k + ",";
			    });
				query = query.substring(0, (query.length - 1));
		        
		        query +=") VALUES (";
		        angular.forEach(params, function(v,k) {
			        query += "?,";
			        parameters.push(v);
			      });
		        query = query.substring(0, (query.length - 1));
		        query +=")";
				
				return DBA.execute(query, parameters);
			}

			function inlineQuery(_query){
				var parameters = [];
		        var query = _query;
				
		        //parameters[params];

				return DBA.execute(_query)
			      		  .then(function(result){
					        return DBA.getAll(result);
					   });

			}
			function insertBatch(_table,_columnData,_arrayData){

				var colums = [];
				var query = "insert into "+_table+" ";

				_columnData.forEach(function(column){
					colums.push(column);
				});

				query += "("+colums.join(",")+") ";

				_arrayData.forEach()

				var query = "insert into Company_Table (CompanyDesc)" ;

				  	query += "select '"+arryCompanies[0].attributes.facdesc+"' as CompanyDesc "
				  	arryCompanies.forEach(function(company) {
				  		query += "union all select '"+company.attributes.facdesc+"' ";

				  	});
				  	console.log("successful company insert");
				  	CRUD.inlineQurey(query).
				  	then(function(){
				  		alert(":");
				  	});


				  	return DBA.execute(query);

			}
			/*
			SELECT HELPER:

			var params = {
						 	"UID":1
						 };
 	 		 CRUD.select("ParCount_Transaction_Table","*",params)
 	 		 	 .then(function(result){
 	 		 	 	console.log('Callback: select result:' + result[0].Quantity);
 	 		 	});
			*/
			function select(table,columns,params,seperator){

				var sep = seperator || "AND";
				var parameters = [];
		 
		        
				var query = "SELECT "+ columns + " FROM "+ table +" ";
				
				if (params) {
					query += " WHERE ";   
				

			        angular.forEach(params, function(v,k) {
				        query += " " + k + " = (?) "+ sep; 
				        parameters.push(v);
				    });
					query = query.substring(0, (query.length - sep.length));
				}	
				
				return DBA.execute(query, parameters)
			      .then(function(result){
			        return DBA.getAll(result);
			     });
			}
			
			function update(table,column,newvalue,params){
				var parameters = [];
		 		var sep = "AND";
		        
				var query = "UPDATE "+ table +" SET ";
				query += column +"="+newvalue;
				
				if (params) {
					query += " WHERE ";   
				

			        angular.forEach(params, function(v,k) {
				        query += " " + k + " = (?) "+ sep; 
				        parameters.push(v);
				    });
					query = query.substring(0, (query.length - sep.length));
				}	
				
				return DBA.execute(query, parameters)
			      .then(function(result){
			        return DBA.getAll(result);
			     });
			}
			function remove(table){
				var parameters = [];
		        
				var query = "DELETE FROM "+ table;
				
				return DBA.execute(query, parameters);
			}
			
			
		}	
	
})();