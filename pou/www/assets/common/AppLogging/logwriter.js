/***************************************
	LOG WRITER
***************************************/
var logOb;
var logDirOb;
var loglocaiton;
var curlogsize = 0;

/*-----------------------------
	SETTINGS
-----------------------------*/
// MAX LOGFILE SIZE (BYTES)
var maxlogsize = 1024;

//	DEFAULT CURRENT LOG FILE NAME
var logfilename = "log.txt";

//	MAX LOG LEVEL (everything below will be logged)
var maxloglevel = 0;

//	CURRENT LOGLEVEL
var loglevel = 0;

//	LOG SAVE PATH CUSTOM FOLDER NAME
var customlogfolder = 'infor';

//	LOG DESTINATION
var logto = 'sdcard';	//	sdcard, app files, app cache, customfolder

/*=============================
	DEVICE READY
=============================*/
document.addEventListener('deviceready', onDeviceReady, false);

function onDeviceReady(){
	//	WRITE AND GET LOG FILE READY
	LogInit(true, true);
}

/*=============================
	LOG FILE INIT
=============================*/
function LogInit(readforoutput, firstrun){	
	// SET LOG LOCAITON	
	if (logto=='sdcard'){
		loglocaiton = cordova.file.externalRootDirectory;	
	}
	else if (logto=='files'){
		loglocaiton = cordova.file.externalDataDirectory;	
	}
	else if (logto=='cache'){
		loglocaiton = cordova.file.externalCacheDirectory;
	}
	else if (logto=='customfolder'){
		loglocaiton = cordova.file.externalRootDirectory + customlogfolder;
	}
	else{
		loglocaiton = cordova.file.externalDataDirectory;
	}	
	
	//	WRITE AND GET LOG FILE READY
	window.resolveLocalFileSystemURL(loglocaiton, function(dir) {
		logDirOb = dir;
		dir.getFile(logfilename, {create:true}, function(file) {			
			logOb = file;
			if (firstrun) writeLog("App started");
		});
	});
}

/*=============================
	READ LOGFILE
=============================*/
function ReadLogFile() {
	if (!logOb) return;	
	window.resolveLocalFileSystemURL(loglocaiton, function(dir) {
		logDirOb = dir;
		dir.getFile(logfilename, {create:true}, function(file) {			
			logOb = file;
			logOb.file(function(file) {
				var reader = new FileReader();
				reader.onloadend = function(e) {
					console.log(this.result);
					return this.result;
				};
				reader.readAsText(file);
			}, ReadLogfail);
		});		
	});
}

function GetReadLogFile() {
	if (!logOb) return "";
	logOb.file(function(file) {
		var reader = new FileReader();
		reader.onloadend = function(e) {
			//console.log(this.result);
			return this.result;
		};
		reader.readAsText(file);
	}, ReadLogfail);
}

/*=============================
	GET LOG SIZE
=============================*/
function GetLogSize(){
	if (!logOb) return 0;
	window.resolveLocalFileSystemURL(loglocaiton, function(dir) {
		logDirOb = dir;
		dir.getFile(logfilename, {create:true}, function(file) {			
			logOb = file;
			logOb.file(function(file) {
				//	GET CURRENT LOG SIZE
				return file.size;				
			}, ReadLogfail);
		});		
	});
}

/*=============================
	WRITE LOG
=============================*/
function writeLog(str) {
	if (maxloglevel>=loglevel){
		if(!logOb){
			console.log("logOb was false - NO LOG ADDED");
			return;	
		}
		var log = "[" + GetLogDate(true) + "] : " + str + "\n";	
		//	SAVE LOG
		logOb.createWriter(function(fileWriter) {		
			fileWriter.seek(fileWriter.length);		
			var blob = new Blob([log], {type:'text/plain'});
			fileWriter.write(blob);
			//console.log("ok, in theory i worked");
			
			//	ADD TO OUPUT
			//var log2 = "[" + GetLogDate(true) + "] : " + str + "<br>";
			//document.getElementById('logoutput').insertAdjacentHTML('beforeend', log2);
		}, ReadLogfail);
		CheckLogSize();
	}	
}
/*=============================
	CHECK LOG SIZE
=============================*/
function CheckLogSize(){
	window.resolveLocalFileSystemURL(loglocaiton, function(dir) {
		logDirOb = dir;
		dir.getFile(logfilename, {create:true}, function(file) {			
			logOb = file;
			logOb.file(function(file) {
				curlogsize = file.size;
				console.dir('current size : ' + curlogsize);
				if (parseFloat(curlogsize)<parseFloat(maxlogsize)){
					//console.log('Log is okay');
				}else{
					//console.log('Log is big - create new');
					renameLogFile('log.txt');
				}
			}, ReadLogfail);
		});		
	});
}

/*=============================
	RENAME LOG FILE
=============================*/
function renameLogFile(currentName){
	window.resolveLocalFileSystemURL(loglocaiton, function(dir) {
		dir.getFile(logfilename, {create:true}, function(file) {		
			var date = new Date();	
			var day = date.getDate();
			var monthIndex = date.getMonth();
			var year = date.getFullYear();
			var newlogname = "log" + GetLogDate(false) + ".txt";
			file.moveTo(logDirOb, newlogname);
			console.log('Log file renamed to : ' + newlogname);
			
			//	RE CREATE FILE/SETUP
			LogInit(false, false);
			
		}, renameLogSuccess);	
	});
}

/*=============================
	SUCCESS & FAIL
=============================*/
function renameLogSuccess(e) {
	console.log("FileSystem renamed OK");
	//console.dir(e);
}

function ReadLogfail(e) {
	console.log("FileSystem Error");
	console.dir(e);
}

/*============================
	GET FILE SIZE
============================*/
function bytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0) return 'n/a';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

/*============================
	GET DATE (SERIAL/NORMAL)
============================*/
function GetLogDate(useseperator){
	//var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October",  "November", "December"];
	var date = new Date();
	var dateYear = date.getFullYear();	
	var dateMonth = LogPad(date.getMonth()+1);
	var dateDay = LogPad(date.getDate());
	var dateHour = LogPad(date.getHours());
	var dateMinute = LogPad(date.getMinutes());
	var dateSeconds = LogPad(date.getSeconds());
	if (useseperator){
		return dateYear + '.' + dateMonth + '.' + dateDay + ' ' + dateHour + ':' + dateMinute + ':' + dateSeconds;	
	}else{
		return dateYear + dateMonth + dateDay + dateHour + dateMinute + dateSeconds;
	}	
}

//	ADD 0 if number is below 9
function LogPad(n){
	return n<10 ? '0'+n : n
}



//	http://www.html5rocks.com/en/tutorials/file/filesystem/
//	https://github.com/apache/cordova-plugin-file