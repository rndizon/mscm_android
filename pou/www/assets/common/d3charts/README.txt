======================================
	D3 Charts
======================================


======================================
	Install
======================================
ADD this to your index.html:

<!-- D3 Charts -->
<link href="assets/common/d3charts/nv.d3.css" rel="stylesheet">
<link href="assets/common/d3charts/style.css" rel="stylesheet">
<script src="assets/common/d3charts/d3.js"></script>
<script src="assets/common/d3charts/nv.d3.js"></script>	
<script src="assets/common/d3charts/app-d3.service.js"></script>

