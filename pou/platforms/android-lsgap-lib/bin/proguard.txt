# view AndroidManifest.xml #generated:38
-keep class com.infor.activity.SampleAppActivity { <init>(...); }

# view res/layout/appinfo.xml #generated:12
# view res/layout/list_header1.xml #generated:30
# view res/layout/list_header1.xml #generated:6
# view res/layout/list_header2.xml #generated:6
# view res/layout/password_login.xml #generated:12
# view res/layout/password_login.xml #generated:146
# view res/layout/password_login.xml #generated:31
# view res/layout/password_login_3.xml #generated:12
# view res/layout/password_login_3.xml #generated:160
# view res/layout/password_login_3.xml #generated:31
# view res/layout/password_login_3_2.xml #generated:144
# view res/layout/profile_creation.xml #generated:12
# view res/layout/profile_creation.xml #generated:149
# view res/layout/profile_creation.xml #generated:31
# view res/layout/profile_creation_3.xml #generated:12
# view res/layout/profile_creation_3.xml #generated:157
# view res/layout/profile_creation_3.xml #generated:31
# view res/layout/profile_creation_3_2.xml #generated:13
# view res/layout/profile_creation_3_2.xml #generated:252
# view res/layout/profile_creation_3_2.xml #generated:33
# view res/layout/profile_creation_3_2.xml #generated:48
# view res/layout/samplewebresponse.xml #generated:12
-keep class com.infor.secconnect.LsButton { <init>(...); }

# view AndroidManifest.xml #generated:63
-keep class com.infor.secconnect.activity.AppInfoActivity { <init>(...); }

# view AndroidManifest.xml #generated:58
-keep class com.infor.secconnect.activity.EndpointCreateActivity { <init>(...); }

# view AndroidManifest.xml #generated:68
-keep class com.infor.secconnect.activity.LaunchWVActivity { <init>(...); }

# view AndroidManifest.xml #generated:53
-keep class com.infor.secconnect.activity.PasswordLoginActivity { <init>(...); }

# view AndroidManifest.xml #generated:48
-keep class com.infor.secconnect.activity.ProfileListActivity { <init>(...); }

# view AndroidManifest.xml #generated:73
-keep class com.infor.secconnect.net.SecConnectionService { <init>(...); }

# onClick res/layout/profile_creation.xml #generated:12
# onClick res/layout/profile_creation_3.xml #generated:12
# onClick res/layout/profile_creation_3_2.xml #generated:13
# onClick res/layout/profile_creation_3_2.xml #generated:33
-keepclassmembers class * { *** onCancel(...); }

# onClick res/layout/launchwv.xml #generated:29
# onClick res/layout/password_login_3_2.xml #generated:167
-keepclassmembers class * { *** onChangeProfile(...); }

# onClick res/layout/appinfo.xml #generated:12
# onClick res/layout/samplewebresponse.xml #generated:12
-keepclassmembers class * { *** onDone(...); }

# onClick res/layout/home.xml #generated:6
-keepclassmembers class * { *** onListProfile(...); }

# onClick res/layout/password_login.xml #generated:146
# onClick res/layout/password_login_3.xml #generated:160
# onClick res/layout/password_login_3_2.xml #generated:144
-keepclassmembers class * { *** onLogin(...); }

# onClick res/layout/password_login.xml #generated:12
# onClick res/layout/password_login_3.xml #generated:12
-keepclassmembers class * { *** onLoginCancel(...); }

# onClick res/layout/profile_creation.xml #generated:149
# onClick res/layout/profile_creation_3.xml #generated:157
# onClick res/layout/profile_creation_3_2.xml #generated:252
-keepclassmembers class * { *** onSave(...); }

# onClick res/layout/home.xml #generated:53
-keepclassmembers class * { *** onTestADFSLM(...); }

# onClick res/layout/home.xml #generated:17
-keepclassmembers class * { *** onTestISS(...); }

# onClick res/layout/home.xml #generated:41
-keepclassmembers class * { *** onTestLSF(...); }

# onClick res/layout/home.xml #generated:29
-keepclassmembers class * { *** onTestLTM(...); }

# onClick res/layout/home.xml #generated:65
-keepclassmembers class * { *** onTestLogout(...); }

