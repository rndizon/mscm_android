package com.infor.mscm.scanner;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;

import android.app.Activity;
import android.view.KeyEvent;
import android.util.Log;

public class Scanner 
{
	public static boolean m_IsEnabled;
	public static CallbackContext callbackContext;
	
	public Scanner() 
	{
		m_IsEnabled = false;
	}
	
	public void enableScanner(CallbackContext context)
	{
		m_IsEnabled = true;
		callbackContext = context;
		 Log.v("Scanner", "enabled scaner");
	}
	
	public void disableScanner() 
	{
    	PluginResult r = new PluginResult(PluginResult.Status.NO_RESULT);
        r.setKeepCallback(false);
        callbackContext.sendPluginResult(r);
		
		callbackContext = null;
		m_IsEnabled = false;
	}
	
	public static void scanSuccess(String barcode) 
	{
		if (callbackContext != null)
		{
	    	PluginResult r = new PluginResult(PluginResult.Status.OK, barcode);
	        r.setKeepCallback(true);
	        callbackContext.sendPluginResult(r);
	        Log.v("Scanner", "scan sucess");
		}
	}
	
	public static void scanFailed() 
	{
		if (callbackContext != null)
		{
			PluginResult r = new PluginResult(PluginResult.Status.ERROR, "Scan Failed");
	        r.setKeepCallback(true);
	        callbackContext.sendPluginResult(r);
	        Log.v("Scanner", "scan failed");
		}
	}
	
	public void initialize(Activity activity) 
	{
		ScannerSound.initialize(activity);
	}
	
	public void deinitialize() 
	{
		ScannerSound.clear();
	}
	
	public boolean processScanKey(int keyCode, KeyEvent event) 
	{
		return false;
	}
}
